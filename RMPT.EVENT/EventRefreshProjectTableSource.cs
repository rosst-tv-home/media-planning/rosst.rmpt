﻿namespace RMPT.EVENT {
    /// <summary>
    ///     Событие обновления каналов таблицы проекта по идентификатору
    /// </summary>
    public class EventRefreshProjectTableSource {
        /// <summary>
        ///     Идентификатор проекта
        /// </summary>
        public readonly int ID;

        /// <summary>
        ///     Констуктор
        /// </summary>
        /// <param name="id">Идентификатор проекта</param>
        public EventRefreshProjectTableSource(int id) {
            this.ID = id;
        }
    }
}
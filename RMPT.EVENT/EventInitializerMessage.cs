﻿namespace RMPT.EVENT {
    public class EventInitializerMessage {
        public string Message;
        public MessageType Type;

        public EventInitializerMessage(string message, MessageType type = MessageType.Info) {
            this.Message = message;
            this.Type = type;
        }
    }
}
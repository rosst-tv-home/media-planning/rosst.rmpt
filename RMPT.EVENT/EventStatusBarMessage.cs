﻿namespace RMPT.EVENT {
    /// <summary>
    /// Сообщение события необходимости изменения статусной строки приложения
    /// </summary>
    public class EventStatusBarMessage {
        /// <summary>
        /// Значение статусной строки приложения
        /// </summary>
        public string Status { get; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="status">Значение статусной строки приложения</param>
        public EventStatusBarMessage(string status) {
            Status = status;
        }
    }
}
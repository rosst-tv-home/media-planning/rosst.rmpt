﻿namespace RMPT.EVENT {
    /// <summary>
    ///     Сообщение запроса обновления коллекции проектов в представлении каталога проектов
    /// </summary>
    public interface IEventRefreshProjectsCatalog { }
}
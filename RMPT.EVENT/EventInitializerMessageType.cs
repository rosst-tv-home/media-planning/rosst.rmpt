﻿namespace RMPT.EVENT {
    public enum MessageType {
        Info,
        Warn,
        Error,
        Success
    }
}
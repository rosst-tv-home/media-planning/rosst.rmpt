﻿namespace RMPT.EVENT {
    /// <summary>
    ///     Сообщение события открытия выбранного проекта для управления
    /// </summary>
    public class EventOpenProject {
        /// <summary>
        ///     Проект
        /// </summary>
        public object Project { get; }

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="project">Проект</param>
        public EventOpenProject(object project) {
            this.Project = project;
        }
    }
}
﻿namespace RMPT.EVENT {
    /// <summary>
    ///     Событие обновления таблицы проекта по идентификатору
    /// </summary>
    public class EventRefreshProjectTable {
        /// <summary>
        ///     Идентификатор проекта
        /// </summary>
        public readonly int ID;

        /// <summary>
        ///     Констуктор
        /// </summary>
        /// <param name="id">Идентификатор проекта</param>
        public EventRefreshProjectTable(int id) {
            this.ID = id;
        }
    }
}
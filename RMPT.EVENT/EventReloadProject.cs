﻿namespace RMPT.EVENT {
    public class EventReloadProject {
        public readonly object Project;

        public EventReloadProject(object project) {
            Project = project;
        }
    }
}
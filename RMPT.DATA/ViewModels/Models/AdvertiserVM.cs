﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;

namespace RMPT.DATA.ViewModels.Models {
    public class AdvertiserVM : AbstractObservable {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="brand">Модель представления бренда</param>
        /// <param name="id">Идентификатор рекламодателя</param>
        /// <param name="name">Наименование рекламодателя</param>
        public AdvertiserVM(BrandVM brand, int id, string name) {
            ID = id;
            Name = name;
            Brand = brand;
        }

        public readonly int ID;

        public readonly string Name;

        public readonly BrandVM Brand;
    }
}
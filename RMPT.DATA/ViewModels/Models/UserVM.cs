﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using System.Collections.Generic;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models {
    public class UserVM : AbstractObservable {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="project">Модель представления проекта</param>
        /// <param name="id">Идентификатор пользователя</param>
        /// <param name="login">Логин пользователя</param>
        /// <param name="firstName">Имя пользователя</param>
        /// <param name="lastName">Фамилия пользователя</param>
        /// <param name="isActive">Признак "Аккаунт пользователя активен"</param>
        /// <param name="roles">Роли пользователя</param>
        public UserVM(
            ProjectVM project, int id, string login, string firstName, string lastName, bool isActive,
            IEnumerable<DCUserRole> roles
        ) {
            Project = project;

            ID = id;
            Login = login;
            FirstName = firstName;
            LastName = lastName;
            IsActive = isActive;
            Roles = roles.Select(x => x.Role!.ToViewModel(this)).ToList();
        }

        public readonly int ID;
        public readonly string Login;
        public readonly string FirstName;
        public readonly string LastName;
        public readonly bool IsActive;
        public readonly ProjectVM Project;
        public readonly List<RoleVM> Roles;
    }
}
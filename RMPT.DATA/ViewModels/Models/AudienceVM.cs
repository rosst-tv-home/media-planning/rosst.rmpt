﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Projects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models {
    public class AudienceVM : AbstractObservable {
        public AudienceVM(
            ChannelVM channel,
            int dictionaryID,
            int id,
            string name,
            DateTime date,
            double tvr,
            double cumReachPercent,
            int sample,
            int weightedTotal,
            double frequencyPercent,
            bool isTarget
        ) {
            DictionaryID = dictionaryID;
            ID = id;
            Name = name;
            Date = date;
            TVR = tvr;
            CumReachPercent = cumReachPercent;
            Sample = sample;
            WeightedTotal = weightedTotal;
            FrequencyPercent = frequencyPercent;
            IsTarget = isTarget;
            Channel = channel;
        }

        public int DictionaryID {
            get => GetProperty(() => DictionaryID);
            private set => SetProperty(() => DictionaryID, value);
        }

        public int ID {
            get => GetProperty(() => ID);
            private set => SetProperty(() => ID, value);
        }

        public string Name {
            get => GetProperty(() => Name);
            private set => SetProperty(() => Name, value);
        }

        public DateTime Date {
            get => GetProperty(() => Date);
            private set => SetProperty(() => Date, value);
        }

        public double TVR {
            get => GetProperty(() => TVR);
            private set => SetProperty(() => TVR, value);
        }

        public double CumReachPercent {
            get => GetProperty(() => CumReachPercent);
            private set => SetProperty(() => CumReachPercent, value);
        }

        public int Sample {
            get => GetProperty(() => Sample);
            private set => SetProperty(() => Sample, value);
        }

        public int WeightedTotal {
            get => GetProperty(() => WeightedTotal);
            private set => SetProperty(() => WeightedTotal, value);
        }

        public double FrequencyPercent {
            get => GetProperty(() => FrequencyPercent);
            set => SetProperty(() => FrequencyPercent, value);
        }

        public bool IsTarget {
            get => GetProperty(() => IsTarget);
            private set => SetProperty(() => IsTarget, value);
        }

        public ChannelVM Channel {
            get => GetProperty(() => Channel);
            private set => SetProperty(() => Channel, value);
        }

        public PDAudience ToEntity(PDChannel? channel = null) {
            var audience = new PDAudience {
                ID = ID,
                DictionaryID = DictionaryID,
                ChannelID = Channel.ID,
                Channel = channel,
                IsTarget = IsTarget,
                WeightedTotal = WeightedTotal,
                CumReachPercent = CumReachPercent,
                Sample = Sample,
                TVR = TVR,
                FrequencyPercent = FrequencyPercent,
                Date = Date
            };
            
            return audience;
        }
    }
}
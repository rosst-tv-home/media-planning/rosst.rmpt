﻿using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.VALIDATE;
using System.Collections.Generic;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models.Timing {
    /// <summary>
    ///     Хронометраж
    /// </summary>
    public class TimingVM : AbstractObservable {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="week">Неделя</param>
        /// <param name="id">Идентификатор</param>
        /// <param name="value">Значение</param>
        /// <param name="portion">Доля</param>
        public TimingVM(WeekVM week, int id, int value, double portion = 0.0) {
            // Если указанный хронометраж не кратен 5 - выбрасываем исключение
            ValuesValidator.IsValidTimingValue(value);

            // Если указанная доля меньше 0% или больше 100% - выбрасываем исключение
            ValuesValidator.IsValidPortion(portion);

            Week = week;

            ID = id;
            Value = value;

            _portion = portion;
            isActive = id != 0;
        }

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации в модель базы данных
        /// </summary>
        /// <param name="week">Неделя</param>
        /// <returns>Модель базы данных</returns>
        public PDTiming ToEntity(PDWeek? week = null) => new() {
            ID = ID,
            WeekID = Week.ID,
            Value = Value,
            Portion = Portion,
            Week = week
        };

        #endregion

        #region Свойства

        /// <summary>
        ///     Идентификатор
        /// </summary>
        public readonly int ID;

        /// <summary>
        ///     Неделя
        /// </summary>
        public readonly WeekVM Week;

        /// <summary>
        ///     Значение
        /// </summary>
        public readonly int Value;

        #region Признаки

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        public bool isActive;

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        public bool IsActive {
            get => isActive;
            set {
                // Если значение не изменилось - завершаем выполнение метода
                if (!SetProperty(ref isActive, value, nameof(IsActive))) {
                    return;
                }

                // Вызываем метод обработки этого события
                InvokePropertyChangedCore(value);
            }
        }

        #endregion

        #region Доли

        /// <summary>
        ///     Доля
        /// </summary>
        public double _portion;

        /// <summary>
        ///     Доля
        /// </summary>
        public double Portion {
            get => _portion;
            set {
                value = value switch {
                    > 1.0 => 1.0,
                    < 0.0 => 0.0,
                    _ => value
                };
                
                int count = 0;
                foreach (var timing in Week.Timings) {
                    if (timing.IsActive == false) continue;
                    count++;
                }

                switch (count) {
                    case 1 when value < 1.0:
                        value = 1.0;
                        break;
                    case 2: {
                        var other = Week.Timings.FirstOrDefault(x => x.IsActive && x.Value != Value);
                        if (other != null) {
                            other._portion = 1.0 - value;
                        }

                        break;
                    }
                }

                // Если значение не изменилось - завершаем выполнение метода
                if (!SetProperty(ref _portion, value, nameof(Portion))) {
                    return;
                }

                // Вызываем метод обработки этого события
                InvokePropertyChangedCore(value);
            }
        }

        #endregion

        #region Инвентарь

        #region Целевой рейтинг

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double TRP => ValuesValidator.NanOrZero(Week.TRP * Portion);

        /// <summary>
        ///     Целевой рейтинг, приведённый к базовому хронометражу
        /// </summary>
        public double GivenTRP => ValuesValidator.NanOrZero(TRP * Value / Week.Month.Channel.Region.Project.BaseTiming);

        #endregion

        #region Суммарный рейтинг

        /// <summary>
        ///     Суммарный рейтинг
        /// </summary>
        public double GRP => ValuesValidator.NanOrZero(TRP / Week.Month.Affinity);

        /// <summary>
        ///     Суммарный рейтинг, приведённый к базовому хронометражу
        /// </summary>
        public double GivenGRP => ValuesValidator.NanOrZero(GRP * Value / Week.Month.Channel.Region.Project.BaseTiming);

        #endregion

        #region Минуты

        /// <summary>
        ///     Минуты
        /// </summary>
        public double Minutes => ValuesValidator.NanOrZero(Value * Outputs / 60);

        #endregion

        #endregion

        #region Выходы

        /// <summary>
        ///     Выходы
        /// </summary>
        public double Outputs {
            get {
                // Если неизмеряемые регион
                if (!Week.Month.Channel.Region.IsMeasurable) {
                    return ValuesValidator.NanOrZero(Week.Month.OutputsPerDay * Week.ActiveDays * Portion);
                }

                var outputsPrime = GivenGRP * Week.Month.Channel.Region.Project.BaseTiming *
                    Week.Month.UnionPrimePortion /
                    (Week.AverageTiming * Week.Month.RatingPrime);

                var outputsOffPrime = GivenGRP * Week.Month.Channel.Region.Project.BaseTiming *
                    (1.0 - Week.Month.UnionPrimePortion) /
                    (Week.AverageTiming * Week.Month.RatingOffPrime);

                return ValuesValidator.NanOrZero(outputsPrime) + ValuesValidator.NanOrZero(outputsOffPrime);
            }
        }

        #endregion

        #endregion
        
        /// <summary>
        ///     Метод сериализации для проверки данных
        /// </summary>
        /// <returns><see cref="string"/>Строка данных хронометража</returns>
        public string GetSerializeString() => $"{Week.ID}{Value}{_portion}";
    }
}
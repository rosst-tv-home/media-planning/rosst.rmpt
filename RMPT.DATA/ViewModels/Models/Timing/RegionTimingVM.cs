﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using RMPT.COMMON.Models;
using RMPT.EVENT;
using RMPT.VALIDATE;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models.Timing {
    public class RegionTimingVM : AbstractObservable {
        private readonly RegionMonthVM _regionMonth;
        private readonly IReadOnlyList<TimingVM> _timings;

        public int Value { get; }

        public bool IsActive => _timings.Any(t => t.IsActive);

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double TRP => _timings.Sum(x => x.TRP);

        /// <summary>
        ///     Выходы
        /// </summary>
        public double Outputs => _timings.Sum(x => x.Outputs);

        /// <summary>
        ///     Доля хронометража
        /// </summary>
        public double Portion {
            get => _regionMonth.Region.IsMeasurable
                // Считаем долю от целевого рейтинга, если измеряемый регион
                ? _timings.Any(t => t.TRP > 0.0)
                    ? ValuesValidator.NanOrZero(TRP / _regionMonth.TRP)
                    : ValuesValidator.NanOrZero(_timings.Sum(t => t.Value * t.Portion) /
                        _regionMonth.Region.Channels
                           .SelectMany(c => c.Months)
                           .Where(m => m.Date == _regionMonth.Date)
                           .SelectMany(m => m.Weeks)
                           .SelectMany(w => w.Timings)
                           .Sum(t => t.Value * t.Portion))
                // Считаем долю от выходов, если неизмеряемый регион
                : _timings.Any(t => t.Outputs > 0.0)
                    ? ValuesValidator.NanOrZero(Outputs / _regionMonth.Outputs)
                    : ValuesValidator.NanOrZero(_timings.Sum(t => t.Value * t.Portion) /
                    _regionMonth.Region.Channels
                       .SelectMany(c => c.Months)
                       .Where(m => m.Date == _regionMonth.Date)
                       .SelectMany(m => m.Weeks)
                       .SelectMany(w => w.Timings)
                       .Sum(t => t.Value * t.Portion));
            set {
                foreach (var timing in _timings) {
                    timing.Portion = value;
                }

                Messenger.Default.Send(new EventRefreshProjectTable(_regionMonth.Region.Project.ID));
            }
        }

        public RegionTimingVM(RegionMonthVM regionMonth, int value, IReadOnlyList<TimingVM> timings) {
            this._regionMonth = regionMonth;
            this._timings = timings;
            this.Value = value;

            foreach (var timing in _timings) {
                timing.PropertyChanged += OnTimingPropertyChanged;
            }
        }

        private void OnTimingPropertyChanged(object sender, PropertyChangedEventArgs arguments) {
            if (sender is not TimingVM) return;
            switch (arguments.PropertyName) {
                case nameof(TimingVM.Portion): {
                    NotifyTimingsPortions();
                    break;
                }
                case nameof(TimingVM.IsActive): {
                    NotifyTimingsPortions();
                    RaisePropertyChanged(() => IsActive);
                    break;
                }
            }
        }

        private void NotifyTimingsPortions() => _regionMonth.Timings.ForEach(t => t.NotifyUI(nameof(t.Portion)));
    }
}
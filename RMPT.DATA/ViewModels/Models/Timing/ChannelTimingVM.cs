﻿using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.VALIDATE;
using System.Collections.Generic;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models.Timing {
    public class ChannelTimingVM : AbstractObservable {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="channel">Канал</param>
        /// <param name="value">Значение</param>
        public ChannelTimingVM(ChannelVM channel, int value) {
            Value = value;
            Channel = channel;
        }

        /// <summary>
        ///     Значение
        /// </summary>
        public int Value { get; }

        /// <summary>
        ///     Канал
        /// </summary>
        public ChannelVM Channel { get; }

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        public bool IsActive => ChildTimings.Any(x => x.IsActive);

        /// <summary>
        ///     Доля хронометража
        /// </summary>
        public double Portion {
            get => Channel.Region.IsMeasurable
                // Считаем долю от целевого рейтинга, если измеряемый регион
                ? ChildTimings.Any(t => t.TRP > 0.0)
                    ? ValuesValidator.NanOrZero(TRP / Channel.TRP)
                    : ValuesValidator.NanOrZero(ChildTimings.Sum(t => t.Value                            * t.Portion) /
                    Channel.Months.Where(m => m.IsActive).SelectMany(m => m.Timings).Sum(t => t.Value * t.Portion))
                // Считаем долю от выходов, если неизмеряемый регион
                : ChildTimings.Any(t => t.Outputs > 0.0)
                    ? ValuesValidator.NanOrZero(Outputs / Channel.Outputs)
                    : ValuesValidator.NanOrZero(ChildTimings.Sum(t => t.Value                                                   * t.Portion) /
                    Channel.Months.Where(m => m.IsActive).SelectMany(m => m.Timings).Sum(t => t.Value * t.Portion));
            set {
                foreach (var timing in ChildTimings.Where(x => x.IsActive).ToList()) {
                    timing.Portion = value;
                }
            }
        }

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double TRP => ChildTimings.Sum(x => x.TRP);

        /// <summary>
        ///     Выходы
        /// </summary>
        public double Outputs => ChildTimings.Sum(x => x.Outputs);

        /// <summary>
        ///     Хронометражи месяцев
        /// </summary>
        private IEnumerable<MonthTimingVM> ChildTimings => Channel.Months
           .Where(m => m.IsActive)
           .SelectMany(m => m.Timings)
           .Where(t => t.Value == Value);
    }
}
﻿using RMPT.COMMON.Models;
using RMPT.VALIDATE;
using System.Collections.Generic;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models.Timing {
    /// <summary>
    ///     Хронометраж месяца
    /// </summary>
    public class MonthTimingVM : AbstractObservable {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="month">Месяц</param>
        /// <param name="value">Значение</param>
        public MonthTimingVM(MonthVM month, int value) {
            Value = value;
            Month = month;
        }

        /// <summary>
        ///     Значение хронометража
        /// </summary>
        public int Value { get; }

        /// <summary>
        ///     Месяц
        /// </summary>
        public MonthVM Month { get; }

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        public bool IsActive => ChildTimings.Any(x => x.IsActive);

        /// <summary>
        ///     Доля
        /// </summary>
        public double Portion {
            get => Month.Channel.Region.IsMeasurable
                // Считаем долю от целевого рейтинга, если измеряемый регион
                ? ChildTimings.Any(t => t.TRP > 0.0)
                    ? ValuesValidator.NanOrZero(TRP / Month.TRP)
                    : ValuesValidator.NanOrZero(ChildTimings.Sum(t => t.Value   * t.Portion) /
                        Month.Weeks.SelectMany(w => w.Timings).Sum(t => t.Value * t.Portion))
                // Считаем долю от выходов, если неизмеряемый регион
                : ChildTimings.Any(t => t.Outputs > 0.0)
                    ? ValuesValidator.NanOrZero(Outputs / Month.Outputs)
                    : ValuesValidator.NanOrZero(ChildTimings.Sum(t => t.Value   * t.Portion) /
                        Month.Weeks.SelectMany(w => w.Timings).Sum(t => t.Value * t.Portion));
            set {
                foreach (var timing in ChildTimings.Where(x => x.IsActive).ToList()) {
                    timing.Portion = value;
                }
            }
        }

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double TRP => ChildTimings.Sum(x => x.TRP);

        /// <summary>
        ///     Выходы
        /// </summary>
        public double Outputs => ChildTimings.Sum(x => x.Outputs);

        /// <summary>
        ///     Хронометражи недель
        /// </summary>
        private IEnumerable<TimingVM> ChildTimings => Month.Weeks
           .SelectMany(w => w.Timings)
           .Where(t => t.Value == Value);
    }
}
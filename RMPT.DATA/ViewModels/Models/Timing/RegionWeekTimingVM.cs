﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using RMPT.COMMON.Models;
using RMPT.EVENT;
using RMPT.VALIDATE;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models.Timing {
    public class RegionWeekTimingVM : AbstractObservable {
        public RegionWeekVM Week { get; }

        public ObservableCollection<TimingVM> Timings { get; }

        public int Value { get; }

        public bool IsActive => this.Timings.Any(t => t.IsActive);

        public double TRP => this.Timings.Sum(t => t.TRP);

        public double Outputs => this.Timings.Sum(t => t.Outputs);

        public double Portion {
            get => Week.Month.Region.IsMeasurable
                // Считаем долю от целевого рейтинга, если измеряемый регион
                ? this.Timings.Any(t => t.TRP > 0.0)
                    ? ValuesValidator.NanOrZero(this.TRP / this.Week.TRP)
                    : 0.0
                // Считаем долю от выходов, если неизмеряемый регион
                : this.Timings.Any(t => t.Outputs > 0.0)
                    ? ValuesValidator.NanOrZero(this.Outputs / this.Week.Outputs)
                    : 0.0;
            set {
                foreach (var timing in this.Timings) {
                    timing.Portion = value;
                }
                
                Messenger.Default.Send(new EventRefreshProjectTable(this.Week.Month.Region.Project.ID));
            }
        }

        public RegionWeekTimingVM(RegionWeekVM week, IEnumerable<TimingVM> timings) {
            this.Week = week;
            this.Timings = timings.ToObservableCollection();
            this.Value = this.Timings.First().Value;
        }
    }
}
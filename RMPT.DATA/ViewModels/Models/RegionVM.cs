﻿using DevExpress.Mvvm.Native;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.VALIDATE;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models {
    public class RegionVM : AbstractObservable {
        public RegionVM(
            ProjectVM project,
            int dictionaryID,
            int id,
            string name,
            bool isMeasurable,
            IEnumerable<PDChannel> channels,
            IEnumerable<DCTimingMargin> timingMargins,
            DCSeasonalOddsSet seasonCoefficientSet
        ) {
            DictionaryID = dictionaryID;
            ID = id;
            Name = name;
            IsMeasurable = isMeasurable;
            Project = project;

            channels
               .Select(channel => {
                    var vm = channel.ToViewModel(this);
                    vm.GenerateTimings();
                    return vm;
                })
               .ForEach(Channels.Add);

            // TODO: Заранее отфильтровать коллекцию по планируемому году проекта
            timingMargins
               .Select(timingMargin => timingMargin.ToViewModel(this))
               .ForEach(TimingMargins.Add);

            // TODO: Заранее отфильтровать коллекцию по планируемому году проекта
            seasonCoefficientSet.SeasonalOdds
               .Select(seasonCoefficient => seasonCoefficient.ToViewModel(this))
               .ForEach(SeasonCoefficients.Add);

            // Пересчитываем TRP
            _trp = Channels.Sum(x => x.TRP);

            // Если первая инициализация проекта
            if (Project.IsFirstInitialization) {
                RecalculateChannelsPortions();
            } else {
                // Пересчитываем долю каналов
                foreach (var channel in Channels) {
                    channel.Portion = TRP == 0.0 ? 0.0 : channel.TRP / TRP;
                }
            }

            Channels
               .SelectMany(channel => channel.Months)
               .GroupBy(month => month.Date)
               .Select(group => new RegionMonthVM(this, group.ToList()))
               .ForEach(Months.Add);
        }

        /// <summary>
        ///     Проект
        /// </summary>
        public ProjectVM Project { get; }

        /// <summary>
        ///     Идентификатор региона
        /// </summary>
        public int DictionaryID {
            get => GetProperty(() => DictionaryID);
            private set => SetProperty(() => DictionaryID, value);
        }

        /// <summary>
        ///     Идентификатор связи региона с проектом
        /// </summary>
        public int ID {
            get => GetProperty(() => ID);
            private set => SetProperty(() => ID, value);
        }

        /// <summary>
        ///     Наименование
        /// </summary>
        public string Name {
            get => GetProperty(() => Name);
            private set => SetProperty(() => Name, value);
        }

        /// <summary>
        ///     Признак "Измеряемый"
        /// </summary>
        public bool IsMeasurable {
            get => GetProperty(() => IsMeasurable);
            private set => SetProperty(() => IsMeasurable, value);
        }

        /// <summary>
        ///     Выходы
        /// </summary>
        public double Outputs => ValuesValidator.NanOrZero(Channels.Sum(m => m.Outputs));

        /// <summary>
        ///     Сумарная доля активных каналов
        /// </summary>
        public double TotalActiveChannelsPortion => Channels.Where(c => c.IsActive).Sum(c => c.Portion);

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double _trp;

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double TRP {
            get => ValuesValidator.NanOrZero(_trp);
            set {
                // Если новое значение равно старому - завершаем выполнение метода
                if (!SetProperty(ref _trp, value, nameof(TRP))) {
                    return;
                }

                // Вызываем метод обработки этого события
                InvokePropertyChangedCore(value);
            }
        }

        /// <summary>
        ///     Целевые рейтинги месячных периодов
        /// </summary>
        public ObservableCollection<RegionMonthVM> Months { get; } = new();

        /// <summary>
        ///     Коллекция каналов
        /// </summary>
        public ObservableCollection<ChannelVM> Channels { get; } = new();

        /// <summary>
        ///     Коллекция наценок на хронометражи
        /// </summary>
        public ObservableCollection<TimingMarginVM> TimingMargins { get; } = new();

        /// <summary>
        ///     Коллекция сезонных коэффициентов
        /// </summary>
        public ObservableCollection<SeasonCoefficientVM> SeasonCoefficients { get; } = new();

        public PDRegion ToEntity(PDProject? project = null) {
            var region = new PDRegion {
                ID = ID,
                DictionaryID = DictionaryID,
                ProjectID = Project.ID,
                Project = project,
            };

            region.Channels.AddRange(IsMeasurable
                ? Channels.Select(c => c.ToEntity(region))
                : Channels.Where(c => c.IsActive).Select(c => c.ToEntity())
            );

            return region;
        }

        public void RecalculateChannelsPortions() {
            var channels = Channels.Where(c => c.IsActive).ToList();

            if (!IsMeasurable || channels.Any(c => c.TRP > 0.0 || c.Outputs > 0.0)) {
                return;
            }

            var totalTvr = channels.Sum(c => c.Audiences
               .GroupBy(a => a.Date.Year).OrderByDescending(g => g.Key).First()
               .OrderByDescending(a => a.Date.Month).First(a => a.IsTarget).TVR
            );

            foreach (var channel in channels) {
                channel._portion = channel.Audiences
                   .GroupBy(a => a.Date.Year).OrderByDescending(g => g.Key).First()
                   .OrderByDescending(a => a.Date.Month).First(a => a.IsTarget).TVR / totalTvr;
                channel.NotifyUI(nameof(ChannelVM.Portion));
            }
        }

        /// <summary>
        ///     Метод сериализации для проверки данных
        /// </summary>
        /// <returns><see cref="string"/>Строка данных</returns>
        public string GetSerializeString() {
            // Строка данных региона
            var regionStr = $"{DictionaryID}{Project.ID}";
            // Возвращаем строку региона и строку каналов
            return regionStr + string.Join("", this.Channels.OrderBy(c => c.Name).Select(c => c.GetSerializeString()));
        }
    }
}
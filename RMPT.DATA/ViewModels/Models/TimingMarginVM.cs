﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using System;

namespace RMPT.DATA.ViewModels.Models {
    public class TimingMarginVM : AbstractObservable {
        public TimingMarginVM(RegionVM region, int id, DateTime date, int timing, double value) {
            Region = region;

            ID = id;
            Date = date;
            Timing = timing;
            Value = value;
        }

        public readonly int ID;

        public readonly DateTime Date;

        public readonly int Timing;

        public readonly double Value;

        public readonly RegionVM Region;
    }
}
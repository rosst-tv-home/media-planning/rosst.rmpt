﻿using DevExpress.Mvvm.Native;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Brand;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Dictionaries.Other;
using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using RMPT.DATA.DataBase.Entities.Projects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models {
    public class ProjectVM : AbstractObservable {
        public ProjectVM(
            int id,
            int planingYear,
            int baseTiming,
            double vat,
            double margin,
            double agencyCommission,
            DateTime createdDate,
            DateTime modifiedDate,
            string? description,
            string frequency,
            bool isFirstInitialization,
            bool isBuying,
            bool isPlacement,
            bool isTender,
            bool isFederal,
            PDViewPreferences viewPreferences,
            DCBrand brand,
            DCStrategist strategist,
            DCStrategist manager,
            DCUser createdUser,
            DCUser modifiedUser,
            DCAudience audience,
            IEnumerable<PDRegion> regions
        ) {
            ID = id;
            PlaningYear = planingYear;
            BaseTiming = baseTiming;
            Vat = vat;
            Margin = margin;
            AgencyCommission = agencyCommission;
            ViewPreferences = viewPreferences.ToViewModel(this);
            Brand = brand.ToViewModel(this);
            Strategist = strategist.ToViewModel(this);
            Manager = manager.ToViewModel(this);
            CreatedUser = createdUser.ToViewModel(this);
            ModifiedUser = modifiedUser.ToViewModel(this);
            CreatedDate = createdDate;
            ModifiedDate = modifiedDate;
            TargetAudienceID = audience.ID;
            Description = description;
            Frequency = frequency;
            IsFirstInitialization = isFirstInitialization;
            IsBuying = isBuying;
            IsPlacement = isPlacement;
            IsTender = isTender;
            IsFederal = isFederal;

            regions
               .Select(region => region.ToViewModel(this))
               .ForEach(Regions.Add);

            IsFirstInitialization = false;
        }

        /// <summary>
        ///     Идентификатор
        /// </summary>
        public readonly int ID;

        /// <summary>
        ///     Планируемый год
        /// </summary>
        public int PlaningYear {
            get => GetProperty(() => PlaningYear);
            set => SetProperty(() => PlaningYear, value);
        }

        /// <summary>
        ///     Базовое значение хронометража
        /// </summary>
        public int BaseTiming {
            get => GetProperty(() => BaseTiming);
            set => SetProperty(() => BaseTiming, value);
        }

        /// <summary>
        ///     НДС
        /// </summary>
        public double Vat {
            get => GetProperty(() => Vat);
            set => SetProperty(() => Vat, value);
        }

        /// <summary>
        ///     Маржа
        /// </summary>
        public double Margin {
            get => GetProperty(() => Margin);
            set => SetProperty(() => Margin, value);
        }

        /// <summary>
        ///     Агентская комиссия
        /// </summary>
        public double AgencyCommission {
            get => GetProperty(() => AgencyCommission);
            set => SetProperty(() => AgencyCommission, value);
        }

        /// <summary>
        ///     Настройки представления
        /// </summary>
        public ViewPreferencesVM ViewPreferences {
            get => GetProperty(() => ViewPreferences);
            set => SetProperty(() => ViewPreferences, value);
        }

        /// <summary>
        ///     Бренд
        /// </summary>
        public BrandVM Brand {
            get => GetProperty(() => Brand);
            private set => SetProperty(() => Brand, value);
        }

        /// <summary>
        ///     Менеджер
        /// </summary>
        public StrategistVM Manager {
            get => GetProperty(() => Manager);
            private set => SetProperty(() => Manager, value);
        }

        /// <summary>
        ///     Стратег
        /// </summary>
        public StrategistVM Strategist {
            get => GetProperty(() => Strategist);
            private set => SetProperty(() => Strategist, value);
        }

        /// <summary>
        ///     Пользователь создавший
        /// </summary>
        public UserVM CreatedUser {
            get => GetProperty(() => CreatedUser);
            private set => SetProperty(() => CreatedUser, value);
        }

        /// <summary>
        ///     Пользователь, последний изменивший
        /// </summary>
        public UserVM ModifiedUser {
            get => GetProperty(() => ModifiedUser);
            private set => SetProperty(() => ModifiedUser, value);
        }

        /// <summary>
        ///     Дата создания
        /// </summary>
        public DateTime CreatedDate {
            get => GetProperty(() => CreatedDate);
            private set => SetProperty(() => CreatedDate, value);
        }

        /// <summary>
        ///     Дата последнего изменения
        /// </summary>
        public DateTime ModifiedDate {
            get => GetProperty(() => ModifiedDate);
            private set => SetProperty(() => ModifiedDate, value);
        }

        /// <summary>
        ///     Идентификатор целевой аудитории
        /// </summary>
        public int TargetAudienceID {
            get => GetProperty(() => TargetAudienceID);
            private set => SetProperty(() => TargetAudienceID, value);
        }

        /// <summary>
        ///     Описание
        /// </summary>
        public string? Description {
            get => GetProperty(() => Description);
            private set => SetProperty(() => Description, value);
        }

        /// <summary>
        ///     Частота
        /// </summary>
        public string Frequency {
            get => GetProperty(() => Frequency);
            private set => SetProperty(() => Frequency, value);
        }

        /// <summary>
        ///     Признак "Первая инициализация проекта"
        /// </summary>
        public bool IsFirstInitialization { get; set; }

        /// <summary>
        ///     Признак "Передан в баинг"
        /// </summary>
        public bool IsBuying {
            get => GetProperty(() => IsBuying);
            private set => SetProperty(() => IsBuying, value);
        }

        /// <summary>
        ///     Признак "В размещении"
        /// </summary>
        public bool IsPlacement {
            get => GetProperty(() => IsPlacement);
            private set => SetProperty(() => IsPlacement, value);
        }

        /// <summary>
        ///     Признак "Тендер"
        /// </summary>
        public bool IsTender {
            get => GetProperty(() => IsTender);
            private set => SetProperty(() => IsTender, value);
        }

        /// <summary>
        ///     Признак "Федеральное ТВ"
        /// </summary>
        public bool IsFederal {
            get => GetProperty(() => IsFederal);
            private set => SetProperty(() => IsFederal, value);
        }

        /// <summary>
        ///     Коллекция регионов
        /// </summary>
        public ObservableCollection<RegionVM> Regions { get; } = new();

        public PDProject ToEntity() {
            var entity = new PDProject {
                ID = ID,
                BrandID = Brand.ID,
                PlaningYear = PlaningYear,
                BaseTiming = BaseTiming,
                TargetAudienceID = TargetAudienceID,
                ManagerID = Manager.ID,
                StrategistID = Strategist.ID,
                CreatedUserID = CreatedUser.ID,
                LastModifiedUserID = ModifiedUser.ID,
                Description = Description,
                IsBuying = IsBuying,
                IsFederal = IsFederal,
                IsPlacement = IsPlacement,
                IsTender = IsTender,
                IsFirstInitialization = IsFirstInitialization,
                Vat = Vat,
                Margin = Margin,
                AgencyCommission = AgencyCommission,
                CreatedDateTime = CreatedDate,
                LastModifiedDateTime = ModifiedDate,
                Frequency = Frequency
            };
            entity.ViewPreferences = ViewPreferences.ToEntity(entity);
            entity.Regions.AddRange(Regions
               .Where(r => r.Channels.Any(c => c.IsActive))
               .Select(r => r.ToEntity(entity))
            );

            return entity;
        }

        /// <summary>
        ///     Метод сереализации для проверки данных
        /// </summary>
        /// <returns><see cref="string"/>Строка данных</returns>
        public string GetSerializeString() {
            // Строка данных проекта
            var projectSt =
                $"{Brand.ID}{Manager.ID}{Strategist.ID}{ModifiedUser.ID}{CreatedUser.ID}{TargetAudienceID}{BaseTiming}{PlaningYear}{CreatedDate}{ModifiedDate}{Frequency}{Vat}{Margin}{AgencyCommission}{IsFirstInitialization}{IsBuying}{IsPlacement}{IsTender}{IsFederal}";

            // Возвращаем строку проекта, представлений и регионов
            return projectSt + this.ViewPreferences.GetSerializeString() + string.Join("",
                this.Regions.OrderBy(r => r.Name).Select(r => r.GetSerializeString()));
        }
    }
}
﻿using DevExpress.Mvvm.Native;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.CORE;
using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.ViewModels.Models.Timing;
using RMPT.VALIDATE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models {
    /// <summary>
    ///     Модель месячного периода проекта
    /// </summary>
    public class MonthVM : AbstractObservable {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        public MonthVM(
            int id,
            ChannelVM channel,
            DateTime date,
            double affinity,
            double fixOrFloatPortion,
            double fixOrFloatPrimePortion,
            double superFixOrFixPrimePortion,
            double price,
            double outputsPerDay = 0.0,
            IReadOnlyCollection<PDWeek>? weeks = null
        ) {
            ID = id;
            Date = date;
            Channel = channel;

            _affinity = affinity;

            // Недели
            weeks?.Select(week => week.ToViewModel(this)).OrderBy(x => x.DateFrom).ForEach(Weeks.Add);

            IsActive = Weeks.Count(x => x.IsActive) > 0;

            _trp = Weeks.Sum(x => x.TRP);
            _outputsPerDay = outputsPerDay;

            // Определяем последний актуальный рейтинг рекламных блоков этого месяца канала
            var channelMonthRatings = channel.Ratings
               .Where(x => x.Date.Year <= Date.Year && x.Date.Month == Date.Month)
               .OrderByDescending(x => x.Date)
               .FirstOrDefault();

            Price = price;
            // Задаём значение рейтинга в prime
            RatingPrime = channelMonthRatings?.Prime ?? 0.0;
            // Задаём значение рейтинга в off prime
            RatingOffPrime = channelMonthRatings?.OffPrime ?? 0.0;

            // Если первая инициализация проекта, устанавливаем начальные доли качества
            if (Channel.Region.Project.IsFirstInitialization) {
                // Если инициализация - задаём первоначальное распределение качества
                FixOrFloatPortion = Math.Abs(Channel.QualityLimit.FixOrFloatMax - 1.0) < 0.001
                    ? 1.0
                    : Channel.QualityLimit.FixOrFloatMax;
                if (FixOrFloatPortion > 0.0) {
                    FixOrFloatPrimePortion = Channel.QualityLimit.FixOrFloatPrimeMax >= 0.6
                        ? 0.6
                        : Channel.QualityLimit.FixOrFloatPrimeMax;
                }

                // Если (Fix или Float) занимает всё распределение, не указываем минимальную долю Prime для качества (Super Fix или Fix)
                if (Math.Abs(FixOrFloatPortion - 1.0) < 0.001) {
                    return;
                }

                SuperFixOrFixPortion = 1.0 - FixOrFloatPortion;
                if (SuperFixOrFixPortion > 0.0) {
                    SuperFixOrFixPrimePortion = Channel.QualityLimit.SuperFixOrFixPrimeMin;
                }
            } else {
                FixOrFloatPortion = fixOrFloatPortion;
                FixOrFloatPrimePortion = fixOrFloatPrimePortion;
                SuperFixOrFixPortion = 1.0 - FixOrFloatPortion;
                SuperFixOrFixPrimePortion = superFixOrFixPrimePortion;
            }
        }

        #endregion

        #region Свойства

        #region Идентификаторы

        /// <summary>
        ///     Идентификатор
        /// </summary>
        public readonly int ID;

        #endregion

        #region Родительский объект

        /// <summary>
        ///     Канал
        /// </summary>
        public readonly ChannelVM Channel;

        #endregion

        #region Неизменяемые свойства

        #region Дата

        /// <summary>
        ///     Дата месячного периода
        /// </summary>
        public readonly DateTime Date;

        #endregion

        #region Дни

        /// <summary>
        ///     Количество дней месячного периода
        /// </summary>
        public int Days => Weeks.Sum(week => week.Days);

        /// <summary>
        ///     Количество активных дней месячного периода
        /// </summary>
        public int ActiveDays => Weeks.Sum(week => week.ActiveDays);

        #endregion

        #region Хронометраж

        /// <summary>
        ///     Средний хронометраж
        /// </summary>
        public double AverageTiming => Timings.Where(x => x.IsActive).Sum(timing => timing.Value * timing.Portion);

        /// <summary>
        ///     Наценка за хронометраж
        /// </summary>
        public double AverageTimingMargin {
            get {
                if (!Channel.Region.TimingMargins.Any()) return 1.0;
                var value = ValuesValidator.NanOrZero(Weeks
                   .SelectMany(x => x.Timings.Where(y => y.IsActive))
                   .Sum(x =>
                        x.GivenGRP / GivenGRP *
                        Channel.Region.TimingMargins.FirstOrDefault(y => y.Timing == x.Value)?.Value ??
                        1.0
                    ));
                if (value == 0.0) value = 1.0;
                return value;
            }
        }

        #endregion

        #region Рейтинги

        #region Целевой рейтинг

        /// <summary>
        ///     Целевой рейтинг, приведённый к базовому хронометражу
        /// </summary>
        public double GivenTRP => Weeks.Where(x => x.IsActive).Sum(week => week.GivenTRP);

        #endregion

        #region Суммарный рейтинг

        /// <summary>
        ///     Суммарный рейтинг
        /// </summary>
        public double GRP => Weeks.Where(x => x.IsActive).Sum(x => x.GRP);

        #endregion

        #region Минуты

        /// <summary>
        ///     Минуты
        /// </summary>
        public double Minutes {
            get {
                if (Channel.Region.IsMeasurable && RatingPrime == 0.0 && RatingOffPrime == 0.0) return 0.0;
                return Weeks.Where(x => x.IsActive).Sum(x => x.Minutes);
            }
        }

        #endregion

        #region Рейтинг рекламного блока

        /// <summary>
        ///     Средний рейтинг рекламного блока в prime в этом месяце канала
        /// </summary>
        public double RatingPrime {
            get => GetProperty(() => RatingPrime);
            private set {
                if (!Channel.Region.IsMeasurable) return;
                SetProperty(() => RatingPrime, value);
            }
        }

        /// <summary>
        ///     Средний рейтинг рекламного блока в Off prime в этом месяце канала
        /// </summary>
        public double RatingOffPrime {
            get => GetProperty(() => RatingOffPrime);
            private set {
                if (!Channel.Region.IsMeasurable) return;
                SetProperty(() => RatingOffPrime, value);
            }
        }

        #endregion

        #region Выходы

        /// <summary>
        ///     Выходы
        /// </summary>
        public double Outputs => ValuesValidator.NanOrZero(Weeks.Sum(w => w.Outputs));

        /// <summary>
        ///     Количество выходов в prime
        /// </summary>
        public double OutputsPrime {
            get {
                if (!Channel.Region.IsMeasurable) {
                    return Outputs * UnionPrimePortion;
                }

                if (RatingPrime == 0.0 && RatingOffPrime == 0.0) return 0.0;
                var value = ValuesValidator.NanOrZero(
                    GivenGRP * Channel.Region.Project.BaseTiming * UnionPrimePortion / (AverageTiming * RatingPrime)
                );
                if (double.IsInfinity(value)) value = 0.0;
                return value;
            }
        }

        /// <summary>
        ///     Количество выходов в off prime
        /// </summary>
        public double OutputsOffPrime {
            get {
                if (!Channel.Region.IsMeasurable) {
                    return Outputs * (1.0 - UnionPrimePortion);
                }

                if (RatingPrime == 0.0 && RatingOffPrime == 0.0) return 0.0;
                var value = ValuesValidator.NanOrZero(
                    GivenGRP * Channel.Region.Project.BaseTiming * (1.0 - UnionPrimePortion) /
                    (AverageTiming * RatingOffPrime)
                );
                if (double.IsInfinity(value)) value = 0.0;
                return value;
            }
        }

        /// <summary>
        ///     Выходы в день, специально для каналов неизмеряемых регионов
        /// </summary>
        public double _outputsPerDay;

        /// <summary>
        ///     Количество выходов в день
        /// </summary>
        public double OutputsPerDay {
            get {
                if (!(Channel?.Region.IsMeasurable ?? false)) return _outputsPerDay;
                if (RatingPrime == 0.0 && RatingOffPrime == 0.0) return 0.0;
                return ValuesValidator.NanOrZero(Outputs / ActiveDays);
            }
            set {
                // Если родительские объекты неактивны
                if (!IsActive || !Channel.IsActive) {
                    // Завершаем выполнение метода
                    return;
                }

                // Новое суммарное количество выходов
                var outputs = value * ActiveDays;

                if (!Channel.Region.IsMeasurable) {
                    _outputsPerDay = value;
                    Channel.Region.InvokePropertyChangedCore(
                        Channel.Region.Outputs,
                        EventType.Ui,
                        nameof(RegionVM.Outputs)
                    );
                    RaisePropertyChanged(() => OutputsPerDay);
                    return;
                }

                double grp;
                if (RatingPrime == 0.0 && RatingOffPrime == 0.0) {
                    return;
                }

                if (RatingPrime == 0.0 || RatingOffPrime == 0.0) {
                    grp = RatingPrime == 0.0
                        ? RatingOffPrime * outputs
                        : RatingPrime    * outputs;
                } else {
                    grp =
                        RatingPrime * RatingOffPrime * outputs /
                        (RatingOffPrime * UnionPrimePortion + RatingPrime * (1.0 - UnionPrimePortion));
                }


                TRP = grp * Affinity;

                // var prime = ValuesValidator.NanOrZero(
                //     (Channel.Region.Project.BaseTiming * UnionPrimePortion) / (AverageTiming * RatingPrime)
                // );
                // var offPrime = ValuesValidator.NanOrZero(
                //     (Channel.Region.Project.BaseTiming * (1.0 - UnionPrimePortion)) / (AverageTiming * RatingOffPrime)
                // );
                //
                // // Cуммарный рейтинг приведённого к базовому хронометражу
                // GivenGRP = ValuesValidator.NanOrZero(outputs / (prime + offPrime));
            }
        }

        #endregion

        #endregion

        #endregion

        #region Изменяемые свойства

        #region Признаки

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        public bool IsActive {
            get => GetProperty(() => IsActive);
            set {
                // Если новое значение равно старому
                if (!SetProperty(() => IsActive, value)) {
                    // Завершаем выполнение метода
                    return;
                }

                // Вызываем метод обработки этого события
                InvokePropertyChangedCore(value);

                // Оповещаем, что изменилось значение свойства месячного периода региона
                Channel.Region.Months.FirstOrDefault(month =>
                    month.Date.Year  == Date.Year &&
                    month.Date.Month == Date.Month
                )?.NotifyUI(nameof(RegionMonthVM.IsActive));
            }
        }

        #endregion

        #region Рейтинги

        #region Аффинитивность

        /// <summary>
        ///     Аффинитивность
        /// </summary>
        public double _affinity;

        /// <summary>
        ///     Аффинитивность
        /// </summary>
        public double Affinity {
            get {
                if ((Channel?.AffinityCorrectivePercent ?? 0.0) > 0.0) return _affinity;
                return (_affinity * (Channel?.AffinityCorrectivePercent ?? 0.0)) + _affinity;
            }
            set {
                if (!Channel.Region.IsMeasurable) return;
                if ((Channel?.AffinityCorrectivePercent ?? 0.0) < 0.0) {
                    value += Math.Abs(value * Channel?.AffinityCorrectivePercent ?? 0.0);
                }
                SetProperty(ref _affinity, value, () => Affinity);
            }
        }

        #endregion

        #region Целевой рейтинг

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double _trp;

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double TRP {
            get => double.IsNaN(_trp) ? 0.0 : _trp;
            set {
                if (!Channel.Region.IsMeasurable) return;

                // Если родительские объекты неактивны
                if (!IsActive || !Channel.IsActive) {
                    // Завершаем выполнение метода
                    return;
                }

                // Если новое значение равно старому
                if (!SetProperty(ref _trp, value, () => TRP)) {
                    // Завершаем выполнение метода
                    return;
                }

                // Вызываем метод обработки этого события
                InvokePropertyChangedCore(value);
            }
        }

        #endregion

        #region Суммарный рейтинг

        /// <summary>
        ///     Суммарный рейтинг, приведённый к базовому хронометражу проекта
        /// </summary>
        public double GivenGRP {
            get => Weeks.Where(x => x.IsActive).Sum(week => week.GivenGRP);
            set {
                if (!Channel.Region.IsMeasurable) return;

                // Если родительские объекты неактивны
                if (!IsActive || !Channel.IsActive) {
                    // Завершаем выполнение метода
                    return;
                }

                // Если старое значение равно новому
                if (Math.Abs(GivenGRP - value) < 0.0001) {
                    RaisePropertyChanged(() => Budget);
                    // Завершаем выполнение метода
                    return;
                }

                // Устанавливаем новое значение TRP
                TRP = value * Channel.Region.Project.BaseTiming / AverageTiming * Affinity;
                RaisePropertyChanged(() => Budget);
            }
        }

        #endregion

        #endregion

        #region Доли

        #region Качество

        /// <summary>
        ///     Доля fix (для Фед. ТВ) или float (для Рег. и Тем. ТВ)
        /// </summary>
        public double FixOrFloatPortion {
            get => GetProperty(() => FixOrFloatPortion);
            set {
                // Если значение больше 100% или меньше 0%
                value = value switch {
                    > 1.0 => 1.0,
                    < 0.0 => 0.0,
                    _ => value
                };

                // Если значение больше чем лимит
                if (value > Channel.QualityLimit.FixOrFloatMax) value = Channel.QualityLimit.FixOrFloatMax;
                // Если значение меньше чем лимит
                if (value < Channel.QualityLimit.FixOrFloatMin) value = Channel.QualityLimit.FixOrFloatMin;
                // Если максимальная доля super fix (для Фед. ТВ) или fix (для Рег. и Тем. ТВ) 0%
                if (Channel.QualityLimit.SuperFixOrFixMax == 0) value = 1.0;


                SetProperty(() => FixOrFloatPortion, value);
                SuperFixOrFixPortion = 1.0 - value;
                // Для обновления по лимиту
                FixOrFloatPrimePortion = FixOrFloatPrimePortion;
            }
        }

        /// <summary>
        ///     Доля fix prime (для Фед. ТВ) или float prime (для Рег. и Тем. ТВ)
        /// </summary>
        public double FixOrFloatPrimePortion {
            get => GetProperty(() => FixOrFloatPrimePortion);
            set {
                // Если значение больше 100% или меньше 0%
                value = value switch {
                    > 1.0 => 1.0,
                    < 0.0 => 0.0,
                    _ => value
                };

                // Если значение больше чем лимит
                if (value > Channel.QualityLimit.FixOrFloatPrimeMax) value = Channel.QualityLimit.FixOrFloatPrimeMax;
                // Если значение меньше чем лимит
                if (value < Channel.QualityLimit.FixOrFloatPrimeMin) value = Channel.QualityLimit.FixOrFloatPrimeMin;

                // Если общая доля FixOrFloatPortion равна 0%
                if (FixOrFloatPortion == 0.0) value = 0.0;


                SetProperty(() => FixOrFloatPrimePortion, value);
            }
        }

        /// <summary>
        ///     Доля super fix (для Фед. ТВ) или fix (для Рег. и Тем. ТВ)
        /// </summary>
        public double SuperFixOrFixPortion {
            get => GetProperty(() => SuperFixOrFixPortion);
            set {
                // Если значение больше 100% или меньше 0%
                value = value switch {
                    > 1.0 => 1.0,
                    < 0.0 => 0.0,
                    _ => value
                };

                // Если значение больше чем лимит
                if (value > Channel.QualityLimit.SuperFixOrFixMax) value = Channel.QualityLimit.SuperFixOrFixMax;
                // Если значение меньше чем лимит
                if (value < Channel.QualityLimit.SuperFixOrFixMin) value = Channel.QualityLimit.SuperFixOrFixMin;

                SetProperty(() => SuperFixOrFixPortion, value);

                // Для обновления по лимиту
                SuperFixOrFixPrimePortion = SuperFixOrFixPrimePortion;
            }
        }

        /// <summary>
        ///     Доля super fix prime (для Фед. ТВ) или fix prime (для Рег. и Тем. ТВ)
        /// </summary>
        public double SuperFixOrFixPrimePortion {
            get => GetProperty(() => SuperFixOrFixPrimePortion);
            set {
                // Если значение больше 100% или меньше 0%
                value = value switch {
                    > 1.0 => 1.0,
                    < 0.0 => 0.0,
                    _ => value
                };

                // Если значение больше чем лимит
                if (value > Channel.QualityLimit.SuperFixOrFixPrimeMax) {
                    value = Channel.QualityLimit.SuperFixOrFixPrimeMax;
                }

                // Если значение меньше чем лимит
                if (value < Channel.QualityLimit.SuperFixOrFixPrimeMin) {
                    value = Channel.QualityLimit.SuperFixOrFixPrimeMin;
                }

                // Если общая доля SuperFixOrFixPortion равна 0%
                if (SuperFixOrFixPortion == 0.0) value = 0.0;

                SetProperty(() => SuperFixOrFixPrimePortion, value);
            }
        }

        /// <summary>
        ///     Суммарная доля размещения в прайм тайм
        /// </summary>
        public double UnionPrimePortion => FixOrFloatPortion * FixOrFloatPrimePortion +
            SuperFixOrFixPortion                             * SuperFixOrFixPrimePortion;

        #endregion

        #endregion

        #region Бюджеты и цена

        /// <summary>
        ///     Цена за 1 пункт инвентаря канала с сезонным коэффициентом
        /// </summary>
        public double Price {
            get => GetProperty(() => Price);
            set => SetProperty(() => Price, value);
        }

        /// <summary>
        ///     Цена за 1 пункт инвентаря канала со всеми наценками:
        ///     1. Наценка за хронометраж
        ///     2. НДС
        ///     3. Маржа
        /// </summary>
        public double PriceWithMarkups => (
            (Price + (Price * Channel.Region.Project.Vat)) * AverageTimingMargin
        ) / (1.0 - Channel.Region.Project.Margin);

        /// <summary>
        ///     Бюджет
        /// </summary>
        public double Budget => Channel.SaleType == SaleType.GRP
            ? PriceWithMarkups * GivenGRP
            : PriceWithMarkups * Minutes;

        #endregion

        #endregion

        #region Коллекции

        #region Недельные периоды

        /// <summary>
        ///     Коллекция недельных периодов
        /// </summary>
        public ObservableCollection<WeekVM> Weeks { get; } = new();

        #endregion

        #region Хронометражи

        /// <summary>
        ///     Коллекция хронометражей
        /// </summary>
        public ObservableCollection<MonthTimingVM> Timings { get; } = new();

        #endregion

        #endregion

        #endregion

        #region Методы

        /// <summary>
        ///     Метод генерирования хронометражей месяца из недельных хронометражей
        /// </summary>
        public void GenerateTimings() {
            Timings.Clear();
            Weeks
               .SelectMany(x => x.Timings)
               .GroupBy(x => x.Value)
               .Select(x => new MonthTimingVM(this, x.Key))
               .OrderBy(x => x.Value)
               .ForEach(Timings.Add);
        }

        public PDMonth ToEntity(PDChannel? channel = null) {
            var entity = new PDMonth {
                ID = ID,
                ChannelID = Channel.ID,
                Channel = channel,
                Affinity = _affinity,
                OutputsPerDay = Channel.Region.IsMeasurable ? 0.0 : OutputsPerDay,
                Date = Date,
                RatingPrime = RatingPrime,
                RatingOffPrime = RatingOffPrime,
                FixOrFloatPortion = FixOrFloatPortion,
                FixOrFloatPrimePortion = FixOrFloatPrimePortion,
                SuperFixOrFixPrimePortion = SuperFixOrFixPrimePortion,
                Price = Price
            };
            entity.Weeks.AddRange(Weeks.Where(w => w.IsActive).Select(w => w.ToEntity(entity)));

            return entity;
        }

        #endregion

        /// <summary>
        ///     Метод сериализации для проверки данных
        /// </summary>
        /// <returns><see cref="string"/>Строка данных</returns>
        public string GetSerializeString() { 
            // Строка данных месяца
            var monthStr = $"{ID}{Date}{_affinity}{_outputsPerDay}{Price}{RatingOffPrime}{RatingPrime}{FixOrFloatPortion}{FixOrFloatPrimePortion}{SuperFixOrFixPrimePortion}";
            // Возвращаем строку месяца и неделей
            return monthStr + string.Join("", this.Weeks.OrderBy(w => w.DateFrom).Select(w => w.GetSerializeString()));
        }
    }
}
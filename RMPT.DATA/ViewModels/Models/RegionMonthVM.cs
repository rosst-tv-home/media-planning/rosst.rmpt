﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using RMPT.COMMON.Models;
using RMPT.CORE;
using RMPT.DATA.ViewModels.Models.Timing;
using RMPT.EVENT;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models {
    public class RegionMonthVM : AbstractObservable {
        public RegionMonthVM(RegionVM region, ICollection<MonthVM> months) {
            this._months = months;
            this.Region = region;
            this.Date = this._months.First().Date;

            foreach (var month in this._months) {
                month.PropertyChanged += OnMonthPropertyChanged;
            }

            this._months
               .SelectMany(m => m.Weeks)
               .SelectMany(w => w.Timings)
               .GroupBy(t => t.Value)
               .Select(g => new RegionTimingVM(this, g.Key, g.ToList()))
               .ForEach(Timings.Add);
            
            this._months
               .SelectMany(m => m.Weeks)
               .GroupBy(w => w.DateFrom)
               .Select(g => new RegionWeekVM(this, g.ToList()))
               .ForEach(this.Weeks.Add);
        }

        private void OnMonthPropertyChanged(object sender, PropertyChangedEventArgs arguments) {
            if (sender is not MonthVM) return;
            switch (arguments.PropertyName) {
                case nameof(MonthVM.TRP): {
                    RaisePropertyChanged(() => TRP);
                    break;
                }
                case nameof(MonthVM.IsActive): {
                    RaisePropertyChanged(() => TRP);
                    RaisePropertyChanged(() => IsActive);
                    break;
                }
            }
        }

        /// <summary>
        ///     Регион
        /// </summary>
        public RegionVM Region { get; }

        /// <summary>
        ///     Дата месячного периода
        /// </summary>
        public readonly DateTime Date;

        /// <summary>
        ///     Месяца
        /// </summary>
        private readonly ICollection<MonthVM> _months;

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        public bool IsActive => _months.Any(x => x.IsActive);

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double TRP {
            get => _months.Where(x => x.IsActive).Sum(x => x.TRP);
            set {
                _months.ForEach(x => x._trp = value * x.Channel.Portion);
                _months.ForEach(x => x.NotifyUI(nameof(MonthVM.TRP)));
                _months.ForEach(x => x.InvokePropertyChangedCore(x.TRP, EventType.Ui, nameof(MonthVM.TRP)));
                Messenger.Default.Send(new EventRefreshProjectTable(Region.Project.ID));
            }
        }

        /// <summary>
        ///     Выходы
        /// </summary>
        public double Outputs => _months.Sum(m => m.Outputs);

        public ObservableCollection<RegionTimingVM> Timings { get; } = new();

        public ObservableCollection<RegionWeekVM> Weeks { get; } = new();
    }
}
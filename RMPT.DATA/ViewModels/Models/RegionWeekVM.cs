﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using RMPT.COMMON.Models;
using RMPT.CORE;
using RMPT.DATA.ViewModels.Models.Timing;
using RMPT.EVENT;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models {
    public class RegionWeekVM : AbstractObservable {
        public RegionMonthVM Month { get; }

        public ObservableCollection<WeekVM> Weeks { get; }

        public DateTime DateFrom { get; }

        public DateTime DateBefore { get; }

        public bool IsActive => this.Weeks.Any(w => w.IsActive);

        public double ActiveDays {
            get => this.Weeks.Where(w => w.IsActive).Average(w => w.ActiveDays);
            set {
                foreach (var week in this.Weeks.Where(w => w.IsActive).ToList()) {
                    week.ActiveDays = Convert.ToInt32(value);
                }
                Messenger.Default.Send(new EventRefreshProjectTable(this.Month.Region.Project.ID));
            }
        }

        public double TRP {
            get => this.Weeks.Where(w => w.IsActive).Sum(w => w.TRP);
            set {
                var weeks = this.Weeks.Where(w => w.IsActive).ToList();
                weeks.ForEach(w => w._trp = value * w.Month.Channel.Portion);
                weeks.ForEach(w => w.NotifyUI(nameof(WeekVM.TRP)));
                weeks.ForEach(w => w.InvokePropertyChangedCore(w.TRP, EventType.Ui, nameof(WeekVM.TRP)));
                Messenger.Default.Send(new EventRefreshProjectTable(this.Month.Region.Project.ID));
            }
        }

        public double Outputs => this.Weeks.Where(w => w.IsActive).Sum(w => w.Outputs);

        public ObservableCollection<RegionWeekTimingVM> Timings { get; } = new();

        public RegionWeekVM(RegionMonthVM month, IEnumerable<WeekVM> weeks) {
            this.Month = month;
            this.Weeks = weeks.ToObservableCollection();
            this.DateFrom = this.Weeks.First().DateFrom;
            this.DateBefore = this.Weeks.First().DateBefore;
            this.Weeks
               .SelectMany(t => t.Timings)
               .GroupBy(t => t.Value)
               .Select(g => new RegionWeekTimingVM(this, g.ToList()))
               .ForEach(this.Timings.Add);
        }
    }
}
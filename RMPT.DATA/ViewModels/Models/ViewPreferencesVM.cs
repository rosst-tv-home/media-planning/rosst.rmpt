﻿using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Projects;
using System.ComponentModel;

namespace RMPT.DATA.ViewModels.Models {
    public class ViewPreferencesVM : AbstractObservable {
        /// <summary>
        ///     Проект
        /// </summary>
        public ProjectVM Project { get; }

        /// <summary>
        ///     Признак "Отображать только активные каналы"
        /// </summary>
        public bool IsShowOnlyActiveChannels {
            get => GetProperty(() => IsShowOnlyActiveChannels);
            set => SetProperty(() => IsShowOnlyActiveChannels, value);
        }

        /// <summary>
        ///     Признак "Отображать данные сырья"
        /// </summary>
        public bool IsShowRawDataBand {
            get => GetProperty(() => IsShowRawDataBand);
            set => SetProperty(() => IsShowRawDataBand, value);
        }

        /// <summary>
        ///     Признак "Отображать недели"
        /// </summary>
        public bool IsShowMonthsWeekBands {
            get => GetProperty(() => IsShowMonthsWeekBands);
            set => SetProperty(() => IsShowMonthsWeekBands, value);
        }

        /// <summary>
        ///     Признак "Отображать распределение по каналам"
        /// </summary>
        public bool IsShowChannelDistributionBands {
            get => GetProperty(() => IsShowChannelDistributionBands);
            set => SetProperty(() => IsShowChannelDistributionBands, value);
        }

        /// <summary>
        ///     Признак "Отображать распределение по месяцам"
        /// </summary>
        public bool IsShowMonthDistributionBands {
            get => GetProperty(() => IsShowMonthDistributionBands);
            set => SetProperty(() => IsShowMonthDistributionBands, value);
        }

        /// <summary>
        ///     Признак "Отображать распределение хронометражей по каналам"
        /// </summary>
        public bool IsShowChannelTimingDistributionBand {
            get => GetProperty(() => IsShowChannelTimingDistributionBand);
            set => SetProperty(() => IsShowChannelTimingDistributionBand, value);
        }

        /// <summary>
        ///     Признак "Отображать распределение хронометражей по месяцам"
        /// </summary>
        public bool IsShowMonthTimingDistributionBand {
            get => GetProperty(() => IsShowMonthTimingDistributionBand);
            set => SetProperty(() => IsShowMonthTimingDistributionBand, value);
        }

        /// <summary>
        ///     Признак "Отображать распределение хронометражей по неделям"
        /// </summary>
        public bool IsShowWeekTimingDistributionBand {
            get => GetProperty(() => IsShowWeekTimingDistributionBand);
            set => SetProperty(() => IsShowWeekTimingDistributionBand, value);
        }

        #region Настриваемые столбцы и группы столбцов

        #region Все столбцы и групы столбцов

        /// <summary>
        ///     Признак "Отображать все настраиваемые столбцы и группы столбцов"
        /// </summary>
        [DefaultValue(true)]
        public bool? IsShowCustomizableAll {
            get => GetProperty(() => IsShowCustomizableAll);
            set => SetProperty(() => IsShowCustomizableAll, value);
        }

        #endregion

        #region Инвентарь

        /// <summary>
        ///     Признак "Отображать группы столбцов Инвентарь"
        /// </summary>
        [DefaultValue(true)]
        public bool? IsShowCustomizableInventoryBand {
            get => GetProperty(() => IsShowCustomizableInventoryBand);
            set => SetProperty(() => IsShowCustomizableInventoryBand, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы TRP"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizableTrpColumn {
            get => GetProperty(() => IsShowCustomizableTrpColumn);
            set => SetProperty(() => IsShowCustomizableTrpColumn, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы GivenTRP"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizableGivenTrpColumn {
            get => GetProperty(() => IsShowCustomizableGivenTrpColumn);
            set => SetProperty(() => IsShowCustomizableGivenTrpColumn, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы GRP"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizableGrpColumn {
            get => GetProperty(() => IsShowCustomizableGrpColumn);
            set => SetProperty(() => IsShowCustomizableGrpColumn, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы GivenGRP"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizableGivenGrpColumn {
            get => GetProperty(() => IsShowCustomizableGivenGrpColumn);
            set => SetProperty(() => IsShowCustomizableGivenGrpColumn, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы Minutes"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizableMinutesColumn {
            get => GetProperty(() => IsShowCustomizableMinutesColumn);
            set => SetProperty(() => IsShowCustomizableMinutesColumn, value);
        }

        #endregion

        #region Выходы

        /// <summary>
        ///     Признак "Отображать группы столбцов Выходы"
        /// </summary>
        [DefaultValue(true)]
        public bool? IsShowCustomizableOutputsBand {
            get => GetProperty(() => IsShowCustomizableOutputsBand);
            set => SetProperty(() => IsShowCustomizableOutputsBand, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы "Prime" в группе столбцов "Выходы""
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizablePrimeOutputsColumn {
            get => GetProperty(() => IsShowCustomizablePrimeOutputsColumn);
            set => SetProperty(() => IsShowCustomizablePrimeOutputsColumn, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы "Off Prime" в группе столбцов "Выходы""
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizableOffPrimeOutputsColumn {
            get => GetProperty(() => IsShowCustomizableOffPrimeOutputsColumn);
            set => SetProperty(() => IsShowCustomizableOffPrimeOutputsColumn, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы "Всего" в группе столбцов "Выходы""
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizableTotalOutputsColumn {
            get => GetProperty(() => IsShowCustomizableTotalOutputsColumn);
            set => SetProperty(() => IsShowCustomizableTotalOutputsColumn, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы "В день" в группе столбцов "Выходы""
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizablePerDayOutputsColumn {
            get => GetProperty(() => IsShowCustomizablePerDayOutputsColumn);
            set => SetProperty(() => IsShowCustomizablePerDayOutputsColumn, value);
        }

        #endregion

        #region Бюджет

        /// <summary>
        ///     Признак "Отображать группы столбцов Бюджет"
        /// </summary>
        [DefaultValue(true)]
        public bool? IsShowCustomizableBudgetBand {
            get => GetProperty(() => IsShowCustomizableBudgetBand);
            set => SetProperty(() => IsShowCustomizableBudgetBand, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы "Цена НРА" в группе столбцов "Бюджет""
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizablePriceColumn {
            get => GetProperty(() => IsShowCustomizablePriceColumn);
            set => SetProperty(() => IsShowCustomizablePriceColumn, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы "Цена с наценками" в группе столбцов "Бюджет""
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizablePriceWithMarginsColumn {
            get => GetProperty(() => IsShowCustomizablePriceWithMarginsColumn);
            set => SetProperty(() => IsShowCustomizablePriceWithMarginsColumn, value);
        }

        /// <summary>
        ///     Признак "Отображать столбцы "Бюджет" в группе столбцов "Бюджет""
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowCustomizableBudgetColumn {
            get => GetProperty(() => IsShowCustomizableBudgetColumn);
            set => SetProperty(() => IsShowCustomizableBudgetColumn, value);
        }

        #endregion

        #endregion

        public ViewPreferencesVM(
            ProjectVM project,
            bool isShowOnlyActiveChannels,
            bool isShowRawDataBand,
            bool isShowMonthsWeekBands,
            bool isShowChannelDistributionBands,
            bool isShowMonthDistributionBands,
            bool isShowChannelTimingDistributionBand,
            bool isShowMonthTimingDistributionBand,
            bool isShowWeekTimingDistributionBand
        ) {
            Project = project;

            IsShowOnlyActiveChannels = isShowOnlyActiveChannels;
            IsShowRawDataBand = isShowRawDataBand;
            IsShowMonthsWeekBands = isShowMonthsWeekBands;

            IsShowChannelDistributionBands = isShowChannelDistributionBands;
            IsShowMonthDistributionBands = isShowMonthDistributionBands;

            IsShowChannelTimingDistributionBand = isShowChannelTimingDistributionBand;
            IsShowMonthTimingDistributionBand = isShowMonthTimingDistributionBand;
            IsShowWeekTimingDistributionBand = isShowWeekTimingDistributionBand;
        }

        public PDViewPreferences ToEntity(PDProject? project = null) => new() {
            Project = project,
            ProjectID = Project.ID,
            IsShowOnlyActiveChannels = IsShowOnlyActiveChannels,
            IsShowRawDataBand = IsShowRawDataBand,
            IsShowMonthsWeekBands = IsShowMonthsWeekBands,
            IsShowChannelDistributionBands = IsShowChannelDistributionBands,
            IsShowMonthsDistributionBands = IsShowMonthDistributionBands,
            IsShowChannelTimingDistributionBand = IsShowChannelTimingDistributionBand,
            IsShowMonthTimingDistributionBand = IsShowMonthTimingDistributionBand,
            IsShowWeekTimingDistributionBand = IsShowWeekTimingDistributionBand,
        };

        /// <summary>
        ///     Метод сеарилизации для проверки данных
        /// </summary>
        /// <returns><see cref="string"/>Строка отображенных столбцов</returns>
        public string GetSerializeString() =>
            $"{IsShowOnlyActiveChannels}{IsShowRawDataBand}{IsShowMonthsWeekBands}{IsShowChannelDistributionBands}{IsShowMonthDistributionBands}" +
            $"{IsShowChannelTimingDistributionBand}{IsShowMonthTimingDistributionBand}{IsShowWeekTimingDistributionBand}";
    }
}
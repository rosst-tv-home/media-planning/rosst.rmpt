﻿using DevExpress.Mvvm.Native;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.CORE;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.DataBase.Repositories;
using RMPT.DATA.ViewModels.Models.Timing;
using RMPT.VALIDATE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.ViewModels.Models {
    /// <summary>
    ///     Модель представления канала региона
    /// </summary>
    public class ChannelVM : AbstractObservable {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="dictionaryID">Идентификатор канала</param>
        /// <param name="id">Идентификатор связи канала с регионом</param>
        /// <param name="iid">Идентификатор инвентаря канала в справочнике НРА Считалки</param>
        /// <param name="name">Наименование</param>
        /// <param name="isActive">Признак "активен"</param>
        /// <param name="region">Регион</param>
        /// <param name="saleType">Тип продаж</param>
        /// <param name="salePoint">Точка продаж</param>
        /// <param name="ratings">Рейтинги</param>
        /// <param name="audiences">Аудитории</param>
        /// <param name="baseAudience">Базовые аудитории</param>
        /// <param name="months">Месячные периоды</param>
        /// <param name="qualityLimit">Лимит качеств</param>
        /// <param name="affinityCorrective">Корректировочный Аффинити</param>
        public ChannelVM(
            RegionVM region,
            int id,
            int dictionaryID,
            int? iid,
            string name,
            bool isActive,
            DCSaleType saleType,
            string? salePoint,
            IEnumerable<DCRating> ratings,
            IEnumerable<PDAudience> audiences,
            DCAudience baseAudience,
            IEnumerable<PDMonth> months,
            DCQualityLimits qualityLimit,
            double affinityCorrective
        ) {
            ID = id;
            DictionaryID = dictionaryID;
            IID = iid;
            Name = name;
            IsActive = isActive;
            Region = region;
            SaleType = (SaleType)saleType;
            SalePoint = salePoint;
            QualityLimit = qualityLimit.ToViewModel(this);

            audiences.Select(audience => audience.ToViewModel(this)).ForEach(Audiences.Add);
            TargetAudience = Audiences.FirstOrDefault(x => x.IsTarget);
            BaseAudience = Audiences.FirstOrDefault(x => !x.IsTarget);

            ratings.Select(rating => rating.ToViewModel(this)).ForEach(Ratings.Add);

            // Месяцы
            months.Select(month => {
                // Модель представления
                var vm = month.ToViewModel(this);
                // Генерируем хронометражи
                vm.GenerateTimings();
                // Возвращаем модель представления
                return vm;
            }).ForEach(Months.Add);

            // Коллекция всех периодов
            var monthsCollection = new List<MonthVM>();
            // Генерируем недостающие периоды
            for (var y = 0; y <= 1; y++) {
                for (var m = 1; m <= 12; m++) {
                    var monthDate = new DateTime(Region.Project.PlaningYear + y, m, 1);
                    var month = Months.FirstOrDefault(x => x.Date == monthDate) ?? new MonthVM(
                        0, this, new DateTime(Region.Project.PlaningYear + y, m, 1),
                        Affinity, 0.0, 0.0, 0.0, 0.0
                    );

                    // Наполняемая коллекция недельных периодов
                    var weeks = new List<Tuple<DateTime, DateTime>>();

                    // Дата окончания месячного периода
                    var monthEndDate = monthDate.AddMonths(1).AddDays(-1);

                    // Дата начала недельного периода
                    DateTime? dateFrom = null;

                    // Итерируемся по дням месяца
                    for (var i = 0; i <= (monthEndDate - monthDate).Days + 1; i++) {
                        // Если начало периода или понедельник
                        if (i == 0 || monthDate.AddDays(i).DayOfWeek == DayOfWeek.Monday) {
                            dateFrom = monthDate.AddDays(i);
                        }

                        // Если не воскресенье и не окончание периода - переходим к следующему дню
                        if (monthDate.AddDays(i).Month != monthDate.Month ||
                            (
                                monthDate.AddDays(i).DayOfWeek != DayOfWeek.Sunday &&
                                i                              != (monthEndDate - monthDate).Days
                            )
                        ) continue;

                        // Дата окончания недельного периода
                        DateTime? dateBefore = monthDate.AddDays(i);

                        // Добавляем недельный период в коллекцию
                        weeks.Add(Tuple.Create(dateFrom!.Value, dateBefore.Value));
                    }

                    var weeksCollection = new List<WeekVM>();
                    foreach (var (from, before) in weeks) {
                        var week =
                            month.Weeks.FirstOrDefault(x => x.DateFrom == from) ??
                            new WeekVM(0, month, from, before, 0, 0.0);

                        var timingsCollection = new List<TimingVM>();
                        for (var t = 5; t <= 60; t += 5) {
                            timingsCollection.Add(
                                week.Timings.FirstOrDefault(x => x.Value == t) ??
                                new TimingVM(week, 0, t)
                            );
                        }

                        week.Timings.Clear();
                        foreach (var timing in timingsCollection.OrderBy(t => t.Value).ToList()) {
                            week.Timings.Add(timing);
                        }

                        weeksCollection.Add(week);
                    }

                    month.Weeks.Clear();
                    foreach (var week in weeksCollection.OrderBy(w => w.DateFrom).ToList()) {
                        month.Weeks.Add(week);
                    }

                    monthsCollection.Add(month);
                }
            }

            // Очищаем месяца канала
            Months.Clear();

            // Наполняем месяца канала
            foreach (var month in monthsCollection.OrderBy(m => m.Date).ToList()) {
                month.GenerateTimings();
                Months.Add(month);
            }

            // Если первая инициализация проекта
            if (Region.Project.IsFirstInitialization) {
                // Рассчитываем аффинитивность периодов
                CalculateMonthsAffinity();
                // Пересчитываем корректировочный % аффинитивности
                CalculateAffinityCorrectivePercent();
            } else AffinityCorrectivePercent = affinityCorrective;

            // Пересчитываем TRP канала
            _trp = Months.Sum(x => x.Weeks.Sum(y => y.TRP));
        }

        #endregion

        #region Свойства

        #region Идентификаторы

        /// <summary>
        ///     Идентификатор канала
        /// </summary>
        public readonly int DictionaryID;

        /// <summary>
        ///     Идентификатор связи канала со связью региона с проектом
        /// </summary>
        public readonly int ID;

        /// <summary>
        ///     Идентификатор инвентаря канала в справочнике НРА Считалки
        /// </summary>
        public readonly int? IID;

        #endregion

        #region Родительский объект

        /// <summary>
        ///     Регион
        /// </summary>
        public RegionVM Region { get; }

        #endregion

        #region Наименования

        /// <summary>
        ///     Наименование
        /// </summary>
        public string Name { get; }

        #endregion

        #region Аудитории

        /// <summary>
        ///     Целевая аудитория проекта
        /// </summary>
        public AudienceVM? TargetAudience { get; }

        /// <summary>
        ///     Базовая аудитория канала
        /// </summary>
        public AudienceVM? BaseAudience { get; }

        #endregion

        #region Продажи

        /// <summary>
        ///     Тип продаж
        /// </summary>
        public SaleType SaleType { get; }

        /// <summary>
        ///     Точка продаж
        /// </summary>
        public string? SalePoint { get; }

        #endregion

        #region Лимит качеств

        /// <summary>
        ///     Лимит качеств
        /// </summary>
        public QualityLimitVM QualityLimit { get; }

        #endregion

        #region Дни

        /// <summary>
        ///     Количество активных дней
        /// </summary>
        public int ActiveDays => Months.Where(x => x.IsActive).Sum(month => month.ActiveDays);

        #endregion

        #region Рейтинги

        #region Целевой рейтинг

        /// <summary>
        ///     Целевой рейтинг, приведённый к базовому хронометражу проекта
        /// </summary>
        public double GivenTRP => Months.Where(x => x.IsActive).Sum(month => month.GivenTRP);

        #endregion

        #region Суммарный рейтинг

        /// <summary>
        ///     Суммарный рейтинг
        /// </summary>
        public double GRP => Months.Where(x => x.IsActive).Sum(month => month.GRP);

        /// <summary>
        ///     Суммарный рейтинг, приведённый к базовому хронометражу проекта
        /// </summary>
        public double GivenGRP => Months.Where(x => x.IsActive).Sum(month => month.GivenGRP);

        #endregion

        #region Минуты

        /// <summary>
        ///     Минуты
        /// </summary>
        public double Minutes => Months.Where(x => x.IsActive).Sum(x => x.Minutes);

        #endregion

        #endregion

        #region Выходы

        /// <summary>
        ///     Выходы
        /// </summary>
        public double Outputs => ValuesValidator.NanOrZero(Months.Sum(m => m.Outputs));

        #endregion

        #region Доли

        /// <summary>
        ///     Суммарная доля хронометражей
        /// </summary>
        public double TotalTimingPortion => Timings.Sum(t => t.Portion);

        #endregion

        /// <summary>
        ///     Корректировачный процент affinity
        /// </summary>
        [DefaultValue(1.0)]
        public double AffinityCorrectivePercent {
            get => GetProperty(() => AffinityCorrectivePercent);
            set => SetProperty(() => AffinityCorrectivePercent, value);
        }

        #endregion

        #region Изменяемые свойства

        #region Признаки

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        public bool IsActive {
            get => GetProperty(() => IsActive);
            set => SetProperty(() => IsActive, value, () => InvokePropertyChangedCore(value));
        }

        #endregion

        #region Распределение долей

        #region Доля

        /// <summary>
        ///     Доля
        /// </summary>
        public double _portion;

        /// <summary>
        ///     Доля
        /// </summary>
        public double Portion {
            get {
                var value = _portion;
                if (double.IsNaN(value) || double.IsInfinity(value)) value = 0.0;
                return value;
            }
            set {
                // Если регион неизмеряемый - завершаем выполнение метода
                if (!Region.IsMeasurable) return;

                value = value switch {
                    > 1.0 => 1.0,
                    < 0.0 => 0.0,
                    _ => value
                };

                // Если канал неактивен или новое значение равно старому
                if (!IsActive || !SetProperty(ref _portion, value, nameof(Portion))) {
                    // Завершаем выполнение метода
                    return;
                }

                // Вызываем метод обработки этого события
                InvokePropertyChangedCore(value);
            }
        }

        #endregion

        #region Качество

        /// <summary>
        ///    Доля fix (для Фед. ТВ) или float (для Рег. и Тем. ТВ)
        /// </summary>
        public double FixOrFloatPortion {
            get {
                var value = Months.Where(x => x.IsActive).Sum(x => x.FixOrFloatPortion) / Months.Count(x => x.IsActive);
                if (double.IsNaN(value) || double.IsInfinity(value)) value = 0.0;
                return value;
            }
            set => InvokePropertyChangedCore(value, EventType.Tunneling);
        }

        /// <summary>
        ///     Доля fix prime (для Фед. ТВ) или float prime (для Рег. и Тем. ТВ)
        /// </summary>
        public double FixOrFloatPrimePortion {
            get {
                var value = Months.Where(x => x.IsActive).Sum(x => x.FixOrFloatPrimePortion) /
                    Months.Count(x => x.IsActive);
                if (double.IsNaN(value) || double.IsInfinity(value)) value = 0.0;
                return value;
            }
            set => InvokePropertyChangedCore(value, EventType.Tunneling);
        }

        /// <summary>
        ///     Доля super fix (для Фед. ТВ) или fix (для Рег. и Тем. ТВ)
        /// </summary>
        public double SuperFixOrFixPortion {
            get {
                var value = Months.Where(x => x.IsActive).Sum(x => x.SuperFixOrFixPortion) /
                    Months.Count(x => x.IsActive);
                if (double.IsNaN(value) || double.IsInfinity(value)) value = 0.0;
                return value;
            }
        }

        /// <summary>
        ///     Доля super fix prime (для Фед. ТВ) или fix prime (для Рег. и Тем. ТВ)
        /// </summary>
        public double SuperFixOrFixPrimePortion {
            get {
                var value = Months.Where(x => x.IsActive).Sum(x => x.SuperFixOrFixPrimePortion) /
                    Months.Count(x => x.IsActive);
                if (double.IsNaN(value) || double.IsInfinity(value)) value = 0.0;
                return value;
            }
            set => InvokePropertyChangedCore(value, EventType.Tunneling);
        }

        #endregion

        #endregion

        #region Рейтинги

        #region Аффинитивность

        /// <summary>
        ///     Аффинитивность
        /// </summary>
        public double Affinity {
            get {
                var value = Months.Where(m => m.IsActive).Sum(month => month.Affinity) /
                    Months.Where(m => m.IsActive).ToList().Count;
                if (double.IsNaN(value) || double.IsInfinity(value)) value = 0.0;
                return value;
            }
            set {
                if (!Region.IsMeasurable) return;
                Months.ForEach(month => month.Affinity = value);
            }
        }

        #endregion

        #region Целевой рейтинг

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double _trp;

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double TRP {
            get => _trp;
            set {
                if (!Region.IsMeasurable) return;

                // Если месяц неактивен или новое значение равно старому - завершаем выполнение метода
                if (!IsActive || !SetProperty(ref _trp, value, nameof(TRP))) {
                    return;
                }

                // Вызываем метод обработки этого события
                InvokePropertyChangedCore(value);
            }
        }

        #endregion

        #endregion

        #endregion

        #region Коллекции

        /// <summary>
        ///     Коллекция рейтингов канала
        /// </summary>
        public ObservableCollection<RatingVM> Ratings { get; } = new();

        /// <summary>
        ///     Коллекция сырьевых данных аудиторий канала
        /// </summary>
        public ObservableCollection<AudienceVM> Audiences { get; } = new();

        /// <summary>
        ///     Коллекция месячных периодов
        /// </summary>
        public ObservableCollection<MonthVM> Months { get; } = new();

        /// <summary>
        ///     Хронометражи
        /// </summary>
        public ObservableCollection<ChannelTimingVM> Timings { get; } = new();

        #endregion

        #region Методы

        /// <summary>
        ///     Метод генерирования хронометражей канала из месячных хронометражей
        /// </summary>
        public void GenerateTimings() => Months
           .SelectMany(month => month.Timings)
           .GroupBy(timing => timing.Value)
           .Select(g => new ChannelTimingVM(this, g.Key))
           .OrderBy(x => x.Value)
           .ForEach(Timings.Add);

        /// <summary>
        ///     Метод корректировки Affinity в периодах канала, путём сравнивания % роста (или снижения)
        ///     affinity в периодах канала по отношению к прошлым периодам. Если медиана этого % роста (или снижения)
        ///     является снижением - корректируем affinity всех текущих периодов на этот % снижения
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        private void CalculateAffinityCorrectivePercent() {
            if (!Region.IsMeasurable) return;
            // % роста или снижения affinity периодов канала
            var percents = new List<double>();

            // Итерируемся по месяцам
            foreach (var groupByMonth in Audiences.GroupBy(a => a.Date.Month).ToList()) {
                // Аффинити по прошлому периоду, и по текущему
                var affinity = new List<KeyValuePair<int, double>>();

                // Итерируемся по годам
                foreach (var groupByYear in groupByMonth.GroupBy(g => g.Date.Year).ToList()) {
                    if (groupByYear.Count() > 2 || groupByYear.Count() < 2) {
                        throw new ArgumentException();
                    }

                    if (groupByYear.All(g => g.IsTarget) || groupByYear.All(g => !g.IsTarget)) {
                        throw new ArgumentException();
                    }

                    affinity.Add(new KeyValuePair<int, double>(
                        groupByYear.First().Date.Year,
                        groupByYear.First(g => g.IsTarget).TVR / groupByYear.First(g => !g.IsTarget).TVR
                    ));
                }

                // Affinity актуального периода
                var currentYearAffinity = affinity.OrderByDescending(g => g.Key).First();
                // Affinity прошлогоднего периода
                var previousYearAffinity = affinity.OrderBy(g => g.Key).First();

                // Рассчитываем % роста (или снижения)
                percents.Add((currentYearAffinity.Value - previousYearAffinity.Value) / previousYearAffinity.Value);
            }

            // Сортируем значения по возрастанию
            percents.Sort();

            // Определяем медиану
            var median = percents.Count % 2 != 0
                ? percents[percents.Count / 2]
                : (percents[percents.Count / 2] + percents[(percents.Count / 2) - 1]) / 2;

            // Если медианный корректировочный Affinity меньше 0.0 - меняем значение
            if (median < 0.0) {
                AffinityCorrectivePercent = median;
            }
        }

        private void CalculateMonthsAffinity() {
            if (!Region.IsMeasurable) return;
            // Аудитории, сгруппированные по году
            var audienceGroupByYear = Audiences.GroupBy(a => a.Date.Year).OrderByDescending(g => g.Key).ToList();

            // Итерируемся по месяцам
            foreach (var month in Months) {
                // Пытаемся найти данные аудиторий в актуальном периоде
                List<AudienceVM> audiences = audienceGroupByYear.First()
                   .Where(a => a.Date.Month == month.Date.Month)
                   .ToList();

                // Если нет данных аудиторий в актуальном периоде, пытаемся найти в прошлом периоде
                if (audiences.Count == 0) {
                    audiences = audienceGroupByYear.Last().Where(a => a.Date.Month == month.Date.Month).ToList();
                }

                // Если данных аудиторий нет - переходим к следующему месяцу
                if (audiences.Count != 2) continue;

                // Устанавливаем аффинитивность периода
                month._affinity = audiences.First(a => a.IsTarget).TVR / audiences.First(a => !a.IsTarget).TVR;
            }
        }

        public PDChannel ToEntity(PDRegion? region = null) {
            var channel = new PDChannel {
                ID = ID,
                DictionaryID = DictionaryID,
                RegionID = Region.ID,
                IsActive = IsActive,
                Region = region,
                AffinityCorrectivePercent = AffinityCorrectivePercent
            };

            channel.Audiences.AddRange(Audiences.Select(a => a.ToEntity(channel)));
            channel.Months.AddRange(Months.Where(m => m.IsActive).Select(m => m.ToEntity(channel)));

            return channel;
        }

        #endregion

        /// <summary>
        ///     Метод сериализации для проверки данных
        /// </summary>
        /// <returns><see cref="string"/>Строка данных</returns>
        public string GetSerializeString() {
            // Строка данных канала 
            var channelStr = $"{DictionaryID}{Region.ID}{IsActive}{AffinityCorrectivePercent}";
            // Возвращаем строку канала и месяцев
            return channelStr + string.Join("", this.Months.OrderBy(m => m.Date).Select(m => m.GetSerializeString()));
        }
    }
}
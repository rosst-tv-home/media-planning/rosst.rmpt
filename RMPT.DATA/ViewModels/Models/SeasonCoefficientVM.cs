﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using System;

namespace RMPT.DATA.ViewModels.Models {
    public class SeasonCoefficientVM : AbstractObservable {
        public SeasonCoefficientVM(RegionVM region, int id, DateTime date, double value) {
            Region = region;

            ID = id;
            Date = date;
            Value = value;
        }

        public readonly int ID;

        public readonly DateTime Date;

        public readonly double Value;

        public readonly RegionVM Region;
    }
}
﻿using RMPT.COMMON;
using RMPT.COMMON.Models;

namespace RMPT.DATA.ViewModels.Models {
    public class QualityLimitVM : AbstractObservable {
        public QualityLimitVM(
            ChannelVM channel, int id,
            double fixOrFloatMin, double fixOrFloatMax, double fixOrFloatPrimeMin, double fixOrFloatPrimeMax,
            double fixOrFloatOffPrimeMin, double fixOrFloatOffPrimeMax, double superFixOrFixMin,
            double superFixOrFixMax, double superFixOrFixPrimeMin, double superFixOrFixPrimeMax,
            double superFixOrFixOffPrimeMin, double superFixOrFixOffPrimeMax
        ) {
            ID = id;
            Channel = channel;
            FixOrFloatMin = fixOrFloatMin;
            FixOrFloatMax = fixOrFloatMax;
            FixOrFloatPrimeMin = fixOrFloatPrimeMin;
            FixOrFloatPrimeMax = fixOrFloatPrimeMax;
            FixOrFloatOffPrimeMin = fixOrFloatOffPrimeMin;
            FixOrFloatOffPrimeMax = fixOrFloatOffPrimeMax;
            SuperFixOrFixMin = superFixOrFixMin;
            SuperFixOrFixMax = superFixOrFixMax;
            SuperFixOrFixPrimeMin = superFixOrFixPrimeMin;
            SuperFixOrFixPrimeMax = superFixOrFixPrimeMax;
            SuperFixOrFixOffPrimeMin = superFixOrFixOffPrimeMin;
            SuperFixOrFixOffPrimeMax = superFixOrFixOffPrimeMax;
        }

        public readonly int ID;
        public readonly ChannelVM Channel;
        public readonly double FixOrFloatMin;
        public readonly double FixOrFloatMax;
        public readonly double FixOrFloatPrimeMin;
        public readonly double FixOrFloatPrimeMax;
        public readonly double FixOrFloatOffPrimeMin;
        public readonly double FixOrFloatOffPrimeMax;
        public readonly double SuperFixOrFixMin;
        public readonly double SuperFixOrFixMax;
        public readonly double SuperFixOrFixPrimeMin;
        public readonly double SuperFixOrFixPrimeMax;
        public readonly double SuperFixOrFixOffPrimeMin;
        public readonly double SuperFixOrFixOffPrimeMax;
    }
}
﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Brand;

namespace RMPT.DATA.ViewModels.Models {
    public class BrandVM : AbstractObservable {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="project">Проект</param>
        /// <param name="id">Идентификатор бренда</param>
        /// <param name="name">Наименование бренда</param>
        /// <param name="advertiser">Рекламодатель бренда</param>
        public BrandVM(
            ProjectVM project,
            int id,
            string name,
            DCAdvertiser advertiser
        ) {
            Project = project;

            ID = id;
            Name = name;
            Advertiser = advertiser.ToViewModel(this);
        }

        public readonly int ID;

        public readonly string Name;

        public readonly AdvertiserVM Advertiser;

        public readonly ProjectVM Project;
    }
}
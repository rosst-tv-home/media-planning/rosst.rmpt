﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;

namespace RMPT.DATA.ViewModels.Models {
    public class StrategistVM : AbstractObservable {
        public StrategistVM(ProjectVM project, int id, string firstName, string lastName) {
            Project = project;

            ID = id;
            FirstName = firstName;
            LastName = lastName;
        }

        public readonly ProjectVM Project;
        public readonly int ID;
        public readonly string FirstName;
        public readonly string LastName;
    }
}
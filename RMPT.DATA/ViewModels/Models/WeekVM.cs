﻿using DevExpress.Mvvm.Native;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.ViewModels.Models.Timing;
using RMPT.VALIDATE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RMPT.DATA.ViewModels.Models {
    /// <summary>
    ///     Модель недельного периода проекта
    /// </summary>
    public class WeekVM : AbstractObservable {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="month">Месяц</param>
        /// <param name="dateFrom">Дата начала недельного периода</param>
        /// <param name="dateBefore">Дата окончания недельного периода</param>
        /// <param name="isActive">Признак "Активен"</param>
        /// <param name="activeDays">Количество активных дней</param>
        /// <param name="trp">Целевой рейтинг</param>
        /// <param name="timings">Коллекция хронометражей</param>
        public WeekVM(
            int id,
            MonthVM month,
            DateTime dateFrom,
            DateTime dateBefore,
            int activeDays,
            double trp,
            IEnumerable<PDTiming>? timings = null
        ) {
            // Вызываем метод проверки валидности дат недельного периода
            ValuesValidator.CheckPeriod(dateFrom, dateBefore);

            ID = id;
            Month = month;

            DateFrom = dateFrom;
            DateBefore = dateBefore;
            Days = DateBefore.Day - DateFrom.Day + 1;

            _isActive = ID != 0;
            _activeDays = activeDays;
            _trp = trp;

            // Заполняем коллекцию хронометражей
            timings?.Select(timing => timing.ToViewModel(this)).OrderBy(x => x.Value).ForEach(Timings.Add);
        }

        #endregion

        #region Свойства

        #region Идентификаторы

        /// <summary>
        ///     Идентификатор
        /// </summary>
        public readonly int ID;

        #endregion

        #region Родительские объекты

        /// <summary>
        ///     Месячный период, которому принадлежит этот недельный период
        /// </summary>
        public readonly MonthVM Month;

        #endregion

        #region Неизменяемые свойства

        #region Даты

        /// <summary>
        ///     Дата начала недельного периода
        /// </summary>
        public readonly DateTime DateFrom;

        /// <summary>
        ///     Дата окончания недельного периода
        /// </summary>
        public readonly DateTime DateBefore;

        #endregion

        #region Дни

        /// <summary>
        ///     Количество дней недельного периода
        /// </summary>
        public int Days { get; }

        #endregion

        #region Рейтинги

        #region Целевой рейтинг

        /// <summary>
        ///     Целевой рейтинг, приведённый к базовому хронометражу проекта
        /// </summary>
        public double GivenTRP => Timings.Where(x => x.IsActive).Sum(timing => timing.GivenTRP);

        #endregion

        #region Суммарный рейтинг

        /// <summary>
        ///     Суммарный рейтинг
        /// </summary>
        public double GRP => Timings.Where(x => x.IsActive).Sum(x => x.GRP);

        #endregion

        #region Минуты

        /// <summary>
        ///     Минуты
        /// </summary>
        public double Minutes {
            get {
                if (
                    Month.Channel.Region.IsMeasurable &&
                    Month.RatingPrime    == 0.0       &&
                    Month.RatingOffPrime == 0.0
                ) return 0.0;
                return Timings.Where(x => x.IsActive).Sum(x => x.Minutes);
            }
        }

        #endregion

        #endregion

        #region Средний хронометраж

        /// <summary>
        ///     Средний хронометраж
        /// </summary>
        public double AverageTiming => Timings.Where(x => x.IsActive).Sum(timing => timing.Value * timing.Portion);

        #endregion

        #region Выходы

        public double Outputs => ValuesValidator.NanOrZero(Timings.Sum(t => t.Outputs));

        #endregion

        #endregion

        #region Изменяемые свойства

        #region Признаки

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        public bool _isActive;

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        public bool IsActive {
            get => _isActive;
            set {
                if (!SetProperty(ref _isActive, value, nameof(IsActive))) {
                    return;
                }

                InvokePropertyChangedCore(value);
            }
        }

        #endregion

        #region Дни

        /// <summary>
        ///     Количество активных дней
        /// </summary>
        public int _activeDays;

        /// <summary>
        ///     Количество активных дней
        /// </summary>
        public int ActiveDays {
            get => _activeDays;
            set {
                if (!IsActive) {
                    if (!SetProperty(ref _activeDays, value, nameof(ActiveDays))) {
                        return;
                    }

                    InvokePropertyChangedCore(value);
                    return;
                }

                if (value > Days) {
                    value = Days;
                } else if (value < 0) {
                    value = 0;
                }

                if (!SetProperty(ref _activeDays, value, nameof(ActiveDays))) {
                    return;
                }

                InvokePropertyChangedCore(value);
            }
        }

        #endregion

        #region Рейтинги

        #region Целевой рейтинг

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double _trp;

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        public double TRP {
            get => double.IsNaN(_trp) ? 0.0 : _trp;
            set {
                if (!Month.Channel.Region.IsMeasurable) return;

                // Если родительские объекты неактивны
                if (!IsActive || !Month.Channel.IsActive) {
                    // Завершаем выполнение метода
                    return;
                }

                // Если старое значение равно новому
                if (!SetProperty(ref _trp, value, nameof(TRP))) {
                    // Завершаем выполнение метода
                    return;
                }

                // Вызываем метод обработки этого события
                InvokePropertyChangedCore(value);
            }
        }

        #endregion

        #region Суммарный рейтинг

        /// <summary>
        ///     Суммарный рейтинг, приведённый к базовому хронометражу проекта
        /// </summary>
        public double GivenGRP {
            get => Timings.Where(x => x.IsActive).Sum(timing => timing.GivenGRP);
            set {
                if (!Month.Channel.Region.IsMeasurable) return;

                // Если родительские объекты неактивны
                if (!IsActive || !Month.Channel.IsActive) {
                    // Завершаем выполнение метода
                    return;
                }

                // Если старое значение равно новому
                if (!SetProperty(() => GivenGRP, value)) {
                    // Завершаем выполнение метода
                    return;
                }

                // Устанавливаем новое значение TRP
                TRP = value * Month.Channel.Region.Project.BaseTiming / AverageTiming * Month.Affinity;
            }
        }

        #endregion

        #endregion

        #endregion

        #region Коллекции

        /// <summary>
        ///     Коллекция хронометражей
        /// </summary>
        public ObservableCollection<TimingVM> Timings { get; } = new();

        #endregion

        #endregion

        #region Методы

        public PDWeek ToEntity(PDMonth? month = null) {
            var entity = new PDWeek {
                ID = ID,
                MonthID = Month.ID,
                Month = month,
                DateFrom = DateFrom,
                DateBefore = DateBefore,
                ActiveDays = ActiveDays,
                TRP = TRP
            };

            entity.Timings.AddRange(Timings.Where(t => t.IsActive).Select(t => t.ToEntity(entity)));

            return entity;
        }

        #endregion

        /// <summary>
        ///     Метод сериализации для проверки данных
        /// </summary>
        /// <returns><see cref="string"/>Строка данных</returns>
        public string GetSerializeString() {
            // Строка недели
            var weekStr = $"{Month.ID}{DateFrom}{DateBefore}{_trp}{_activeDays}";
            // Возвращаем строку недели и хронометражи
            return weekStr + string.Join("", this.Timings.OrderBy(t => t.Value).Select(t => t.GetSerializeString()));
        }
    }
}
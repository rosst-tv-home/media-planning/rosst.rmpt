﻿using RMPT.COMMON.Models;
using System;

namespace RMPT.DATA.ViewModels.Models {
    public class RatingVM : AbstractObservable {
        public RatingVM(
            ChannelVM channel,
            int id,
            DateTime date,
            double prime,
            double offPrime
        ) {
            Channel = channel;

            ID = id;
            Date = date;
            Prime = prime;
            OffPrime = offPrime;
        }

        public readonly ChannelVM Channel;
        public readonly int ID;
        public readonly DateTime Date;

        public double Prime {
            get => GetProperty(() => Prime);
            set => SetProperty(() => Prime, value);
        }

        public double OffPrime {
            get => GetProperty(() => OffPrime);
            set => SetProperty(() => OffPrime, value);
        }
    }
}
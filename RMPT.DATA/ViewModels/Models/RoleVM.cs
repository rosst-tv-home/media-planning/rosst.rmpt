﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;

namespace RMPT.DATA.ViewModels.Models {
    public class RoleVM : AbstractObservable {
        /// <summary>
        ///     Констурктор
        /// </summary>
        /// <param name="id">Идентификатор роли</param>
        /// <param name="name">Наименование роли</param>
        /// <param name="user">Модель представления пользователя</param>
        public RoleVM(int id, string name, UserVM user) {
            ID = id;
            Name = name;
            User = user;
        }

        public int ID {
            get => GetProperty(() => ID);
            private set => SetProperty(() => ID, value);
        }

        public string Name {
            get => GetProperty(() => Name);
            private set => SetProperty(() => Name, value);
        }

        public UserVM User {
            get => GetProperty(() => User);
            private set => SetProperty(() => User, value);
        }
    }
}
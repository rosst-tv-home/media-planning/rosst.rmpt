﻿using RMPT.CORE;
using RMPT.DATA.ViewModels.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.ViewModels.Handlers {
    /// <summary>
    ///     Обработчик событий изменения значения свойств <see cref="ChannelVM" />
    /// </summary>
    public static class ChannelEventHandler {
        /// <summary>
        ///     Метод обработки события изменения значения свойства <see cref="ChannelVM" />
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        public static async void OnPropertyChanged(object sender, EventArgument argument) {
            // Если источник события невалидного типа - выбрасываем исключение
            if (sender is not ChannelVM channel) {
                throw new InvalidOperationException(
                    $"Источник события [{nameof(OnPropertyChanged)}] " +
                    $"должен являться [{nameof(ChannelVM)}] типом"
                );
            }

            // Определяем тип доставки события
            switch (argument.EventType) {
                // Если событие было вызванно из интерфейса
                case EventType.Ui: {
                    // Определяем свойство, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменился признак активности
                        case nameof(ChannelVM.IsActive): {
                            // Вызываем метод обработки этого события
                            await OnIsActiveChangedAsyncUI(channel, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                        // Если изменилась доля
                        case nameof(ChannelVM.Portion): {
                            // Вызываем метод обработки этого события
                            await OnPortionChangedAsyncUI(channel, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                        // Если изменилось значение целевого рейтинга
                        case nameof(ChannelVM.TRP): {
                            // Вызываем метод обработки этого события
                            await OnTRPChangedAsyncUI(channel, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если событие вызвано из другого события и предназначено для родительских элементов
                case EventType.Bubbling: {
                    // Определяем свойство, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение целевого рейтинга
                        case nameof(ChannelVM.TRP): {
                            // Вызываем метод обработки этого события
                            await OnTRPChangedAsyncBubbling(channel, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    return;
                }
                // Если событие нужно доставить до дочерних элементов
                case EventType.Tunneling: {
                    // Определяем наименование свойства, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение целевого рейтинга
                        case nameof(ChannelVM.TRP): {
                            // Вызываем метод обработки этого события
                            await OnTRPChangedAsyncTunneling(channel, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                        // Если изменилось значение доли качества
                        case nameof(ChannelVM.FixOrFloatPortion):
                        case nameof(ChannelVM.FixOrFloatPrimePortion):
                        case nameof(ChannelVM.SuperFixOrFixPortion):
                        case nameof(ChannelVM.SuperFixOrFixPrimePortion): {
                            // Вызываем метод обработки этого события
                            OnQualityPortionChanged(channel, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если не удалось определить тип доставки события - выбрасываем исключение
                default: {
                    throw new ArgumentOutOfRangeException(
                        nameof(argument.EventType),
                        "Не удалось определить тип доставки события"
                    );
                }
            }
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="ChannelVM" />.<see cref="ChannelVM.IsActive" />,
        ///     вызванного из интерфейса. В данном методе обнуляется
        ///     <see cref="ChannelVM" />.<see cref="ChannelVM.Portion" /> и
        ///     <see cref="ChannelVM" />.<see cref="ChannelVM.TRP" /> в случае если
        ///     <see cref="ChannelVM" />.<see cref="ChannelVM.IsActive" /> равно <see cref="bool.False" />
        /// </summary>
        /// <param name="channel">Канал</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static Task OnIsActiveChangedAsyncUI(ChannelVM channel, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not bool value) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(bool)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(ChannelVM)}]",
                    nameof(argument.Value)
                );
            }

            // Если активация - завершаем выполнение метода
            if (value) {
                channel.Region.RecalculateChannelsPortions();
                return Task.CompletedTask;
            }
            
            // Если неизмеряемый регион
            if (!channel.Region.IsMeasurable) {
                foreach (var month in channel.Months) {
                    month._outputsPerDay = 0.0;
                    month.NotifyUI(nameof(month.OutputsPerDay));
                }
                
                channel.Region.InvokePropertyChangedCore(
                    channel.Region.Outputs,
                    EventType.Ui,
                    nameof(RegionVM.Outputs)
                );

                return Task.CompletedTask;
            }

            // Обнуляем долю канала
            channel._portion = 0;
            // Оповещаем интерфейс об изменении значения
            channel.NotifyUI(nameof(ChannelVM.Portion));

            // Обновляем целевой рейтинг канала
            channel._trp = 0;
            // Оповещаем интерфейс об измении значения
            channel.NotifyUI(nameof(ChannelVM.TRP));

            // Вызываем метод перераспределения целевого рейтинга канала по активным месяцам
            OnTRPChangedAsyncTunneling(channel, new EventArgument(
                channel.TRP,
                EventType.Tunneling,
                nameof(ChannelVM.TRP)
            ));
            
            channel.Region.RecalculateChannelsPortions();

            // Завершаем выполнение метода
            return Task.CompletedTask;
        }

        #region Целевой рейтинг

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="ChannelVM" />.<see cref="ChannelVM.TRP" />,
        ///     вызванного из интерфейса. В данном методе вызываются методы
        ///     <see cref="ChannelEventHandler" />.<see cref="OnTRPChangedAsyncBubbling" /> и
        ///     <see cref="ChannelEventHandler" />.<see cref="OnTRPChangedAsyncTunneling" />
        /// </summary>
        /// <param name="channel">Канал</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Задача</returns>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static async Task OnTRPChangedAsyncUI(ChannelVM channel, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(ChannelVM)}]",
                    nameof(argument.Value)
                );
            }

            // Вызываем метод обработки этого события
            await OnTRPChangedAsyncBubbling(channel, argument);
            // Вызываем метод обработки этого события
            await OnTRPChangedAsyncTunneling(channel, argument);
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="ChannelVM" />.<see cref="ChannelVM.TRP" />,
        ///     вызванного из другого события. В данном методе пересчитывается новое значение
        ///     <see cref="RegionVM" />.<see cref="RegionVM.TRP" /> и вызывается событие
        ///     <see cref="RegionEventHandler" />.<see cref="RegionEventHandler.OnTRPChangedAsyncBubbling" />
        /// </summary>
        /// <param name="channel">Канал</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Задача</returns>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static Task OnTRPChangedAsyncBubbling(ChannelVM channel, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(ChannelVM)}]",
                    nameof(argument.Value)
                );
            }

            // Устанавливаем новое значение целевого рейтинга региона
            channel.Region._trp = channel.Region.Channels.Where(x => x.IsActive).Sum(x => x.TRP);
            // Оповещаем интерфейс об изменении значения
            channel.Region.NotifyUI(nameof(RegionVM.TRP));
            // Вызываем метод обработки этого события
            channel.Region.InvokePropertyChangedCore(channel.Region.TRP, EventType.Bubbling, nameof(RegionVM.TRP));

            // Завершаем выполнение метода
            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="ChannelVM" />.<see cref="ChannelVM.TRP" />,
        ///     вызванного из другого события. В данном методе новое значение
        ///     <see cref="ChannelVM" />.<see cref="ChannelVM.TRP" /> перераспределяется по
        ///     <see cref="ChannelVM" />.<see cref="ChannelVM.Months" />
        /// </summary>
        /// <param name="channel">Канал</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Задача</returns>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static Task OnTRPChangedAsyncTunneling(ChannelVM channel, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double value) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(ChannelVM)}]",
                    nameof(argument.Value)
                );
            }

            foreach (var month in channel.Months) {
                if (!month.IsActive) continue;
                // Устанавливаем новое значение целевого рейтинга месяца
                month._trp = value * (month.ActiveDays / (double)month.Channel.ActiveDays);
                // Оповещаем интерфейс об изменении значения
                month.NotifyUI(nameof(MonthVM.TRP));
                // Вызываем метод обработки события изменения значения целевого рейтинга месяца
                month.InvokePropertyChangedCore(month.TRP, EventType.Tunneling, nameof(MonthVM.TRP));
            }

            // Завершаем выполнение метода
            return Task.CompletedTask;
        }

        #endregion

        #region Доли

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="ChannelVM" />.<see cref="ChannelVM.Portion" />,
        ///     вызыванного из интерфейса. В данном методе вызывается событие
        ///     <see cref="RegionEventHandler" />.<see cref="RegionEventHandler.OnTRPChangedAsyncUI" />
        /// </summary>
        /// <param name="channel">Канал</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Задача</returns>
        /// <exception cref="ArgumentException">Аргумент события</exception>
        private static Task OnPortionChangedAsyncUI(ChannelVM channel, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(ChannelVM)}]",
                    nameof(argument.Value)
                );
            }

            // Вызываем метод обработки события перераспределения целевого рейтинга региона по активным каналам
            channel.Region.InvokePropertyChangedCore(
                channel.Region.TRP,
                EventType.Ui,
                nameof(RegionVM.TRP)
            );

            // Завершаем выполнение метода
            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки события изменения значения доли качества
        /// </summary>
        /// <param name="channel">Канал</param>
        /// <param name="argument">Аргумент события</param>
        private static void OnQualityPortionChanged(ChannelVM channel, EventArgument argument) {
            // Если значение аргумента события не является числом с плавающей точкой - выбрасываем исключение
            if (argument.Value is not double value) {
                throw new ArgumentException(
                    "Значение аргумента события не является числом с плавающей точкой, " +
                    "что недопустимо для целевого рейтинга канала",
                    nameof(argument.Value)
                );
            }


            // Устанавливаем такуюже долю качества по активным месяцам канала
            Parallel.ForEach(channel.Months.Where(x => x.IsActive), month => month
               .GetType()
               .GetProperty(argument.PropertyName)
              ?.SetValue(month, value)
            );
        }

        #endregion
    }
}
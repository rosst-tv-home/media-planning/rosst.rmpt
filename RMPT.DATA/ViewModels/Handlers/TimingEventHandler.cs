﻿using RMPT.CORE;
using RMPT.DATA.ViewModels.Models;
using RMPT.DATA.ViewModels.Models.Timing;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.ViewModels.Handlers {
    /// <summary>
    ///     Обработчик событий изменения значения свойств <see cref="TimingVM" />
    /// </summary>
    public static class TimingEventHandler {
        /// <summary>
        ///     Метод обработки события изменения значения свойства <see cref="TimingVM" />
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        public static async void OnPropertyChanged(object sender, EventArgument argument) {
            // Если источник события невалидного типа - выбрасываем исключение
            if (sender is not TimingVM timing) {
                throw new InvalidOperationException(
                    $"Источник события [{nameof(OnPropertyChanged)}] " +
                    $"должен являться [{nameof(TimingVM)}] типом"
                );
            }

            // Определяем тип доставки события
            switch (argument.EventType) {
                // Если событие было вызвано из интерфейса
                case EventType.Ui: {
                    // Определяем свойство, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение признака активности
                        case nameof(TimingVM.IsActive): {
                            // Вызываем метод обработки этого события
                            await OnActiveChangedAsyncUI(timing, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                        // Если изменилось значение доли
                        case nameof(TimingVM.Portion): {
                            // Вызываем метод обработки этого события
                            await OnPortionChangedAsyncUI(timing, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если не удалось определить тип доставки события - выбрасываем исключение
                default: {
                    throw new ArgumentOutOfRangeException(
                        nameof(argument.EventType),
                        "Не удалось определить тип доставки события"
                    );
                }
            }
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="TimingVM" />.<see cref="TimingVM.Portion" />,
        ///     вызванног из интерфейса. В данном методе новое значение значение
        ///     <see cref="WeekVM" />.<see cref="WeekVM.TRP" /> перераспределяется по
        ///     <see cref="WeekVM" />.<see cref="WeekVM.Timings" />
        /// </summary>
        /// <param name="timing">Хронометраж</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        private static Task OnPortionChangedAsyncUI(TimingVM timing, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(TimingVM)}]",
                    nameof(argument.Value)
                );
            }

            // Вызываем метод перераспределения целевого рейтинга недельного периода по активным хронометражам
            timing.Week.InvokePropertyChangedCore(
                timing.Week.TRP,
                EventType.Tunneling,
                nameof(WeekVM.TRP)
            );

            // Завершаем выполнение метода
            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="TimingVM.IsActive" />, вызванного из интерфейса
        /// </summary>
        /// <param name="timing">Хронометраж</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        private static Task OnActiveChangedAsyncUI(TimingVM timing, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not bool value) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(bool)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(TimingVM)}]",
                    nameof(argument.Value)
                );
            }

            // Если активация
            if (value) {
                // Если это не единственный хронометраж - завершаем выполнение метода
                if (timing.Week.Timings.Count(x => x.IsActive) > 1) {
                    return Task.CompletedTask;
                }

                // Иначе устанавливаем долю хронометража равной 100%
                timing._portion = 1.0;
                // Оповещаем интерфейс об изменении значения
                timing.NotifyUI(nameof(TimingVM.Portion));

                // Если недельный период хронометража неактивен,
                // или количество активных дней равно 0,
                // или целевой рейтинг равен 0 - завершаем выполнение метода
                if (
                    !timing.Week.IsActive       ||
                    timing.Week.ActiveDays == 0 ||
                    timing.Week.TRP        == 0
                ) {
                    return Task.CompletedTask;
                }

                // Иначе вызываем метод перераспределения целевого рейтинга недельного периода по хронометражам
                timing.Week.InvokePropertyChangedCore(
                    timing.Week.TRP,
                    EventType.Tunneling,
                    nameof(WeekVM.TRP)
                );

                // Завершаем выполнение метода
                return Task.CompletedTask;
            }

            // Если после деактивации этого хронометража в недельном периоде остался один активный хронометраж
            if (timing.Week.Timings.Count(x => x.IsActive) == 1) {
                // Устанавливаем долю оставшегося активного хронометража равной 100%
                timing.Week.Timings.First(x => x.IsActive)._portion = 1.0;
                // Оповещаем интерфейс об изменении значения
                timing.NotifyUI(nameof(TimingVM.Portion));
            }

            // Обнуляем долю хронометража
            timing._portion = 0;
            // Оповещаем интерфейс об изменении значения
            timing.NotifyUI(nameof(TimingVM.Portion));

            // // Обнуляем целевой рейтинг хронометража
            // timing._trp = 0;
            // // Оповещаем интерфейс об изменении значения
            // timing.InvokeNotifyUI(nameof(TimingVM.TRP));

            // Если нет активных хронометражей в недельном периоде - завершаем выполнение метода
            if (timing.Week.Timings.Count(x => x.IsActive) == 0) {
                return Task.CompletedTask;
            }

            // Вызываем метод перераспределения целевого рейтинга недельного периода по хронометражам
            timing.Week.InvokePropertyChangedCore(
                timing.Week.TRP,
                EventType.Tunneling,
                nameof(WeekVM.TRP)
            );

            return Task.CompletedTask;
        }
    }
}
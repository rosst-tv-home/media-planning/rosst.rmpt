﻿using RMPT.CORE;
using RMPT.DATA.ViewModels.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.ViewModels.Handlers {
    /// <summary>
    ///     Обработчик событий изменения значения свойств <see cref="MonthVM" />
    /// </summary>
    public static class MonthEventHandler {
        /// <summary>
        ///     Метод обработки события изменения значения свойства <see cref="MonthVM" />
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        public static async void OnPropertyChanged(object sender, EventArgument argument) {
            // Если источник события невалидного типа - выбрасываем исключение
            if (sender is not MonthVM month) {
                throw new InvalidOperationException(
                    $"Источник события [{nameof(OnPropertyChanged)}] " +
                    $"должен являться [{nameof(MonthVM)}] типом"
                );
            }

            // Определяем тип доставки события
            switch (argument.EventType) {
                // Если событие было вызвано из интерфейса
                case EventType.Ui: {
                    // Определяем свойство, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение признака активности
                        case nameof(MonthVM.IsActive): {
                            // Вызываем метод обработки этого события
                            await OnActiveChangedAsyncUI(month, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                        // Если изменилось значение целевого рейтинга
                        case nameof(MonthVM.TRP): {
                            // Вызываем метод обработки этого события
                            await OnTRPChangedAsyncUI(month, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                        // Если изменилось значение аффинитивности
                        case nameof(MonthVM.Affinity): {
                            // Вызываем метод обработки этого события
                            throw new NotImplementedException();
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если событие нужно доставить до родительских элементов
                case EventType.Bubbling: {
                    // Определяем наименование свойства, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение целевого рейтинга
                        case nameof(MonthVM.TRP): {
                            // Вызываем метод обработки этого события
                            await OnTRPChangedAsyncBubbling(month, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если событие нужно доставить до дочерних элементов
                case EventType.Tunneling: {
                    // Определяем наименование свойства, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение целевого рейтинга
                        case nameof(MonthVM.TRP): {
                            // Вызываем метод обработки этого события
                            await OnTRPChangedAsyncTunneling(month, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если не удалось определить тип доставки события - выбрасываем исключение
                default: {
                    throw new ArgumentOutOfRangeException(
                        nameof(argument.EventType),
                        "Не удалось определить тип доставки события"
                    );
                }
            }
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="MonthVM.IsActive" />, вызванного из интерфейса
        /// </summary>
        /// <param name="month">Месяц</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        private static Task OnActiveChangedAsyncUI(MonthVM month, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not bool value) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(bool)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(MonthVM)}]",
                    nameof(argument.Value)
                );
            }

            // Если активация - завершаем выполнение метода
            if (value) {
                return Task.CompletedTask;
            }

            // Обнуляем значение целевого рейтинга
            month._trp = 0;
            // Оповещаем интерфейс об изменении значения
            month.NotifyUI(nameof(MonthVM.TRP));

            // Вызываем метод перераспределения целевого рейтинга канала по активным месячным периодам
            month.Channel.InvokePropertyChangedCore(
                month.Channel.TRP,
                EventType.Tunneling,
                nameof(ChannelVM.TRP)
            );

            return Task.CompletedTask;
        }

        #region Целевой рейтинг

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="MonthVM" />.<see cref="MonthVM.TRP" />, вызванного
        ///     из интерфейса. В данном методе вызываются методы
        ///     <see cref="MonthEventHandler" />.<see cref="OnTRPChangedAsyncBubbling" /> и
        ///     <see cref="MonthEventHandler" />.<see cref="OnTRPChangedAsyncTunneling" />
        /// </summary>
        /// <param name="month">Месячный период</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Задача</returns>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static async Task OnTRPChangedAsyncUI(MonthVM month, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(MonthVM)}]",
                    nameof(argument.Value)
                );
            }

            // Вызываем метод обработки этого события
            await OnTRPChangedAsyncBubbling(month, argument);
            // Вызываем метод обработки этого события
            await OnTRPChangedAsyncTunneling(month, argument);
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="MonthVM" />.<see cref="MonthVM.TRP" />, вызванного
        ///     из другого события. В данном методе пересчитывается новое значение
        ///     <see cref="ChannelVM" />.<see cref="ChannelVM.TRP" /> и вызывается событие
        ///     <see cref="ChannelEventHandler" />.<see cref="ChannelEventHandler.OnTRPChangedAsyncBubbling" />
        /// </summary>
        /// <param name="month">Месячный период</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Задача</returns>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static Task OnTRPChangedAsyncBubbling(MonthVM month, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(MonthVM)}]",
                    nameof(argument.Value)
                );
            }

            // Устанавливаем новое значение целевого рейтинга канала
            month.Channel._trp = month.Channel.Months.Where(x => x.IsActive).Sum(x => x.TRP);
            // Оповещаем интерфейс об изменении значения
            month.Channel.NotifyUI(nameof(ChannelVM.TRP));
            // Вызываем метод обработки этого события
            month.Channel.InvokePropertyChangedCore(month.Channel.TRP, EventType.Bubbling, nameof(ChannelVM.TRP));

            // Завершаем выполнение метода
            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="MonthVM" />.<see cref="MonthVM.TRP" />, вызванного
        ///     из другого события. В данном методе новое значение <see cref="MonthVM" />.<see cref="MonthVM.TRP" />
        ///     перераспределяется по <see cref="MonthVM" />.<see cref="MonthVM.Weeks" />
        /// </summary>
        /// <param name="month">Месячный период</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Задача</returns>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static Task OnTRPChangedAsyncTunneling(MonthVM month, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double value) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(MonthVM)}]",
                    nameof(argument.Value)
                );
            }

            foreach (var week in month.Weeks) {
                // Устанавливаем новое значение целевого рейтинга недели
                week._trp = value * (week.ActiveDays / (double)month.ActiveDays);
                // Оповещаем интерфейс об изменении значения
                week.NotifyUI(nameof(WeekVM.TRP));
                // Вызываем метод обработки события изменения значения целевого рейтинга недели
                week.InvokePropertyChangedCore(week.TRP, EventType.Tunneling, nameof(WeekVM.TRP));
            }

            // Завершаем выполнение метода
            return Task.CompletedTask;
        }

        #endregion
    }
}
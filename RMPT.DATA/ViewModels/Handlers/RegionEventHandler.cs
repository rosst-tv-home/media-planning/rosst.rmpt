﻿using RMPT.CORE;
using RMPT.DATA.ViewModels.Models;
using RMPT.VALIDATE;
using System;
using System.Threading.Tasks;

namespace RMPT.DATA.ViewModels.Handlers {
    /// <summary>
    ///     Обработчик событий изменения значения свойств <see cref="RegionVM" />
    /// </summary>
    public static class RegionEventHandler {
        /// <summary>
        ///     Метод обработки события изменения значения свойства <see cref="RegionVM" />
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        /// <exception cref="ArgumentException">Ошибка невалидного типа источника события</exception>
        /// <exception cref="ArgumentOutOfRangeException">Ошибка невалидного типа доставки события</exception>
        public static async void OnPropertyChanged(object sender, EventArgument argument) {
            // Если источник события невалидного типа - выбрасываем исключение
            if (sender is not RegionVM region) {
                throw new ArgumentException(
                    $"Источник события [{nameof(OnPropertyChanged)}] " +
                    $"должен являться [{nameof(RegionVM)}] типом",
                    nameof(sender)
                );
            }

            // Определяем тип доставки события
            switch (argument.EventType) {
                // Если событие вызвано из интерфейса
                case EventType.Ui: {
                    // Определяем свойство, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение целевого рейтинга
                        case nameof(RegionVM.TRP): {
                            // Вызываем метод обработки этого события
                            await OnTRPChangedAsyncUI(region, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                        // Если изменилось значение выходов
                        case nameof(RegionVM.Outputs): {
                            // Вызываем метод обработки этого события
                            await OnOutputsChangedAsyncUI(region, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если событие вызвано из другого события и предназначено для родительских элементов
                case EventType.Bubbling: {
                    // Определяем свойство, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение целевого рейтинга
                        case nameof(RegionVM.TRP): {
                            // Вызываем метод обработки этого события
                            await OnTRPChangedAsyncBubbling(region, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если не удалось определить тип доставки события - выбрасываем исключение
                default: {
                    throw new ArgumentOutOfRangeException(
                        nameof(argument.EventType),
                        "Не удалось определить тип доставки события"
                    );
                }
            }
        }

        #region Целевой рейтинг

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="RegionVM" />.<see cref="RegionVM.TRP" />,
        ///     вызванног из интерфейса. В данном методе новое значение
        ///     <see cref="RegionVM" />.<see cref="RegionVM.TRP" /> перераспределяется по
        ///     <see cref="RegionVM" />.<see cref="RegionVM.Channels" />
        /// </summary>
        /// <param name="region">Регион</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Задача</returns>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static Task OnTRPChangedAsyncUI(RegionVM region, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(RegionVM)}]",
                    nameof(argument.Value)
                );
            }

            foreach (var channel in region.Channels) {
                // Если канал неактивен - пропускаем
                if (!channel.IsActive) continue;

                // Расчёт целевого рейтинга канала от его доли
                channel._trp = region.TRP * channel.Portion;

                // Оповещаем интерфейс об изменении значения
                channel.NotifyUI(nameof(ChannelVM.TRP));
            }

            foreach (var channel in region.Channels) {
                // Если канал неактивен - пропускаем
                if (!channel.IsActive) continue;
                
                // Генерируем событие дальнейшего распределения целевого рейтинга
                channel.InvokePropertyChangedCore(channel.TRP, EventType.Tunneling, nameof(ChannelVM.TRP));
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="RegionVM" />.<see cref="RegionVM.TRP" />,
        ///     вызванного из другого события. В данном методе пересчитывается значение
        ///     <see cref="ChannelVM" />.<see cref="ChannelVM.Portion" /> в
        ///     <see cref="RegionVM" />.<see cref="RegionVM.Channels" />
        /// </summary>
        /// <param name="region">Регион</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Задача</returns>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static Task OnTRPChangedAsyncBubbling(RegionVM region, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(WeekVM)}]",
                    nameof(argument.Value)
                );
            }

            foreach (var channel in region.Channels) {
                // Если канала неактивен - пропускаем
                if (!channel.IsActive) continue;

                // Расчёт доли канала от целевого рейтинга
                channel._portion = channel.TRP / region.TRP;

                // Оповещаем интерфейс об изменении значения
                channel.NotifyUI(nameof(ChannelVM.Portion));
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="RegionVM"/>.<see cref="RegionVM.Outputs"/>,
        ///     вызванного из другого события. В данном методе пересчитывается значение
        ///     <see cref="ChannelVM"/>.<see cref="ChannelVM.Portion"/> в неизмеряемом регионе
        /// </summary>
        /// <param name="region">Регион</param>
        /// <param name="argument">Аргумент события</param>
        /// <exception cref="ArgumentException">Ошибка невалидного типа значения аргумента</exception>
        private static Task OnOutputsChangedAsyncUI(RegionVM region, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(WeekVM)}]",
                    nameof(argument.Value)
                );
            }

            // Если регион измеряемый - завершаем выполнение метода
            if (region.IsMeasurable) return Task.CompletedTask;
            
            foreach (var channel in region.Channels) {
                // Расчёт доли канала от выходов
                channel._portion = ValuesValidator.NanOrZero(channel.Outputs / region.Outputs);
                // Оповещаем интерфейс об изменении значения
                channel.NotifyUI(nameof(ChannelVM.Portion));
            }

            return Task.CompletedTask;
        }

        #endregion
    }
}
﻿using RMPT.CORE;
using RMPT.DATA.ViewModels.Models;
using RMPT.DATA.ViewModels.Models.Timing;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.ViewModels.Handlers {
    /// <summary>
    ///     Обработчик событий изменения значения свойств <see cref="WeekVM" />
    /// </summary>
    public class WeekEventHandler {
        /// <summary>
        ///     Метод обработки события изменения значения свойства <see cref="WeekVM" />
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        public static async void OnPropertyChanged(object sender, EventArgument argument) {
            // Если источник события невалидного типа - выбрасываем исключение
            if (sender is not WeekVM week) {
                throw new InvalidOperationException(
                    $"Источник события [{nameof(OnPropertyChanged)}] " +
                    $"должен являться [{nameof(TimingVM)}] типом"
                );
            }

            // Определяем тип доставки события
            switch (argument.EventType) {
                // Если событие было вызвано из интерфейса
                case EventType.Ui: {
                    // Определяем наименование свойства, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение признака активности
                        case nameof(WeekVM.IsActive): {
                            // Вызываем метод обработки этого события
                            await OnActiveChangedAsyncUI(week, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                        // Если изменилось значение активных дней
                        case nameof(WeekVM.ActiveDays): {
                            // Вызываем метод обработки этого события
                            await OnActiveDaysChangedAsyncUI(week, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                        // Если изменилось значение целевого рейтинга
                        case nameof(WeekVM.TRP): {
                            // Вызываем метод обработки этого события
                            await OnTRPChangedAsyncUI(week, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если событие нужно доставить до дочерних элементов
                case EventType.Tunneling: {
                    // Определяем наименование свойства, значение которого изменилось
                    switch (argument.PropertyName) {
                        // Если изменилось значение целевого рейтинга
                        case nameof(WeekVM.TRP): {
                            // Вызываем метод обработки этого события
                            // await OnTRPChangedAsyncTunneling(week, argument);
                            // Завершаем выполнение метода
                            return;
                        }
                    }

                    // Завершаем выполнение метода
                    return;
                }
                // Если не удалось определить тип доставки события - выбрасываем исключение
                default: {
                    throw new ArgumentOutOfRangeException(
                        nameof(argument.EventType),
                        "Не удалось определить тип доставки события"
                    );
                }
            }
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="WeekVM.IsActive" />, вызванного из интерфейса
        /// </summary>
        /// <param name="week">Неделя</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        private static Task OnActiveChangedAsyncUI(WeekVM week, EventArgument argument) {
            if (argument.Value is not bool) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(bool)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(WeekVM)}]",
                    nameof(argument.Value)
                );
            }

            week.Month.IsActive = week.IsActive switch {
                true when !week.Month.IsActive => true,
                false when week.Month.Weeks.Count(x => x.IsActive) == 0 => false,
                _ => week.Month.IsActive
            };

            week.ActiveDays = week.IsActive switch {
                true => week.Days,
                false => 0
            };
            
            // Если деактивация - деактивируем хронометражи
            if (!week.IsActive) {
                foreach (var timing in week.Timings) {
                    timing.IsActive = false;
                }

                return Task.CompletedTask;
            }
            
            // Определяем активные хронометражи
            var activeTimings = week.Month.Channel.Timings.Where(t => t.IsActive).Select(t => t.Value).ToHashSet();
            // Активируем хронометражи
            foreach (var timing in week.Timings) {
                if (!activeTimings.Contains(timing.Value)) continue;
                timing.IsActive = true;
            }
            

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="WeekVM.ActiveDays" />, вызванного из интерфейса
        /// </summary>
        /// <param name="week">Неделя</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        private static Task OnActiveDaysChangedAsyncUI(WeekVM week, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not int) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(int)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(WeekVM)}]",
                    nameof(argument.Value)
                );
            }
            
            // Если измеряемый регион
            if (week.Month.Channel.Region.IsMeasurable) {
                // Вызываем метод перераспределения месячного целевого рейтинга по недельным периодам
                week.Month.InvokePropertyChangedCore(
                    week.Month.TRP,
                    EventType.Tunneling,
                    nameof(MonthVM.TRP)
                );
            } else {
                // Вызываем метод перераспределения выходов региона по каналам (пересчёт долей)
                week.Month.Channel.Region.InvokePropertyChangedCore(
                    week.Month.Channel.Region.Outputs,
                    EventType.Ui,
                    nameof(RegionVM.Outputs)
                );
            }


            return Task.CompletedTask;
        }

        #region Целевой рейтинг

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="WeekVM.TRP" />, вызванного из интерфейса, т.е.
        ///     событие происходит, когда пользователь вводит значение напрямую в свойство через элемент интерфейса.
        ///     В таком случае целевой рейтинг недельного периода должен перераспределиться по активным хронометражам
        ///     этого недельного периода, а также должна пересчитаться сумма целевого рейтинга в месяце, канале
        ///     и регионе. При этом равномерное распределение нарушается, что нормально. Также доля канала в регионе
        ///     должна пересчитаться
        /// </summary>
        /// <param name="week">Неделя</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        private static async Task OnTRPChangedAsyncUI(WeekVM week, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(WeekVM)}]",
                    nameof(argument.Value)
                );
            }

            // Вызываем метод обработки этого события
            await OnTRPChangedAsyncBubbling(week, argument);
            // Вызываем метод распределения целевого рейтинга по активным хронометражам
            // await OnTRPChangedAsyncTunneling(week, argument);
        }

        /// <summary>
        ///     Метод обработки события изменения значения <see cref="WeekVM" />.<see cref="WeekVM.TRP" />,
        ///     вызванного из другого события. В данном методе рассчитывается новое значение
        ///     <see cref="MonthVM" />.<see cref="MonthVM.TRP" /> и вызывается событие
        ///     <see cref="MonthEventHandler" />.<see cref="MonthEventHandler.OnTRPChangedAsyncBubbling" />
        /// </summary>
        /// <param name="week">Недельный период</param>
        /// <param name="argument">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        private static Task OnTRPChangedAsyncBubbling(WeekVM week, EventArgument argument) {
            // Если значение аргумента события невалидного типа - выбрасываем исключение
            if (argument.Value is not double) {
                throw new ArgumentException(
                    $"Значение [{argument.Value}] аргумента события не является [{typeof(double)}], " +
                    $"что недопустимо для свойства [{argument.PropertyName}] объекта типа [{nameof(WeekVM)}]",
                    nameof(argument.Value)
                );
            }

            // Устанавливаем новое значение целевого рейтинга месяца
            week.Month._trp = week.Month.Weeks.Where(x => x.IsActive).Sum(x => x.TRP);
            // Оповещаем интерфейс об изменении значения
            week.Month.NotifyUI(nameof(MonthVM.TRP));
            // Вызываем метод обработки этого события
            week.Month.InvokePropertyChangedCore(week.Month.TRP, EventType.Bubbling, nameof(MonthVM.TRP));

            // Завершаем выполнение метода
            return Task.CompletedTask;
        }

        #endregion
    }
}
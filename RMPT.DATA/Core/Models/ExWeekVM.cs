﻿using RMPT.DATA.Core.Wrapper;
using System;

namespace RMPT.DATA.Core.Models {
    public class ExWeekVM {
        public ExWeekVM(ExMonthVM month, DateTime dateFrom, DateTime dateBefore) {
            this.Month = month;

            this.DateFrom = dateFrom;
            this.DateBefore = dateBefore;
            this.Days = DateBefore.Day - DateFrom.Day + 1;
        }

        #region Даты

        public DateTime DateFrom { get; }
        public DateTime DateBefore { get; }

        #endregion

        #region Признак "Активна"

        protected internal bool _isActive = true;

        public bool IsActive {
            get => _isActive;
            set {
                if (_isActive == value) return;
                WrapperWeek.IsActive(this, value);
            }
        }

        #endregion

        #region Дни

        public int Days { get; }

        #endregion

        #region Активные дни

        protected internal int _activeDays;

        public int ActiveDays {
            get => _activeDays;
            set {
                if (_activeDays == value) return;
                WrapperWeek.ActiveDays(this, value);
            }
        }

        #endregion

        #region Инвентарь

        #region Целевой рейтинг

        protected internal double _trp;

        public double TRP {
            get => _trp;
            set {
                if (Math.Abs(_trp - value) < 0.0) return;
                WrapperWeek.SpreadTrp(this, value);
            }
        }

        #endregion

        #endregion

        #region Активные хронометражи

        public int ActiveTimingsCount { get; set; }

        #endregion

        #region Ссылки

        public ExMonthVM Month { get; }
        public ExTimingVM[] Timings { get; } = new ExTimingVM[12];

        #endregion
    }
}
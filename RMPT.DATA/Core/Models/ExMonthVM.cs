﻿using RMPT.DATA.Core.Wrapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RMPT.DATA.Core.Models {
    public class ExMonthVM : ExAbstractVM {
        public ExMonthVM(ExChannelVM channel, DateTime date) {
            this.Channel = channel;

            this.Date = date;
        }

        #region Дата

        public DateTime Date;

        #endregion

        #region Признак "Активен"

        protected internal bool _isActive = true;

        public bool IsActive {
            get => _isActive;
            set {
                if (_isActive == value) return;
                WrapperMonth.IsActive(this, value);
            }
        }

        #endregion

        #region Дни

        public int Days { get; set; }

        #endregion

        #region Активные дни

        public int ActiveDays { get; set; }

        #endregion

        #region Инвентарь

        #region Целевой рейтинг

        protected internal double _trp;

        public double TRP {
            get => _trp;
            set {
                if (Math.Abs(_trp - value) < 0.0) return;
                WrapperMonth.SpreadTrp(this, value);
            }
        }

        #endregion

        #endregion

        #region Активные недели

        public int ActiveWeeksCount { get; set; }

        #endregion

        #region Ссылки

        public ExChannelVM Channel { get; }

        public Collection<ExWeekVM> Weeks { get; } = new();

        #endregion

        #region Методы

        public void AddWeeks(IEnumerable<ExWeekVM> weeks) {
            foreach (var week in weeks) {
                Weeks.Add(week);
            }

            WrapperMonth.ComputeTotalDays(this);
            WrapperMonth.ComputeActiveDays(this);
        }

        #endregion
    }
}
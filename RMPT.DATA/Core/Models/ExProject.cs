﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RMPT.DATA.Core.Models {
    public class ExProject {
        #region Ссылки

        public ICollection<ExRegionVM> Regions { get; } = new Collection<ExRegionVM>();

        #endregion
    }
}
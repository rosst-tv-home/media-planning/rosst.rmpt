﻿using RMPT.DATA.Core.Wrapper;
using System;

namespace RMPT.DATA.Core.Models {
    public class ExChannelVM {
        public ExChannelVM(ExRegionVM region, string name) {
            this.Region = region;

            this.Name = name;
        }

        #region Название

        public string Name { get; }

        #endregion

        #region Признак "Активен"

        internal protected bool _isActive = true;

        public bool IsActive {
            get => _isActive;
        }

        #endregion

        #region Активные дни

        public int ActiveDays { get; set; }

        #endregion

        #region Доля

        protected internal double _portion;

        public double Portion {
            get => _portion;
            set => _portion = value;
        }

        #endregion

        #region Инвентарь

        #region Целевой рейтинг

        protected internal double _trp;

        public double TRP {
            get => _trp;
            set {
                if (Math.Abs(_trp - value) < 0.0) return;
                WrapperChannel.SpreadTrp(this, value);
            }
        }

        #endregion

        #endregion

        #region Ссылки

        public ExRegionVM Region { get; }
        public ExMonthVM?[] Months { get; } = new ExMonthVM[24];

        #endregion

        #region Методы

        public void AddMonths(ExMonthVM[] months) {
            var index = 0;
            foreach (var month in months) {
                Months.SetValue(month, index);
                index++;
            }
        }

        #endregion
    }
}
﻿using DevExpress.Mvvm;
using System.Runtime.CompilerServices;

namespace RMPT.DATA.Core.Models {
    public class ExAbstractVM : BindableBase {
        public void NotifyUi([CallerMemberName] string propertyName = "") => RaisePropertyChanged(propertyName);
    }
}
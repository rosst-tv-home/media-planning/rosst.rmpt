﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RMPT.DATA.Core.Models {
    public class ExRegionVM {
        public ExRegionVM(ExProject project, string name) {
            this.Project = project;

            this.Name = name;
        }

        #region Название

        public string Name { get; }

        #endregion

        #region Инвентарь

        #region Целевой рейтинг

        public double TRP { get; set; }

        #endregion

        #endregion

        #region Ссылки

        public ExProject Project { get; }
        public ICollection<ExChannelVM> Channels { get; } = new Collection<ExChannelVM>();

        #endregion
    }
}
﻿using RMPT.DATA.Core.Wrapper;
using System;

namespace RMPT.DATA.Core.Models {
    public class ExTimingVM {
        public ExTimingVM(ExWeekVM week, int value) {
            this.Week = week;

            this.Value = value;
        }

        #region Значение

        public int Value { get; }

        #endregion

        #region Признак "Активен"

        protected internal bool _isActive = true;

        public bool IsActive {
            get => _isActive;
            set {
                if (_isActive == value) return;
                WrapperTiming.IsActive(this, value);
            }
        }

        #endregion

        #region Доля

        protected internal double _portion;

        public double Portion {
            get => _portion;
            set {
                if (Math.Abs(_portion - value) < 0.0) return;
                WrapperTiming.Portion(this, value);
            }
        }

        #endregion

        #region Инвентарь

        #region Целевой рейтинг

        public double TRP { get; set; }

        #endregion

        #endregion

        #region Ссылки

        public ExWeekVM Week { get; }

        #endregion
    }
}
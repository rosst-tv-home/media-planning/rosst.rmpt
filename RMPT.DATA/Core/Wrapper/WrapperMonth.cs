﻿using DevExpress.Mvvm.POCO;
using RMPT.DATA.Core.Models;

namespace RMPT.DATA.Core.Wrapper {
    internal static class WrapperMonth {
        internal static void IsActive(ExMonthVM month, bool value) {
        WrapperTable.Lock();
            month._isActive = value;
            if (month.IsActive == false) WrapperChannel.SpreadTrp(month.Channel, month.Channel.TRP);
            WrapperTable.UnLock();
        }

        internal static void SpreadTrp(ExMonthVM month, double value) {
        WrapperTable.Lock();
            month._trp = value;
            foreach (var week in month.Weeks) {
                WrapperWeek.SpreadTrp(week, WrapperValidator.Zero(month.TRP / month.ActiveDays * week.ActiveDays));
            }
            WrapperTable.UnLock();
        }

        internal static void ComputeTrp(ExMonthVM month) {
        WrapperTable.Lock();
            month._trp = 0;
            foreach (var week in month.Weeks) {
                month._trp += week.TRP;
            }

            WrapperChannel.ComputeTrp(month.Channel);
            WrapperTable.UnLock();
        }

        internal static void ComputeActiveWeeks(ExMonthVM month) {
        WrapperTable.Lock();
            month.ActiveWeeksCount = 0;
            foreach (var week in month.Weeks) {
                if (week.IsActive == false) continue;
                month.ActiveWeeksCount++;
            }
            WrapperTable.UnLock();
        }

        internal static void ComputeActiveDays(ExMonthVM month) {
        WrapperTable.Lock();
            month.ActiveDays = 0;
            foreach (var week in month.Weeks) {
                if (!week.IsActive) continue;
                month.ActiveDays += week.ActiveDays;
            }
            month.NotifyUi(nameof(month.ActiveDays));

            WrapperChannel.ComputeActiveDays(month.Channel);
            WrapperTable.UnLock();
        }

        internal static void ComputeTotalDays(ExMonthVM month) {
        WrapperTable.Lock();
            month.Days = 0;
            foreach (var week in month.Weeks) {
                month.Days += week.Days;
            }
            WrapperTable.UnLock();
        }
    }
}
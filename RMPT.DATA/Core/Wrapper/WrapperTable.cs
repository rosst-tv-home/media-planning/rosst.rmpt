﻿using DevExpress.Xpf.Grid;
using System.Diagnostics;

namespace RMPT.DATA.Core.Wrapper {
    public static class WrapperTable {
        public static GridControl? GridControl { get; set; }

        private static int _lock;
        
        public static void Lock() {
            _lock++;
            if (_lock != 1) return;
            GridControl?.BeginDataUpdate();
        }

        public static void UnLock() {
            _lock--;
            if (_lock != 0) return;
            var measure = Stopwatch.StartNew();
            GridControl?.EndDataUpdate();
            Debug.WriteLine($"{nameof(UnLock)}: {measure.ElapsedMilliseconds}ms");
            measure.Stop();
        }
    }
}
﻿using RMPT.DATA.Core.Models;

namespace RMPT.DATA.Core.Wrapper {
    internal static class WrapperTiming {
        internal static void IsActive(ExTimingVM timing, bool value) {
            WrapperTable.Lock();
            timing._isActive = value;
            WrapperWeek.ComputeActiveTimings(timing.Week);
            if (timing.IsActive == false) {
                timing.Portion = 0;
                if (timing.Week.ActiveTimingsCount != 1) {
                    WrapperTable.UnLock();
                    return;
                }

                foreach (var item in timing.Week.Timings) {
                    if (item.IsActive == false || item.Value == timing.Value) continue;
                    item.Portion = 1;
                    WrapperTable.UnLock();
                    return;
                }

                WrapperTable.UnLock();
                return;
            }

            if (timing.Week.ActiveTimingsCount == 1) {
                timing.Portion = 1;
            }

            WrapperTable.UnLock();
        }

        internal static void Portion(ExTimingVM timing, double value) {
            WrapperTable.Lock();
            timing._portion = value;
            WrapperWeek.SpreadTrp(timing.Week, timing.Week.TRP);
            WrapperTable.UnLock();
        }

        internal static void ComputeTrp(ExTimingVM timing) {
            WrapperTable.Lock();
            WrapperValidator.Zero(timing.TRP = timing.Week.TRP * timing.Portion);
            WrapperTable.UnLock();
        }
    }
}
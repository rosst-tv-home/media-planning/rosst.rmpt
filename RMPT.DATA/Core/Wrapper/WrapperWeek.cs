﻿using RMPT.DATA.Core.Models;

namespace RMPT.DATA.Core.Wrapper {
    internal static class WrapperWeek {
        internal static void IsActive(ExWeekVM week, bool value) {
            WrapperTable.Lock();
            week._isActive = value;
            week.ActiveDays = week.IsActive ? week.Days : 0;
            WrapperMonth.ComputeActiveWeeks(week.Month);
            week.Month.IsActive = week.Month.ActiveWeeksCount > 0;
            WrapperTable.UnLock();
        }

        internal static void ActiveDays(ExWeekVM week, int value) {
            WrapperTable.Lock();
            week._activeDays = value;
            WrapperMonth.ComputeActiveDays(week.Month);
            WrapperMonth.SpreadTrp(week.Month, week.Month.TRP);
            WrapperTable.UnLock();
        }

        internal static void SpreadTrp(ExWeekVM week, double value) {
            WrapperTable.Lock();
            week._trp = value;
            foreach (var timing in week.Timings) {
                WrapperTiming.ComputeTrp(timing);
            }

            WrapperMonth.ComputeTrp(week.Month);
            WrapperTable.UnLock();
        }

        internal static void ComputeActiveTimings(ExWeekVM week) {
            WrapperTable.Lock();
            week.ActiveTimingsCount = 0;
            foreach (var timing in week.Timings) {
                if (timing.IsActive == false) continue;
                week.ActiveTimingsCount++;
            }

            WrapperTable.UnLock();
        }
    }
}
﻿using RMPT.DATA.Core.Models;

namespace RMPT.DATA.Core.Wrapper {
    internal static class WrapperChannel {
        internal static void SpreadTrp(ExChannelVM channel, double value) {
            WrapperTable.Lock();
            channel._trp = value;
            foreach (var month in channel.Months) {
                if (month == null) continue;
                WrapperMonth.SpreadTrp(
                    month,
                    WrapperValidator.Zero(channel.TRP / channel.ActiveDays * month.ActiveDays)
                );
            }

            WrapperTable.UnLock();
        }

        internal static void ComputeActiveDays(ExChannelVM channel) {
            WrapperTable.Lock();
            channel.ActiveDays = 0;
            foreach (var month in channel.Months) {
                if (month          == null) continue;
                if (month.IsActive == false) continue;
                channel.ActiveDays += month.ActiveDays;
            }

            WrapperTable.UnLock();
        }

        internal static void ComputeTrp(ExChannelVM channel) {
            WrapperTable.Lock();
            channel._trp = 0;
            foreach (var month in channel.Months) {
                if (month == null) continue;
                channel._trp += month.TRP;
            }

            WrapperRegion.ComputeTrp(channel.Region);
            WrapperRegion.RecalculateChannelsPortions(channel.Region);
            WrapperTable.UnLock();
        }
    }
}
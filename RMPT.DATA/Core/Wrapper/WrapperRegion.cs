﻿using RMPT.DATA.Core.Models;

namespace RMPT.DATA.Core.Wrapper {
    internal static class WrapperRegion {
        internal static void ComputeTrp(ExRegionVM region) {
            WrapperTable.Lock();
            region.TRP = 0;
            foreach (var channel in region.Channels) {
                region.TRP += channel.TRP;
            }
            WrapperTable.UnLock();
        }

        internal static void RecalculateChannelsPortions(ExRegionVM region) {
            if (region.TRP == 0) return;
            WrapperTable.Lock();
            foreach (var channel in region.Channels) {
                channel.Portion = WrapperValidator.Zero(channel.TRP / region.TRP);
            }
            WrapperTable.UnLock();
        }
    }
}
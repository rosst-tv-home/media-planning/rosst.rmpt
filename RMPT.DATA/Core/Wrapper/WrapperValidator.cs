﻿namespace RMPT.DATA.Core.Wrapper {
    internal class WrapperValidator {
        internal static double Zero(double value) => !double.IsInfinity(value) ? !double.IsNaN(value) ? value : 0 : 0;
    }
}
﻿namespace RMPT.DATA.DataBase {
    public static class Schema {
        /// <summary>
        ///     Схема данных проектов
        /// </summary>
        public const string Projects = "projects";

        /// <summary>
        ///     Схема данных справочников
        /// </summary>
        public const string Dictionaries = "dictionaries";
    }
}
﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Brand;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Dictionaries.Other;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using RMPT.DATA.DataBase.Entities.Projects;
using System.Data.Entity;

namespace RMPT.DATA.DataBase {
    /// <summary>
    ///     Контекст базы данных
    ///     IDbSet.AddOrUpdate()
    ///     - обновляет собственные свойства сущности
    ///     - добавляет сущность и дочерние сущности
    ///     - ошибка, если есть ссылка на родительский объёкт - нужно затирать ссылку
    ///     IDbSet.Remove()
    ///     - удаляет сущность и дочерние сущности
    ///     - не требует затирания ссылок
    /// </summary>
    public class Context : DbContext {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        public Context() : base(
            #if DEBUG
            "DebugConnection"
            #else
            "DefaultConnection"
            #endif
        ) {
            #if DEBUG
            // Устанавливаем инициализатор "Сброс базы данных, если модели изменились"
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<Context>());
            // Создание базы данных, если она отсутсвует
            Database.CreateIfNotExists();
            #endif

            // Отключаем ленивую инициализацию
            Configuration.LazyLoadingEnabled = false;
            // Отключаем динамическое создание моделей
            Configuration.ProxyCreationEnabled = false;
        }

        #endregion

        #region Свойства

        /// <summary>
        ///     Коллекция регионов
        /// </summary>
        public IDbSet<DCRegion> Regions => Set<DCRegion>();

        /// <summary>
        ///     Коллекция каналов
        /// </summary>
        public IDbSet<DCChannel> Channels => Set<DCChannel>();

        /// <summary>
        ///     Рейтинги
        /// </summary>
        public IDbSet<DCRating> Ratings => Set<DCRating>();

        /// <summary>
        ///     Коллекция базовых аудитории
        /// </summary>
        public IDbSet<DCAudience> Audiences => Set<DCAudience>();

        /// <summary>
        ///     Коллекция рекламодателей
        /// </summary>
        public IDbSet<DCAdvertiser> Advertisers => Set<DCAdvertiser>();

        /// <summary>
        ///     Коллекция стратегов
        /// </summary>
        public IDbSet<DCStrategist> Strategists => Set<DCStrategist>();

        /// <summary>
        ///     Коллекция пользователей
        /// </summary>
        public IDbSet<DCUser> Users => Set<DCUser>();

        /// <summary>
        ///     Коллекция ролей
        /// </summary>
        public IDbSet<DCRole> Roles => Set<DCRole>();

        /// <summary>
        ///     Проекты
        /// </summary>
        public IDbSet<PDProject> Projects => Set<PDProject>();

        /// <summary>
        ///     Настройки представления проектов
        /// </summary>
        public IDbSet<PDViewPreferences> ViewPreferencesPD => Set<PDViewPreferences>();

        /// <summary>
        ///     Регионы проектов
        /// </summary>
        public IDbSet<PDRegion> RegionsPD => Set<PDRegion>();

        /// <summary>
        ///     Каналы проектов
        /// </summary>
        public IDbSet<PDChannel> ChannelsPD => Set<PDChannel>();

        /// <summary>
        ///     Месяца проектов
        /// </summary>
        public IDbSet<PDMonth> MonthsPD => Set<PDMonth>();

        /// <summary>
        ///     Недели проектов
        /// </summary>
        public IDbSet<PDWeek> WeeksPD => Set<PDWeek>();

        /// <summary>
        ///     Хронометражи проектов
        /// </summary>
        public IDbSet<PDTiming> TimingsPD => Set<PDTiming>();

        /// <summary>
        ///     Коллекция версий справочников НРА Считалки
        /// </summary>
        public IDbSet<DCVersion> CalculateDictionaries => Set<DCVersion>();

        /// <summary>
        ///     Наборы сезонных коэффициентов
        /// </summary>
        public IDbSet<DCSeasonalOddsSet> SeasonCoefficientSets => Set<DCSeasonalOddsSet>();

        /// <summary>
        ///     Лимиты качеств
        /// </summary>
        public IDbSet<DCQualityLimits> QualityLimits => Set<DCQualityLimits>();

        #endregion

        #region Методы

        #region Обработчики событий

        /// <summary>
        ///     Метод вызывается во время создания моделей таблиц
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            // Вызываем базовый метод из класса, от которого наследуемся
            base.OnModelCreating(modelBuilder);

            // Конфигурируем отношения с таблицей пользователей
            ConfigureUserTableRelation(modelBuilder);
            // Конфигурируем отношения с таблицей пользовательских ролей
            ConfigureRoleTableRelation(modelBuilder);

            // Конфигурируем отношения с таблицей рекламодателей
            ConfigureAdvertiserTableRelation(modelBuilder);

            // Конфигурируем отношения с таблицей регионов
            ConfigureRegionTableRelation(modelBuilder);
            // Конфигурируем отношения с таблицей сезонных коэффициентов
            ConfigureSeasonCoefficientTableRelation(modelBuilder);
            // Конфигурируем отношения с таблицей каналов
            ConfigureChannelTableRelation(modelBuilder);
            // Конфигурируем отношения с таблицей лимитов качеств
            ConfigureQualityLimitTableRelation(modelBuilder);

            // Конфигурируем отношения с таблицей аудиторий
            ConfigureAudienceTableRelation(modelBuilder);

            // Конфигурируем отношения с таблицей проектов
            ConfigureProjectTableRelation(modelBuilder);
            // Конфигурируем отношения с таблицей регионов проектов
            ConfigureProjectRegionTableRelation(modelBuilder);
            // Конфигурируем отношения с таблицей каналов регионов проектов
            ConfigureProjectChannelTableRelation(modelBuilder);
            // Конфигурируем отношения с таблицей месячных перодов каналов регионов проектов
            ConfigureProjectMonthTableRelation(modelBuilder);
            // Конфигурируем отношения с таблицей недельных периодов месячных перодов каналов регионов проектов
            ConfigureProjectWeekTableRelation(modelBuilder);
        }

        #endregion

        #region Настройка отношений таблиц

        /// <summary>
        ///     Метод конфигурации отношений:
        ///     <see cref="DCUser" /> [One] -> [Many] <see cref="DCUserRole" />
        ///     {Cascade.DELETE}
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureUserTableRelation(DbModelBuilder modelBuilder) => modelBuilder
           .Entity<DCUser>()
           .HasMany(user => user.Roles)
           .WithRequired(userRole => userRole.User!)
           .HasForeignKey(userRole => userRole.UserID)
           .WillCascadeOnDelete(true);

        /// <summary>
        ///     Метод конфигурации отношений:
        ///     <see cref="DCRole" /> [One] -> [Many] <see cref="DCUserRole" />
        ///     {Cascade.DELETE}
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureRoleTableRelation(DbModelBuilder modelBuilder) => modelBuilder
           .Entity<DCRole>()
           .HasMany(role => role.Users)
           .WithRequired(userRole => userRole.Role!)
           .HasForeignKey(userRole => userRole.RoleID)
           .WillCascadeOnDelete(true);

        /// <summary>
        ///     Метод конфигурации отношений:
        ///     <see cref="DCAdvertiser" /> [One] -> [Many] <see cref="DCBrand" />
        ///     {Cascade.DELETE}
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureAdvertiserTableRelation(DbModelBuilder modelBuilder) => modelBuilder
           .Entity<DCAdvertiser>()
           .HasMany(advertiser => advertiser.Brands)
           .WithRequired(brand => brand.Advertiser!)
           .HasForeignKey(brand => brand.AdvertiserId)
           .WillCascadeOnDelete(true);

        /// <summary>
        ///     Метод конфигурации отношений модели <see cref="DCRegion" />
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureRegionTableRelation(DbModelBuilder modelBuilder) {
            // Region [One] -> [Many] RegionNameVariant {Cascade.DELETE}
            modelBuilder
               .Entity<DCRegion>()
               .HasMany(region => region.NameVariants)
               .WithRequired(variant => variant.Region!)
               .HasForeignKey(variant => variant.RegionId)
               .WillCascadeOnDelete(true);

            // Region [One] -> [Many] RegionTimingMargin {Cascade.DELETE}
            modelBuilder
               .Entity<DCRegion>()
               .HasMany(region => region.TimingMargins)
               .WithRequired(margin => margin.Region!)
               .HasForeignKey(margin => margin.RegionId)
               .WillCascadeOnDelete(true);

            // Region [One] -> [Many] Channel {Cascade.DELETE}
            modelBuilder
               .Entity<DCRegion>()
               .HasMany(region => region.Channels)
               .WithRequired(channel => channel.Region!)
               .HasForeignKey(channel => channel.RegionID)
               .WillCascadeOnDelete(true);

            // Region [One] -> [Many] ProjectRegion
            modelBuilder
               .Entity<DCRegion>()
               .HasMany(region => region.ProjectsRegions)
               .WithRequired(project => project.Region!)
               .HasForeignKey(project => project.DictionaryID)
               .WillCascadeOnDelete(false);
        }

        /// <summary>
        ///     Метод конфигурации отногений модели <see cref="DCSeasonalOddsSet"/>
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureSeasonCoefficientTableRelation(DbModelBuilder modelBuilder) {
            // SeasonCoefficientSet [One] -> [Many] SeasonCoefficient {Cascade.DELETE}
            modelBuilder
               .Entity<DCSeasonalOddsSet>()
               .HasMany(seasonCoefficientSet => seasonCoefficientSet.SeasonalOdds)
               .WithRequired(seasonCoefficient => seasonCoefficient.SeasonalOddsSet!)
               .HasForeignKey(seasonCoefficient => seasonCoefficient.SeasonalOddsSetID)
               .WillCascadeOnDelete(true);

            // SeasonCoefficient [One] -> [Many] Region {Cascade.DELETE}
            modelBuilder
               .Entity<DCSeasonalOddsSet>()
               .HasMany(seasonCoefficientSet => seasonCoefficientSet.Regions)
               .WithRequired(region => region.SeasonalOddsSet!)
               .HasForeignKey(region => region.SeasonalOddsSetID)
               .WillCascadeOnDelete(true);
        }

        /// <summary>
        ///     Метод конфигурации отношений модели <see cref="DCChannel" />
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureChannelTableRelation(DbModelBuilder modelBuilder) {
            // Channel [One] -> [Many] ChannelNameVariant {Cascade.DELETE}
            modelBuilder
               .Entity<DCChannel>()
               .HasMany(channel => channel.NameVariants)
               .WithRequired(variant => variant.Channel!)
               .HasForeignKey(variant => variant.ChannelID)
               .WillCascadeOnDelete(true);

            // Channel [One] -> [Many] ChannelRating {Cascade.DELETE}
            modelBuilder
               .Entity<DCChannel>()
               .HasMany(channel => channel.Ratings)
               .WithRequired(rating => rating.Channel!)
               .HasForeignKey(rating => rating.ChannelID)
               .WillCascadeOnDelete(true);

            // Channel [One] -> [Many] ProjectRegionChannel
            modelBuilder
               .Entity<DCChannel>()
               .HasMany(channel => channel.ProjectsChannels)
               .WithRequired(projectChannel => projectChannel.Channel!)
               .HasForeignKey(projectChannel => projectChannel.DictionaryID)
               .WillCascadeOnDelete(false);
        }

        /// <summary>
        ///     Метод конфигурации отношений модели <see cref="DCQualityLimits"/>
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureQualityLimitTableRelation(DbModelBuilder modelBuilder) => modelBuilder
           .Entity<DCQualityLimits>()
           .HasMany(x => x.Channels)
           .WithRequired(x => x.QualityLimits!)
           .HasForeignKey(x => x.QualityLimitsID)
           .WillCascadeOnDelete(true);

        /// <summary>
        ///     Метод конфигурации отношений модели <see cref="DCAudience" />
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureAudienceTableRelation(DbModelBuilder modelBuilder) {
            // Audience [One] -> [Many] ChannelBaseAudience {Cascade.DELETE}
            modelBuilder
               .Entity<DCAudience>()
               .HasMany(audience => audience.Channels)
               .WithOptional(channel => channel.Audience!)
               .HasForeignKey(channel => channel.AudienceID)
               .WillCascadeOnDelete(true);

            // Audience [One] -> [Many] ProjectRegionChannelAudience
            modelBuilder
               .Entity<DCAudience>()
               .HasMany(audience => audience.ProjectsAudiences)
               .WithRequired(channelAudience => channelAudience.Audience!)
               .HasForeignKey(channelAudience => channelAudience.DictionaryID)
               .WillCascadeOnDelete(false);
        }

        /// <summary>
        ///     Метод конфигурации отношений модели <see cref="PDProject" />
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureProjectTableRelation(DbModelBuilder modelBuilder) {
            // Project [One] -> [One] ViewPreferences
            modelBuilder
               .Entity<PDProject>()
               .HasRequired(project => project.ViewPreferences)
               .WithRequiredPrincipal(preferences => preferences!.Project!)
               .WillCascadeOnDelete(true);

            // Project [Many] -> [One] User[Created]
            modelBuilder
               .Entity<PDProject>()
               .HasRequired(project => project.CreatedUser)
               .WithMany(user => user!.CreatedProjects)
               .HasForeignKey(project => project.CreatedUserID)
               .WillCascadeOnDelete(false);

            // Project [Many] -> [One] User[LastModified]
            modelBuilder
               .Entity<PDProject>()
               .HasRequired(project => project.LastModifiedUser)
               .WithMany(user => user!.LastModifiedProjects)
               .HasForeignKey(project => project.LastModifiedUserID)
               .WillCascadeOnDelete(false);

            // Project [Many] -> [One] Brand
            modelBuilder
               .Entity<PDProject>()
               .HasRequired(project => project.Brand)
               .WithMany(brand => brand!.Projects)
               .HasForeignKey(project => project.BrandID)
               .WillCascadeOnDelete(false);

            // Project [Many] -> [One] Strategist
            modelBuilder
               .Entity<PDProject>()
               .HasRequired(project => project.Strategist)
               .WithMany(strategist => strategist!.StrategistProjects)
               .HasForeignKey(project => project.StrategistID)
               .WillCascadeOnDelete(false);

            // Project [Many] -> [One] Manager (Strategist)
            modelBuilder
               .Entity<PDProject>()
               .HasRequired(project => project.Manager)
               .WithMany(manager => manager!.ManagerProjects)
               .HasForeignKey(project => project.ManagerID)
               .WillCascadeOnDelete(false);

            // Project [Many] -> [One] ProjectRegion {Cascade.DELETE}
            modelBuilder
               .Entity<PDProject>()
               .HasMany(project => project.Regions)
               .WithRequired(region => region.Project!)
               .HasForeignKey(region => region.ProjectID)
               .WillCascadeOnDelete(true);
        }

        /// <summary>
        ///     Метод конфигурации отношений:
        ///     <see cref="PDRegion" /> [One] -> [Many] <see cref="PDChannel" />
        ///     {Cascade.DELETE}
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureProjectRegionTableRelation(DbModelBuilder modelBuilder) => modelBuilder
           .Entity<PDRegion>()
           .HasMany(region => region.Channels)
           .WithRequired(channel => channel.Region!)
           .HasForeignKey(channel => channel.RegionID)
           .WillCascadeOnDelete(true);

        /// <summary>
        ///     Метод конфигурации отношений модели <see cref="PDChannel" />
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureProjectChannelTableRelation(DbModelBuilder modelBuilder) {
            // ProjectRegionChannel [One] -> [Many] ProjectRegionChannelAudience {Cascade.DELETE}
            modelBuilder
               .Entity<PDChannel>()
               .HasMany(channel => channel.Audiences)
               .WithRequired(audience => audience.Channel!)
               .HasForeignKey(audience => audience.ChannelID)
               .WillCascadeOnDelete(true);

            // ProjectRegionChannel [One] -> [Many] ProjectRegionChannelMonth {Cascade.DELETE}
            modelBuilder
               .Entity<PDChannel>()
               .HasMany(channel => channel.Months)
               .WithRequired(month => month.Channel!)
               .HasForeignKey(month => month.ChannelID)
               .WillCascadeOnDelete(true);
        }

        /// <summary>
        ///     Метод конфигурации отношений:
        ///     <see cref="PDMonth" /> [One] -> [Many] <see cref="PDWeek" />
        ///     {Cascade.DELETE}
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureProjectMonthTableRelation(DbModelBuilder modelBuilder) => modelBuilder
           .Entity<PDMonth>()
           .HasMany(month => month.Weeks)
           .WithRequired(week => week.Month!)
           .HasForeignKey(week => week.MonthID)
           .WillCascadeOnDelete(true);

        /// <summary>
        ///     Метод конфигурации отношений:
        ///     <see cref="PDWeek" /> [One] -> [Many] <see cref="PDTiming" />
        ///     {Cascade.DELETE}
        /// </summary>
        /// <param name="modelBuilder">Объект создания моделей</param>
        private static void ConfigureProjectWeekTableRelation(DbModelBuilder modelBuilder) => modelBuilder
           .Entity<PDWeek>()
           .HasMany(week => week.Timings)
           .WithRequired(timing => timing.Week!)
           .HasForeignKey(timing => timing.WeekID)
           .WillCascadeOnDelete(true);

        #endregion

        #endregion
    }
}
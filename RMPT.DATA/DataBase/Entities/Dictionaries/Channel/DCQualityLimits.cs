﻿using RMPT.DATA.ViewModels.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Channel {
    /// <summary>
    ///     Модель справочной таблицы "quality_limits" - лимиты качества
    /// </summary>
    [Table("quality_limits", Schema = Schema.Dictionaries)]
    public class DCQualityLimits {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None), Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Собственные свойства

        #region FixOrFloat

        #region Common

        /// <summary>
        ///     Мин. знач. доли fix (для Фед. ТВ) или float (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("fix_or_float_min", Order = 1)]
        public double FixOrFloatMin { get; set; }

        /// <summary>
        ///     Макс. знач. доли fix (для Фед. ТВ) или float (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("fix_or_float_max", Order = 2)]
        public double FixOrFloatMax { get; set; }

        #endregion

        #region Prime

        /// <summary>
        ///     Мин. знач. доли fix prime (для Фед. ТВ) или float prime (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("fix_or_float_prime_min", Order = 3)]
        public double FixOrFloatPrimeMin { get; set; }

        /// <summary>
        ///     Макс. знач. доли fix prime (для Фед. ТВ) или float prime (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("fix_or_float_prime_max", Order = 4)]
        public double FixOrFloatPrimeMax { get; set; }

        #endregion

        #region Off Prime

        /// <summary>
        ///     Мин. знач. доли fix off prime (для Фед. ТВ) или float off prime (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("fix_or_float_off_prime_min", Order = 5)]
        public double FixOrFloatOffPrimeMin { get; set; }

        /// <summary>
        ///     Макс. знач. доли fix off prime (для Фед. ТВ) или float off prime (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("fix_or_float_off_prime_max", Order = 6)]
        public double FixOrFloatOffPrimeMax { get; set; }

        #endregion

        #endregion

        #region SuperFixOrFix

        #region Common

        /// <summary>
        ///     Мин. знач. доли super fix (для Фед. ТВ) или fix (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("super_fix_or_fix_min", Order = 7)]
        public double SuperFixOrFixMin { get; set; }

        /// <summary>
        ///     Макс. знач. доли super fix (для Фед. ТВ) или fix (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("super_fix_or_fix_max", Order = 8)]
        public double SuperFixOrFixMax { get; set; }

        #endregion

        #region Prime

        /// <summary>
        ///     Мин. знач. доли super fix prime (для Фед. ТВ) или fix prime (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("super_fix_or_fix_prime_min", Order = 9)]
        public double SuperFixOrFixPrimeMin { get; set; }

        /// <summary>
        ///     Макс. знач. доли super fix prime (для Фед. ТВ) или fix prime (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("super_fix_or_fix_prime_max", Order = 10)]
        public double SuperFixOrFixPrimeMax { get; set; }

        #endregion

        #region Off Prime

        /// <summary>
        ///     Мин. знач. доли super fix off prime (для Фед. ТВ) или fix off prime (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("super_fix_or_fix_off_prime_min", Order = 11)]
        public double SuperFixOrFixOffPrimeMin { get; set; }

        /// <summary>
        ///     Макс. знач. доли super fix off prime (для Фед. ТВ) или fix off prime (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("super_fix_or_fix_off_prime_max", Order = 12)]
        public double SuperFixOrFixOffPrimeMax { get; set; }

        #endregion

        #endregion

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Каналы, к которым применяются лимиты качества
        /// </summary>
        public virtual List<DCChannel> Channels { get; set; } = new();

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="channel">Модель представления канала</param>
        /// <returns>Модель представления</returns>
        public QualityLimitVM ToViewModel(ChannelVM channel) => new(channel, ID, FixOrFloatMin,
            FixOrFloatMax, FixOrFloatPrimeMin, FixOrFloatPrimeMax, FixOrFloatOffPrimeMin, FixOrFloatOffPrimeMax,
            SuperFixOrFixMin, SuperFixOrFixMax, SuperFixOrFixPrimeMin, SuperFixOrFixPrimeMax, SuperFixOrFixOffPrimeMin,
            SuperFixOrFixOffPrimeMax
        );

        #endregion
    }
}
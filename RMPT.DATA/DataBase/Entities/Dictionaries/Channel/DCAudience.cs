﻿using RMPT.DATA.DataBase.Entities.Projects;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Channel {
    /// <summary>
    ///     Модель справочной таблицы "audiences" - аудитории
    /// </summary>
    [Table("audiences", Schema = Schema.Dictionaries)]
    public class DCAudience {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Наименование
        /// </summary>
        [Required]
        [MaxLength(32)]
        [Index(IsUnique = true)]
        [Column("name", Order = 1)]
        public string Name { get; set; } = "";

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Каналы
        /// </summary>
        public virtual List<DCChannel> Channels { get; } = new();

        /// <summary>
        ///     Аудитории проектов
        /// </summary>
        public virtual List<PDAudience> ProjectsAudiences { get; } = new();

        #endregion
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Channel {
    /// <summary>
    ///     Модель справочной таблицы "channels_variants" - варианты наименований каналов
    /// </summary>
    [Table("channels_variants", Schema = Schema.Dictionaries)]
    public class DCChannelVariant {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор канала
        /// </summary>
        [Required]
        [Column("channel_id", Order = 1)]
        public int ChannelID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Наименование
        /// </summary>
        [Required]
        [MaxLength(128)]
        [Column("name", Order = 2)]
        public string Name { get; set; } = "";

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Канал
        /// </summary>
        [ForeignKey(nameof(ChannelID))]
        public virtual DCChannel? Channel { get; set; }

        #endregion
    }
}
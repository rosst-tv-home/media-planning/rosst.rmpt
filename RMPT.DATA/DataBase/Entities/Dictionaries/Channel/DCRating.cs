﻿using RMPT.DATA.ViewModels.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Channel {
    /// <summary>
    ///     Модель справочной таблицы "ratins" - рейтинги
    /// </summary>
    [Table("ratings", Schema = Schema.Dictionaries)]
    public class DCRating {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key, Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор канала
        /// </summary>
        [Required, Column("channel_id", Order = 1)]
        public int ChannelID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Дата: год и месяц
        /// </summary>
        [Required, Column("date", Order = 2)]
        public DateTime Date { get; set; }

        /// <summary>
        ///     Рейтинг prime
        /// </summary>
        [Required, Column("prime", Order = 3)]
        public double Prime { get; set; }

        /// <summary>
        ///     Рейтинг off prime
        /// </summary>
        [Required, Column("off_prime", Order = 4)]
        public double OffPrime { get; set; }

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Канал
        /// </summary>
        [ForeignKey(nameof(ChannelID))]
        public DCChannel? Channel { get; set; }

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="channel">Модель представления канала</param>
        /// <returns>Модель представления</returns>
        public RatingVM ToViewModel(ChannelVM channel) => new(channel, ID, Date, Prime, OffPrime);

        #endregion
    }
}
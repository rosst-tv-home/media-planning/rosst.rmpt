﻿namespace RMPT.DATA.DataBase.Entities.Dictionaries.Channel {
    public enum DCSaleType : byte {
        GRP = 1,
        Minute = 2
    }
}
﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.DATA.DataBase.Entities.Projects;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Channel {
    /// <summary>
    ///     Модель справочной таблицы "channels" - каналы
    /// </summary>
    [Table("channels", Schema = Schema.Dictionaries)]
    public class DCChannel {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор канала из справочника НРА Считалки, либо внесённый вручную
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion
        
        #region Идентификаторы

        /// <summary>
        ///     Дополнительный идентификатор канала из справочника НРА Считалки
        /// </summary>
        [Column("uid", Order = 1)]
        public int? UID { get; set; }

        /// <summary>
        ///     Идентификатор инвентаря канала из справочника НРА Считалки
        /// </summary>
        [Column("iid", Order = 2)]
        public int? IID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор региона, которому принадлежит канал
        /// </summary>
        [Required]
        [Column("region_id", Order = 3)]
        [Index("UX_channels", IsUnique = true, Order = 0)]
        public int RegionID { get; set; }

        /// <summary>
        ///     Идентификатор базовой аудитории
        /// </summary>
        [Column("audience_id", Order = 4)]
        public int? AudienceID { get; set; }

        /// <summary>
        ///     Идентификатор лимитов качества
        /// </summary>
        [Required]
        [Column("quality_limits_id", Order = 5)]
        public int QualityLimitsID { get; set; }

        #endregion

        

        #region Собственные свойства

        /// <summary>
        ///     Наименование
        /// </summary>
        [Required]
        [MaxLength(64)]
        [Column("name", Order = 7)]
        [Index("UX_channels", IsUnique = true, Order = 1)]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        ///     Идентификатор типа продажи инвентаря
        /// </summary>
        [Column("sale_type_id", Order = 6)]
        public DCSaleType SaleType { get; set; }

        /// <summary>
        ///     Наименование точки продаж
        /// </summary>
        [Column("sale_point", Order = 8)]
        public string? SalePoint { get; set; }

        /// <summary>
        ///     Признак "Орбитальный"
        /// </summary>
        [Column("is_orbital", Order = 9)]
        public bool IsOrbital { get; set; }

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Регион
        /// </summary>
        [ForeignKey(nameof(RegionID))]
        public virtual DCRegion? Region { get; set; }

        /// <summary>
        ///     Базовая аудитория
        /// </summary>
        [ForeignKey(nameof(AudienceID))]
        public virtual DCAudience? Audience { get; set; }

        /// <summary>
        ///     Лимиты качества
        /// </summary>
        [ForeignKey(nameof(QualityLimitsID))]
        public virtual DCQualityLimits? QualityLimits { get; set; }

        /// <summary>
        ///     Варианты наименований
        /// </summary>
        public virtual List<DCChannelVariant> NameVariants { get; } = new();

        /// <summary>
        ///     Рейтинги
        /// </summary>
        public virtual List<DCRating> Ratings { get; } = new();

        /// <summary>
        ///     Каналы проектов
        /// </summary>
        public virtual List<PDChannel> ProjectsChannels { get; } = new();

        #endregion
    }
}
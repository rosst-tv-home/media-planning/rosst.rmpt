﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Other {
    /// <summary>
    ///     Модель справочной таблицы "versions" - версии справочников НРА Считалки
    /// </summary>
    [Table("versions", Schema = Schema.Dictionaries)]
    public class DCVersion {
        /// <summary>
        ///     Идентификатор - версия справочника НРА Считалки
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("id", Order = 0)]
        public int ID { get; set; }
    }
}
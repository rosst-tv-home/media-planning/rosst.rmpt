﻿using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.ViewModels.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Other {
    /// <summary>
    ///     Модель справочной таблицы "strategists" стратеги
    /// </summary>
    [Table("strategists", Schema = Schema.Dictionaries)]
    public class DCStrategist {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Имя
        /// </summary>
        [Required]
        [MaxLength(64)]
        [Column("first_name", Order = 1)]
        public string FirstName { get; set; } = "";

        /// <summary>
        ///     Фамилия
        /// </summary>
        [Required]
        [MaxLength(64)]
        [Column("last_name", Order = 2)]
        public string LastName { get; set; } = "";

        /// <summary>
        ///     Имя и фамилия
        /// </summary>
        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Проекты, связанные со сратегом
        /// </summary>
        [InverseProperty(nameof(PDProject.Strategist))]
        public virtual List<PDProject> StrategistProjects { get; } = new();

        /// <summary>
        ///     Проекты, связанные с менеджером
        /// </summary>
        [InverseProperty(nameof(PDProject.Manager))]
        public virtual List<PDProject> ManagerProjects { get; } = new();

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="project">Модель представления проекта</param>
        /// <returns>
        ///     <see cref="StrategistVM" />
        /// </returns>
        public StrategistVM ToViewModel(ProjectVM project) => new(project, ID, FirstName, LastName);

        #endregion
    }
}
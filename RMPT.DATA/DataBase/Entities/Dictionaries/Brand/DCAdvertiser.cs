﻿using RMPT.DATA.ViewModels.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Brand {
    /// <summary>
    ///     Модель справочной таблицы "advertisers" - рекламодатели
    /// </summary>
    [Table("advertisers", Schema = Schema.Dictionaries)]
    public class DCAdvertiser {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Наименование
        /// </summary>
        [Required]
        [MaxLength(64)]
        [Index(IsUnique = true)]
        [Column("name", Order = 1)]
        public string Name { get; set; } = "";

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Бренды
        /// </summary>
        public virtual List<DCBrand> Brands { get; } = new();

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="brand">Модель представления бренда</param>
        /// <returns>
        ///     <see cref="AdvertiserVM" />
        /// </returns>
        public AdvertiserVM ToViewModel(BrandVM brand) => new(brand, ID, Name);

        #endregion
    }
}
﻿using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Brand {
    /// <summary>
    ///     Модель справочной таблицы "brands" - бренды
    /// </summary>
    [Table("brands", Schema = Schema.Dictionaries)]
    public class DCBrand {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор рекламодателя
        /// </summary>
        [Required]
        [Column("advertiser_id", Order = 1)]
        public int AdvertiserId { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Наименование
        /// </summary>
        [Required]
        [MaxLength(64)]
        [Index(IsUnique = true)]
        [Column("name", Order = 2)]
        public string Name { get; set; } = "";

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Рекламодатель
        /// </summary>
        [ForeignKey(nameof(AdvertiserId))]
        public virtual DCAdvertiser? Advertiser { get; set; }

        /// <summary>
        ///     Проекты
        /// </summary>
        public virtual List<PDProject> Projects { get; } = new();

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="project">Модель представления проекта</param>
        /// <returns>
        ///     <see cref="BrandVM" />
        /// </returns>
        public BrandVM ToViewModel(ProjectVM project) {
            var exceptionMessage = $"Не удалось создать модель представления {nameof(BrandVM)}, так как:";

            if (Advertiser == null) {
                throw new NullReferenceException($"{exceptionMessage} не загружена ссылка на ${nameof(Advertiser)}");
            }

            return new BrandVM(
                project,
                ID,
                Name,
                Advertiser
            );
        }

        #endregion
    }
}
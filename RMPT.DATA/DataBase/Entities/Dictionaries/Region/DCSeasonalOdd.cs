﻿using RMPT.DATA.ViewModels.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Region {
    /// <summary>
    ///     Модель справочной таблицы "seasonal_odds" - сезонные коэффициенты
    /// </summary>
    [Table("seasonal_odds", Schema = Schema.Dictionaries)]
    public class DCSeasonalOdd {
        #region Виртуальные свойства

        /// <summary>
        ///     Модель набора сезонных коэффициентов, к которому относится сезонный коэффициент
        /// </summary>
        [ForeignKey(nameof(SeasonalOddsSetID))]
        public virtual DCSeasonalOddsSet? SeasonalOddsSet { get; set; }

        #endregion

        #region Методы

        /// <summary>
        ///     Метод преобразования модели таблицы в модель представления
        /// </summary>
        /// <param name="region">Регион, через который формируется модель сезонного коэффициента</param>
        /// <returns>
        ///     <seealso cref="SeasonCoefficientVM" />
        /// </returns>
        public SeasonCoefficientVM ToViewModel(RegionVM region) => new(region, SeasonalOddsSetID, Date, Value);

        #endregion

        #region Главный ключ

        /// <summary>
        ///     Идентификатор набора сезонных коэффициентов, к которому относится сезонный коэффициент
        /// </summary>
        [Key]
        [Column("seasonal_odds_set_id", Order = 0)]
        public int SeasonalOddsSetID { get; set; }

        /// <summary>
        ///     Месяц, на который применяется сезонный коэффициент
        /// </summary>
        [Key]
        [Column("date", Order = 1)]
        public DateTime Date { get; set; }

        /// <summary>
        ///     Значение сезонного коэффициента
        /// </summary>
        [Required]
        [Column("value", Order = 2)]
        public double Value { get; set; }

        #endregion

        #region Методы сравнения

        private bool Equals(DCSeasonalOdd other) => SeasonalOddsSetID == other.SeasonalOddsSetID
         && Date.Equals(other.Date)
         && Value.Equals(other.Value);

        public override bool Equals(object? other) => other?.GetType() == GetType()
         && Equals((DCSeasonalOdd)other);

        public override int GetHashCode() {
            unchecked {
                var hashCode = SeasonalOddsSetID;
                hashCode = (hashCode * 397) ^ Date.GetHashCode();
                hashCode = (hashCode * 397) ^ Value.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(DCSeasonalOdd? left, DCSeasonalOdd? right) => Equals(left, right);

        public static bool operator !=(DCSeasonalOdd? left, DCSeasonalOdd? right) => !Equals(left, right);

        #endregion
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Region {
    /// <summary>
    ///     Модель справочной таблицы "seasonal_odds_sets" - наборы сезонных коэффициентов
    /// </summary>
    [Table("seasonal_odds_sets", Schema = Schema.Dictionaries)]
    public class DCSeasonalOddsSet {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор набора
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Регионы, к которым применяется набор сезонных коэффициентов
        /// </summary>
        public virtual List<DCRegion> Regions { get; } = new();

        /// <summary>
        ///     Сезонные коэффициенты
        /// </summary>
        public virtual List<DCSeasonalOdd> SeasonalOdds { get; } = new();

        #endregion

        #region Методы сравнения

        private bool Equals(DCSeasonalOddsSet other) => ID == other.ID
         && SeasonalOdds.All(other.SeasonalOdds.Contains);

        public override bool Equals(object? other) => other?.GetType() == this.GetType()
         && Equals((DCSeasonalOddsSet)other);

        public override int GetHashCode() {
            unchecked {
                var hashCode = ID;
                hashCode = SeasonalOdds.Aggregate(hashCode, (_, next) => (hashCode * 397) ^ next.GetHashCode());
                return hashCode;
            }
        }

        public static bool operator ==(DCSeasonalOddsSet? left, DCSeasonalOddsSet? right) => Equals(left, right);

        public static bool operator !=(DCSeasonalOddsSet? left, DCSeasonalOddsSet? right) => !Equals(left, right);

        #endregion
    }
}
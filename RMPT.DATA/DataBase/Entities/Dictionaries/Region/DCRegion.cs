﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Projects;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Region {
    /// <summary>
    ///     Модель справочной таблицы "regions" - регионы
    /// </summary>
    [Table("regions", Schema = Schema.Dictionaries)]
    public class DCRegion {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор региона из справочника НРА Считалки, либо внесённый вручную
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор набора сезонных коэффициентов
        /// </summary>
        [Required]
        [Column("seasonal_odds_set_id", Order = 1)]
        public int SeasonalOddsSetID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Наименование
        /// </summary>
        [Required]
        [MaxLength(32)]
        [Index(IsUnique = true)]
        [Column("name", Order = 2)]
        public string Name { get; set; } = "";

        /// <summary>
        ///     Признак "Измеряемый"
        /// </summary>
        [Required]
        [Column("is_measurable", Order = 3)]
        public bool IsMeasurable { get; set; }

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Набор сезонных коэффициентов
        /// </summary>
        [ForeignKey(nameof(SeasonalOddsSetID))]
        public virtual DCSeasonalOddsSet? SeasonalOddsSet { get; set; }

        /// <summary>
        ///     Варианты наименований
        /// </summary>
        public virtual List<DCRegionVariant> NameVariants { get; } = new();

        /// <summary>
        ///     Наценки за хронометраж
        /// </summary>
        public virtual List<DCTimingMargin> TimingMargins { get; } = new();

        /// <summary>
        ///     Каналы
        /// </summary>
        public virtual List<DCChannel> Channels { get; } = new();

        /// <summary>
        ///     Регионы проектов
        /// </summary>
        public virtual List<PDRegion> ProjectsRegions { get; } = new();

        #endregion
    }
}
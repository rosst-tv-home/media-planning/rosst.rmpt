﻿using RMPT.DATA.ViewModels.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Region {
    /// <summary>
    ///     Модель справочной таблицы "timing_margins" - наценки за хронометраж
    /// </summary>
    [Table("timing_margins", Schema = Schema.Dictionaries)]
    public class DCTimingMargin {
        #region Виртуальные свойства

        /// <summary>
        ///     Регион
        /// </summary>
        [ForeignKey(nameof(RegionId))]
        public DCRegion? Region { get; set; }

        #endregion

        #region Методы конвертации

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="region">Модель представления региона</param>
        /// <returns>
        ///     <see cref="RegionVM" />
        /// </returns>
        public TimingMarginVM ToViewModel(
            RegionVM region
        ) => new(
            region,
            Id,
            Date,
            Timing,
            Value
        );

        #endregion

        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int Id { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор региона
        /// </summary>
        [Required]
        [Column("region_id", Order = 1)]
        public int RegionId { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Дата актуальности
        /// </summary>
        [Required]
        [Column("date", Order = 2)]
        public DateTime Date { get; set; }

        /// <summary>
        ///     Хронометраж
        /// </summary>
        [Required]
        [Column("timing", Order = 3)]
        public int Timing { get; set; }

        /// <summary>
        ///     Наценка
        /// </summary>
        [Required]
        [Column("value", Order = 4)]
        public double Value { get; set; }

        #endregion
    }
}
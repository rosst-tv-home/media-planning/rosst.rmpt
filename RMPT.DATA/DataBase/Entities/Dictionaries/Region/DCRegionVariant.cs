﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.Region {
    /// <summary>
    ///     Модель справочной таблицы "regions_variants" - варианты наименований регионов
    /// </summary>
    [Table("regions_variants", Schema = Schema.Dictionaries)]
    public class DCRegionVariant {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int Id { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор региона
        /// </summary>
        [Required]
        [Column("region_id", Order = 1)]
        public int RegionId { get; set; }

        #endregion
        
        #region Собственные свойства

        /// <summary>
        ///     Наименование
        /// </summary>
        [Required]
        [MaxLength(64)]
        [Column("name", Order = 2)]
        public string Name { get; set; } = "";

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Регион
        /// </summary>
        [ForeignKey(nameof(RegionId))]
        public virtual DCRegion? Region { get; set; }

        #endregion
    }
}
﻿using RMPT.DATA.ViewModels.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.User {
    /// <summary>
    ///     Модель справочной таблицы "roles" - роли
    /// </summary>
    [Table("roles", Schema = Schema.Dictionaries)]
    public class DCRole {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Тип
        /// </summary>
        [Required]
        [MaxLength(64)]
        [Index(IsUnique = true)]
        [Column("type", Order = 1)]
        public string Type { get; set; } = "";

        /// <summary>
        ///     Описание
        /// </summary>
        [Required]
        [MaxLength(256)]
        [Index(IsUnique = true)]
        [Column("description", Order = 2)]
        public string Description { get; set; } = "";

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Пользователи
        /// </summary>
        public virtual List<DCUserRole> Users { get; } = new();

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="user">Модель представления пользователя</param>
        /// <returns>
        ///     <see cref="RoleVM" />
        /// </returns>
        public RoleVM ToViewModel(UserVM user) => new(ID, Type, user);

        #endregion
    }
}
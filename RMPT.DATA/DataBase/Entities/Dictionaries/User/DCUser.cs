﻿using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.User {
    /// <summary>
    ///     Модель справочной таблицы "users" - пользователи
    /// </summary>
    [Table("users", Schema = Schema.Dictionaries)]
    public class DCUser {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Методы

        public UserVM ToViewModel(ProjectVM project) {
            var exceptionMessage = $"Не удалось создать модель представления {nameof(UserVM)}, так как:";

            if (Roles.Count == 0 || Roles.Any(x => x.Role == null)) {
                throw new NullReferenceException($"{exceptionMessage} не загружена ссылка на ${nameof(Roles)}");
            }

            return new(
                project,
                ID,
                Login,
                FirstName,
                LastName,
                IsActive,
                Roles
            );
        }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Логин windows
        /// </summary>
        [Required]
        [MaxLength(32)]
        [Index(IsUnique = true)]
        [Column("login", Order = 2)]
        public string Login { get; set; } = "";

        /// <summary>
        ///     Имя
        /// </summary>
        [Required]
        [MaxLength(32)]
        [Column("first_name", Order = 3)]
        public string FirstName { get; set; } = "";

        /// <summary>
        ///     Фамилия
        /// </summary>
        [Required]
        [MaxLength(32)]
        [Column("last_name", Order = 4)]
        public string LastName { get; set; } = "";

        /// <summary>
        ///     Имя и фамилия
        /// </summary>
        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        ///     Признак "Аккаунт пользователя активен"
        /// </summary>
        [Required]
        [Column("is_active", Order = 5)]
        public bool IsActive { get; set; } = true;

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Роли (требуется setter для присвоения ролей пользователю в диалоговом окне справончика пользователей)
        /// </summary>
        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Global
        public virtual List<DCUserRole> Roles { get; set; } = new();

        /// <summary>
        ///     Созданные проекты
        /// </summary>
        [InverseProperty(nameof(PDProject.CreatedUser))]
        public virtual List<PDProject> CreatedProjects { get; } = new();

        /// <summary>
        ///     Последние изменённые проекты
        /// </summary>
        [InverseProperty(nameof(PDProject.LastModifiedUser))]
        public virtual List<PDProject> LastModifiedProjects { get; } = new();

        #endregion
    }
}
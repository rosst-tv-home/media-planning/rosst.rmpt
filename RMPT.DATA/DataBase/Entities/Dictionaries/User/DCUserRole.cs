﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Dictionaries.User {
    /// <summary>
    ///     Модель справочной таблицы "users_x_roles" роли пользователей
    /// </summary>
    [Table("users_x_roles", Schema = Schema.Dictionaries)]
    public class DCUserRole {
        #region Составной ключ

        #region Идентификаторы

        /// <summary>
        ///     Составной ключ
        ///     Идентификатор пользователя
        /// </summary>
        [Key]
        [Column("user_id", Order = 0)]
        public int UserID { get; set; }

        /// <summary>
        ///     Составной ключ
        ///     Идентификатор роли
        /// </summary>
        [Key]
        [Column("role_id", Order = 1)]
        public int RoleID { get; set; }

        #endregion

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Пользователь
        /// </summary>
        [ForeignKey(nameof(UserID))]
        public virtual DCUser? User { get; set; }

        /// <summary>
        ///     Роль
        /// </summary>
        [ForeignKey(nameof(RoleID))]
        public virtual DCRole? Role { get; set; }

        #endregion
    }
}
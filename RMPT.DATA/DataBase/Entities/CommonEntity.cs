﻿using System;

namespace RMPT.DATA.DataBase.Entities {
    public abstract class CommonEntity {
        protected void ThrowNullReferenceException<T>(T property) => throw new NullReferenceException(
            $"Не удалось создать модель представления {GetType().Name}, " +
            $"так как: не загружена ссылка на ${nameof(property)}"
        );
    }
}
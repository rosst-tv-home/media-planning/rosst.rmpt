﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.ViewModels.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Projects {
    /// <summary>
    ///     Модель таблицы "channels" - каналы
    /// </summary>
    [Table("channels", Schema = Schema.Projects)]
    public class PDChannel : CommonEntity {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор канала в справочной таблице
        /// </summary>
        [Required]
        [Column("dictionary_id", Order = 1)]
        [Index("UX_dictionary_channel_id", 0, IsUnique = true)]
        public int DictionaryID { get; set; }

        /// <summary>
        ///     Идентификатор региона
        /// </summary>
        [Required]
        [Column("region_id", Order = 2)]
        [Index("UX_dictionary_channel_id", 1, IsUnique = true)]
        public int RegionID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Признак "Активен"
        /// </summary>
        [Required]
        [Column("is_active", Order = 3)]
        public bool IsActive { get; set; }

        // /// <summary>
        // ///     Корректировочный Аффинити
        // /// </summary>
        [Required]
        [Column("affinity_corrective_percent", Order = 4)]
        public double AffinityCorrectivePercent { get; set; }

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Канал
        /// </summary>
        [ForeignKey(nameof(DictionaryID))]
        public virtual DCChannel? Channel { get; set; }

        /// <summary>
        ///     Связь с регионом проекта
        /// </summary>
        [ForeignKey(nameof(RegionID))]
        public virtual PDRegion? Region { get; set; }

        /// <summary>
        ///     Коллекция аудиторий
        /// </summary>
        public virtual List<PDAudience> Audiences { get; } = new();

        /// <summary>
        ///     Коллекция месячных периодов
        /// </summary>
        public virtual List<PDMonth> Months { get; } = new();

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="region">Модель представления региона</param>
        /// <returns>Модель представления</returns>
        public ChannelVM ToViewModel(RegionVM region) {
            if (Channel                == null) ThrowNullReferenceException(Channel);
            if (Channel!.QualityLimits == null) ThrowNullReferenceException(Channel.QualityLimits);


            return new ChannelVM(
                region,
                ID,
                Channel!.ID,
                Channel.IID,
                Channel.Name,
                IsActive,
                Channel.SaleType,
                Channel.SalePoint,
                Channel.Ratings,
                Audiences,
                Channel.Audience!,
                Months,
                Channel.QualityLimits!,
                AffinityCorrectivePercent
            );
        }

        #endregion
    }
}
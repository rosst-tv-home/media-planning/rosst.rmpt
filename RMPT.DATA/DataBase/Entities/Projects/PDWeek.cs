﻿using RMPT.DATA.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Projects {
    /// <summary>
    ///     Модель таблицы "weeks" - недели
    /// </summary>
    [Table("weeks", Schema = Schema.Projects)]
    public class PDWeek {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id")]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор месяца
        /// </summary>
        [Required]
        [Index("UX_weeks", IsUnique = true, Order = 0)]
        [Column("month_id")]
        public int MonthID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Дата нача
        /// </summary>
        [Required]
        [Index("UX_weeks", IsUnique = true, Order = 1)]
        [Column("date_from")]
        public DateTime DateFrom { get; set; }

        /// <summary>
        ///     Дата окончания
        /// </summary>
        [Required]
        [Index("UX_weeks", IsUnique = true, Order = 2)]
        [Column("date_before")]
        public DateTime DateBefore { get; set; }

        /// <summary>
        ///     Целевой рейтинг
        /// </summary>
        [Required]
        [Column("trp")]
        public double TRP { get; set; }

        /// <summary>
        ///     Активные дни
        /// </summary>
        [Required]
        [Column("active_days")]
        public int ActiveDays { get; set; }

        #endregion

        #region Вирутальные свойства

        /// <summary>
        ///     Месяц
        /// </summary>
        [ForeignKey(nameof(MonthID))]
        public virtual PDMonth? Month { get; set; }

        /// <summary>
        ///     Хронометражи
        /// </summary>
        public virtual List<PDTiming> Timings { get; } = new();

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели базы данных в модель представления
        /// </summary>
        /// <param name="month">Месяц</param>
        /// <returns>
        ///     <see cref="WeekVM" />
        /// </returns>
        public WeekVM ToViewModel(MonthVM month) => new(
            ID, month, DateFrom, DateBefore,
            ActiveDays, TRP, Timings
        );

        #endregion
    }
}
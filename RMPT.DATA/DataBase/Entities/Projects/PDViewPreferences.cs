﻿using RMPT.DATA.ViewModels.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Projects {
    /// <summary>
    ///     Модель таблицы "views_preferences" - настройки представлений
    /// </summary>
    [Table("views_preferences", Schema = Schema.Projects)]
    public class PDViewPreferences {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор проекта
        ///     Является и главным и внешним ключом
        /// </summary>
        [Key]
        [Column("project_id", Order = 0)]
        public int ProjectID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Признак "Отображать только активные каналы"
        /// </summary>
        [Required]
        [Column("is_show_only_active_channels", Order = 1)]
        public bool IsShowOnlyActiveChannels { get; set; }

        /// <summary>
        ///     Признак "Отображать группу столбцов 'Данные сырья'"
        /// </summary>
        [Required]
        [Column("is_show_raw_data_band", Order = 2)]
        public bool IsShowRawDataBand { get; set; }

        /// <summary>
        ///     Признак "Отображать группы столбцов недель"
        /// </summary>
        [Required]
        [Column("is_show_week_bands", Order = 3)]
        public bool IsShowMonthsWeekBands { get; set; }
        
        /// <summary>
        ///     Признак "Отображать группу столбцов 'Распределение' в каналах"
        /// </summary>
        [Required]
        [Column("is_show_channel_distribution_bands", Order = 4)]
        public bool IsShowChannelDistributionBands { get; set; }

        /// <summary>
        ///     Признак "Отображать группу столбцов 'Распределение' в месяцах"
        /// </summary>
        [Required]
        [Column("is_show_month_distribution_bands", Order = 5)]
        public bool IsShowMonthsDistributionBands { get; set; }
        /// <summary>
        ///     Признак "Отображать группу столбцов 'Распределение хронометражей' в каналах"
        /// </summary>
        [Required]
        [Column("is_show_channel_timing_distribution_bands", Order = 6)]
        public bool IsShowChannelTimingDistributionBand {get; set;}
        /// <summary>
        ///     Признак "Отображать группу столбцов 'Распределение хронометражей' в месяцах"
        /// </summary>
        [Required]
        [Column("is_show_month_timing_distribution_bands", Order = 7)]
        public bool IsShowMonthTimingDistributionBand {get; set;}
        /// <summary>
        ///     Признак "Отображать группу столбцов 'Распределение хронометражей' в неделях"
        /// </summary>
        [Required]
        [Column("is_show_week_timing_distribution_bands", Order = 8)]
        public bool IsShowWeekTimingDistributionBand {get; set;}

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Проект
        /// </summary>
        [ForeignKey(nameof(ProjectID))]
        public virtual PDProject? Project { get; set; }

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="project">Модель представления проекта</param>
        /// <returns>Модель представления</returns>
        public ViewPreferencesVM ToViewModel(ProjectVM project) => new(
            project,
            
            IsShowOnlyActiveChannels,
            IsShowRawDataBand,
            IsShowMonthsWeekBands,
            
            IsShowChannelDistributionBands,
            IsShowMonthsDistributionBands,
            
            IsShowChannelTimingDistributionBand,
            IsShowMonthTimingDistributionBand,
            IsShowWeekTimingDistributionBand
        );

        #endregion
    }
}
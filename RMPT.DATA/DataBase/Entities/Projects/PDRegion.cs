﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.DATA.ViewModels.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Projects {
    /// <summary>
    ///     Модель таблицы "regions" - регионы
    /// </summary>
    [Table("regions", Schema = Schema.Projects)]
    public class PDRegion : CommonEntity {
        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="project">Модель представления проекта</param>
        /// <returns>
        ///     <see cref="RegionVM" />
        /// </returns>
        public RegionVM ToViewModel(
            ProjectVM project
        ) {
            if (Region                  == null) ThrowNullReferenceException(Region);
            if (Region!.SeasonalOddsSet == null) ThrowNullReferenceException(Region.SeasonalOddsSet);

            return new RegionVM(
                project,
                DictionaryID,
                ID,
                Region.Name,
                Region.IsMeasurable,
                Channels,
                Region.TimingMargins,
                Region.SeasonalOddsSet!
            );
        }

        #endregion

        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор проекта
        /// </summary>
        [Column("project_id", Order = 2)]
        public int ProjectID { get; set; }

        /// <summary>
        ///     Идентификатор региона в справочной таблице
        /// </summary>
        [Column("dictionary_id", Order = 1)]
        public int DictionaryID { get; set; }

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Регион
        /// </summary>
        [ForeignKey(nameof(DictionaryID))]
        public virtual DCRegion? Region { get; set; }

        /// <summary>
        ///     Проект
        /// </summary>
        [ForeignKey(nameof(ProjectID))]
        public virtual PDProject? Project { get; set; }

        /// <summary>
        ///     Коллекция каналов, связанных с регионом проекта
        /// </summary>
        public virtual List<PDChannel> Channels { get; } = new();

        #endregion
    }
}
﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.ViewModels.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Projects {
    /// <summary>
    ///     Модель таблицы "audience" - аудитории
    /// </summary>
    [Table("audiences", Schema = Schema.Projects)]
    public class PDAudience : CommonEntity {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор аудитории в справочнике
        /// </summary>
        [Index("UX_audiences_composite", 0, IsUnique = true)]
        [Column("audience_id", Order = 1)]
        public int DictionaryID { get; set; }

        /// <summary>
        ///     Идентификатор канала проекта
        /// </summary>
        [Index("UX_audiences_composite", 1, IsUnique = true)]
        [Column("channel_id", Order = 2)]
        public int ChannelID { get; set; }
        
        /// <summary>
        ///     Период данных аудитории
        /// </summary>
        [Index("UX_audiences_composite", 2, IsUnique = true)]
        [Column("date", Order = 3)]
        public DateTime Date { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Рейтинг
        /// </summary>
        [Required]
        [Column("tvr", Order = 4)]
        public double TVR { get; set; }

        /// <summary>
        ///     Процент накопительного охвата
        /// </summary>
        [Required]
        [Column("cum_reach_percent", Order = 5)]
        public double CumReachPercent { get; set; }

        /// <summary>
        ///     Выборка, количество опрошенных респондентов (человек)
        /// </summary>
        [Required]
        [Column("sample", Order = 6)]
        public int Sample { get; set; }

        /// <summary>
        ///     Взвешенное (тысяч человек) количество респондентов, на которых распространялось исследование (опрос)
        /// </summary>
        [Required]
        [Column("weighted_total", Order = 7)]
        public int WeightedTotal { get; set; }

        /// <summary>
        ///     Процент людей видевших эфирное событие с указанной в проекте частотой
        /// </summary>
        [Required]
        [Column("frequency_percent", Order = 8)]
        public double FrequencyPercent { get; set; }

        /// <summary>
        ///     Признак "Целевая аудитория"
        /// </summary>
        [Required]
        [Index("UX_audiences_composite", 3, IsUnique = true)]
        [Column("is_target", Order = 9)]
        public bool IsTarget { get; set; }

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Аудитория в справочнике
        /// </summary>
        [ForeignKey(nameof(DictionaryID))]
        public virtual DCAudience? Audience { get; set; }

        /// <summary>
        ///     Канал проекта
        /// </summary>
        [ForeignKey(nameof(ChannelID))]
        public virtual PDChannel? Channel { get; set; }

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <param name="channel">Модель представления канала</param>
        /// <returns>
        ///     <see cref="AudienceVM" />
        /// </returns>
        public AudienceVM ToViewModel(ChannelVM channel) {
            if (Audience == null) ThrowNullReferenceException(Audience);

            return new AudienceVM(
                channel,
                Audience!.ID,
                ID,
                Audience.Name,
                Date,
                TVR,
                CumReachPercent,
                Sample,
                WeightedTotal,
                FrequencyPercent,
                IsTarget
            );
        }

        #endregion
    }
}
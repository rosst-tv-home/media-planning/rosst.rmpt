﻿using RMPT.DATA.ViewModels.Models;
using RMPT.DATA.ViewModels.Models.Timing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Projects {
    /// <summary>
    ///     Модель таблицы "timings" - хронометражи
    /// </summary>
    [Table("timings", Schema = Schema.Projects)]
    public class PDTiming {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id")]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор недельного периода
        /// </summary>
        [Required]
        [Index("UX_timings", IsUnique = true, Order = 0)]
        [Column("week_id")]
        public int WeekID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Значение
        /// </summary>
        [Required]
        [Index("UX_timings", IsUnique = true, Order = 1)]
        [Column("value")]
        public int Value { get; set; }

        /// <summary>
        ///     Доля
        /// </summary>
        [Required]
        [Column("portion")]
        public double Portion { get; set; }

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Недельный период
        /// </summary>
        [ForeignKey(nameof(WeekID))]
        public virtual PDWeek? Week { get; set; }

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели базы данных в модель представления
        /// </summary>
        /// <param name="week">Недельный период</param>
        /// <returns>
        ///     <see cref="TimingVM" />
        /// </returns>
        public TimingVM ToViewModel(WeekVM week) => new(week, ID, Value, Portion);

        #endregion
    }
}
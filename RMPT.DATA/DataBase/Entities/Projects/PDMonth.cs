﻿using RMPT.DATA.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Projects {
    /// <summary>
    ///     Модель таблицы "months" - месяца
    /// </summary>
    [Table("months", Schema = Schema.Projects)]
    public class PDMonth {
        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [Column("id")]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор канала
        /// </summary>
        [Required]
        [Index("UX_months", IsUnique = true, Order = 0)]
        [Column("channel_id")]
        public int ChannelID { get; set; }

        #endregion

        #region Собственный свойства

        /// <summary>
        ///     Дата
        /// </summary>
        [Required]
        [Index("UX_months", IsUnique = true, Order = 1)]
        [Column("date")]
        public DateTime Date { get; set; }

        /// <summary>
        ///     Аффинитивность
        /// </summary>
        [Required]
        [Column("affinity")]
        public double Affinity { get; set; }
        
        /// <summary>
        ///     Выходы в день
        /// </summary>
        [Required]
        [Column("outputs_per_day")]
        public double OutputsPerDay { get; set; }

        /// <summary>
        ///     Рассчитанная цена за еденицу инвентаря
        /// </summary>
        [Required]
        [Column("price")]
        public double Price { get; set; }

        /// <summary>
        ///     Рейтинг в off prime
        /// </summary>
        [Required]
        [Column("rating_off_prime")]
        public double RatingOffPrime { get; set; }

        /// <summary>
        ///     Рейтинг в prime
        /// </summary>
        [Required]
        [Column("rating_prime")]
        public double RatingPrime { get; set; }

        /// <summary>
        ///     Доля fix (для Фед. ТВ) или float (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("fix_or_float_portion")]
        public double FixOrFloatPortion { get; set; }

        /// <summary>
        ///     Доля fix prime (для Фед. ТВ) или float prime (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("fix_or_float_prime_portion")]
        public double FixOrFloatPrimePortion { get; set; }

        /// <summary>
        ///     Доля super fix (для Фед. ТВ) или fix (для Рег. и Тем. ТВ)
        /// </summary>
        [Required]
        [Column("super_fix_or_fix_prime_portion")]
        public double SuperFixOrFixPrimePortion { get; set; }

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Канал
        /// </summary>
        [ForeignKey(nameof(ChannelID))]
        public virtual PDChannel? Channel { get; set; }

        /// <summary>
        ///     Коллекция недельных периодов
        /// </summary>
        public virtual List<PDWeek> Weeks { get; } = new();

        #endregion

        #region Методы

        /// <summary>
        ///     Метод конвертации модели базы данных  в модель представления
        /// </summary>
        /// <param name="channel">Канал</param>
        /// <returns>
        ///     <see cref="MonthVM" />
        /// </returns>
        public MonthVM ToViewModel(ChannelVM channel) => new(
            ID,
            channel,
            Date,
            Affinity,
            FixOrFloatPortion,
            FixOrFloatPrimePortion,
            SuperFixOrFixPrimePortion,
            Price,
            OutputsPerDay,
            Weeks
        );

        #endregion
    }
}
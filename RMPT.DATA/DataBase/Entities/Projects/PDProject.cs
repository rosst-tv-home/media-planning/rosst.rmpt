﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Brand;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Dictionaries.Other;
using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using RMPT.DATA.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RMPT.DATA.DataBase.Entities.Projects {
    /// <summary>
    ///     Модель таблицы "projects" - проекты
    /// </summary>
    [Table("projects", Schema = Schema.Projects)]
    public class PDProject : CommonEntity {
        #region Методы

        /// <summary>
        ///     Метод конвертации модели таблицы в модель представления
        /// </summary>
        /// <returns>
        ///     <see cref="ProjectVM" />
        /// </returns>
        public ProjectVM ToViewModel() {
            // Если настройки представления не загружены из базы данных
            if (ViewPreferences == null) ThrowNullReferenceException(ViewPreferences);
            // Если бренд не был загружен из базы данных
            if (Brand == null) ThrowNullReferenceException(Brand);
            // Если стратег не был загружен из базы данных
            if (Strategist == null) ThrowNullReferenceException(Strategist);
            // Если менеджер не был загружен из базы данных
            if (Manager == null) ThrowNullReferenceException(Manager);
            // Если пользователь, создавший проект не был загружен из базы данных
            if (CreatedUser == null) ThrowNullReferenceException(CreatedUser);
            // Если пользователь, последний изменивший проект не был загружен из базы данных
            if (LastModifiedUser == null) ThrowNullReferenceException(LastModifiedUser);
            // Если целевая аудитория не была загружен из базы данных
            if (TargetAudience == null) ThrowNullReferenceException(TargetAudience);
            // Если регионы не были загружены из базы данных
            if (Regions.Count == 0) ThrowNullReferenceException(Regions);
            // Если не определена частота проекта
            if (string.IsNullOrWhiteSpace(Frequency)) ThrowNullReferenceException(Frequency);


            return new ProjectVM(
                ID,
                PlaningYear,
                BaseTiming,
                Vat,
                Margin,
                AgencyCommission,
                CreatedDateTime,
                LastModifiedDateTime,
                Description,
                Frequency!,
                IsFirstInitialization,
                IsBuying,
                IsPlacement,
                IsTender,
                IsFederal,
                ViewPreferences!,
                Brand!,
                Strategist!,
                Manager!,
                CreatedUser!,
                LastModifiedUser!,
                TargetAudience!,
                Regions
            );
        }

        #endregion

        #region Главный ключ

        /// <summary>
        ///     Идентификатор
        /// </summary>
        [Key]
        [ForeignKey(nameof(ViewPreferences))]
        [Column("id", Order = 0)]
        public int ID { get; set; }

        #endregion

        #region Внешние ключи

        /// <summary>
        ///     Идентификатор бренда
        /// </summary>
        [Required]
        [Column("brand_id", Order = 1)]
        public int BrandID { get; set; }

        /// <summary>
        ///     Индетификатор менеджера
        /// </summary>
        [Required]
        [Column("manager_id", Order = 2)]
        public int ManagerID { get; set; }

        /// <summary>
        ///     Индентификатор стратега
        /// </summary>
        [Required]
        [Column("strategist_id", Order = 3)]
        public int StrategistID { get; set; }

        /// <summary>
        ///     Идентификатор пользователя, создавшего
        /// </summary>
        [Required]
        [Column("created_user_id", Order = 4)]
        public int CreatedUserID { get; set; }

        /// <summary>
        ///     Идентификатор пользователя, последним изменившего
        /// </summary>
        [Required]
        [Column("last_modified_user_id", Order = 5)]
        public int LastModifiedUserID { get; set; }

        /// <summary>
        ///     Идентификатор целевой аудитории
        /// </summary>
        [Column("target_audience_id", Order = 6)]
        public int? TargetAudienceID { get; set; }

        #endregion

        #region Собственные свойства

        /// <summary>
        ///     Базовый хронометраж
        /// </summary>
        [Required]
        [Column("base_timing", Order = 7)]
        public int BaseTiming { get; set; } = 20;

        /// <summary>
        ///     Планируемый год
        /// </summary>
        [Required]
        [Column("planing_year", Order = 8)]
        public int PlaningYear { get; set; } = DateTime.Now.Year;

        /// <summary>
        ///     Дата и время создания
        /// </summary>
        [Required]
        [Column("created_date_time", Order = 9)]
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;

        /// <summary>
        ///     Дата и время последнего изменения
        /// </summary>
        [Required]
        [Column("last_modified_date_time", Order = 10)]
        public DateTime LastModifiedDateTime { get; set; } = DateTime.Now;

        /// <summary>
        ///     Описание
        /// </summary>
        [Column("description", Order = 11)]
        public string? Description { get; set; }

        /// <summary>
        ///     Значение частотного распределения в аудиториях каналов проекта
        /// </summary>
        [Required]
        [Column("frequency", Order = 12)]
        public string? Frequency { get; set; }

        #region Наценки

        /// <summary>
        ///     НДС
        /// </summary>
        [Required]
        [Column("vat", Order = 13)]
        public double Vat { get; set; } = 0.2;

        /// <summary>
        ///     Маржа
        /// </summary>
        [Required]
        [Column("margin", Order = 14)]
        public double Margin { get; set; }

        /// <summary>
        ///     Агентская комиссия
        /// </summary>
        [Required]
        [Column("agency_commission", Order = 15)]
        public double AgencyCommission { get; set; }

        #endregion

        #region Признаки

        /// <summary>
        ///     Признак "Первая инициализация проекта"
        /// </summary>
        [Required]
        [Column("is_first_initialization", Order = 16)]
        public bool IsFirstInitialization { get; set; }

        /// <summary>
        ///     Признак "Передан в баинг"
        /// </summary>
        [Required]
        [Column("is_buying", Order = 17)]
        public bool IsBuying { get; set; }

        /// <summary>
        ///     Признак "В размещении"
        /// </summary>
        [Required]
        [Column("is_placement", Order = 18)]
        public bool IsPlacement { get; set; }

        /// <summary>
        ///     Признак "Тендер"
        /// </summary>
        [Required]
        [Column("is_tender", Order = 19)]
        public bool IsTender { get; set; }

        /// <summary>
        ///     Признак "Федаральное ТВ"
        /// </summary>
        [Required]
        [Column("is_federal", Order = 20)]
        public bool IsFederal { get; set; }

        #endregion

        #endregion

        #region Виртуальные свойства

        /// <summary>
        ///     Настройки представления
        /// </summary>
        public virtual PDViewPreferences? ViewPreferences { get; set; }

        /// <summary>
        ///     Бренд
        /// </summary>
        [ForeignKey(nameof(BrandID))]
        public virtual DCBrand? Brand { get; set; }

        /// <summary>
        ///     Менеджер
        /// </summary>
        [ForeignKey(nameof(ManagerID))]
        public virtual DCStrategist? Manager { set; get; }

        /// <summary>
        ///     Стратег
        /// </summary>
        [ForeignKey(nameof(StrategistID))]
        public virtual DCStrategist? Strategist { get; set; }

        /// <summary>
        ///     Пользователь, создавший
        /// </summary>
        [ForeignKey(nameof(CreatedUserID))]
        public virtual DCUser? CreatedUser { get; set; }

        /// <summary>
        ///     Пользователь, последний изменивший
        /// </summary>
        [ForeignKey(nameof(LastModifiedUserID))]
        public virtual DCUser? LastModifiedUser { get; set; }

        /// <summary>
        ///     Целевая аудитория
        /// </summary>
        [ForeignKey(nameof(TargetAudienceID))]
        public virtual DCAudience? TargetAudience { get; set; }

        /// <summary>
        ///     Регионы
        /// </summary>
        public virtual List<PDRegion> Regions { get; } = new();

        #endregion
    }
}
﻿using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryRoles {
        /// <summary>
        ///     Метод запроса всех пользовательских ролей
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task<List<DCRole>> FindAllWithUsersAsync() {
            // Контекст базы данных
            using var context = new Context();

            // Возвращаем коллекцию пользовательских ролей
            return await context.Roles
                // Подключаем пользователей
               .Include(role => role.Users)
               .AsNoTracking()
               .ToListAsync();
        }

        /// <summary>
        ///     Метод запроса добавления новых пользовательских ролей
        /// </summary>
        /// <param name="roles">Коллекция действительных пользовательских ролей</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task InsertAsync(IEnumerable<DCRole> roles) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Итерируемся по коллекции новых пользовательских ролей
            foreach (var role in roles.Where(role => role.ID == 0).ToList()) {
                // Добавляем пользовательскую роль в базу данных
                context.Roles.Add(role);
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса удаления старых пользовательских ролей
        /// </summary>
        /// <param name="roles">Коллекция действительных пользовательских ролей</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task DeleteAsync(IEnumerable<DCRole> roles) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Коллекция идентификаторов действительных пользовательских ролей
            var actualRolesIdList = roles
               .Select(role => role.ID)
               .ToHashSet();

            // Определяем недействительные пользовательские роли
            var invalidRoles = await context.Roles
               .Where(role => !actualRolesIdList.Contains(role.ID))
               .ToListAsync();

            // Итерируемся по коллекции недействительных пользовательских ролей
            foreach (var role in invalidRoles) {
                // Удаляем пользовательскую роль из базы данных
                context.Roles.Remove(role);
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса обновления действительных пользовательских ролей
        /// </summary>
        /// <param name="roles">Коллекция действительных пользовательских ролей</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task UpdateAsync(ICollection<DCRole> roles) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Получаем коллекцию пользовательских ролей из базы данных
            var dbRoles = await context.Roles.ToListAsync();

            // Итерируемся по коллекции пользовательских ролей из базы данных
            foreach (var role in dbRoles) {
                // Определяем обновлённые данные пользовательской роли
                var roleData = roles.FirstOrDefault(x => x.ID == role.ID);
                // Если обновлённые данные не были найдены - переходим к следующей пользовательской роли
                if (roleData == null) {
                    continue;
                }

                // Обновляем наименование
                role.Type = !string.IsNullOrWhiteSpace(roleData.Type.Trim())
                    ? roleData.Type.Trim()
                    : role.Type;
                // Обновляем описание
                role.Description = !string.IsNullOrWhiteSpace(roleData.Description.Trim())
                    ? roleData.Description.Trim()
                    : role.Description;
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }
    }
}
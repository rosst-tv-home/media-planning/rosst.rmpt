﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Brand;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using RMPT.DATA.DataBase.Entities.Projects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryProject {
        /// <summary>
        ///     Метод запроса проектов из базы данных
        /// </summary>
        /// <param name="dateFrom">Дата начала периода создания проекта</param>
        /// <param name="dateBefore">Дата окончания периода создания проекта</param>
        /// <returns>Проекты</returns>
        public static async Task<List<PDProject>> FindAllByPeriodLight(DateTime dateFrom, DateTime dateBefore) {
            // Контекст базы данных
            using var context = new Context();

            return await context.Projects
               .Where(x => x.CreatedDateTime <= dateBefore && x.CreatedDateTime >= dateFrom)
               .Include(nameof(PDProject.Manager))
               .Include(nameof(PDProject.Strategist))
               .Include(nameof(PDProject.CreatedUser))
               .Include(nameof(PDProject.LastModifiedUser))
               .Include(nameof(PDProject.TargetAudience))
               .Include($"{nameof(PDProject.Brand)}.{nameof(DCBrand.Advertiser)}")
               .ToListAsync();
        }

        /// <summary>
        ///     Метод запроса поиска проекта по его идентификатору
        /// </summary>
        /// <param name="id">Идентификатор проекта</param>
        /// <param name="externalContext">Внешний контекст</param>
        /// <returns>Проект</returns>
        public static async Task<PDProject> FindFullByID(int id, Context? externalContext = null) {
            // Контекст базы данных
            var context = externalContext ?? new Context();

            var project = await context.Projects
               .Include(nameof(PDProject.Manager))
               .Include(nameof(PDProject.Strategist))
               .Include(nameof(PDProject.ViewPreferences))
               .Include(
                    $"{nameof(PDProject.CreatedUser)}." +
                    $"{nameof(DCUser.Roles)}."          +
                    $"{nameof(DCUserRole.Role)}"
                )
               .Include(
                    $"{nameof(PDProject.LastModifiedUser)}." +
                    $"{nameof(DCUser.Roles)}."               +
                    $"{nameof(DCUserRole.Role)}"
                )
               .Include(nameof(PDProject.TargetAudience))
                // Бренд, Рекламодатель
               .Include($"{nameof(PDProject.Brand)}.{nameof(DCBrand.Advertiser)}")
                // Регионы, Сезонные коэффициенты
               .Include(
                    $"{nameof(PDProject.Regions)}."        +
                    $"{nameof(PDRegion.Region)}."          +
                    $"{nameof(DCRegion.SeasonalOddsSet)}." +
                    $"{nameof(DCSeasonalOddsSet.SeasonalOdds)}"
                )
                // Регионы, Наценки за хронометраж
               .Include(
                    $"{nameof(PDProject.Regions)}." +
                    $"{nameof(PDRegion.Region)}."   +
                    $"{nameof(DCRegion.TimingMargins)}"
                )
                // Каналы, Лимиты качеств
               .Include(
                    $"{nameof(PDProject.Regions)}." +
                    $"{nameof(PDRegion.Channels)}." +
                    $"{nameof(PDChannel.Channel)}." +
                    $"{nameof(DCChannel.QualityLimits)}"
                )
                // Каналы, Рейтинги
               .Include(
                    $"{nameof(PDProject.Regions)}." +
                    $"{nameof(PDRegion.Channels)}." +
                    $"{nameof(PDChannel.Channel)}." +
                    $"{nameof(DCChannel.Ratings)}"
                )
                // Каналы, Базовая аудитория
               .Include(
                    $"{nameof(PDProject.Regions)}." +
                    $"{nameof(PDRegion.Channels)}." +
                    $"{nameof(PDChannel.Channel)}." +
                    $"{nameof(DCChannel.Audience)}"
                )
                // Каналы, Аудитории
               .Include(
                    $"{nameof(PDProject.Regions)}."   +
                    $"{nameof(PDRegion.Channels)}."   +
                    $"{nameof(PDChannel.Audiences)}." +
                    $"{nameof(PDAudience.Audience)}"
                )
                // Месяца, Недели, Хронометражи
               .Include(
                    $"{nameof(PDProject.Regions)}." +
                    $"{nameof(PDRegion.Channels)}." +
                    $"{nameof(PDChannel.Months)}."  +
                    $"{nameof(PDMonth.Weeks)}."     +
                    $"{nameof(PDWeek.Timings)}"
                )
               .FirstOrDefaultAsync(x => x.ID == id);

            if (externalContext == null) {
                context.Dispose();
            }

            return project;
        }

        /// <summary>
        ///     Метод запроса удалений старых проектов из базы данных
        /// </summary>
        /// <param name="project"></param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task DeleteAsync(PDProject project) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }

            // Контекст базы данных
            using var context = new Context();

            // Проект из базы данных
            var dbProject = await context.Projects.FirstOrDefaultAsync(x => x.ID == project.ID);

            // Если не найден завершаем метод
            if (dbProject == null) {
                return;
            }

            // Удаляем проект из базы данных
            context.Projects.Remove(dbProject);

            // Применяем изменение базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса создание новых проектов в базу данных
        /// </summary>
        /// <param name="project"></param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task InsertAsync(PDProject project, bool isFirstInitialization = true) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }

            // Контекст базы данных
            using var context = new Context();

            // Добавляем пользователя в проект
            project.CreatedUserID = user.ID;
            project.LastModifiedUserID = user.ID;

            // Нормализация данных проекта
            project.Brand = null;
            project.Manager = null;
            project.Strategist = null;
            project.CreatedUser = null;
            project.LastModifiedUser = null;
            project.TargetAudience = null;

            // Нормализация данных регионов проекта
            foreach (var region in project.Regions) {
                region.Region = null;

                // Нормализация данных каналов региона
                foreach (var channel in region.Channels) {
                    channel.Channel = null;

                    // Нормализация данных аудиторий канала
                    foreach (var audience in channel.Audiences.Where(audience => audience.DictionaryID != 0)) {
                        audience.Audience = null;
                    }
                }
            }

            project.IsFirstInitialization = isFirstInitialization;

            context.Projects.Add(project);

            // Применяем изменение базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод обновления данных проекта
        /// </summary>
        /// <param name="vm">Модель проекта, конвертированная из модели представления</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task UpdateProject(PDProject vm, bool fromPropertiesDW = false) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }

            // Контекст базы данных
            using var context = new Context();

            // Данные проекта из базы данных
            var db = await FindFullByID(vm.ID, context);

            // Обновление и добавление сущностей
            foreach (var region in vm.Regions.ToList()) {
                foreach (var channel in region.Channels.ToList()) {
                    foreach (var month in channel.Months.ToList()) {
                        foreach (var week in month.Weeks.ToList()) {
                            foreach (var timing in week.Timings.ToList()) {
                                timing.Week = null;
                                context.TimingsPD.AddOrUpdate(timing);
                            }

                            week.Month = null;
                            context.WeeksPD.AddOrUpdate(week);
                        }

                        month.Channel = null;
                        context.MonthsPD.AddOrUpdate(month);
                    }

                    channel.Region = null;
                    context.ChannelsPD.AddOrUpdate(channel);
                }

                region.Project = null;
                context.RegionsPD.AddOrUpdate(region);
            }

            // Обновление или добавление настроек проекта
            if (vm.ViewPreferences != null) {
                vm.ViewPreferences.Project = null;
                context.ViewPreferencesPD.AddOrUpdate(vm.ViewPreferences);
            }

            // Обновление или добавление проекта
            vm.ViewPreferences = null;
            vm.LastModifiedDateTime = DateTime.Now;
            // Определяем пользователя приложения
            vm.LastModifiedUserID = user.ID;
            vm.LastModifiedUser = null;
            context.Projects.AddOrUpdate(vm);

            // Если вызвано из свойсв проекта (из списка проектов), то не проверяется старое сырьё
            if (fromPropertiesDW) {
                // Сохраняем изменение в базу данных
                await context.SaveChangesAsync();
                return;
            }

            // Удаление неактуальных регионов
            var actualRegionIDs = vm.Regions.Select(r => r.DictionaryID).ToHashSet();
            foreach (var region in db.Regions.Where(r => !actualRegionIDs.Contains(r.DictionaryID)).ToList()) {
                context.RegionsPD.Remove(region);
            }

            // Удаление неактуальных каналов
            foreach (var region in db.Regions) {
                var actualChannelIDs = vm.Regions
                   .FirstOrDefault(r => r.DictionaryID == region.DictionaryID)
                  ?.Channels.Select(c => c.DictionaryID)
                   .ToHashSet() ?? new HashSet<int>();
                foreach (var channel in region.Channels.Where(c => !actualChannelIDs.Contains(c.DictionaryID))
                   .ToList()) {
                    context.ChannelsPD.Remove(channel);
                }
            }

            // Удаление неактуальных месяцев
            foreach (var region in db.Regions) {
                foreach (var channel in region.Channels) {
                    var actualMonthDates = vm
                       .Regions.FirstOrDefault(r => r.DictionaryID  == region.DictionaryID)?
                       .Channels.FirstOrDefault(c => c.DictionaryID == channel.DictionaryID)?
                       .Months.Select(m => m.Date).ToList() ?? new List<DateTime>();
                    foreach (var month in channel.Months.Where(m => !actualMonthDates.Contains(m.Date)).ToList()) {
                        context.MonthsPD.Remove(month);
                    }
                }
            }

            // Удаление неактульных недель
            foreach (var region in db.Regions) {
                foreach (var channel in region.Channels) {
                    foreach (var month in channel.Months) {
                        var actualWeekDates = vm
                           .Regions.FirstOrDefault(r => r.DictionaryID  == region.DictionaryID)?
                           .Channels.FirstOrDefault(c => c.DictionaryID == channel.DictionaryID)?
                           .Months.FirstOrDefault(m => m.Date           == month.Date)?
                           .Weeks.Select(w => w.DateFrom).ToList() ?? new List<DateTime>();
                        foreach (var week in month.Weeks.Where(w => !actualWeekDates.Contains(w.DateFrom)).ToList()) {
                            context.WeeksPD.Remove(week);
                        }
                    }
                }
            }

            // Удаление неактульных хронометражей
            foreach (var region in db.Regions) {
                foreach (var channel in region.Channels) {
                    foreach (var month in channel.Months) {
                        foreach (var week in month.Weeks) {
                            var actualTimings = vm
                               .Regions.FirstOrDefault(r => r.DictionaryID  == region.DictionaryID)?
                               .Channels.FirstOrDefault(c => c.DictionaryID == channel.DictionaryID)?
                               .Months.FirstOrDefault(m => m.Date           == month.Date)?
                               .Weeks.FirstOrDefault(w => w.DateFrom        == week.DateFrom)?
                               .Timings.Select(t => t.Value).ToHashSet() ?? new HashSet<int>();
                            foreach (var timing in week.Timings.Where(t => !actualTimings.Contains(t.Value)).ToList()) {
                                context.TimingsPD.Remove(timing);
                            }
                        }
                    }
                }
            }

            // Сохраняем изменение в базу данных
            await context.SaveChangesAsync();
        }
    }
}
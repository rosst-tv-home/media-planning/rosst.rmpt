﻿using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryUsers {
        /// <summary>
        ///     Метод запроса текущего пользователя приложения
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task<DCUser> GetCurrentUser() {
            // Контекст базы данных
            using var context = new Context();
            // Поиск пользователя по логину windows
            return await context.Users.Include(user => user.Roles).AsNoTracking().FirstAsync(user => user.Login.Equals(
                Environment.UserName,
                StringComparison.InvariantCultureIgnoreCase
            ));
        }

        /// <summary>
        ///     Метод получения всех пользователей из базы данных
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task<List<DCUser>> FindAllWithRolesAsync() {
            // Контекст базы данных
            using var context = new Context();

            // Возвращаем коллекцию пользователей
            return await context.Users
                // Подключение ролей
               .Include(user => user.Roles)
               .AsNoTracking()
               .ToListAsync();
        }

        /// <summary>
        ///     Метод запроса добавления новых пользователей в базу данных
        /// </summary>
        /// <param name="users">Коллекция действительных пользователей</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task InsertAsync(IEnumerable<DCUser> users) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Итерируемся по коллекции новых пользователей
            foreach (var item in users.Where(user => user.ID == 0).ToList()) {
                // Итерируемя по ролям пользователя
                foreach (var role in item.Roles) {
                    // Нормализуем модель
                    role.RoleID = role.Role?.ID ?? 0;
                    role.Role = null;
                    role.UserID = item.ID;
                    role.User = item;
                }

                // Добавляем пользователя в базу данных
                context.Users.Add(item);
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса удаления недействительных пользователей
        /// </summary>
        /// <param name="users">Коллекция действительных пользователей</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task DeleteAsync(IEnumerable<DCUser> users) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Идентификаторы действительных пользователей
            var actualUsersIdList = users
               .Select(item => item.ID)
               .ToHashSet();

            // Определяем недействительных пользователей
            var invalidUsers = await context.Users
               .Where(u => !actualUsersIdList.Contains(u.ID))
               .ToListAsync();

            // Итерируемся по коллекции недействительных пользователей
            foreach (var item in invalidUsers) {
                // Удаляем пользователя из базы данных
                context.Users.Remove(item);
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса обновления действительных пользователей
        /// </summary>
        /// <param name="users">Коллекция действительных пользователей</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task UpdateAsync(ICollection<DCUser> users) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Получаем коллекция пользователей из базы данных
            var dbUsers = await context.Users
               .Include(u => u.Roles)
               .ToListAsync();

            // Итерируемся по коллекции пользователей из базы данных
            foreach (var item in dbUsers) {
                // Определяем обновлённые данные пользователя
                var userData = users.FirstOrDefault(x => x.ID == item.ID);
                // Если обновлённые данные не были найдены - переходим к следующему пользователю
                if (userData == null) {
                    continue;
                }

                // Обновляем логин
                item.Login = !string.IsNullOrWhiteSpace(userData.Login.Trim())
                    ? userData.Login.Trim()
                    : item.Login;
                // Обновляем имя
                item.FirstName = !string.IsNullOrWhiteSpace(userData.FirstName.Trim())
                    ? userData.FirstName.Trim()
                    : item.FirstName;
                // Обновляем фамилию
                item.LastName = !string.IsNullOrWhiteSpace(userData.LastName.Trim())
                    ? userData.LastName.Trim()
                    : item.LastName;
                // Обновляем признак "активен"
                item.IsActive = userData.IsActive;

                // Нормализуем коллекцию пользовательских ролей
                foreach (var role in userData.Roles) {
                    role.RoleID = role.Role?.ID ?? 0;
                    role.Role = null;
                    role.UserID = item.ID;
                    role.User = null;
                }

                // Очищаем коллекцию пользовательских ролей
                item.Roles.Clear();
                // Добавляем обновлённую коллекцию пользовательских ролей
                userData.Roles.ForEach(item.Roles.Add);
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }
    }
}
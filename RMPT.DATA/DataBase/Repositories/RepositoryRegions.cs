﻿using RMPT.DATA.DataBase.Entities.Dictionaries;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryRegions {
        /// <summary>
        ///     Метод запроса всех регионов с каналами из базы данных
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        public static async Task<List<DCRegion>> FindAllOnlyWithChannels() {
            using var context = new Context();
            return await context.Regions
               .Include(
                    $"{nameof(DCRegion.Channels)}." +
                    $"{nameof(DCChannel.Audience)}"
                )
               .ToListAsync();
        }

        /// <summary>
        ///     Метод получения всех регионов без каналов из базы данных
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        public static async Task<List<DCRegion>> FindAllWithoutChannels() {
            // Контекст базы данных
            using var context = new Context();

            // Возвращаем коллекцию регионов
            return await context.Regions
               .Include(nameof(DCRegion.NameVariants))
               .Include(nameof(DCRegion.TimingMargins))
               .Include($"{nameof(DCRegion.SeasonalOddsSet)}.{nameof(DCSeasonalOddsSet.SeasonalOdds)}")
               .OrderByDescending(region => region.IsMeasurable)
               .ThenBy(region => region.Name)
               .AsNoTracking()
               .ToListAsync();
        }

        public static async Task<DCRegion?> FindByID(int id) {
            // Контекст базы данных
            using var context = new Context();
            return await context.Regions
               .Include(region => region.NameVariants)
               .Include(region => region.SeasonalOddsSet)
               .Include(region => region.TimingMargins)
               .AsNoTracking()
               .FirstOrDefaultAsync(region => region.ID == id);
        }

        public static async Task<DCRegion?> FindByName(string name) {
            // Контекст базы данных
            using var context = new Context();
            return await context.Regions
               .Include(region => region.NameVariants)
               .Include(region => region.SeasonalOddsSet)
               .Include(region => region.TimingMargins)
               .AsNoTracking()
               .FirstOrDefaultAsync(region =>
                    region.Name.Trim().Equals(name.Trim(), StringComparison.InvariantCultureIgnoreCase) ||
                    region.NameVariants.Any(regionNameVariant => regionNameVariant.Name.Trim().Equals(
                        name.Trim(),
                        StringComparison.InvariantCultureIgnoreCase
                    ))
                );
        }

        /// <summary>
        ///     Метод получение наименование региона из базы данных
        /// </summary>
        /// <param name="regionName">Наименование</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task<string?> FindRegionName(string regionName) {
            // Контекст базы данных
            using var context = new Context();
            // Ищем регион по его наименованию или варианту наименованию
            var dbRegionName = await context.Regions
               .Include(region => region.NameVariants)
               .FirstOrDefaultAsync(region =>
                    region.Name.Trim().Equals(regionName.Trim(), StringComparison.InvariantCultureIgnoreCase) ||
                    region.NameVariants.Any(regionNameVariant => regionNameVariant.Name.Trim().Equals(
                        regionName.Trim(),
                        StringComparison.InvariantCultureIgnoreCase
                    ))
                );

            // Возвращаем наименование региона
            return dbRegionName.Name;
        }

        /// <summary>
        ///     Метод запроса удаление старых регионов из базы данных
        /// </summary>
        /// <param name="regions"></param>
        /// <returns></returns>
        public static async Task DeleteAsync(IEnumerable<DCRegion> regions) {
            // Контекст базы данных
            using var context = new Context();
            // Коллекция действительных индентификаторов регионов
            var regionIdList = regions.Select(x => x.ID).ToList();

            // Коллекция из баззы данных не действительных регионов 
            var bdRegion = context.Regions.Where(x => !regionIdList.Contains(x.ID));

            // Итерируемся по коллекции не действительных регионов
            foreach (var region in bdRegion) {
                // Удаляем регион из базы данных
                context.Regions.Remove(region);
            }

            // Применяем изменение базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса обновление старых регионов
        /// </summary>
        /// <param name="regions"></param>
        /// <returns></returns>
        public static async Task UpdateAsync(ICollection<DCRegion> regions) {
            // Контекст базы данныъ
            using var context = new Context();

            // Коллекция действительных индентификаторов регионов
            var regionsList = regions.Where(x => x.ID != 0).Select(x => x.ID);

            // Коллекция действительных регионов из базы данных
            var regionsBase = await context.Regions.Where(x => regionsList.Contains(x.ID))
               .Include(region => region.NameVariants)
               .Include(region => region.SeasonalOddsSet)
               .Include(region => region.TimingMargins)
               .ToListAsync();

            // Итерируемся по коллекции действительных регионов базы данных
            foreach (var region in regionsBase) {
                // Ищем свежие данные регионов
                var dataRegion = regions.FirstOrDefault(x => x.ID == region.ID);
                // Если не нашли продолжаем
                if (dataRegion == null) {
                    continue;
                }

                // Иначе обновляем наименование региона
                region.Name = dataRegion.Name;

                // Обновляем признак измеряемости
                region.IsMeasurable = dataRegion.IsMeasurable;

                // Итерируемся по вариантам наименованиям региона
                foreach (var regionNameVariant in region.NameVariants.ToList()) {
                    // Ищем свежие данные вариантов наименование 
                    var dataNameVariant = dataRegion.NameVariants.FirstOrDefault(x => x.Id == regionNameVariant.Id);
                    // Если не нашли или нет наименования отмечаем как удаленные и переходим к следующему
                    if (dataNameVariant == null || string.IsNullOrWhiteSpace(regionNameVariant.Name)) {
                        context.Entry(regionNameVariant).State = EntityState.Deleted;
                        continue;
                    }

                    // Иначе обновляем вариант наимнование
                    regionNameVariant.Name = dataNameVariant.Name;
                }

                // Итерируемся по сезонным коэффициента региона
                // foreach (var seasonCoefficient in region.SeasonCoefficients.ToList()) {
                //     // Ищем свежие данные коэффициентов регионов
                //     var dataSeasonCoefficient =
                //         dataRegion.SeasonCoefficients.FirstOrDefault(x => seasonCoefficient.Id == x.Id);
                //     // Если не нашли отмечаем как удаленные и переходим к следующему
                //     if (dataSeasonCoefficient == null) {
                //         context.Entry(seasonCoefficient).State = EntityState.Deleted;
                //         continue;
                //     }
                //
                //     // Иначе обнавляем данные коэффициента
                //     seasonCoefficient.Date = dataSeasonCoefficient.Date;
                //     seasonCoefficient.Value = dataSeasonCoefficient.Value;
                // }

                // Итерируемся по наценкам за хронометраж
                foreach (var timingMargin in region.TimingMargins.ToList()) {
                    // Ищем свежие данные наценок за хронометраж
                    var dataTimingMargin = dataRegion.TimingMargins.FirstOrDefault(x => timingMargin.Id == x.Id);
                    // Если не нашли или меньше пяти или деление на пять без остатка то отмечаем как удаленные и переходим к следующему 
                    if (dataTimingMargin == null || dataTimingMargin.Timing < 5 || dataTimingMargin.Timing % 5 != 0) {
                        context.Entry(timingMargin).State = EntityState.Deleted;
                        continue;
                    }

                    // Иначе обновляем данные наценок за хронометраж
                    timingMargin.Date = dataTimingMargin.Date;
                    timingMargin.Timing = dataTimingMargin.Timing;
                    timingMargin.Value = dataTimingMargin.Value;
                }

                // Итерируемся по новым вариантов наименований
                dataRegion.NameVariants
                   .Where(nameVariant =>
                        !string.IsNullOrWhiteSpace(nameVariant.Name) &&
                        region.NameVariants.All(x => x.Id != nameVariant.Id)
                    )
                   .ToList()
                   .ForEach(x => {
                        // Нормализация данных
                        x.RegionId = region.ID;
                        x.Region = null;
                        // Добавляем новый вариант наименований в коллекцию базы данных
                        region.NameVariants.Add(x);
                    });

                // Итерируемся по новым сезонный коэффициентам
                // dataRegion.SeasonCoefficients
                //    .Where(seasonCoefficient => region.SeasonCoefficients.All(x => x.Id != seasonCoefficient.Id))
                //    .ToList()
                //    .ForEach(x => {
                //         // Нормализация данных
                //         x.RegionId = region.ID;
                //         x.Region = null;
                //         // Добавляем новый сезонный коэффициент в коллекцию базы данных
                //         region.SeasonCoefficients.Add(x);
                //     });

                // Итерируемся по новым наценкам за хронометраж
                dataRegion.TimingMargins
                   .Where(timingMargin => region.TimingMargins.All(x => x.Id != timingMargin.Id))
                   .ToList()
                   .ForEach(x => {
                        // Номрализация данных
                        x.RegionId = region.ID;
                        x.Region = null;
                        // Добавляем новую наценку за хронометраж в коллекцию базы данных
                        region.TimingMargins.Add(x);
                    });
            }

            // Применяем изменение базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса создание новых региново
        /// </summary>
        /// <param name="regions"></param>
        /// <returns></returns>
        public static async Task InsertAsync(IEnumerable<DCRegion> regions) {
            // Контекст базы данных
            using var context = new Context();

            // Итерируемся по новым регионам
            foreach (var region in regions.Where(x => x.ID == 0)) {
                var lastID = (await FindAllWithoutChannels()).Max(r => r.ID) + 1;
                region.ID = lastID;
                // Итерируемся по вариантам наименований
                foreach (var nameVariant in region.NameVariants) {
                    // Нормализация данных
                    nameVariant.Region = null;
                }

                // Итерируемся по сезонным коэффициентам
                // foreach (var seasonCoefficient in region.SeasonCoefficients) {
                //     // Нормализация данных
                //     seasonCoefficient.Region = null;
                // }

                // Итерируемся по наценкам за хронометраж
                foreach (var timingMargin in region.TimingMargins) {
                    // Нормализация данных
                    timingMargin.Region = null;
                }

                // Добавляем новый регион в коллекцию базы данных
                context.Regions.Add(region);
            }

            // Применяем изменение базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса добавления нового региона в базу данных
        /// </summary>
        /// <param name="dcRegion">Регион</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task<DCRegion> InsertSingleAsync(DCRegion dcRegion) {
            // Контекст базы данных
            using var context = new Context();

            // Итерируемся по вариантам наименований
            foreach (var nameVariant in dcRegion.NameVariants) {
                // Нормализация данных
                nameVariant.Region = null;
            }

            // Итерируемся по сезонным коэффициентам
            // foreach (var seasonCoefficient in region.SeasonCoefficients) {
            //     // Нормализация данных
            //     seasonCoefficient.Region = null;
            // }

            // Итерируемся по наценкам за хронометраж
            foreach (var timingMargin in dcRegion.TimingMargins) {
                // Нормализация данных
                timingMargin.Region = null;
            }

            // Добавляем новый регион в коллекцию базы данных
            var entity = context.Regions.Add(dcRegion);

            // Применяем изменение базы данных
            await context.SaveChangesAsync();

            // Возвращаем новый регион
            return entity;
        }
    }
}
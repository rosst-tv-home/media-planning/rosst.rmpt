﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryChannels {
        public static async Task<DCChannel?> FindByID(int id) {
            using var context = new Context();
            return await context.Channels
                // Лимиты качеств
               .Include(nameof(DCChannel.QualityLimits))
                // Рейтинги
               .Include(nameof(DCChannel.Ratings))
                // Базовая аудитория
               .Include(nameof(DCChannel.Audience))
               .FirstOrDefaultAsync(c => c.ID == id);
        }

        /// <summary>
        ///     Метод получения всех каналов из базы данных
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task<List<DCChannel>> FindAll() {
            using var context = new Context();
            return await context.Channels
               .Include(channel => channel.NameVariants)
               .Include(channel => channel.Audience)
               .Include(channel => channel.Ratings)
               .Include(channel => channel.Region)
               .AsNoTracking()
               .ToListAsync();
        }

        /// <summary>
        ///     Метод запроса каналов неизмеряемых регионов
        /// </summary>
        /// <returns>Каналы неизмеряемых регионов</returns>
        public static async Task<List<DCChannel>> FindAllNotMeasurable() {
            using var context = new Context();
            return await context.Channels
               .Include(nameof(DCChannel.Region))
               .Where(c => !c.Region!.IsMeasurable)
               .AsNoTracking()
               .ToListAsync();
        }

        /// <summary>
        ///     Метод запроса идентификаторов измеряемых каналов
        /// </summary>
        /// <returns>Идентификаторы измеряемых каналов</returns>
        public static async Task<List<int>> FindAllMeasurableIDs() {
            using var context = new Context();
            return await context.Channels
               .Where(x => x.Region!.IsMeasurable)
               .Select(x => x.ID)
               .ToListAsync();
        }

        public static async Task<List<int>> FindAllIDs() {
            // Контекст базы данных
            using var context = new Context();

            // Возвращаем идентификаторы каналов
            return await context.Channels.Select(x => x.ID).ToListAsync();
        }

        public static async Task<DCChannel?> FindByRegionAndChannelName(string regionName, string channelName) {
            // Контекст базы данных
            using var context = new Context();

            return await context.Channels
               .Include(channel => channel.Region)
               .Include(channel => channel.Audience)
               .FirstOrDefaultAsync(channel =>
                    // Наименование региона или его варианты наименований совпадают
                    (channel.Region!.Name.Trim().Equals(
                        regionName.Trim(),
                        StringComparison.InvariantCultureIgnoreCase
                    ) || channel.Region.NameVariants.Any(regionNameVariant => regionNameVariant.Name.Trim().Equals(
                        regionName.Trim(),
                        StringComparison.InvariantCultureIgnoreCase
                    ))) &&
                    // Наименование канала или его варианты наименований совпадают
                    (channel.Name.Trim().Equals(
                        channelName.Trim(),
                        StringComparison.InvariantCultureIgnoreCase
                    ) || channel.NameVariants.Any(channelNameVariant => channelNameVariant.Name.Trim().Equals(
                        channelName.Trim(),
                        StringComparison.InvariantCultureIgnoreCase
                    )))
                );
        }

        /// <summary>
        ///     Метод запроса обновления каналов в базе данных
        /// </summary>
        /// <param name="channels">Коллекция действительных каналов</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task UpdateAsync(ICollection<DCChannel> channels) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Коллекция идентификаторов действительных каналов
            var channelsIdList = channels
               .Select(channel => channel.ID)
               .ToHashSet();

            // Каналы базы данных
            var dataBaseChannels = await context.Channels
               .Where(channel => channelsIdList.Contains(channel.ID))
               .Include(channel => channel.NameVariants)
               .Include(channel => channel.Audience)
               .Include(channel => channel.Ratings)
               .ToListAsync();

            // Итерируемся по каналам базы данных, данные которых требуется обновить
            foreach (var channel in dataBaseChannels) {
                // Ищем обновлённые данные канала
                var channelData = channels.FirstOrDefault(x => x.ID == channel.ID);
                // Если данные не нашлись - переходим к следующему каналу
                if (
                    channelData == null ||
                    string.IsNullOrWhiteSpace(channelData.Name.Trim())
                ) {
                    continue;
                }

                // Обновляем данные канала
                channel.RegionID = channelData.Region?.ID ?? channel.RegionID;
                channel.Region = null;
                channel.Name = await NormalizeChannelName(channelData.Name);

                // Определяем и итерируемся по недействительным вариантам наименований каналов
                foreach (var nameVariant in channel.NameVariants
                   .Where(nameVariant => channelData.NameVariants.All(x => x.ID != nameVariant.ID))
                   .ToList()
                ) {
                    // Удаляем недействительный вариант наименования канала
                    context.Entry(nameVariant).State = EntityState.Deleted;
                }

                // Итерируемся по действительным вариантам наименований
                foreach (var nameVariant in channel.NameVariants) {
                    // Ищем обновлённые данные варианта наименования
                    var nameVariantData = channelData.NameVariants.FirstOrDefault(x => x.ID == nameVariant.ID);
                    // Если данные не нашлись - переходим к следующему варианту наименования
                    if (nameVariantData == null || string.IsNullOrWhiteSpace(nameVariantData.Name.Trim())) {
                        continue;
                    }

                    // Обновляем данные варианта наименования
                    nameVariant.Name = nameVariantData.Name.Trim();
                }

                // Определяем и итерируемся по новым вариантам наименований
                foreach (var nameVariant in channelData.NameVariants.Where(nameVariant =>
                    nameVariant.ID == 0 &&
                    !string.IsNullOrWhiteSpace(nameVariant.Name.Trim())
                ).ToList()) {
                    // Нормализируем данные варианта наименования
                    nameVariant.ChannelID = nameVariant.Channel?.ID ?? channel.ID;
                    nameVariant.Channel = null;
                    nameVariant.Name = nameVariant.Name.Trim();

                    // Добавляем новый вариант наименования в коллекцию
                    channel.NameVariants.Add(nameVariant);
                }

                // Определяем и итерируемся по недействительным рейтингам
                foreach (var rating in channel.Ratings
                   .Where(rating => channelData.Ratings.All(x => x.ID != rating.ID))
                   .ToList()
                ) {
                    // Удаляем недействительный рейтинги
                    context.Entry(rating).State = EntityState.Deleted;
                }

                // Обновление рейтинга канала 
                foreach (var rating in channel.Ratings) {
                    // Ищем обновлённые данные варианта 
                    var channelRatingData =
                        channelData.Ratings.FirstOrDefault(x => x.ID == rating.ID && x.Date == rating.Date);
                    // Если данные не нашлись 
                    if (channelRatingData == null) {
                        continue;
                    }

                    // Обновляем данные
                    rating.Prime = channelRatingData.Prime;
                    rating.OffPrime = channelRatingData.OffPrime;
                }

                // Определяем и итерируемся по рейтинргам каналов которых нет в баз данных
                foreach (var rating in channelData.Ratings
                   .Where(x => x.ID == 0)
                   .ToList()
                ) {
                    // Нормализируем данные 
                    rating.ChannelID = channel.ID;
                    rating.Channel = null;

                    rating.Prime = rating.Prime;
                    rating.OffPrime = rating.OffPrime;

                    // Добавляем новые данные цены в коллекцию
                    channel.Ratings.Add(rating);
                }
            }

            // Применяем изменения базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса добавления новых каналов в базу данных
        /// </summary>
        /// <param name="channels">Каналы</param>
        /// <returns>
        ///     Задача <see cref="Task" /> добавления новых каналов <see cref="List{T}"/> в базу данных
        /// </returns>
        public static async Task InsertAsync(IEnumerable<DCChannel> channels) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            var context = new Context();

            // Итерируемся по новым каналам
            foreach (var channel in channels) {
                await InsertSingleAsync(channel);
            }

            // Применяем изменения базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса добавления нового канала в базу данных
        /// </summary>
        /// <param name="dcChannel">Канал</param>
        /// <param name="context">Контекст базы данных</param>
        /// <returns>
        ///     Задача <see cref="Task" /> добавления нового канала <see cref="DCChannel"/> в базу данных
        /// </returns>
        public static async Task<DCChannel> InsertSingleAsync(DCChannel dcChannel, Context? context = null) {
            #region Создание контекста

            // Признак "Был передан контекст базы данных"
            var isContext = context != null;

            // Если контекст базы данных не был передан - создаём новый
            context ??= new Context();

            #endregion

            #region Регион

            // Удаляем ссылку на регион
            dcChannel.RegionID = dcChannel.RegionID == 0 ? dcChannel.Region?.ID ?? 0 : dcChannel.RegionID;
            dcChannel.Region = null;

            #endregion

            #region Наименования

            // Нормализируем наименование канала
            dcChannel.Name = !string.IsNullOrWhiteSpace(dcChannel.Name)
                ? await NormalizeChannelName(dcChannel.Name.Trim())
                : throw new Exception("Наименование канала не может быть пустым");

            #endregion

            #region Варианты наименований

            // Валидные варианты наименований канала
            var nameVariants = dcChannel.NameVariants
                // Отбрасываем пустые варианты наименвоаний
               .Where(nameVariant => !string.IsNullOrWhiteSpace(nameVariant.Name))
                // Группируем по наименованию
               .GroupBy(nameVariant => nameVariant.Name.Trim().ToLowerInvariant())
                // Берём только первый вариант из группы
               .Select(group => group.First())
               .ToList();
            // Нормализируем варианты наименований канала
            nameVariants.ForEach(nameVariant => nameVariant.Name = nameVariant.Name.Trim());
            // Очищаем старые варианты наименований канала
            dcChannel.NameVariants.Clear();
            // Добавляем нормализированные варианты наименований канала
            dcChannel.NameVariants.AddRange(nameVariants);

            #endregion

            dcChannel.Audience = null;

            if (string.IsNullOrWhiteSpace(dcChannel.SalePoint)) {
                dcChannel.SalePoint = "НРА";
            }

            if (dcChannel.SaleType == 0) {
                dcChannel.SaleType = DCSaleType.Minute;
            }

            if (dcChannel.Audience == null && dcChannel.AudienceID == 0) {
                dcChannel.Audience = await RepositoryAudiences.FindByName("ALL 4+");
                dcChannel.AudienceID = dcChannel.Audience?.ID ?? 0;
                dcChannel.Audience = null;
            }

            if (dcChannel.QualityLimits != null) {
                dcChannel.QualityLimitsID = dcChannel.QualityLimits.ID;
                dcChannel.QualityLimits = null;
            } else if (dcChannel.QualityLimitsID == 0) {
                dcChannel.QualityLimitsID = 999;
            }

            if (dcChannel.ID == 0) {
                var ids = await FindAllIDs();
                if (ids.Count <= 0) {
                    dcChannel.ID = 1;
                } else dcChannel.ID = ids.Max() + 1;
            }

            #region Утилизация контекста

            // Добавляем канал в базу данных
            context.Channels.Add(dcChannel);

            // Если при вызове метода был передан контекст
            if (isContext) {
                // Возвращаем сущность
                return dcChannel;
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();

            // Утилизируем созданный контекст
            context.Dispose();

            // Возвращаем сохранённую сущность
            return dcChannel;

            #endregion
        }

        /// <summary>
        ///     Метод запроса удаления старых каналов из базы данных
        /// </summary>
        /// <param name="channels">Коллекция действительных каналов</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task DeleteAsync(IEnumerable<DCChannel> channels) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            var context = new Context();

            // Коллекция идентификаторов действительных каналов
            var channelsIdList = channels
               .Select(channel => channel.ID)
               .ToHashSet();

            // Итерируемся по каналам базы данных, которых нет в коллекции дейтсвительных каналов
            foreach (var channel in await context.Channels
               .Where(channel => !channelsIdList.Contains(channel.ID))
               .ToListAsync()
            ) {
                // Удаляем канал из базы данных
                context.Channels.Remove(channel);
            }

            // Применяем изменения базы данных
            await context.SaveChangesAsync();
        }

        public static async Task AddOrUpdateDictionaryRatings(IEnumerable<DCRating> ratings) {
            using var context = new Context();
            foreach (var rating in ratings) {
                var dbRating = await context.Ratings.FirstOrDefaultAsync(r =>
                    r.ChannelID == rating.ChannelID &&
                    r.Date      == rating.Date
                );

                if (dbRating == null) {
                    context.Ratings.Add(rating);
                    continue;
                }

                dbRating.Prime = rating.Prime;
                dbRating.OffPrime = rating.OffPrime;
            }

            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Нормализация наименования канала
        /// </summary>
        /// <param name="name">Ненормализированное наименование канала</param>
        /// <returns>
        ///     Задача <see cref="Task" /> нормализации наименования <see cref="string"/> канала <see cref="DCChannel"/>
        /// </returns>
        private static Task<string> NormalizeChannelName(string name) {
            // Ищем совпадения по регулярному выражению
            var match = Regex.Match(
                name.Trim(),
                @"^(.+?)(\s?-?\s?(?<![a-zа-яA-ZА-Я\d])[oO0Оо])$"
            );

            // Группы совпадений
            var groups = match.Groups
               .Cast<Group>()
               .Skip(1)
               .Where(group => !string.IsNullOrWhiteSpace(group.Value))
               .ToList();

            // Возвращаем нормализированное наименование канала
            return groups.Count == 0
                // Если нет совпадений, значит канал не является орбитальным и не требует нормализации
                ? Task.FromResult(name)
                // Иначе нормализуем наименование орбитального канала
                : Task.FromResult(groups.ElementAt(0).Value.Trim() + " - 0");
        }
    }
}
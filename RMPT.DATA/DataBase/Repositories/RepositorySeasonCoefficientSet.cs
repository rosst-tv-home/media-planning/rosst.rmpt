﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositorySeasonCoefficientSet {
        /// <summary>
        ///     Метод запроса всех идентификаторов наборов сезонных коэффициентов
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        public static async Task<List<int>> FindAllSetIDs() {
            // Контекст базы данных
            using var context = new Context();

            // Возвращаем набор идентификаторов наборов сезонных коэффициентов 
            return await context.SeasonCoefficientSets.Select(x => x.ID).ToListAsync();
        }

        /// <summary>
        ///     Метод запроса наборов сезонных коэффициентов по указанным идентификаторам
        /// </summary>
        /// <param name="list">Идентификаторы наборов сезонных коэффициентов</param>
        /// <returns><see cref="Task"/></returns>
        public static async Task<List<DCSeasonalOddsSet>> FindAllByIDs(IEnumerable<int> list) {
            // Контекст базы данных
            using var context = new Context();

            // Возвращаем наборы сезонных коэффициентов
            return await context.SeasonCoefficientSets.Where(x => list.Contains(x.ID)).ToListAsync();
        }

        /// <summary>
        ///     Метод запроса добавления наборов сезонных коэффициентов в базу данных
        /// </summary>
        /// <param name="sets">Наборы сезонных коэффициентов</param>
        public static async Task InsertAllAsync(IEnumerable<DCSeasonalOddsSet> sets) {
            // Контекст базы данных
            using var context = new Context();

            // Итерируемся по наборам сезонных коэффициентов
            foreach (var set in sets.ToHashSet()) {
                // Нормализуем набор сезонных коэффициентов
                set.Regions.Clear();

                // Итерируемся по сезонным коэффициентам
                foreach (var seasonCoefficient in set.SeasonalOdds) {
                    // Нормализуем сезонный коэффициент
                    seasonCoefficient.SeasonalOddsSet = set;
                    seasonCoefficient.SeasonalOddsSetID = 0;
                }

                // Добавляем набор сезонных коэффициентов в базу данных
                context.SeasonCoefficientSets.Add(set);
            }

            // Ожидаем применение изменений в базу данных
            await context.SaveChangesAsync();
        }
    }
}
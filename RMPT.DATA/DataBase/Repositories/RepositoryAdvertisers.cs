﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Brand;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryAdvertisers {
        /// <summary>
        ///     Метод запроса всех рекламодателей и их брендов из базы данных
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task<List<DCAdvertiser>> FindAllWithBrandsAsync() {
            // Контекст базы данных
            using var context = new Context();
            // Возвращаем коллекцию всех рекламодателей
            return await context.Advertisers
                // Подключаем бренды
               .Include(advertiser => advertiser.Brands)
               .AsNoTracking()
               .ToListAsync();
        }

        /// <summary>
        ///     Метод запроса вставки новых рекламодателей в базу данных
        /// </summary>
        /// <param name="advertisers">Коллекция действительных рекламодателей</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task InsertAsync(IEnumerable<DCAdvertiser> advertisers) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Итерируемся по коллекции новых рекламодателей
            foreach (var advertiser in advertisers.Where(advertiser => advertiser.ID == 0).ToList()) {
                // Добавляем рекламодателя в базу данных
                context.Advertisers.Add(advertiser);
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса удаления старых рекламодателей из базы данных
        /// </summary>
        /// <param name="advertisers">Коллекция действительных рекламодателей</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task DeleteAsync(IEnumerable<DCAdvertiser> advertisers) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Идентификаторы действительных рекламодателей
            var advertisersIdList = advertisers
               .Select(advertiser => advertiser.ID)
               .ToHashSet();

            // Определяем рекламодателей базы данных,
            // которых нет в действительной коллекции рекламодателей - 
            // которых требуется удалить
            var oldAdvertisers = await context.Advertisers
               .Where(advertiser => !advertisersIdList.Contains(advertiser.ID))
               .ToListAsync();

            // Итерируемся по коллекции старых рекламодателей
            foreach (var advertiser in oldAdvertisers) {
                // Удаляем рекламодателя из базы данных
                context.Advertisers.Remove(advertiser);
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса обновления данных имеющихся рекламодателей в базе данных
        /// </summary>
        /// <param name="advertisers">Коллекция действительных рекламодателей</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task UpdateAsync(ICollection<DCAdvertiser> advertisers) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Идентификаторы действительных рекламодателей
            var advertisersIdList = advertisers
               .Select(advertiser => advertiser.ID)
               .ToHashSet();

            // Определяем рекламодателей базы данных,
            // которые в действительной коллекции рекламодателей - 
            // которые требуется обновить
            var existsAdvertisers = await context.Advertisers
               .Where(advertiser => advertisersIdList.Contains(advertiser.ID))
               .Include(advertiser => advertiser.Brands)
               .ToListAsync();

            // Итерируемся по коллекции имеющихся рекламодателей
            foreach (var advertiser in existsAdvertisers) {
                // Находим обновлённые данные рекламодателя
                var advertiserData = advertisers.FirstOrDefault(x => x.ID == advertiser.ID);
                // Если не нашли - переходим к следующему
                if (advertiserData == null) {
                    continue;
                }

                // Коллекция идентификаторов брендов рекламодателя
                var brandIdList = advertiser.Brands.Select(brand => brand.ID).ToHashSet();
                // Коллекция идентификаторов брендов обновлённых данных рекламодателя
                var brandDataIdList = advertiserData.Brands.Select(brand => brand.ID).ToHashSet();

                // Удаляем бренды, которых нет в коллекции брендов рекламодателя из действительной коллекции
                foreach (var brand in advertiser.Brands.Where(brand => !brandDataIdList.Contains(brand.ID)).ToList()) {
                    context.Entry(brand).State = EntityState.Deleted;
                }

                // Итерируемся по коллекции брендов рекламодателя из базы данных
                foreach (var brand in advertiser.Brands) {
                    // Определяем обновлённые данные бренда
                    var brandData = advertiserData.Brands.FirstOrDefault(x => x.ID.Equals(brand.ID));
                    // Если не нашли обновлённые данные - переходим к следующему
                    if (brandData == null) {
                        continue;
                    }

                    // Обновляем наименование бренда
                    brand.Name = brandData.Name;
                }

                // Добавляем новые бренды в коллекцию брендов рекламодателя
                advertiser.Brands.AddRange(advertiserData.Brands
                   .Where(brand => !brandIdList.Contains(brand.ID))
                   .ToList()
                );
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }
    }
}
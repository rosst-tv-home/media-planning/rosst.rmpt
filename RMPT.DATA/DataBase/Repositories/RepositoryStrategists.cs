﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Other;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryStrategists {
        /// <summary>
        ///     Метод запроса всех стратегов
        /// </summary>
        /// <returns>Коллекция стратегов</returns>
        public static async Task<List<DCStrategist>> FindAllAsync() {
            // Контекст базы данных
            using var context = new Context();

            // Запрашиваем коллекцию всех стратегов
            return await context.Strategists
               .AsNoTracking()
               .ToListAsync();
        }

        /// <summary>
        ///     Метод запроса вставки новых стратегов в базу данных
        /// </summary>
        /// <param name="strategists">Коллекция действительных стратегов</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task InsertAsync(IEnumerable<DCStrategist> strategists) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Итерируемся по коллекции новых стратегов
            foreach (var strategist in strategists.Where(strategist => strategist.ID == 0).ToList()) {
                // Добавляем стратега в базу данных
                context.Strategists.Add(strategist);
            }

            // Сохраняем изменения 
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса удаления старых стратегов из базы данных
        /// </summary>
        /// <param name="strategists">Коллекция действительных стратегов</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task DeleteAsync(IEnumerable<DCStrategist> strategists) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Идентификаторы дейсвтительных стратегов
            var strategistsIdList = strategists
               .Select(strategist => strategist.ID)
               .ToHashSet();

            // Определяем старых стратегов
            var oldStrategists = await context.Strategists
               .Where(strategist => !strategistsIdList.Contains(strategist.ID))
               .ToListAsync();

            // Итерируемся по коллекции старых стратегов
            foreach (var strategist in oldStrategists) {
                // Удаляем стратега из базы данных
                context.Strategists.Remove(strategist);
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса обновления данных имеющихся стратегов в базе данных
        /// </summary>
        /// <param name="strategists">Коллекция действительных стратегов</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task UpdateAsync(ICollection<DCStrategist> strategists) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Идентификаторы действительных стратегов
            var strategistsIdList = strategists
               .Select(strategist => strategist.ID)
               .ToHashSet();

            // Коллекция стратегов базы данных, данные которые требуется обновить
            var existsStrategists = await context.Strategists
               .Where(strategist => strategistsIdList.Contains(strategist.ID))
               .ToListAsync();

            // Итерируемся по коллекции имеющихся стратегов
            foreach (var strategist in existsStrategists) {
                // Определяем обновлённые данные стратега
                var strategistData = strategists.FirstOrDefault(x => x.ID == strategist.ID);
                // Если не нашли обновлённые данные - переходим к следующему стратегу
                if (strategistData == null) {
                    continue;
                }

                // Обновляем имя стратега
                strategist.FirstName = strategistData.FirstName.Trim();
                // Обновляем фамилию стратега
                strategist.LastName = strategistData.LastName.Trim();
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }
    }
}
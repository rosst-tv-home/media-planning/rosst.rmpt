﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryAudiences {
        /// <summary>
        ///     Метод получения всех базовых аудиторий из базы данных
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task<List<DCAudience>> FindAll() {
            using var context = new Context();
            return await context.Audiences
               .Include(audience => audience.Channels)
               .AsNoTracking()
               .ToListAsync();
        }

        /// <summary>
        ///     Метод запроса всех аудиторий, без запроса связанных сущностей
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        public static async Task<List<DCAudience>> FindAllWithoutInclude() {
            // Контекст базы данных
            using var context = new Context();
            
            // Возвращаем аудитории
            return await context.Audiences.ToListAsync();
        }

        /// <summary>
        ///     Метод поиска аудитории по наименованию
        /// </summary>
        /// <param name="audienceName">Наименование аудитории</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        public static async Task<DCAudience?> FindByName(string audienceName) {
            // Контекст базы данных
            using var context = new Context();

            return await context.Audiences.FirstOrDefaultAsync(audience => audience.Name.Trim().Equals(
                audienceName.Trim(),
                StringComparison.InvariantCultureIgnoreCase
            ));
        }

        /// <summary>
        ///     Метод запроса удалений старых аудиторий из базы данных
        /// </summary>
        /// <param name="audiences"></param>
        /// <returns></returns>
        public static async Task DeleteAsync(IEnumerable<DCAudience> audiences) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Коллекция индентификаторов действительных аудиторий
            var audienceIdList = audiences
               .Select(x => x.ID);

            // Коллекция аудиторий которые есть в базе данных 
            var dataBaseAudiences = await context.Audiences.ToListAsync();

            // Коллекция аудиторий которых нет в коллекции 
            var deleteBaseAudiences = dataBaseAudiences
               .Where(x => !audienceIdList.Contains(x.ID));

            // Итерируемся по аудиториям которых нет в коллекции
            foreach (var deleteBaseAudience in deleteBaseAudiences) {
                // Удаляем коллекцию из базы данных
                context.Audiences.Remove(deleteBaseAudience);
            }

            // Применяем изменение базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса обновлений старых аудиторий в базу данных
        /// </summary>
        /// <param name="audiences"></param>
        /// <returns></returns>
        public static async Task UpdateAsync(ICollection<DCAudience> audiences) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Коллекция действительных идентификаторов аудиторий
            var audienceIdList = audiences
               .Where(x => x.ID != 0)
               .Select(x => x.ID);

            // Коллекция аудиторий из базы данных
            var dataBaseAudiences = await context.Audiences.ToListAsync();

            // Коллекция действительных аудиторий из базы даных
            var updateAudiencesBaseList = dataBaseAudiences
               .Where(x => audienceIdList.Contains(x.ID));

            // Итерируемя по аудиториям которые нужно обновить
            foreach (var audience in updateAudiencesBaseList) {
                // Ищем новые данные
                var audienceData = audiences.FirstOrDefault(x => x.ID == audience.ID);
                // Если нет то переходим к следующей аудитории
                if (audienceData == null) {
                    continue;
                }

                // Обновляем данные аудитории
                audience.Name = await NormalizeAudienceName(audienceData.Name);
            }

            // Применяем изменение базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса добавления новой аудитории в базу данных
        /// </summary>
        /// <param name="audience">Новая аудитория</param>
        /// <param name="context">Контекст базы данных</param>
        /// <returns>
        ///     Задача <see cref="Task" /> добавления новой аудитории <see cref="DCAudience" /> в базу данных
        /// </returns>
        public static async Task<DCAudience> InsertSingleAsync(DCAudience audience, Context? context = null) {
            // Признак "Был передан контекст базы данных"
            var isContext = context != null;

            // Если контекст базы данных не был передан - создаём новый
            context ??= new Context();

            // Нормализируем наименование аудитории
            audience.Name = await NormalizeAudienceName(audience.Name);
            
            // Добавляем аудиторию в базу данных
            context.Audiences.Add(audience);

            // Если при вызове метода был передан контекст
            if (isContext) {
                // Возвращаем сущность
                return audience;
            }

            // Сохраняем изменения
            await context.SaveChangesAsync();

            // Утилизируем созданный контекст
            context.Dispose();

            // Возвращаем сохранённую сущность
            return audience;
        }

        /// <summary>
        ///     Метод запроса добавления новых аудиторий в базу данных
        /// </summary>
        /// <param name="audiences">Аудитории</param>
        /// <returns>
        ///     Задача <see cref="Task" /> добавления новых аудиторий <see cref="IEnumerable{T}" /> в базу данных
        /// </returns>
        public static async Task InsertAsync(IEnumerable<DCAudience> audiences) {
            var user = await RepositoryUsers.GetCurrentUser();
            var userRoles = user.Roles.Select(r => r.RoleID).ToList();
            if (!userRoles.Contains(1) && !userRoles.Contains(2) && !userRoles.Contains(3) && !userRoles.Contains(4) &&
                !userRoles.Contains(6)) {
                return;
            }
            // Контекст базы данных
            using var context = new Context();

            // Итерируемся по новым аудиториям
            foreach (var audience in audiences.Where(x => x.ID == 0)) {
                await InsertSingleAsync(audience, context);
            }

            // Применяем изменение базы данных
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод нормализации наименования аудитории
        /// </summary>
        /// <param name="name">Ненормализированное наименование аудитории</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        private static async Task<string> NormalizeAudienceName(string name) => await Task.Factory.StartNew(() => {
            // Ищем совпадения по регулярному выражению
            // ^(([a-zA-Z]*)=?\s?(\d{1,2}[-\+]\d{0,2})\s?([a-z]*)?([A-C]*)?$)
            var match = Regex.Match(
                name.Trim(),
                @"^(([а-яА-Яa-zA-Z]*)=?\s?(\d{1,2}[-\+]\d{0,2})\s?([a-zA-Z.]*)?\s?([A-C|BigTV]*)?)$"
            );

            // Группы совпадений регулярного выражения
            var groups = match.Groups
               .Cast<Group>()
               .Skip(2)
               .Where(group => !string.IsNullOrWhiteSpace(group.Value))
               .ToList();

            // Формируем наименование аудитории
            return groups.Aggregate("", (str, group) => str + $"{group.Value.Trim().ToUpper()} ").Trim();
        });
    }
}
﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryQualityLimits {
        /// <summary>
        ///     Метод запроса всех идентификаторов лимитов качеств
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        public static async Task<List<int>> FindAllIDs() {
            // Контекст базы данных
            using var context = new Context();

            return await context.QualityLimits.Select(x => x.ID).ToListAsync();
        }

        public static async Task<List<DCQualityLimits>> FindAllWithoutChannels() {
            using var context = new Context();
            return await context.QualityLimits.ToListAsync();
        }

        /// <summary>
        ///     Метод запроса добавления лимитов качеств в базу данных
        /// </summary>
        /// <param name="qualityLimits">Лимиты качеств</param>
        public static async Task InsertAllAsync(IEnumerable<DCQualityLimits> qualityLimits) {
            // Контекст базы данных
            using var context = new Context();

            // Итерируемся по лимитам качеств
            foreach (var qualityLimit in qualityLimits.ToHashSet()) {
                // Нормализуем лимиты качеств
                qualityLimit.Channels.Clear();

                // Добавляем лимит качеств в базу данных
                context.QualityLimits.Add(qualityLimit);
            }

            // Ожидаем применение изменений в базу данных
            await context.SaveChangesAsync();
        }
    }
}
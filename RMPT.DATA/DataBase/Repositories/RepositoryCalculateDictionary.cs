﻿using RMPT.DATA.DataBase.Entities.Dictionaries.Other;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.DATA.DataBase.Repositories {
    public static class RepositoryCalculateDictionary {
        /// <summary>
        ///     Метод запроса последней версии справочника НРА Считалки
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        public static DCVersion? GetCurrentVersionAsync() {
            // Контекст базы данных
            using var context = new Context();

            // Возвращаем последнюю версию справочника НРА Считалки
            return context.CalculateDictionaries.FirstOrDefault();
        }

        /// <summary>
        ///     Метод запроса удаления указанной версии справочника НРА Считалки
        /// </summary>
        /// <param name="dcVersion">Версия справочника НРА Считалки</param>
        public static async Task DeleteAsync(DCVersion dcVersion) {
            // Контекст базы данных
            using var context = new Context();

            // Удаляем указанную версию справочника НРА Считалки
            context.Entry(dcVersion).State = EntityState.Deleted;

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }

        /// <summary>
        ///     Метод запроса добавления казанной версии справочника НРА Считалки
        /// </summary>
        /// <param name="dcVersion">Версия справочника НРА Считалки</param>
        public static async Task InsertAsync(DCVersion dcVersion) {
            // Контекст базы данных
            using var context = new Context();

            // Добавляем указанную версию справочника НРА Считалки
            context.CalculateDictionaries.Add(dcVersion);

            // Сохраняем изменения
            await context.SaveChangesAsync();
        }
    }
}
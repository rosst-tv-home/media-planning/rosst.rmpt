﻿namespace RMPT.GRID.Elements {
    /// <summary>
    ///     Тег столбцов таблиц
    /// </summary>
    public enum ExtendedColumnTag {
        /// <summary>
        ///     Не на что не указывает (по умолчанию)
        /// </summary>
        None,

        /// <summary>
        ///     Тег столбца наименования региона в таблице целевых рейтингов регионов
        /// </summary>
        RegionTRPTableName,

        /// <summary>
        ///     Тег столбца наименования месячного периода в таблице целевых рейтингов регионов
        /// </summary>
        RegionTRPTableMonth
    }
}
﻿using DevExpress.Data;
using DevExpress.Mvvm;

namespace RMPT.GRID.Elements {
    /// <summary>
    ///     Модель итогового столбца сгруппированных строк
    /// </summary>
    public sealed class ExtendedGroupSummary : BindableBase {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="fieldName">Наименование свойства, значение которого используются</param>
        /// <param name="type">Тип расчёта итогового значения</param>
        /// <param name="mask">Макска отображения значения</param>
        /// <param name="columnName">Столбец, по которому считается значение</param>
        public ExtendedGroupSummary(string fieldName, SummaryItemType type, string mask, string? columnName = null) {
            FieldName = fieldName;
            ShowInColumn = columnName;
            SummaryType = type;
            DisplayFormat = mask;
        }

        /// <summary>
        ///     Название свойства объекта, по которому считается значение сводки
        /// </summary>
        public string FieldName {
            get => GetProperty(() => FieldName);
            private set => SetProperty(() => FieldName, value);
        }

        /// <summary>
        ///     Тип сводки
        /// </summary>
        public SummaryItemType SummaryType {
            get => GetProperty(() => SummaryType);
            private set => SetProperty(() => SummaryType, value);
        }

        /// <summary>
        ///     Формат (маска), в котором отображается значение сводки
        /// </summary>
        public string DisplayFormat {
            get => GetProperty(() => DisplayFormat);
            private set => SetProperty(() => DisplayFormat, value);
        }

        /// <summary>
        ///     Столбец, по которому считается значение
        /// </summary>
        public string? ShowInColumn {
            get => GetProperty(() => ShowInColumn);
            private set => SetProperty(() => ShowInColumn, value);
        }
    }
}
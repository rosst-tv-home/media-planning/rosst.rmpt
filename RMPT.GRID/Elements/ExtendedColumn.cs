﻿using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Grid;
using System.Windows;
using System.Windows.Data;

namespace RMPT.GRID.Elements {
    public class ExtendedColumn : GridColumn {
        public object? UID;

        public ExtendedColumn(
            string header,
            string binding,
            ExtendedBand? band = null,
            bool isReadOnly = false,
            string displayMask = "",
            string editMask = "",
            MaskType editMaskType = MaskType.Simple,
            ExtendedColumnTag tag = ExtendedColumnTag.None,
            bool isVisible = true,
            int groupIndex = -1,
            bool fixedWidth = true,
            DefaultBoolean allowResizing = DefaultBoolean.False,
            double width = 50.0,
            object? uid = null
        ) {
            UID = uid;
            AllowResizing = allowResizing;
            FixedWidth = fixedWidth;
            Width = new GridColumnWidth(width);

            Band = band;
            Header = header;

            ReadOnly = isReadOnly;
            AllowEditing = isReadOnly ? DefaultBoolean.False : DefaultBoolean.True;
            Visible = isVisible;
            UnboundType = UnboundColumnType.Bound;
            if (!string.IsNullOrWhiteSpace(binding)) {
                Binding = new Binding {
                    Path = new PropertyPath(binding),
                    Mode = isReadOnly ? BindingMode.OneWay : BindingMode.TwoWay
                };
            }

            DisplayFormat = displayMask;
            Mask = editMask;
            MaskType = editMaskType;

            SetDisplayFormat();
            SetEditMask();

            Tag = tag;
            GroupIndex = groupIndex;
            CopyValueAsDisplayText = false;
        }

        public override string? FieldName { get; set; }

        public FixedStyle FixedStyle { get; } = FixedStyle.None;

        public bool IsUseMaskAsDisplayFormat { get; } = false;

        public ExtendedBand? Band { get; }

        public string Mask { get; }

        public MaskType MaskType { get; }

        public string DisplayFormat { get; }

        private void SetEditMask() {
            if (string.IsNullOrWhiteSpace(Mask)) {
                return;
            }

            if (EditSettings is TextEditSettings textEditSettings) {
                textEditSettings.Mask = Mask;
                textEditSettings.MaskType = MaskType;
            } else {
                EditSettings = new TextEditSettings {
                    Mask = Mask,
                    MaskType = MaskType
                };
            }
        }

        private void SetDisplayFormat() {
            if (string.IsNullOrWhiteSpace(DisplayFormat)) {
                return;
            }

            if (EditSettings != null) {
                EditSettings.DisplayFormat = DisplayFormat;
            } else {
                EditSettings = new TextEditSettings {
                    DisplayFormat = DisplayFormat
                };
            }
        }
    }
}
﻿namespace RMPT.GRID.Elements {
    /// <summary>
    ///     Модель строки связывания объекта с ячейкой таблицы
    /// </summary>
    internal class ExtendedBinding {
        /// <summary>
        ///     Признак "Свойство является коллекцией"
        /// </summary>
        public bool IsCollection { get; set; }

        /// <summary>
        ///     Индекс элемента в свойстве (если это коллекция)
        /// </summary>
        public int BindingIndex { get; set; }

        /// <summary>
        ///     Наименование свойства
        /// </summary>
        public string BindingPropertyName { get; set; } = "";
    }
}
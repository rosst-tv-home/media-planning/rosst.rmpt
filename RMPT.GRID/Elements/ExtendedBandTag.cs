﻿namespace RMPT.GRID.Elements {
    /// <summary>
    ///     Теги групп столбцов
    /// </summary>
    public enum ExtendedBandTag {
        /// <summary>
        ///     Тег по умолчанию
        /// </summary>
        None,

        /// <summary>
        ///     Тег группы столбцов 'Данные сырья'
        /// </summary>
        RawData,

        /// <summary>
        ///     Тег группы столбцов месячного периода
        /// </summary>
        Month,

        /// <summary>
        ///     Тег группы столбцов "Распределение" месяца
        /// </summary>
        MonthDistribution,

        /// <summary>
        ///     Тег группы столбцов недельного периода
        /// </summary>
        Week,

        /// <summary>
        ///     Тег группы столбцов распределения хронометражей
        /// </summary>
        TimingDistribution
    }
}
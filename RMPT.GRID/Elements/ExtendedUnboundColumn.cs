﻿using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Xpf.Editors;

namespace RMPT.GRID.Elements {
    public class ExtendedUnboundColumn : ExtendedColumn {
        public ExtendedUnboundColumn(
            string header,
            string binding,
            ExtendedBand? band = null,
            bool isReadOnly = false,
            string displayMask = "",
            string editMask = "",
            MaskType editMaskType = MaskType.Simple,
            ExtendedColumnTag tag = ExtendedColumnTag.None,
            bool isVisible = true,
            UnboundColumnType type = UnboundColumnType.Object,
            int groupIndex = -1,
            bool fixedWidth = true,
            DefaultBoolean allowResizing = DefaultBoolean.False,
            double width = 50.0,
            object? uid = null
        ) : base(
            header,
            "",
            band,
            isReadOnly,
            displayMask,
            editMask,
            editMaskType,
            tag,
            isVisible,
            groupIndex,
            fixedWidth,
            allowResizing,
            width,
            uid
        ) {
            FieldName = binding;
            UnboundType = type;
        }
    }
}
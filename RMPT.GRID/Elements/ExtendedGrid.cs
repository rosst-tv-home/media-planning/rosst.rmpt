﻿using DevExpress.Xpf.Grid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;

namespace RMPT.GRID.Elements {
    /// <summary>
    ///     Модель таблицы данных
    /// </summary>
    public class ExtendedGrid : GridControl {
        /// <summary>
        ///     Уже пропарсированные строки связывания
        /// </summary>
        private readonly Dictionary<string, IReadOnlyList<ExtendedBinding>> _parsedFields = new();

        /// <summary>
        ///     Конструктор
        /// </summary>
        public ExtendedGrid() {
            // Подписываемся на событие вставки значений из буфера обмена
            PastingFromClipboard += OnPastingFromClipboard;
            // Подписываемся на событие запроса или установки значений отвязанных ячеек таблицы
            CustomUnboundColumnData += OnUnboundColumnData;
        }

        /// <summary>
        ///     Метод обработки события вставки значений из буфера обмена
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="arguments">Аргументы события</param>
        private void OnPastingFromClipboard(object sender, PastingFromClipboardEventArgs arguments) {
            // Содержит ли буфер обмена значения, разделённые точкой с запятой (если их несколько)
            if (!Clipboard.ContainsText(TextDataFormat.CommaSeparatedValue)) return;

            // Строка буфер обмена
            var clipboardText = Clipboard.GetText(TextDataFormat.CommaSeparatedValue);
            // Если строка буфер обмена пустая - завершаем выполнение метода
            if (string.IsNullOrWhiteSpace(clipboardText)) return;

            // Если представление сетки не является таблицей - завершаем выполнение метода
            if (this.View is not TableView table) return;

            // Определяем индекс первой строки
            var firstRow = table.GetSelectedRows()
               .OrderBy(r => r.RowHandle)
               .FirstOrDefault()?.RowHandle;
            // Определяем индекс первого столбца
            var firstCol = table.GetSelectedCells()
               .OrderBy(c => c.Column.VisibleIndex)
               .FirstOrDefault()?.Column.VisibleIndex;
            // Если строка или столбец не определены - завершаем выполнение метода
            if (!firstRow.HasValue || !firstCol.HasValue) return;

            int row = firstRow.Value;
            int col = firstCol.Value;

            // Разделяем строку буфер обмена на несколько строк
            foreach (var stringRow in clipboardText.Split(new[] {
                "\r\n"
            }, StringSplitOptions.RemoveEmptyEntries)) {
                // Разделяес подстроку буфер обмена на отдельные значения
                foreach (var stringCol in stringRow.Split(new[] {
                    ";"
                }, StringSplitOptions.RemoveEmptyEntries)) {
                    // Определяем ячейку, в которую будет вставлено значение
                    var column = this.Columns.FirstOrDefault(c => c.VisibleIndex == col);
                    // Если ячейка только для чтения, или не определена - переходим к следующей
                    if (column == null || column.ReadOnly) {
                        col++;
                        continue;
                    }

                    // Вставляем значения в ячейку строки
                    this.SetCellValue(row, column.FieldName, stringCol);
                    col++;
                }

                row++;
                col = firstCol.Value;
            }

            arguments.Handled = true;
        }

        /// <summary>
        ///     Метод обработки запроса или установки значений отвязанных ячеек
        /// </summary>
        /// <param name="source">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private void OnUnboundColumnData(object source, GridColumnDataEventArgs argument) {
            // Если группа столбцов скрыта - нам не нужно отображать данные для данной ячейки
            if (argument.Column.ParentBand is { Visible: false } ||
                argument.Column.ParentBand.ParentBand is { Visible: false }
            ) {
                return;
            }

            // Строка связывания
            var fieldName = argument.Column.FieldName;

            // Если строка пустая - выбрасываем исключение
            if (string.IsNullOrWhiteSpace(fieldName)) {
                throw new InvalidOperationException("Не удалось определить строку привязки");
            }

            // Определяем объект источника элементов таблицы, данные которого рисуются в таблице
            var item = argument.Source.GetRowByListIndex(argument.ListSourceRowIndex);

            // Если объект не был определён - выбрасываем исключение
            if (item == null) {
                throw new InvalidOperationException("Не удалось определить объект привязки");
            }

            // Свойства объекта
            var bindingStrings = _parsedFields.ContainsKey(fieldName)
                ? _parsedFields[fieldName]
                : ParseBindingString(fieldName);

            // Итерируемся по свойствам, указанным в строке привязки
            foreach (var str in bindingStrings) {
                // Если это последнее свойство, не является коллекцией и запрос установки нового значения
                if (bindingStrings.Last().Equals(str) && argument.IsSetData && !str.IsCollection) {
                    // Определяем свойство, значение которого будет установлено
                    var property = item?.GetType().GetProperty(str.BindingPropertyName);

                    // Устанавливаем новое значение свойства объекта
                    if (item?.GetType().GetProperty(str.BindingPropertyName)?.PropertyType == typeof(Double)) {
                        if (argument.Value is string stringify) {
                            argument.Value = stringify.Replace(".", ",");
                        }

                        property?.SetValue(item, Convert.ToDouble(argument.Value));
                    } else {
                        item?.GetType().GetProperty(str.BindingPropertyName)?.SetValue(item, argument.Value);
                    }

                    // Завершаем выполнение метода
                    return;
                }

                // Переопределяем объект
                item = item?.GetType().GetProperty(str.BindingPropertyName)?.GetValue(item, null);

                // Если свойство не является коллекцией - переходим к следующему свойству
                if (!str.IsCollection) {
                    continue;
                }

                // Индекс
                var index = 0;
                // Итерируемся по коллекции
                foreach (var subItem in (item as IEnumerable)!) {
                    // Если индексы не совпадают - переходим к следующему элементу
                    if (index != str.BindingIndex) {
                        index++;
                        continue;
                    }

                    // Переопределяем объект
                    item = subItem;
                    // Прерываем цыкл
                    break;
                }
            }

            // Устанавливаем значение ячейки из данных объекта
            if (argument.IsGetData) {
                argument.Value = item;
            }
        }

        /// <summary>
        ///     Метод парсинга строки связывания
        /// </summary>
        /// <param name="bindingString">Строка связывания</param>
        /// <returns>Коллекция свойств, которые были в строке связывания</returns>
        private IReadOnlyList<ExtendedBinding> ParseBindingString(string bindingString) {
            var parsed = bindingString
               .Replace("RowData.Row.", "")
                // Разбираем строку по точкам (обращение к свойствам)
               .Split('.')
                // Парсим каждую строку со свойством
               .Select(propertyName => {
                    // Проверка строки - ссылка на массив или обычное свойство
                    var matching = Regex.Match(propertyName, @"\[\d+\]");
                    return new ExtendedBinding {
                        IsCollection = matching.Success,
                        BindingIndex = matching.Success
                            ? int.Parse(matching.Value.Substring(1, matching.Value.Length - 2))
                            : -1,
                        BindingPropertyName = matching.Success
                            ? propertyName.Substring(0, matching.Index)
                            : propertyName
                    };
                })
               .ToList();
            _parsedFields[bindingString] = parsed;
            return parsed;
        }
    }
}
﻿using DevExpress.Xpf.Grid;

namespace RMPT.GRID.Elements {
    public class ExtendedBand : GridControlBand {
        public object? UID;

        public ExtendedBand(
            string header,
            bool isVisible = true,
            FixedStyle fixedStyle = FixedStyle.None,
            ExtendedBandTag tag = ExtendedBandTag.None,
            object? uid = null
        ) {
            UID = uid;
            Tag = tag;
            Fixed = fixedStyle;
            Header = header;
            Visible = isVisible;
        }
    }
}
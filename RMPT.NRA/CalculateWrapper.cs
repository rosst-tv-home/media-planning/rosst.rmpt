﻿using RMPT.NRA.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using VI.BL;
using VI.Calc;
using VI.Calc.Interface;
using VI.Calc.Serialization;
using VI.Common;
using VI.Common.Configuration;
using Geography = VI.Calc.Interface.Geography;
using MediaType = VI.Calc.Interface.MediaType;
using QualityCoefficient = VI.Calc.Interface.QualityCoefficient;
using QualityCoefficientValue = VI.Calc.Interface.QualityCoefficientValue;
using Site = VI.Calc.Interface.Site;
using TimeSlot = VI.Calc.Serialization.TimeSlot;

namespace RMPT.NRA {
    /// <summary>
    ///     Обёртка над НРА Считалкой
    /// </summary>
    public static class CalculateWrapper {
        #region Свойства

        #region Свойства ядра НРА Считалки

        /// <summary>
        ///     Экземпляр хранилища заявки НРА Считалки
        /// </summary>
        private static MediaRequestStorage? _storage;

        /// <summary>
        ///     Признак справочника НРА Считалки "Инициализирован"
        /// </summary>
        private static bool _isDictionaryInitialized;
        
        /// <summary>
        ///     Сообщение считалки
        /// </summary>
        private static string _calcKernelLog = "";

        #endregion

        #region Свойства справочных данных НРА Считалки

        /// <summary>
        ///     Регионы
        /// </summary>
        private static readonly HashSet<CalcRegion> Regions = new();

        /// <summary>
        ///     Аудитории
        /// </summary>
        private static readonly HashSet<CalcAudience> Audiences = new();

        /// <summary>
        ///     Направления продаж
        /// </summary>
        private static readonly HashSet<CalcSellingDirection> SellingDirections = new();

        /// <summary>
        ///     Сезонные коэффициенты
        /// </summary>
        private static readonly HashSet<CalcSeasonCoefficient> SeasonCoefficients = new();

        /// <summary>
        ///     Лимиты качества
        /// </summary>
        private static readonly HashSet<CalcQualityLimit> QualityLimits = new();

        #endregion

        #endregion

        #region Методы ядра считалки

        /// <summary>
        ///     Метод инициализации справочника НРА Считалки
        ///     При инициализации происходит загрузка актульного справочника НРА Считалки
        /// </summary>
        private static async Task InitializeDictionary() {
            // Если справочник уже инициализирован - завершаем выполнение метода
            if (_isDictionaryInitialized) {
                Debug.WriteLine(
                    $"{nameof(CalculateWrapper)}::{nameof(InitializeDictionary)}: " +
                    "справочник НРА Считалки уже инициализирован"
                );
                return;
            }

            Debug.WriteLine(
                $"{nameof(CalculateWrapper)}::{nameof(InitializeDictionary)}: " +
                "инициализация справочника НРА Считалки..."
            );

            // Устанавливаем анонимную авторизацию
            DocumentBase.WSFacadeAuthDefault();
            Debug.WriteLine(
                $"{nameof(CalculateWrapper)}::{nameof(InitializeDictionary)}: " +
                "установлена анонимная авторизация..."
            );

            // Загружаем последнюю версию справочника
            await ReloadDictionary();

            // Инициализируем ядро
            await ReInitKernel();

            // Устанавливаем флаг
            _isDictionaryInitialized = true;

            Debug.WriteLine(
                $"{nameof(CalculateWrapper)}::{nameof(InitializeDictionary)}: " +
                $"версия актульного справочника НРА Считалки: {(await GetDictionaryVersion()).ToString()}"
            );
        }

        public static async Task ReInitKernel() {
            // Инициализируем ядро НРА Считалки из последнего справочника
            Debug.WriteLine(
                $"{nameof(CalculateWrapper)}::{nameof(ReInitKernel)}: " +
                "инициализация ядра НРА Считалки из актульного справочника..."
            );
            await Task.Run(() => DocumentBase.Instance.ReInitKernel());
            Debug.WriteLine(
                $"{nameof(CalculateWrapper)}::{nameof(ReInitKernel)}: " +
                "ядро НРА Считалки успешно инициализировано..."
            );
        }

        private static async Task ReloadDictionary() {
            // Делаем запрос на загрузку последнего справочника
            Debug.WriteLine(
                $"{nameof(CalculateWrapper)}::{nameof(ReloadDictionary)}: " +
                "отправка запроса загрузки актульной версии справочника НРА Считалки..."
            );
            await Task.Run(() => DocumentBase.Instance.ReloadDictionary());
            Debug.WriteLine(
                $"{nameof(CalculateWrapper)}::{nameof(ReloadDictionary)}: " +
                "актуальная версия справочника НРА Считалки загружена..."
            );
        }

        /// <summary>
        ///     Метод получения последних актульных данных справочника НРА Считалки
        /// </summary>
        /// <returns>Последние актуальные данные справочника НРА Считалки</returns>
        private static async Task<InitParams> GetActualDictionaryData() {
            // Инициализируем справочник НРА Считалки, если требуется
            await InitializeDictionary();

            // Возвращаем последние актульные данные справочника НРА Считалки
            return DocumentBase.Instance.LastPersistParam;
        }

        /// <summary>
        ///     Метод получения актуальной версии справочника НРА Считалки
        /// </summary>
        /// <returns>Актуальная версия справочника НРА Считалки</returns>
        public static async Task<int> GetDictionaryVersion() {
            // Инициализируем справочник НРА Считалки, если требуется
            await InitializeDictionary();

            // Возвращаем версию актуального справочника НРА Считалки
            return DocumentBase.Instance.DictionaryVersion;
        }

        /// <summary>
        ///     Метод получения экземпляра хранилища заявки НРА Считалки
        /// </summary>
        /// <returns>Экземпляр хранилища заявки НРА Считалки</returns>
        public static async Task<MediaRequestStorage> GetStorageInstance() {
            // Инициализируем справочник НРА Считалки, если требуется
            await InitializeDictionary();

            // Возвращаем имеющийся экземпляр хранилища заявки НРА Считалки, или создаём новый
            return _storage ??= CalcKernel.GetExportStorage();
        }

        /// <summary>
        ///     Метод очистки экземпляра хранилища заявки НРА Считалки
        /// </summary>
        public static void ClearStorage() => _storage = null;

        /// <summary>
        ///     Метод получения инвентарей, в которых есть рассчитанная помесячная цена
        /// </summary>
        /// <returns>Инвентари с рассчитанными ценами</returns>
        public static List<CalcInventory> ClaimPricedInventories() => CalcKernel.Inventories
           .Where(x => x.PeriodPrice.AllElements.Any(y => y.Value > 0.0))
           .ToList();

        #endregion

        #region Методы импорта заявки НРА Считалки

        /// <summary>
        ///     Метод импорта заявки НРА Считалки с указанием пути до файла
        /// </summary>
        /// <param name="path">Путь до файла заявки НРА Считалки</param>
        public static async Task<MediaRequestStorage> ImportOrder(string path) {
            // Инициализируем справочник НРА Считалки, если требуется
            await InitializeDictionary();

            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(ImportOrder)}: loading file data started");

            // Загружаем файл заявки НРА Считалки
            var document = CompressionHelper.LoadXmlFileToXmlDocument(path);

            #if DEBUG
            document.Save("imported_order.xml");
            #endif

            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(ImportOrder)}: loading file data finished");

            // Десериализуем файл заявки считалки в объект модели заявки
            return DeserializeOrder(document);
        }

        /// <summary>
        ///     Метод импорта заявки НРА Считалки с указанием потока данных заявки НРА Считалки
        /// </summary>
        /// <param name="stream">Поток данных заявки НРА Считалки</param>
        public static async Task<MediaRequestStorage> ImportOrder(Stream stream) {
            // Инициализируем справочник НРА Считалки, если требуется
            await InitializeDictionary();

            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(ImportOrder)}: loading file data started");

            // Загружаем файл заявки НРА Считалки
            var document = CompressionHelper.LoadXmlFileToXmlDocument(stream);

            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(ImportOrder)}: loading file data finished");

            // Десериализуем файл заявки считалки в объект модели заявки
            return DeserializeOrder(document);
        }

        /// <summary>
        ///     Метод десериализации данных файла заявки НРА Считалки в объект модели заявки
        /// </summary>
        /// <param name="document">Документ заявки НРА Считалки в формате XML</param>
        /// <returns>Объект модели заявки</returns>
        private static MediaRequestStorage DeserializeOrder(XmlNode document) {
            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(DeserializeOrder)}: deserialize started");

            // Десериализация данных файла заявки НРА Считалки в объект модели заявки
            var storage = (new XmlSerializer(
                typeof(MediaRequestStorage)
            ).Deserialize(new StringReader(
                document.SelectSingleNode(DocumentBase.XmlMainPath)!.OuterXml
            )) as MediaRequestStorage)!;

            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(DeserializeOrder)}: deserialize finished");

            return storage;
        }

        /// <summary>
        ///     Метод внедрения хранилища заявки НРА Считалки в ядро НРА Считалки
        /// </summary>
        /// <param name="storage">Хранилище заявки НРА Считалки</param>
        /// <exception cref="Exception">Ошибка проверки данных заявки НРА Считалки</exception>
        public static async Task InjectImportedOrder(MediaRequestStorage storage) {
            // Инициализируем справочник НРА Считалки, если требуется
            await InitializeDictionary();

            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(InjectImportedOrder)}: inject started");
            
            CalcKernel.OnWritingLog += CalcKernelOnOnWritingLog;

            // Блокируем ядро НРА Считалки
            lock (CalcKernel.LockObject) {
                // Заменяем хранилище заявки НРА Считалки
                CalcKernel.Storage = storage;

                // Проверяем данные заявки НРА Считалки
                if (!CalcKernel.CheckStoredData()) {
                    var log = _calcKernelLog;
                    _calcKernelLog = "";
                    CalcKernel.OnWritingLog -= CalcKernelOnOnWritingLog;
                    throw new Exception(log);
                }

                // Запоминаем экземпляр заявки НРА Считалки
                _storage = storage;
            }
            
            CalcKernel.OnWritingLog -= CalcKernelOnOnWritingLog;
            _calcKernelLog = "";

            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(InjectImportedOrder)}: inject finished");
        }

        /// <summary>
        ///     Метод обработки события сообщения считалки
        /// </summary>
        /// <param name="message">Сообщение считалки</param>
        private static void CalcKernelOnOnWritingLog(string message) => _calcKernelLog += $"{message}\n";

        #endregion

        #region Методы экспорта заявки НРА Считалки

        /// <summary>
        ///     Метод экспорта заявки НРА Считалки в файл
        /// </summary>
        public static async Task ExportOrder(string path) {
            // Инициализируем справочник НРА Считалки, если требуется
            await InitializeDictionary();

            // Присваиваем новый идентификатор заявки НРА Считалки
            CalcKernel.Consolidation.NewGUID();

            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(ExportOrder)}: file write started");

            // Получаем массив байт для записи в файл
            var bytes = GetExportedBytes();

            // Записываем массив байт в файл
            WriteFile(path, bytes);

            Debug.WriteLine($"{nameof(CalculateWrapper)}::{nameof(ExportOrder)}: file write finished");
        }

        /// <summary>
        ///     Метод конвертации заявки НРА Считалки в массив байт
        /// </summary>
        /// <returns>Массив байт</returns>
        private static byte[] GetExportedBytes() {
            // Поток записи
            var memoryStream = new MemoryStream();
            // Параметры словаря
            var dictionaryParams = DocumentBase.Instance.DictionaryInfo(CalcKernel.CurrentDictionaryVersion.Version);
            using (var xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8)) {
                #region Document

                xmlTextWriter.WriteStartDocument();

                #region MediaRequest

                xmlTextWriter.WriteStartElement("MediaRequest");
                xmlTextWriter.WriteAttributeString(
                    "VersionNumber",
                    CalcKernel.CurrentDictionaryVersion.Version.ToString()
                );
                try {
                    xmlTextWriter.WriteAttributeString(
                        "AppMajorVersionNumber",
                        AssemblyName.GetAssemblyName("VicomClient.exe").Version.Major.ToString()
                    );
                    xmlTextWriter.WriteAttributeString(
                        "AppFullVersionNumber",
                        AssemblyName.GetAssemblyName("VicomClient.exe").Version.ToString()
                    );
                } catch (FileNotFoundException) {
                    xmlTextWriter.WriteAttributeString(
                        "AppMajorVersionNumber",
                        0.ToString()
                    );
                    xmlTextWriter.WriteAttributeString(
                        "AppFullVersionNumber",
                        0.ToString()
                    );
                }

                if (dictionaryParams != null) {
                    xmlTextWriter.WriteAttributeString(
                        "DictionaryType",
                        ((int)dictionaryParams.DictionaryType).ToString()
                    );
                    xmlTextWriter.WriteAttributeString(
                        "DictionaryCreatedDate",
                        dictionaryParams.CreatedDate.ToString()
                    );
                }

                #region RequestState

                xmlTextWriter.WriteStartElement("RequestState");

                #region GridDataTables

                xmlTextWriter.WriteStartElement("GridDataTables");

                #region MediaRequestStorage

                new XmlSerializer(typeof(MediaRequestStorage)).Serialize(
                    xmlTextWriter,
                    GetStorageInstance().Result
                );

                #endregion

                xmlTextWriter.WriteEndElement();

                #endregion

                xmlTextWriter.WriteEndElement();

                #endregion

                #region GridConfiguration

                xmlTextWriter.WriteStartElement("GridConfiguration");
                xmlTextWriter.WriteEndElement();

                #endregion

                xmlTextWriter.WriteEndElement();

                #endregion

                xmlTextWriter.WriteEndDocument();

                #endregion
            }

            return memoryStream.ToArray();
        }

        /// <summary>
        ///     Метод записи массива байт заявки НРА Считалки в файл
        /// </summary>
        /// <param name="path">Путь до файла</param>
        /// <param name="bytes">Массив байт заявки НРА Считалки</param>
        private static void WriteFile(string path, byte[] bytes) {
            // Открываем поток записи в файл
            using (var fileStream = new FileStream(path, FileMode.Create, FileAccess.Write)) {
                // Записываем массив байт в файл
                fileStream.Write(bytes, 0, bytes.Length);
            }

            // Устанавливаем номер сборки считалки, чтобы сгенерировать соль при сжатии файла
            AppBuildConfig.Instance.AppBuildVersion = Configuration.Instance.Settings.ApplicationVersionBuild;

            // Сжимаем данные файла
            CompressionHelper.ZipCompressXml(path, false);
        }

        #endregion

        #region Методы формирования заявки НРА Считалки

        /// <summary>
        ///     Метод установки наименвоания рекламного агенста в заявку НРА Считалки
        /// </summary>
        /// <param name="name">Наименование рекламного агенства</param>
        public static async Task SetAgencyName(string name) => (await GetStorageInstance())
           .Consolidation.AdvAgency = name;

        /// <summary>
        ///     Метод установки наименования рекламодателя в заявку НРА Считалки
        /// </summary>
        /// <param name="name">Наименование рекламодателя</param>
        public static async Task SetAdvertiserName(string name) => (await GetStorageInstance())
           .Consolidation.AdvertiserName = name;

        public static async Task EnableGeoDistribution(string name) {
            var storage = await GetStorageInstance();

            var geoDistribution = CalcKernel.GeographyDistributions.First(x => x.Name == name);
            storage.GeoDistributions.Add(new LineGeoDistribution {
                Visible = true,
                ID = geoDistribution.ID,
                MediaTypeID = geoDistribution.MediaTypeRef.ID,
                GeographyID = geoDistribution.GeografyRef.ID,
                LastYearBudget = new LineCell(),
                AdditionalBudget = new LineCell(),
                ProductionBudget = new LineCell(),
                RealCPP = new LineCell(),
                LastYearCPP = new LineCell()
            });
        }

        /// <summary>
        ///     Метод активации федерального канала в заявке НРА Считалки
        /// </summary>
        /// <exception cref="Exception">Ошибка определения канала</exception>
        public static async Task EnableChannel(
            bool isFederal,
            int inventoryID,
            IReadOnlyCollection<Tuple<int, double>> inventories,
            IReadOnlyDictionary<int, double[]> quality
        ) {
            var storage = await GetStorageInstance();
            var dictionaryInventories = isFederal
                ? storage.InitParams.MediaTypes.SelectMany(x =>
                    x.Sites.SelectMany(y => y.Inventories)
                ) ?? throw new InvalidOperationException()
                : storage.InitParams.MediaTypes.SelectMany(x =>
                    x.Sites.SelectMany(y => y.Departments.SelectMany(z => z.Inventories))
                ) ?? throw new InvalidOperationException();
            var inventory = dictionaryInventories.FirstOrDefault(x => x.UID == inventoryID)
             ?? throw new InvalidOperationException();

            var mediaType = isFederal
                ? storage.InitParams.MediaTypes.FirstOrDefault(x =>
                    x.Sites.Any(y => y.Inventories.Any(z => z.UID == inventoryID))
                ) ?? throw new InvalidOperationException()
                : storage.InitParams.MediaTypes.FirstOrDefault(x =>
                    x.Sites.Any(y => y.Departments.Any(z => z.Inventories.Any(k => k.UID == inventoryID)))
                ) ?? throw new InvalidOperationException();

            // Добавляем созданный инвентарь канала в модель заявки НРА Считалки
            storage.Inventories.Add(await AddInventory(mediaType, inventory, inventories, quality));
        }

        /// <summary>
        ///     Метод проверки активности направления продаж
        /// </summary>
        /// <param name="mediaType">Направление продаж</param>
        /// <exception cref="Exception">Ошибка определения направления продаж</exception>
        private static async Task CheckMediaType(MediaType mediaType) {
            // Определяем направление продаж в контексте заявки
            var requestMediaType = (await GetStorageInstance()).MediaTypes.FirstOrDefault(x => x.ID == mediaType.ID);
            if (requestMediaType == null) {
                throw new Exception("Не удалось определить направление продаж по идентификатору в заявке НРА Считалки");
            }

            // Делаем направление продаж активным
            requestMediaType.Visible = true;
            // Задаём направление расчёта - от инвентаря
            requestMediaType.CalcMode = CalcDirections.FromInventory;
        }

        /// <summary>
        ///     Метод добавления инвентаря канала
        /// </summary>
        /// <param name="mediaType">Направление продаж</param>
        /// <param name="inventory">Инвентарь канала</param>
        /// <param name="inventories">Массив GRP'20 канала помесячно</param>
        /// <param name="quality">Словарь помесячных долей качества</param>
        private static async Task<LineInventory> AddInventory(
            MediaType mediaType,
            Inventory inventory,
            IReadOnlyCollection<Tuple<int, double>> inventories,
            IReadOnlyDictionary<int, double[]> quality
        ) {
            // Проверяем и активируем направление продаж
            await CheckMediaType(mediaType);

            // Идентификатор super fix (для Фед. ТВ) или fix (для Орб., Рег. и Тем. ТВ)
            var fixOrSuperFixTimeSlotID = mediaType.ID switch {
                1 => 11,
                4 => 23,
                2 => 1,
                _ => throw new NotImplementedException()
            };
            // Идентификатор super fix prime (для Фед. ТВ) или fix prime (для Орб., Рег. и Тем. ТВ)
            var fixOrSuperFixPrimeTimeSlotID = mediaType.ID switch {
                1 => 13,
                4 => 24,
                2 => 3,
                _ => throw new NotImplementedException()
            };
            // Идентификатор fix (для Фед. ТВ) или float (для Орб., Рег. и Тем. ТВ)
            var fixOrFloatTimeSlotID = mediaType.ID switch {
                1 => 16,
                4 => 26,
                2 => 6,
                _ => throw new NotImplementedException()
            };
            // Идентификатор fix prime (для Фед. ТВ) или float prime (для Орб., Рег. и Тем. ТВ)
            var fixOrFloatPrimeTimeSlotID = mediaType.ID switch {
                1 => 18,
                4 => 28,
                2 => 8,
                _ => throw new NotImplementedException()
            };

            // Добавляем инвентарь
            var inventoryRequest = new LineInventory {
                ID = inventory.UID,
                Visible = true,
                RealCPP = new LineCell(),
                LastYearCPP = new LineCell(),
                LastYearBudget = new LineCell(),
                InMediaInflationSet = new LineCheck(),
                AdditionalBudget = new LineCell(),
                ProductionBudget = new LineCell(),
                PeriodInventory = new List<LineComboCell>(Enumerable
                   .Range(0, 12)
                   .Select(_ => new LineComboCell(0.0, 0.0, CalcFixType.None, "", null))
                ),
                PeriodSaleSchemes = new List<SaleScheme>(Enumerable
                   .Range(0, 12)
                   .Select(_ => new SaleScheme {
                        TimeSlots = new List<TimeSlot> {
                            new() {
                                TimeSlotID = fixOrSuperFixTimeSlotID,
                                Share = 0.0
                            },
                            new() {
                                TimeSlotID = fixOrSuperFixPrimeTimeSlotID,
                                Share = 0.0
                            },
                            new() {
                                TimeSlotID = fixOrFloatTimeSlotID,
                                Share = 0.0
                            },
                            new() {
                                TimeSlotID = fixOrFloatPrimeTimeSlotID,
                                Share = 0.0
                            }
                        }
                    }))
            };

            // Добавляем данные помесячно
            for (var i = 0; i < 12; i++) {
                // Если качество не указано для текущего месяца - переходим к следующему
                if (!quality.ContainsKey(i)) {
                    continue;
                }

                // Указываем инвентарь месяца
                inventoryRequest.PeriodInventory[i].Value = inventories.FirstOrDefault(x => x.Item1 == i)?.Item2 ?? 0.0;

                // Указываем долю super fix (для Фед. ТВ) или fix (для Орб., Рег. и Тем. ТВ)
                var timeSlot = inventoryRequest.PeriodSaleSchemes[i].TimeSlots.FirstOrDefault(x =>
                    x.TimeSlotID == fixOrSuperFixTimeSlotID
                ) ?? throw new NullReferenceException("Не удалось найти ячейку качества");
                timeSlot.Share = quality[i][0];

                // Указываем долю super fix prime (для Фед. ТВ) или fix prime (для Орб., Рег. и Тем. ТВ)
                timeSlot = inventoryRequest.PeriodSaleSchemes[i].TimeSlots.FirstOrDefault(x =>
                    x.TimeSlotID == fixOrSuperFixPrimeTimeSlotID
                ) ?? throw new NullReferenceException("Не удалось найти ячейку качества");
                timeSlot.Share = quality[i][1];

                // Указываем долю fix (для Фед. ТВ) или float (для Орб., Рег. и Тем. ТВ)
                timeSlot = inventoryRequest.PeriodSaleSchemes[i].TimeSlots.FirstOrDefault(x =>
                    x.TimeSlotID == fixOrFloatTimeSlotID
                ) ?? throw new NullReferenceException("Не удалось найти ячейку качества");
                timeSlot.Share = 1.0 - quality[i][0];

                // Указываем долю fix prime (для Фед. ТВ) или float prime (для Орб., Рег. и Тем. ТВ)
                timeSlot = inventoryRequest.PeriodSaleSchemes[i].TimeSlots.FirstOrDefault(x =>
                    x.TimeSlotID == fixOrFloatPrimeTimeSlotID
                ) ?? throw new NullReferenceException("Не удалось найти ячейку качества");
                timeSlot.Share = quality[i][2];
            }

            return inventoryRequest;
        }

        #endregion

        #region Методы взаимодействия со справочной информацией считалки

        public static async Task<HashSet<CalcSellingDirection>> GenerateData() {
            // Инициализируем справочник НРА Считалки, если требуется
            await InitializeDictionary();

            if ((await GetActualDictionaryData()) == null) return new HashSet<CalcSellingDirection>();

            // Генерируем данные каналов по федеральному, региональному и тематическому направлению продаж
            foreach (var site in FindMediaType(1).Sites.Union(FindMediaType(2).Sites).Union(FindMediaType(4).Sites)) {
                // Генерируем данные канала
                await GenerateSiteData(site);
            }

            // Переопределяем аудитории для измеряемых минутных каналов
            await PostProcessChannelsAudiences();

            return SellingDirections;
        }

        private static async Task GenerateSiteData(Site site) {
            // Определяем направление продаж
            var sellingDirection = GenerateSellingDirection(site.MediaTypeID);
            // Определяем признак "Орбитальное вещание"
            var isOrbital = DefineIsOrbital(site.MassMediaID);

            // Если не региональное направление продаж
            if (sellingDirection.ID != 2) {
                var inventory = site.Inventories.First();

                var id = site.ID;
                var uid = site.UID;
                var name = CalculateUtils.NormalizeChannelName(site.Name);

                var inventoryUid = inventory.UID;

                // Определяем регион
                var region = GenerateRegion(sellingDirection);
                sellingDirection.AddRegion(region);

                // Определяем аудиторию
                var audience = GenerateAudience(inventory.Audience);
                // Определяем тип продаж инвентаря
                var saleType = (CalcSaleType)FindSaleType(inventory.InventoryUnitID).ID;

                // Определяем сезонный коэффициент
                var seasonCoefficient = GenerateSeasonCoefficient(inventory.SeasonCoefficientID);
                region.SeasonCoefficient = seasonCoefficient;

                // Определяем коэффициент качества
                var qualityCoefficient = GenerateQualityCoefficient(inventory.QualityCoefficientID, sellingDirection);

                region.AddChannel(new CalcChannel(
                    id, uid, await name, inventoryUid, region, await audience, saleType, isOrbital, qualityCoefficient
                ));
            }

            // Если региональное направление продаж - итерируемся по каналам регионов
            foreach (var department in site.Departments) {
                var inventory = department.Inventories.FirstOrDefault();
                if (inventory == null) {
                    continue;
                }

                var id = department.ID;
                var uid = department.UID;
                var name = CalculateUtils.NormalizeChannelName(site.Name);

                var inventoryUid = inventory.UID;

                // Определяем регион
                var region = GenerateRegion(sellingDirection, department.GeographyID);
                sellingDirection.AddRegion(region);

                // Определяем аудиторию
                var audience = GenerateAudience(inventory.Audience);
                // Определяем тип продаж инвентаря
                var saleType = (CalcSaleType)FindSaleType(inventory.InventoryUnitID).ID;

                // Определяем сезонный коэффициент
                var seasonCoefficient = GenerateSeasonCoefficient(inventory.SeasonCoefficientID);
                region.SeasonCoefficient = seasonCoefficient;

                // Определяем коэффициент качества
                var qualityCoefficient = GenerateQualityCoefficient(inventory.QualityCoefficientID, sellingDirection);

                region.AddChannel(new CalcChannel(
                    id, uid, await name, inventoryUid, region, await audience, saleType, isOrbital, qualityCoefficient
                ));
            }
        }

        /// <summary>
        ///     Метод генерации лимита качества
        /// </summary>
        /// <param name="id">Идентификатор лимита качества</param>
        /// <param name="sellingDirection">Направление продаж</param>
        /// <returns>Сгенерированный лимит качества</returns>
        private static CalcQualityLimit GenerateQualityCoefficient(int id, CalcSellingDirection sellingDirection) {
            // Определяем лимит качества, если он уже сгенерирован
            var qualityLimit = QualityLimits.FirstOrDefault(x => x.ID == id);
            if (qualityLimit != null) {
                return qualityLimit;
            }

            var limits = FindQualityCoefficients(id).QualityCoefficientValues;

            // Fix (для Фед. ТВ) или float (для Рег. и Тем. ТВ)
            QualityCoefficientValue fixOrFloat;
            // Fix prime (для Фед. ТВ) или float prime (для Рег. и Тем. ТВ)
            QualityCoefficientValue fixOrFloatPrime;
            // Fix off prime (для Фед. ТВ) или float off prime (для Рег. и Тем. ТВ)
            QualityCoefficientValue fixOrFloatOffPrime;

            // Super fix (для Фед. ТВ) или fix (для Рег. и Тем. ТВ)
            QualityCoefficientValue superFixOrFix;
            // Super fix prime (для Фед. ТВ) или fix prime (для Рег. и Тем. ТВ)
            QualityCoefficientValue superFixOrFixPrime;
            // Super fix off prime (для Фед. ТВ) или fix off prime (для Рег. и Тем. ТВ)
            QualityCoefficientValue superFixOrFixOffPrime;

            switch (sellingDirection.ID) {
                // Федеральное ТВ
                case 1:
                    fixOrFloat = limits.First(x => x.TimeSlotID         == 16);
                    fixOrFloatPrime = limits.First(x => x.TimeSlotID    == 18);
                    fixOrFloatOffPrime = limits.First(x => x.TimeSlotID == 17);

                    superFixOrFix = limits.First(x => x.TimeSlotID         == 11);
                    superFixOrFixPrime = limits.First(x => x.TimeSlotID    == 13);
                    superFixOrFixOffPrime = limits.First(x => x.TimeSlotID == 12);
                    break;
                // Региональное ТВ
                case 2:
                    fixOrFloat = limits.First(x => x.TimeSlotID         == 6);
                    fixOrFloatPrime = limits.First(x => x.TimeSlotID    == 8);
                    fixOrFloatOffPrime = limits.First(x => x.TimeSlotID == 7);

                    superFixOrFix = limits.First(x => x.TimeSlotID         == 1);
                    superFixOrFixPrime = limits.First(x => x.TimeSlotID    == 3);
                    superFixOrFixOffPrime = limits.First(x => x.TimeSlotID == 2);
                    break;
                // Тематическое ТВ
                case 4:
                    fixOrFloat = limits.First(x => x.TimeSlotID         == 26);
                    fixOrFloatPrime = limits.First(x => x.TimeSlotID    == 28);
                    fixOrFloatOffPrime = limits.First(x => x.TimeSlotID == 29);

                    superFixOrFix = limits.First(x => x.TimeSlotID         == 23);
                    superFixOrFixPrime = limits.First(x => x.TimeSlotID    == 24);
                    superFixOrFixOffPrime = limits.First(x => x.TimeSlotID == 25);
                    break;
                default:
                    throw new NotImplementedException();
            }

            // Иначе генерируем лимит качества
            qualityLimit = new CalcQualityLimit(
                id,
                fixOrFloat.MinimumShare, fixOrFloat.MaximumShare,
                fixOrFloatPrime.MinimumShare, fixOrFloatPrime.MaximumShare,
                fixOrFloatOffPrime.MinimumShare, fixOrFloatOffPrime.MaximumShare,
                superFixOrFix.MinimumShare, superFixOrFix.MaximumShare,
                superFixOrFixPrime.MinimumShare, superFixOrFixPrime.MaximumShare,
                superFixOrFixOffPrime.MinimumShare, superFixOrFixOffPrime.MaximumShare
            );

            QualityLimits.Add(qualityLimit);

            return qualityLimit;
        }

        /// <summary>
        ///     Метод генерации сезонного коэффициента
        /// </summary>
        /// <param name="id">Идентификатор сезонного коэффициента</param>
        /// <returns>Сгенерированный сезонный коэффициент</returns>
        private static CalcSeasonCoefficient GenerateSeasonCoefficient(int id) {
            // Определяем сезонный коэффициент, если он уже сгенерирован
            var seasonCoefficient = SeasonCoefficients.FirstOrDefault(x => x.ID == id);
            if (seasonCoefficient != null) {
                return seasonCoefficient;
            }

            // Иначе генерируем сезонный коэффициент
            seasonCoefficient = new CalcSeasonCoefficient(
                id,
                GetActualDictionaryData().Result.Year,
                FindSeasonCoefficients(id)
            );

            SeasonCoefficients.Add(seasonCoefficient);

            return seasonCoefficient;
        }

        /// <summary>
        ///     Метод генерации аудитории
        /// </summary>
        /// <param name="name">Наименование аудитории</param>
        /// <returns>Сгенерированный экземпляр аудитории</returns>
        private static async Task<CalcAudience?> GenerateAudience(string? name = null) {
            // Нормализуем наименование аудитории
            name = await CalculateUtils.NormalizeAudienceName(name);

            if (string.IsNullOrWhiteSpace(name)) {
                return null;
            }

            // Определяем аудиторию, если она уже сгенерирована
            var audience = Audiences.FirstOrDefault(x => x.Name == name);
            if (audience != null) {
                return audience;
            }

            // Иначе генерируем аудиторию
            audience = new CalcAudience(name!);

            Audiences.Add(audience);

            return audience;
        }

        /// <summary>
        ///     Метод генерации региона
        /// </summary>
        /// <param name="sellingDirection">Направление продаж</param>
        /// <param name="id">Идентификатор географии</param>
        /// <returns>Сгенерированный экземпляр региона</returns>
        private static CalcRegion GenerateRegion(CalcSellingDirection sellingDirection, int? id = null) {
            // Если идентификатор региона не задан (Федеральное и Тематическое ТВ) - задаём вручную
            id ??= sellingDirection.ID switch {
                1 => 1,
                4 => 666,
                _ => throw new NotImplementedException()
            };

            // Определяем регион, если он уже сгенерирован
            var region = Regions.FirstOrDefault(x => x.ID == id);
            if (region != null) {
                return region;
            }

            // Иначе генерируем регион
            var geography = FindGeography(id) ?? new Geography {
                GeographyID = id.Value,
                Name = sellingDirection.Name
            };

            region = new CalcRegion(sellingDirection, geography.GeographyID, geography.Name);

            Regions.Add(region);

            return region;
        }

        /// <summary>
        ///     Метод генерации направления продаж
        /// </summary>
        /// <param name="id">Идентификатор направления продаж</param>
        /// <returns>Сгенерированный экземпляр направления продаж</returns>
        private static CalcSellingDirection GenerateSellingDirection(int id) {
            // Определяем направление продаж, если оно уже сгенерировано
            var sellingDirection = SellingDirections.FirstOrDefault(x => x.ID == id);
            if (sellingDirection != null) {
                return sellingDirection;
            }

            // Иначе генерируем направление продаж
            var mediaType = FindMediaType(id);
            sellingDirection = new CalcSellingDirection(mediaType.ID, mediaType.Name);

            SellingDirections.Add(sellingDirection);

            return sellingDirection;
        }

        /// <summary>
        ///     Метод пост обработки аудиторий для измеряемых минутных каналов
        /// </summary>
        private static async Task PostProcessChannelsAudiences() {
            foreach (var channel in SellingDirections.SelectMany(x => x.Regions).SelectMany(x => x.Channels)) {
                // Если не измеряемый регион, или название аудитории присутсвует - переходим к следующему каналу
                if (!channel.Region.IsMeasurable || !string.IsNullOrWhiteSpace(channel.Audience?.Name)) {
                    continue;
                }

                // Переопределяем аудиторию канала
                channel.Audience = await GenerateAudience("ALL 4+");
            }
        }

        /// <summary>
        ///     Метод поиска географии по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор искомой географии</param>
        /// <returns>География</returns>
        private static Geography? FindGeography(int? id) => GetActualDictionaryData().Result
           .Geographies.FirstOrDefault(x => x.GeographyID == id);

        /// <summary>
        ///     Метод поиска сезонных коэффициентов
        /// </summary>
        /// <param name="id">Идентификатор сезонных коэффициентов</param>
        /// <returns>Сезонные коэффициенты</returns>
        private static double[] FindSeasonCoefficients(int id) => GetActualDictionaryData().Result
           .SeasonCoefficients.First(x => x.ID == id).SeasonCoefficientValues;

        /// <summary>
        ///     Метод поиска коэффициентов качества
        /// </summary>
        /// <param name="id">Идентификатор коэффициентов качества</param>
        /// <returns>Коэффициент качества</returns>
        private static QualityCoefficient FindQualityCoefficients(int id) => GetActualDictionaryData().Result
           .QualityCoefficients.First(x => x.ID == id);

        /// <summary>
        ///     Метод поиска типа продаж инвентаря канала
        /// </summary>
        /// <param name="id">Идентификатор типа продаж</param>
        /// <returns>Тип продаж инвентаря канала</returns>
        private static InventoryUnit FindSaleType(int id) => GetActualDictionaryData().Result
           .InventoryUnits.First(x => x.ID == id);

        /// <summary>
        ///     Метод поиска направления продаж инвентаря канала
        /// </summary>
        /// <param name="id">Идентификатор направления продаж</param>
        /// <returns>Направление продаж инвентаря канала</returns>
        private static MediaType FindMediaType(int id) => GetActualDictionaryData().Result
           .MediaTypes.First(x => x.ID == id);

        /// <summary>
        ///     Метод определения признака "Орбитальное вещание"
        /// </summary>
        /// <param name="id">Идентификатор медиа</param>
        /// <returns>Значение признака "Орбитальное вещание"</returns>
        private static bool DefineIsOrbital(int id) => GetActualDictionaryData().Result
           .MassMedias.First(x => x.ID == id).BroadcastType == BroadcastTypeEnum.Orbit;

        #endregion
    }
}
﻿using System.Collections.Generic;

namespace RMPT.NRA.Models {
    public class CalcSellingDirection {
        public readonly int ID;
        public readonly string Name;
        public readonly HashSet<CalcRegion> Regions = new();

        public CalcSellingDirection(int id, string name) {
            ID = id;
            Name = name;
        }

        public void AddRegion(CalcRegion region) {
            if (Regions.Contains(region)) {
                return;
            }

            Regions.Add(region);
        }

        private bool Equals(CalcSellingDirection other) => ID == other.ID;

        public override bool Equals(object? obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }

            if (ReferenceEquals(this, obj)) {
                return true;
            }

            return obj.GetType() == GetType() && Equals((CalcSellingDirection)obj);
        }

        public override int GetHashCode() => ID;

        public static bool operator ==(CalcSellingDirection? left, CalcSellingDirection? right) => Equals(left, right);

        public static bool operator !=(CalcSellingDirection? left, CalcSellingDirection? right) => !Equals(left, right);
    }
}
﻿namespace RMPT.NRA.Models {
    public class CalcAudience {
        public readonly string Name;

        public CalcAudience(string name) {
            Name = name;
        }

        private bool Equals(CalcAudience other) => Name == other.Name;

        public override bool Equals(object? obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }

            if (ReferenceEquals(this, obj)) {
                return true;
            }

            return obj.GetType() == GetType() && Equals((CalcAudience)obj);
        }

        public override int GetHashCode() => Name.GetHashCode();

        public static bool operator ==(CalcAudience? left, CalcAudience? right) => Equals(left, right);

        public static bool operator !=(CalcAudience? left, CalcAudience? right) => !Equals(left, right);
    }
}
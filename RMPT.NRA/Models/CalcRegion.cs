﻿using System.Collections.Generic;
using System.Linq;

namespace RMPT.NRA.Models {
    public class CalcRegion {
        public readonly HashSet<CalcChannel> Channels = new();
        public readonly int ID;
        public readonly string Name;
        public readonly CalcSellingDirection SellingDirection;
        public CalcSeasonCoefficient? SeasonCoefficient;

        public CalcRegion(CalcSellingDirection sellingDirection, int id, string name) {
            ID = id;
            Name = name;
            SellingDirection = sellingDirection;
        }

        public bool IsMeasurable => Channels.Any(x => x.SaleType == CalcSaleType.GRP);

        public void AddChannel(CalcChannel channel) {
            if (Channels.Contains(channel)) {
                return;
            }

            Channels.Add(channel);
        }

        private bool Equals(CalcRegion other) => ID == other.ID;

        public override bool Equals(object? obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }

            if (ReferenceEquals(this, obj)) {
                return true;
            }

            return obj.GetType() == GetType() && Equals((CalcRegion)obj);
        }

        public override int GetHashCode() => ID;

        public static bool operator ==(CalcRegion? left, CalcRegion? right) => Equals(left, right);

        public static bool operator !=(CalcRegion? left, CalcRegion? right) => !Equals(left, right);
    }
}
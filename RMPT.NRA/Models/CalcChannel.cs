﻿namespace RMPT.NRA.Models {
    public class CalcChannel {
        public readonly int ID;
        public readonly int InventoryUID;
        public readonly bool IsOrbital;
        public readonly string Name;
        public readonly CalcQualityLimit QualityLimit;

        public readonly CalcRegion Region;
        public readonly CalcSaleType SaleType;
        public readonly int UID;

        public CalcChannel(
            int id, int uid, string name, int inventoryUid,
            CalcRegion region, CalcAudience? audience, CalcSaleType saleType, bool isOrbital,
            CalcQualityLimit qualityLimit
        ) {
            ID = id;
            UID = uid;
            Name = name;
            InventoryUID = inventoryUid;
            Region = region;
            Audience = audience;
            SaleType = saleType;
            IsOrbital = isOrbital;
            QualityLimit = qualityLimit;
        }

        public CalcAudience? Audience { get; set; }

        private bool Equals(CalcChannel other) => UID == other.UID;

        public override bool Equals(object? obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }

            if (ReferenceEquals(this, obj)) {
                return true;
            }

            return obj.GetType() == GetType() && Equals((CalcChannel)obj);
        }

        public override int GetHashCode() => UID;

        public static bool operator ==(CalcChannel? left, CalcChannel? right) => Equals(left, right);

        public static bool operator !=(CalcChannel? left, CalcChannel? right) => !Equals(left, right);
    }
}
﻿using System;
using System.Collections.Generic;

namespace RMPT.NRA.Models {
    public class CalcSeasonCoefficient {
        public readonly int ID;
        public readonly Dictionary<DateTime, double> SeasonCoefficients = new();

        public CalcSeasonCoefficient(int id, int year, IReadOnlyList<double> values) {
            ID = id;
            for (var i = 0; i < values.Count; i++) {
                SeasonCoefficients.Add(new DateTime(year, i + 1, 1), values[i]);
            }
        }

        private bool Equals(CalcSeasonCoefficient other) =>
            ID == other.ID && SeasonCoefficients.Equals(other.SeasonCoefficients);

        public override bool Equals(object? obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }

            if (ReferenceEquals(this, obj)) {
                return true;
            }

            return obj.GetType() == GetType() && Equals((CalcSeasonCoefficient)obj);
        }

        public override int GetHashCode() {
            unchecked {
                return (ID * 397) ^ SeasonCoefficients.GetHashCode();
            }
        }

        public static bool operator ==(CalcSeasonCoefficient? left, CalcSeasonCoefficient? right) =>
            Equals(left, right);

        public static bool operator !=(CalcSeasonCoefficient? left, CalcSeasonCoefficient? right) =>
            !Equals(left, right);
    }
}
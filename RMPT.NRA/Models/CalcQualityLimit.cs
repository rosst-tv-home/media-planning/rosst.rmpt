﻿namespace RMPT.NRA.Models {
    public class CalcQualityLimit {
        public readonly double FixOrFloatMax;
        public readonly double FixOrFloatMin;
        public readonly double FixOrFloatOffPrimeMax;
        public readonly double FixOrFloatOffPrimeMin;
        public readonly double FixOrFloatPrimeMax;
        public readonly double FixOrFloatPrimeMin;
        public readonly int ID;
        public readonly double SuperFixOrFixMax;
        public readonly double SuperFixOrFixMin;
        public readonly double SuperFixOrFixOffPrimeMax;
        public readonly double SuperFixOrFixOffPrimeMin;
        public readonly double SuperFixOrFixPrimeMax;
        public readonly double SuperFixOrFixPrimeMin;

        public CalcQualityLimit(
            int id, double fixOrFloatMin, double fixOrFloatMax, double fixOrFloatPrimeMin, double fixOrFloatPrimeMax,
            double fixOrFloatOffPrimeMin, double fixOrFloatOffPrimeMax, double superFixOrFixMin,
            double superFixOrFixMax, double superFixOrFixPrimeMin, double superFixOrFixPrimeMax,
            double superFixOrFixOffPrimeMin, double superFixOrFixOffPrimeMax
        ) {
            ID = id;
            FixOrFloatMin = fixOrFloatMin;
            FixOrFloatMax = fixOrFloatMax;
            FixOrFloatPrimeMin = fixOrFloatPrimeMin;
            FixOrFloatPrimeMax = fixOrFloatPrimeMax;
            FixOrFloatOffPrimeMin = fixOrFloatOffPrimeMin;
            FixOrFloatOffPrimeMax = fixOrFloatOffPrimeMax;
            SuperFixOrFixMin = superFixOrFixMin;
            SuperFixOrFixMax = superFixOrFixMax;
            SuperFixOrFixPrimeMin = superFixOrFixPrimeMin;
            SuperFixOrFixPrimeMax = superFixOrFixPrimeMax;
            SuperFixOrFixOffPrimeMin = superFixOrFixOffPrimeMin;
            SuperFixOrFixOffPrimeMax = superFixOrFixOffPrimeMax;
        }

        private bool Equals(CalcQualityLimit other) => ID == other.ID;

        public override bool Equals(object? obj) {
            if (ReferenceEquals(null, obj)) {
                return false;
            }

            if (ReferenceEquals(this, obj)) {
                return true;
            }

            return obj.GetType() == GetType() && Equals((CalcQualityLimit)obj);
        }

        public override int GetHashCode() => ID;

        public static bool operator ==(CalcQualityLimit? left, CalcQualityLimit? right) => Equals(left, right);

        public static bool operator !=(CalcQualityLimit? left, CalcQualityLimit? right) => !Equals(left, right);
    }
}
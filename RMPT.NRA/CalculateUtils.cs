﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RMPT.NRA {
    internal static class CalculateUtils {
        /// <summary>
        ///     Метод нормализации наименвоания канала НРА Считалки
        /// </summary>
        /// <param name="name">Оригинальное наименование канала</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        internal static Task<string> NormalizeChannelName(string name) {
            // Группы совпадений по регулярному выражению
            var groups = Regex.Match(
                    name.Trim(),
                    @"^(.+?)(\s?-?\s?(?<![a-zа-яA-ZА-Я\d])[oO0Оо])$"
                )
               .Groups
               .Cast<Group>()
               .Skip(1)
               .Where(group => !string.IsNullOrWhiteSpace(group.Value))
               .ToList();

            // Нормализированное наименование канала
            string normalizedName;

            // Если есть совпадения по регулярному выражению, то это орбитальный канал
            if (groups.Count != 0) {
                normalizedName = groups.ElementAt(0).Value.Trim() + " - 0";
            } else {
                normalizedName = name.Trim();
            }

            return Task.FromResult(normalizedName);
        }

        /// <summary>
        ///     Метод нормализации наименования аудитории канала НРА Считалки
        /// </summary>
        /// <param name="name">Оригинальное наименование аудитории</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        internal static async Task<string?> NormalizeAudienceName(string? name) => await Task.Run(() => {
            // Если наименование аудитории пустое - завершаем выполнение метода
            if (string.IsNullOrWhiteSpace(name?.Trim())) {
                return null;
            }

            // Группы совпадений по регулярному выражению
            var groups = Regex.Match(
                    name!.Trim(),
                    @"^(([а-яА-Яa-zA-Z]*)=?\s?(\d{1,2}[-\+]\d{0,2})\s?([a-z]*)?([A-C|BigTV]*)?)$"
                )
               .Groups
               .Cast<Group>()
               .Skip(2)
               .Where(group => !string.IsNullOrWhiteSpace(group.Value))
               .ToList();

            // Нормализированное наименование аудитории
            var normalizedName = groups.Aggregate("", (value, group) => {
                // Если "BigTV" - не включаем в строку
                if (groups.IndexOf(group) == 2) {
                    return value;
                }

                // Если возраст - включаем в строку
                if (groups.IndexOf(group) != 0) {
                    return value + group.Value;
                }

                // Если пол - меняем на анлийский и включаем в строку
                if (
                    group.Value.Equals("ВСЕ", StringComparison.InvariantCultureIgnoreCase) ||
                    group.Value.Equals("ALL", StringComparison.InvariantCultureIgnoreCase)
                ) {
                    value += "ALL ";
                } else if (
                    group.Value.Equals("М", StringComparison.InvariantCultureIgnoreCase) ||
                    group.Value.Equals("M", StringComparison.InvariantCultureIgnoreCase)
                ) {
                    value += "M ";
                } else if (
                    group.Value.Equals("Ж", StringComparison.InvariantCultureIgnoreCase) ||
                    group.Value.Equals("W", StringComparison.InvariantCultureIgnoreCase)
                ) {
                    value += "W ";
                } else {
                    throw new NotImplementedException("Не удалось определить аудиторию");
                }

                return value;
            });

            return normalizedName;
        });
    }
}
﻿namespace RMPT.CORE {
    /// <summary>
    ///     Делегат на метод обработки события изменения значения свойства объекта, реализиющего интерфейс
    ///     <see cref="IObservable" />
    /// </summary>
    /// <param name="sender">Объект, являющийся инициатором события</param>
    /// <param name="argument">Аргумент события</param>
    public delegate void EventHandler(object sender, EventArgument argument);
}
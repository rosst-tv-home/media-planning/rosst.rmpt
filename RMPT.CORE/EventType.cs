﻿namespace RMPT.CORE {
    /// <summary>
    ///     Тип направленности события
    /// </summary>
    public enum EventType {
        /// <summary>
        ///     Событие вызванное из пользовательского интерфейса
        /// </summary>
        Ui,

        /// <summary>
        ///     Событие доставляется конкретному объекту
        /// </summary>
        Direct,

        /// <summary>
        ///     Событие доставляется к родительским объектам
        /// </summary>
        Bubbling,

        /// <summary>
        ///     Событие доставляется к дочерним объектам
        /// </summary>
        Tunneling
    }
}
﻿namespace RMPT.CORE {
    /// <summary>
    ///     Интерфейс для реализации объекта, генерируещего событие изменения значения свойства
    /// </summary>
    public interface IObservable {
        /// <summary>
        ///     Событие изменения значения свойства этого объекта
        /// </summary>
        event EventHandler? OnPropertyChangedCore;
    }
}
﻿using System.Runtime.CompilerServices;

namespace RMPT.CORE {
    /// <summary>
    ///     Модель аргумента события
    /// </summary>
    public class EventArgument {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="value">Новое значение свойства</param>
        /// <param name="eventType">Тип направленности события</param>
        /// <param name="propertyName">Наименование свойства, значение которого изменилось</param>
        public EventArgument(
            object? value = null,
            EventType eventType = EventType.Ui,
            [CallerMemberName] string propertyName = ""
        ) {
            Value = value;
            EventType = eventType;
            PropertyName = propertyName;
        }

        #region Свойства

        /// <summary>
        ///     Наименование свойства, значение которого было изменено
        /// </summary>
        public string PropertyName { get; }

        /// <summary>
        ///     Тип направленности события
        /// </summary>
        public EventType EventType { get; }

        /// <summary>
        ///     Новое значение свойства
        /// </summary>
        public object? Value { get; }

        #endregion
    }
}
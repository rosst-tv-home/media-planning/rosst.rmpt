﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace RMPT.FTP {
    public static class UpdaterFTP {
        public static async Task<List<List<int>>> CheckUpdates() {
            var request = WebRequest.Create("ftp://192.168.0.14/RMPT");
            request.Credentials = new NetworkCredential("RMP", "");
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            var response = await request.GetResponseAsync();
            var responseStream = response.GetResponseStream() ?? throw new NullReferenceException(
                "Не удалось получить данные с FTP сервера"
            );
            var responseString = await new StreamReader(responseStream).ReadToEndAsync();
            responseString = responseString.Replace("RMPT/", "");

            // Получаем коллекцию файлов
            var files = responseString.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

            // Текущая версия RMP
            var currentVersionStringify = FileVersionInfo.GetVersionInfo(
                AppDomain.CurrentDomain.BaseDirectory + @"\RMPT.exe"
            ).FileVersion;
            
            // Текущая версия RMP, переведённая в числа
            var currentVersionNumeric = new List<int>();
            currentVersionStringify.Split('.').ToList().ForEach(character => {
                if (int.TryParse(character, out var number)) {
                    currentVersionNumeric.Add(number);
                }
            });
            
            // Коллекция версий RMP на FTP сервере, переведённые в числа
            var ftpVersions = new List<List<int>>();
            files.ForEach(versionString => {
                // Коллекция чисел текущей рассматриваемой версии
                var versionList = new List<int>();
                versionString.Split('.').ToList().ForEach(character => {
                    if (int.TryParse(character, out var number)) {
                        versionList.Add(number);
                    }
                });
                // Добавляем версию, переведённую в числа в коллекцию версий
                ftpVersions.Add(versionList);
            });
            // Фильтруем коллекцию версий FTP и возвращаем версии, которые актуальней текущей
            var versions = ftpVersions.Where(v => v.Count == 3).Where(ftpVersion => {
                // Если "major версия больше"
                if (ftpVersion[0] > currentVersionNumeric[0]) {
                    return true;
                }

                // Если "major версия равна и sub версия больше"
                if (ftpVersion[0] == currentVersionNumeric[0] && ftpVersion[1] > currentVersionNumeric[1]) {
                    return true;
                }

                // Если "major и sub версии равны, а minor версия больше"
                return ftpVersion[0] == currentVersionNumeric[0] &&
                       ftpVersion[1] == currentVersionNumeric[1] &&
                       ftpVersion[2] > currentVersionNumeric[2];
            }).ToList();

            // Если есть более новые версии
            return versions;
        }

        /// <summary>
        ///     Метод обновления RMP с FTP сервера.
        /// </summary>
        /// <param name="versions">Коллекция доступных актуальных версий RMP на FTP сервере.</param>
        public static async Task UpdateFromFTP(IEnumerable<List<int>> versions) {
            // Самая последняя версия
            var latestVersion = versions
                .OrderBy(version => version[0])
                .ThenBy(version => version[1])
                .ThenBy(version => version[2])
                .ToList()
                .Last();

            var archiveName = $"{latestVersion[0]}.{latestVersion[1]}.{latestVersion[2]}.zip";

            var request = WebRequest.Create($"ftp://192.168.0.14/RMPT/{archiveName}");
            request.Credentials = new NetworkCredential("RMP", "");
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            // Скачиваем файл
            using (var response = await request.GetResponseAsync()) {
                using (var stream = response.GetResponseStream()) {
                    if (stream == null) {
                        return;
                    }
                    using (var file = File.Create($"{AppDomain.CurrentDomain.BaseDirectory}/{archiveName}")) {
                        await stream.CopyToAsync(file);
                    }
                }
            }

            using (var file = File.OpenRead($"{AppDomain.CurrentDomain.BaseDirectory}/{archiveName}")) {
                using (var archive = new ZipArchive(file, ZipArchiveMode.Read)) {
                    archive.ExtractToDirectory(AppDomain.CurrentDomain.BaseDirectory + "/temp");
                }
            }
            
            File.Delete($"{AppDomain.CurrentDomain.BaseDirectory}/{archiveName}");

            // Файлы RMP Updater
            var files = Directory
                .GetFiles(AppDomain.CurrentDomain.BaseDirectory + "/temp")
                .Where(name => name.Contains("UPDATER"))
                .ToList();

            // Если они есть - обновляем файлы
            if (files.Any()) {
                files.ForEach(fileName => {
                    var localFileName = $"{AppDomain.CurrentDomain.BaseDirectory}\\{Path.GetFileName(fileName)}";
                    if (File.Exists(localFileName)) {
                        File.SetAttributes(localFileName, FileAttributes.Normal);
                        File.Delete(localFileName);
                    }

                    File.Move(fileName, localFileName);
                });
            }

            await Task.Delay(250);
            var startInfo = new ProcessStartInfo(
                $"{AppDomain.CurrentDomain.BaseDirectory}\\RMPT.UPDATER.exe"
            ) { UseShellExecute = true };
            Process.Start(startInfo);
            Environment.Exit(0);
        }
    }
}
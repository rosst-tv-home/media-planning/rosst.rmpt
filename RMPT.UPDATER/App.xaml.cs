﻿using Sentry;
using System.Windows.Threading;

namespace RMPT.UPDATER {
    public partial class App {
        public App() {
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            SentrySdk.Init(o => {
                // Tells which project in Sentry to send events to:
                o.Dsn = "https://c1ad54781cd2499f85f8f4be8dd61fac@o307887.ingest.sentry.io/5942559";
                // When configuring for the first time, to see what the SDK is doing:
                o.Debug = true;
                // Set traces_sample_rate to 1.0 to capture 100% of transactions for performance monitoring.
                // We recommend adjusting this value in production.
                o.TracesSampleRate = 1.0;
            });
        }

        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e) {
            SentrySdk.CaptureException(e.Exception);

            // If you want to avoid the application from crashing:
            e.Handled = true;
        }
    }
}
﻿using RMPT.COMMON.Dictionaries;
using RMPT.COMMON.Models;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UPDATER {
    public class MainVM : AbstractVM {
        public string Title {
            get => GetProperty(() => Title);
            set => SetProperty(() => Title, value);
        }

        public string Subtitle {
            get => GetProperty(() => Subtitle);
            set => SetProperty(() => Subtitle, value);
        }

        public string Copyright {
            get => GetProperty(() => Copyright);
            set => SetProperty(() => Copyright, value);
        }

        public bool IsIndeterminate {
            get => GetProperty(() => IsIndeterminate);
            set => SetProperty(() => IsIndeterminate, value);
        }

        public string Status {
            get => GetProperty(() => Status);
            set => SetProperty(() => Status, value);
        }

        public MainVM() {
            this.Title = CommonDictionary.ApplicationName.ToUpperInvariant();
            this.Subtitle = "Обновление";
            this.Copyright =
                $"Copyright {(DateTime.Now.Year - 2).ToString()} - " +
                $"{DateTime.Now.Year.ToString()} "                   +
                $"{CommonDictionary.CompanyName}";
            IsIndeterminate = true;
        }

        protected override async Task OnLoadedAsync(RoutedEventArgs arguments) {
            Collection<string> files = new();
            
            try {
                foreach (var file in Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "temp"))) {
                    if (Path.GetFileName(file).Contains("UPDATER")) continue;
                    files.Add(file);
                }
            } catch (Exception exception) {
                ShowErrorDialogWindow(exception.Message);
            }

            if (!files.Any()) {
                await this.Initialize();
                return;
            }

            foreach (var updateFilePath in files) {
                string fileName = Path.GetFileName(updateFilePath);
                string localFilePath = Path.Combine(Environment.CurrentDirectory, fileName);

                if (!File.Exists(localFilePath)) await this.MoveFile(updateFilePath, localFilePath);
                else await this.ReplaceFile(updateFilePath, localFilePath);
            }

            try {
                Directory.Delete($"{Environment.CurrentDirectory}\\temp", true);
            } catch (Exception exception) {
                this.ShowErrorDialogWindow(exception.Message);
                throw;
            }

            await this.Initialize();
        }

        private async Task MoveFile(string source, string destination) {
            try {
                await this.ChangeStatus($"Перемещение файла: {Path.GetFileName(source)}");
                File.Move(source, destination);
                await Task.Delay(25);
            } catch (Exception exception) {
                this.ShowErrorDialogWindow(exception.Message);
                throw;
            }
        }

        private async Task ReplaceFile(string source, string destination) {
            try {
                await this.ChangeStatus($"Замена файла: {Path.GetFileName(source)}");
                File.Replace(source, destination, null, true);
                await Task.Delay(25);
            } catch (Exception exception) {
                this.ShowErrorDialogWindow(exception.Message);
                throw;
            }
        }

        private async Task Initialize() {
            this.Status = $"Запуск {CommonDictionary.ApplicationName.ToUpperInvariant()}";
            await Task.Delay(250);

            Process.Start(new ProcessStartInfo(
                $"{Environment.CurrentDirectory}\\RMPT.exe"
            ) {
                UseShellExecute = true
            });
            Environment.Exit(0);
        }

        private async Task ChangeStatus(string message) {
            this.Status = message;
            await Task.Delay(25);
        }

        private void ShowErrorDialogWindow(string message) {
            MessageBox.Show(message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            Environment.Exit(0);
        }
    }
}
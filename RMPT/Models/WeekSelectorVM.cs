﻿using RMPT.COMMON;
using RMPT.COMMON.Models;
using System;
using System.Globalization;

namespace RMPT.Models {
    /// <summary>
    ///     Модель недельного периода для выборки
    /// </summary>
    public class WeekSelectorVM : AbstractVM {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="month">Месяц</param>
        /// <param name="dateFrom">Дата начала</param>
        /// <param name="dateBefore">Дата окончания</param>
        public WeekSelectorVM(int index, MonthSelectorVM month, DateTime dateFrom, DateTime dateBefore) {
            Index = index;
            Month = month;
            DateFrom = dateFrom;
            DateBefore = dateBefore;
            YearWeekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
                DateFrom,
                CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule,
                CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek
            );
        }

        #endregion

        #region Свойсвта

        public int Index;

        /// <summary>
        ///     Дата начала
        /// </summary>
        public DateTime DateFrom { get; }

        /// <summary>
        ///     Дата окончания
        /// </summary>
        public DateTime DateBefore { get; }

        /// <summary>
        ///     Номер недели в году
        /// </summary>
        public int YearWeekNumber { get; }

        /// <summary>
        ///     Месяц
        /// </summary>
        public MonthSelectorVM Month { get; }

        #endregion
    }
}
﻿using RMPT.COMMON;
using RMPT.COMMON.Models;

namespace RMPT.Models {
    /// <summary>
    ///     Модель хронометража для выборки
    /// </summary>
    public class TimingSelectorVM : AbstractVM {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="value">Значение хронометража</param>
        public TimingSelectorVM(int value) {
            Value = value;
        }

        #endregion

        #region Свойства

        /// <summary>
        ///     Значение хронометража
        /// </summary>
        public int Value {
            get => GetProperty(() => Value);
            private set => SetProperty(() => Value, value);
        }

        #endregion
    }
}
﻿namespace RMPT.Models {
    /// <summary>
    ///     Тип отчёта медиаплана
    /// </summary>
    public class MediaPlanType {
        /// <summary>
        ///     Наименование типа
        /// </summary>
        public string Name { get; }

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="name">Наименование типа</param>
        public MediaPlanType(string name) {
            this.Name = name;
        }
    }
}
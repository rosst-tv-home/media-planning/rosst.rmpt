﻿using RMPT.COMMON;
using RMPT.COMMON.Models;
using System;

namespace RMPT.Models {
    /// <summary>
    ///     Модель месячного периода для выборки
    /// </summary>
    public class MonthSelectorVM : AbstractVM {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="date">Дата</param>
        public MonthSelectorVM(int index, DateTime date) {
            Index = index;
            Date = date;
        }

        #endregion

        #region Свойства

        public int Index;

        /// <summary>
        ///     Дата
        /// </summary>
        public DateTime Date { get; }

        #endregion
    }
}
﻿namespace RMPT.Models {
    /// <summary>
    ///     Модель типа флоучарта
    /// </summary>
    public class FlowChartType {
        /// <summary>
        ///     Название типа флоучарта
        /// </summary>
        public string Name { get; }

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="name">Название типа флоучарта</param>
        public FlowChartType(string name) {
            Name = name;
        }
    }
}
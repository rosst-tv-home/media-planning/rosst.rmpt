﻿using DevExpress.Mvvm;
using RMPT.DATA.DataBase;
using RMPT.DATA.DataBase.Entities.Dictionaries.Brand;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Dictionaries.Other;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using RMPT.DATA.DataBase.Repositories;
using RMPT.EVENT;
using RMPT.NRA;
using RMPT.NRA.Models;
using RMPT.RATING;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.Initializers {
    /// <summary>
    ///     Класс инициализации приложения
    /// </summary>
    public static class ApplicationInitializer {
        /// <summary>
        ///     Справочник наценок за хронометраж
        /// </summary>
        private static readonly Dictionary<string[], Dictionary<int, Dictionary<int, double>>> TimingMargins = new() {
            {
                new[] {
                    "Федеральное ТВ",
                    "Тематическое ТВ",
                    "Москва"
                },
                new Dictionary<int, Dictionary<int, double>> {
                    {
                        2021, new Dictionary<int, double> {
                            {
                                5, 1.3
                            }, {
                                10, 1.15
                            }, {
                                15, 1.05
                            }, {
                                20, 1.0
                            }, {
                                25, 0.98
                            }, {
                                30, 0.96
                            }, {
                                35, 0.95
                            }, {
                                40, 0.95
                            }, {
                                45, 0.95
                            }, {
                                50, 0.95
                            }, {
                                55, 0.95
                            }, {
                                60, 0.9
                            }
                        }
                    }
                }
            }, {
                new[] {
                    "Санкт-Петербург",
                    "Екатеринбург",
                    "Новосибирск",
                    "Нижний Новгород"
                },
                new Dictionary<int, Dictionary<int, double>> {
                    {
                        2021, new Dictionary<int, double> {
                            {
                                5, 1.1
                            }, {
                                10, 1.05
                            }, {
                                15, 1.03
                            }, {
                                20, 1.0
                            }, {
                                25, 0.99
                            }, {
                                30, 0.98
                            }, {
                                35, 0.98
                            }, {
                                40, 0.98
                            }, {
                                45, 0.98
                            }, {
                                50, 0.98
                            }, {
                                55, 0.98
                            }, {
                                60, 0.95
                            }
                        }
                    }
                }
            }
        };

        /// <summary>
        ///     Метод начала инициализации приложения
        /// </summary>
        public static async Task Initialize() {
            #region База данных

            // Если база данных отсутствует
            if (!await CheckDataBaseExistence()) {
                Messenger.Default.Send(new EventInitializerMessage("База данных отсутсвует", MessageType.Error));
                return;
            }

            // Инициализируем начальные данные базы данных
            await InitializeDataBaseData();

            #endregion

            await CheckCalculateDictionaryVersion();

            await UpdateChannelsRatings();
        }

        #region Методы базы данных

        /// <summary>
        ///     Метод проверки наличия базы данных
        /// </summary>
        private static Task<bool> CheckDataBaseExistence() {
            Messenger.Default.Send(new EventInitializerMessage("Проверка наличия базы данных"));
            using var context = new Context();
            return Task.FromResult(context.Database.Exists());
        }

        /// <summary>
        ///     Метод инициализации базы данных
        /// </summary>
        private static async Task InitializeDataBaseData() {
            Messenger.Default.Send(new EventInitializerMessage("Инициализация данных базы данных"));

            var dbQualityLimits = await RepositoryQualityLimits.FindAllWithoutChannels();
            if (dbQualityLimits.Count <= 0 || dbQualityLimits.All(q => q.ID != 999)) {
                await RepositoryQualityLimits.InsertAllAsync(new[] {
                    new DCQualityLimits {
                        ID = 999,
                        FixOrFloatMin = 0,
                        FixOrFloatMax = 1,
                        FixOrFloatPrimeMin = 0,
                        FixOrFloatPrimeMax = 1,
                        SuperFixOrFixMin = 0,
                        SuperFixOrFixMax = 1,
                        FixOrFloatOffPrimeMin = 0,
                        FixOrFloatOffPrimeMax = 1,
                        SuperFixOrFixPrimeMin = 0,
                        SuperFixOrFixPrimeMax = 1,
                        SuperFixOrFixOffPrimeMin = 0,
                        SuperFixOrFixOffPrimeMax = 1
                    }
                });
            }

            var dbRolesTask = RepositoryRoles.FindAllWithUsersAsync();

            var roles = new List<DCRole> {
                new() {
                    Type = "Разработчик",
                    Description = "Разработчик"
                },
                new() {
                    Type = "Администратор",
                    Description = "Администратор"
                },
                new() {
                    Type = "Медиа-планер",
                    Description = "Медиа-планер"
                },
                new() {
                    Type = "Архивариус",
                    Description = "Архивариус"
                }
            };

            var dbRoleNames = (await dbRolesTask).Select(role => role.Type).ToList();
            roles = roles.Where(role => !dbRoleNames.Contains(role.Type)).ToList();
            await RepositoryRoles.InsertAsync(roles);
            var dbRoles = await RepositoryRoles.FindAllWithUsersAsync();

            var users = new List<DCUser>();

            var user = new DCUser {
                Login = "popkov",
                FirstName = "Михаил",
                LastName = "Попков",
                IsActive = true
            };
            user.Roles.Add(new DCUserRole {
                Role = dbRoles.First(role => role.Type == "Разработчик"),
                User = user
            });
            users.Add(user);

            user = new DCUser {
                Login = "pakalin",
                FirstName = "Сергей",
                LastName = "Пакалин",
                IsActive = true
            };
            user.Roles.Add(new DCUserRole {
                Role = dbRoles.First(role => role.Type == "Разработчик"),
                User = user
            });
            users.Add(user);

            var dbUserLogins = (await RepositoryUsers.FindAllWithRolesAsync()).Select(x => x.Login).ToList();
            users = users.Where(x => !dbUserLogins.Contains(x.Login)).ToList();
            await RepositoryUsers.InsertAsync(users);

            var advertisers = new List<DCAdvertiser>();

            var advertiser = new DCAdvertiser {
                Name = "Гедеон Рихтер"
            };
            advertiser.Brands.Add(new DCBrand {
                Name = "Панангин",
                Advertiser = advertiser
            });
            advertiser.Brands.Add(new DCBrand {
                Name = "Аэртал",
                Advertiser = advertiser
            });
            advertiser.Brands.Add(new DCBrand {
                Name = "Стопдиар",
                Advertiser = advertiser
            });
            advertisers.Add(advertiser);

            advertiser = new DCAdvertiser {
                Name = "МПК"
            };
            advertiser.Brands.Add(new DCBrand {
                Name = "Жигули",
                Advertiser = advertiser
            });
            advertiser.Brands.Add(new DCBrand {
                Name = "Мотор",
                Advertiser = advertiser
            });
            advertisers.Add(advertiser);

            var dbAdvertiserNames = (await RepositoryAdvertisers.FindAllWithBrandsAsync()).Select(x => x.Name).ToList();
            advertisers = advertisers.Where(x => !dbAdvertiserNames.Contains(x.Name)).ToList();
            await RepositoryAdvertisers.InsertAsync(advertisers);

            var strategists = new List<DCStrategist> {
                new() {
                    FirstName = "Людмила",
                    LastName = "Моргун"
                },
                new() {
                    FirstName = "Антон",
                    LastName = "Сипович"
                },
                new() {
                    FirstName = "Наталья",
                    LastName = "Моторина"
                }
            };
            var dbStrategistFullNames = (await RepositoryStrategists.FindAllAsync())
               .Select(strategist => $"{strategist.FirstName} {strategist.LastName}")
               .ToList();
            strategists = strategists
               .Where(strategist => !dbStrategistFullNames.Contains($"{strategist.FirstName} {strategist.LastName}"))
               .ToList();
            await RepositoryStrategists.InsertAsync(strategists);
        }

        #endregion

        #region Методы считалки

        /// <summary>
        ///     Метод проверки версии справочника НРА Считалки
        /// </summary>
        private static async Task CheckCalculateDictionaryVersion() {
            Messenger.Default.Send(new EventInitializerMessage("Проверка версии справочника НРА Считалки"));
            await Task.Delay(1000);

            // Последняя версия справочника НРА Считалки
            var actualDictionaryVersion = CalculateWrapper.GetDictionaryVersion();
            // Последняя импортированная версия справочника НРА Считалки в RMPT
            var dataBaseDictionaryVersion = RepositoryCalculateDictionary.GetCurrentVersionAsync();

            // Если версии сходятся - завершаем выполнение метода
            if (await actualDictionaryVersion == dataBaseDictionaryVersion?.ID) {
                return;
            }

            Messenger.Default.Send(
                new EventInitializerMessage("Версия справочника НРА Считалки изменилась, обновление"));
            await Task.Delay(1000);

            Messenger.Default.Send(new EventInitializerMessage(
                $"Обновление версии {(dataBaseDictionaryVersion?.ID ?? 0).ToString()} " +
                $"до версии {actualDictionaryVersion.Result.ToString()}"
            ));
            await Task.Delay(1000);

            Messenger.Default.Send(new EventInitializerMessage(
                "Обновление справочных данных RMPT из справочника НРА Считалки"
            ));
            await Task.Delay(1000);

            // Ожидаем импортированные данные из справочника НРА Считалки
            var sellingDirections = await CalculateWrapper.GenerateData();
            await ImportData(sellingDirections);

            // Сохраняем последнюю версию справочника НРА Считалки в базу данных
            await RepositoryCalculateDictionary.InsertAsync(new DCVersion {
                ID = actualDictionaryVersion.Result
            });

            // Удаляем старую версию справочника НРА Считалки из базы данных
            if (dataBaseDictionaryVersion != null) {
                await RepositoryCalculateDictionary.DeleteAsync(dataBaseDictionaryVersion);
            }
        }

        private static async Task ImportData(HashSet<CalcSellingDirection> sellingDirections) {
            // Проверяем наборы сезонных коэффициентов
            await CheckSeasonCoefficientSets(sellingDirections
               .SelectMany(x => x.Regions)
               .Select(x => x.SeasonCoefficient)
            );

            // Проверяем регионы
            await CheckRegions(sellingDirections.SelectMany(x => x.Regions));

            // Проверяем аудитории
            await CheckAudiences(sellingDirections
               .SelectMany(x => x.Regions)
               .SelectMany(x => x.Channels)
               .Select(x => x.Audience)
               .Where(x => x != null)
               .Cast<CalcAudience>()
               .ToHashSet()
            );

            // Проверяем лимиты качеств
            await CheckQualityLimits(sellingDirections
               .SelectMany(x => x.Regions)
               .SelectMany(x => x.Channels)
               .Select(x => x.QualityLimit)
               .ToHashSet()
            );

            // Проверяем каналы
            await CheckChannels(sellingDirections.SelectMany(x => x.Regions).SelectMany(x => x.Channels));
        }

        private static async Task CheckSeasonCoefficientSets(
            IEnumerable<CalcSeasonCoefficient?> seasonCoefficientSets
        ) {
            // Идентификаторы наборов сезонных коэффициентов из базы данных
            var dbSeasonCoefficientSetIDs = await RepositorySeasonCoefficientSet.FindAllSetIDs();
            // Определяем наборы, которых  нет в базе данных
            var newSeasonCoefficientSets = seasonCoefficientSets
               .Where(x => x != null && !dbSeasonCoefficientSetIDs.Contains(x.ID))
               .ToList();

            // Наборы сезонных коэффициентов, которые будут добавлены в базу данных
            var dbSeasonCoefficientSets = new List<DCSeasonalOddsSet>();

            // Если есть новые сезонные коэффициенты - добавляем в базу данных
            if (newSeasonCoefficientSets.Any()) {
                // Итерируемся по новым сезонным коэффициентам
                foreach (var set in newSeasonCoefficientSets) {
                    // Формируем модель набора сезонных коэффициентов
                    var seasonCoefficientSet = new DCSeasonalOddsSet {
                        ID = set!.ID
                    };
                    // Итерируемся по значениям сезонных коэффициентов
                    foreach (var seasonCoefficient in set.SeasonCoefficients) {
                        // Формируем модель сезонного коэффициента, и добавляем в набор
                        seasonCoefficientSet.SeasonalOdds.Add(new DCSeasonalOdd {
                            Date = seasonCoefficient.Key,
                            Value = seasonCoefficient.Value
                        });
                    }

                    dbSeasonCoefficientSets.Add(seasonCoefficientSet);
                }
            }

            await RepositorySeasonCoefficientSet.InsertAllAsync(dbSeasonCoefficientSets);
        }

        /// <summary>
        ///     Метод проверки аудиторий справочника и базы данных
        /// </summary>
        /// <param name="audiences">Аудитории из справочника НРА Считалки</param>
        private static async Task CheckAudiences(IEnumerable<CalcAudience> audiences) {
            // Определяем аудитории, имеющиеся в базе данных
            var dbAudiences = (await RepositoryAudiences.FindAllWithoutInclude()).Select(x => x.Name).ToHashSet();
            // Определяем аудитории, которых нет в базе данных, но имеются в справочнике
            var newAudiences = audiences
               .Where(x => !dbAudiences.Contains(x.Name))
               .Select(x => new DCAudience {
                    Name = x.Name
                })
               .ToList();

            // Если новых аудиторий нет - завершаем выполнение метода
            if (!newAudiences.Any()) {
                return;
            }

            await RepositoryAudiences.InsertAsync(newAudiences);
        }

        /// <summary>
        ///     Метод проверки лимитов качеств справочника и базы данных
        /// </summary>
        /// <param name="qualityLimits">Лимиты качеств из справочника НРА Считалки</param>
        private static async Task CheckQualityLimits(IEnumerable<CalcQualityLimit> qualityLimits) {
            // Определяем лимиты качеств, имеющиеся в базе данных
            var dbQualityLimits = await RepositoryQualityLimits.FindAllIDs();
            // Определяем лимиты качеств, которых нет в базе данных, но имеются в справочнике
            var newQualityLimits = qualityLimits
               .Where(x => !dbQualityLimits.Contains(x.ID))
               .Select(x => new DCQualityLimits {
                    ID = x.ID,
                    FixOrFloatMin = x.FixOrFloatMin,
                    FixOrFloatMax = x.FixOrFloatMax,
                    FixOrFloatPrimeMin = x.FixOrFloatPrimeMin,
                    FixOrFloatPrimeMax = x.FixOrFloatPrimeMax,
                    FixOrFloatOffPrimeMin = x.FixOrFloatOffPrimeMin,
                    FixOrFloatOffPrimeMax = x.FixOrFloatOffPrimeMax,
                    SuperFixOrFixMin = x.SuperFixOrFixMin,
                    SuperFixOrFixMax = x.SuperFixOrFixMax,
                    SuperFixOrFixPrimeMin = x.SuperFixOrFixPrimeMin,
                    SuperFixOrFixPrimeMax = x.SuperFixOrFixPrimeMax,
                    SuperFixOrFixOffPrimeMin = x.SuperFixOrFixOffPrimeMin,
                    SuperFixOrFixOffPrimeMax = x.SuperFixOrFixOffPrimeMax
                })
               .ToHashSet();

            // Если новых лимитов нет - завершаем выполнение метода
            if (!newQualityLimits.Any()) {
                return;
            }

            // Сохраняем новые лимиты качеств в базу данных
            await RepositoryQualityLimits.InsertAllAsync(newQualityLimits);
        }

        /// <summary>
        ///     Метод проверки регионов справочника и базы данных
        /// </summary>
        /// <param name="regions">Регионы из справочника НРА Считалки</param>
        private static async Task CheckRegions(IEnumerable<CalcRegion> regions) {
            // Определяем идентификаторы регионов, имеющихся в базе данных
            var dbRegionIds = (await RepositoryRegions.FindAllWithoutChannels()).Select(x => x.ID);
            // Определяем регионы, которых нет в базе данных, но имеются в справочнике
            var newRegions = regions.Where(x => !dbRegionIds.Contains(x.ID)).ToList();

            // Если новых регионов не нашлось - завершаем выполнение метода
            if (!newRegions.Any()) {
                return;
            }

            // Задачи добавления новых регионов
            var tasks = new List<Task>();

            // Итерирруемся по новым регионам
            foreach (var calcRegion in newRegions) {
                // Определяем регион
                var region = new DCRegion {
                    ID = calcRegion.ID,
                    Name = calcRegion.Name,
                    IsMeasurable = calcRegion.IsMeasurable,
                    SeasonalOddsSetID = calcRegion.SeasonCoefficient?.ID ?? 0
                };

                if (
                    region.Name.Equals("Федеральное ТВ", StringComparison.InvariantCultureIgnoreCase) ||
                    region.Name.Equals("Тематическое ТВ", StringComparison.InvariantCultureIgnoreCase)
                ) {
                    region.NameVariants.Add(new DCRegionVariant {
                        Name = "Сетевое вещание",
                        Region = region,
                        RegionId = region.ID
                    });
                }

                var timingMargins = TimingMargins.FirstOrDefault(x => x.Key.Any(y => y.Equals(
                    region.Name,
                    StringComparison.InvariantCultureIgnoreCase
                )));

                if (timingMargins.Key != null) {
                    foreach (var year in timingMargins.Value) {
                        foreach (var timing in year.Value) {
                            region.TimingMargins.Add(new DCTimingMargin {
                                Region = region,
                                Date = new DateTime(year.Key, DateTime.Now.Month, 1),
                                Timing = timing.Key,
                                Value = timing.Value
                            });
                        }
                    }
                }


                // Добавляем задачу добавления нового региона в базу данных
                tasks.Add(RepositoryRegions.InsertSingleAsync(region));
            }

            // Ожидаем выполнение задач
            await Task.WhenAll(tasks);
        }

        /// <summary>
        ///     Метод сравнения каналов справочника НРА Считалки и базы данных
        /// </summary>
        /// <param name="channels">Каналы из справочника НРА Считалки</param>
        private static async Task CheckChannels(IEnumerable<CalcChannel> channels) {
            // Определяем идентификаторы каналов, имеющихся в базе данных
            var dbChannelIDs = await RepositoryChannels.FindAllIDs();

            // Определяем каналы, которых нет в базе данных, но имеются в справочнике
            var newChannels = channels.Where(x => !dbChannelIDs.Contains(x.ID)).ToList();

            // Если новые каналы не были найдены - завершаем выполнение метода
            if (!newChannels.Any()) {
                return;
            }

            // Формируем модели каналов
            var channelEntities = new List<DCChannel>();
            var audiencesCache = new List<DCAudience>();

            foreach (var channel in newChannels) {
                // Формируем модель канала
                var channelEntity = new DCChannel {
                    ID = channel.ID,
                    UID = channel.UID,
                    IID = channel.InventoryUID,
                    Name = channel.Name,
                    RegionID = channel.Region.ID,
                    SaleType = (DCSaleType)channel.SaleType,
                    SalePoint = "НРА",
                    IsOrbital = channel.IsOrbital,
                    QualityLimitsID = channel.QualityLimit.ID
                };

                // Добавляем лимиты качеста

                // Добавляем базовую аудиторию
                if (channel.Audience != null) {
                    var audienceEntity = audiencesCache.FirstOrDefault(x => x.Name.Equals(channel.Audience.Name));
                    if (audienceEntity == null) {
                        audienceEntity = await RepositoryAudiences.FindByName(channel.Audience.Name) ??
                            throw new Exception(
                                "Не удалось найти указанную аудиторию в базе данных. " +
                                "Аудитория должна быть заранее определена в базе данных"
                            );
                        audiencesCache.Add(audienceEntity);
                    }

                    channelEntity.Audience = audienceEntity;
                    channelEntity.AudienceID = audienceEntity.ID;
                }

                // Добавляем сформированную модель канала в коллекцию
                channelEntities.Add(channelEntity);
            }

            // Добавляем каналы в базу данных
            await RepositoryChannels.InsertAsync(channelEntities);
        }

        #endregion

        #region Методы программатика

        private static async Task UpdateChannelsRatings() {
            Messenger.Default.Send(new EventInitializerMessage("Обновление рейтингов каналов"));
            await Task.Delay(250);

            var date = DateTime.Now;
            var idsTask = RepositoryChannels.FindAllMeasurableIDs();
            Collection<DateTime> dates = new();

            for (var y = 0; y <= 1; y++) {
                for (var m = 1; m <= 12; m++) {
                    dates.Add(new DateTime(date.Year + y, m, 1));
                }
            }

            Messenger.Default.Send(new EventInitializerMessage("Ожидание ответа от сервера"));
            var ids = await idsTask;
            var response = await RatingDownloader.Download(ids, dates);

            Messenger.Default.Send(new EventInitializerMessage("Обновление рейтингов каналов"));
            await Task.Delay(250);
            Collection<DCRating> ratings = new();
            foreach (var group in response.GroupBy(r => r.PrimaryKey.ID).ToList()) {
                foreach (var mGroup in group.GroupBy(g => g.PrimaryKey.Month)) {
                    ratings.Add(new DCRating {
                        ChannelID = group.Key,
                        Date = mGroup.Key,
                        Prime = mGroup.FirstOrDefault(g => g.PrimaryKey.IsPrime)?.Rating     ?? 0.0,
                        OffPrime = mGroup.FirstOrDefault(g => !g.PrimaryKey.IsPrime)?.Rating ?? 0.0
                    });
                }
            }

            await RepositoryChannels.AddOrUpdateDictionaryRatings(ratings);
            Messenger.Default.Send(new EventInitializerMessage("Рейтинги успешно обновлены"));
            await Task.Delay(250);
        }

        #endregion
    }
}
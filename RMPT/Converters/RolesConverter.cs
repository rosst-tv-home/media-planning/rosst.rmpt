﻿using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RMPT.Converters {
    public class RolesConverter : IValueConverter {
        /// <summary>
        /// Метод ковертации для отображения элементов и интерфейсе
        /// </summary>
        public object Convert(
            object? value,
            Type targetType,
            object parameter,
            CultureInfo culture
        ) => value == null ? new List<DCRole>() : ((List<DCUserRole>)value).Select(x => x.Role).ToList()!;

        /// <summary>
        /// Метод ковертации для присвоения значения свойству модели
        /// </summary>
        public object ConvertBack(
            object? value,
            Type targetType,
            object parameter,
            CultureInfo culture
        ) => value == null
            ? new List<DCUserRole>()
            : ((List<object>)value).Select(x => new DCUserRole {
                Role = x as DCRole,
                RoleID = (x as DCRole)!.ID
            }).ToList()!;
    }
}
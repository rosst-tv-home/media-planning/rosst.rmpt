﻿using Newtonsoft.Json;
using System;

namespace RMPT.Web.Models {
    public class RequestChannelMonthRatings {
        [JsonProperty("id")]
        public int ID { get; set; }
        [JsonProperty("month")]
        public DateTime Month { get; set; }
    }
}
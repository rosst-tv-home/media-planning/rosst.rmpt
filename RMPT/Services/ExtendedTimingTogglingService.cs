﻿using DevExpress.Xpf.Grid;
using RMPT.DATA.ViewModels.Models;
using RMPT.DATA.ViewModels.Models.Timing;
using RMPT.GRID.Elements;
using RMPT.Models;
using RMPT.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Threading;
using System.Threading.Tasks;

namespace RMPT.Services {
    /// <summary>
    ///     Сервис переключения хронометражей проекта
    /// </summary>
    public class ExtendedTimingTogglingService {
        #region Сервисы

        /// <summary>
        ///     Сервис управления таблицей проекта
        /// </summary>
        private readonly IExtendedGridService _gridSRV;

        /// <summary>
        ///     Генератор таблицы проекта
        /// </summary>
        private readonly ExtendedTableFormatter _tableFormatter;

        #endregion

        #region Свойства

        /// <summary>
        ///     Проект
        /// </summary>
        private readonly ProjectVM _project;

        /// <summary>
        ///     Выбранные хронометражи
        /// </summary>
        private readonly ObservableCollection<TimingSelectorVM> _selectedTimings;

        /// <summary>
        ///     Количество блокировок таблицы
        /// </summary>
        private int _lock;

        /// <summary>
        ///     Источник токена отмены выполняемой задачи
        /// </summary>
        private CancellationTokenSource? _cancellationTokenSource;

        #endregion

        #region Коллекции

        /// <summary>
        ///     Коллекция хронометражей
        /// </summary>
        private readonly ConcurrentDictionary<TimingVM, bool> _timings = new();

        /// <summary>
        ///     Коллекция столбцов
        /// </summary>
        private readonly ConcurrentDictionary<GridColumn, bool> _columns = new();

        #endregion

        #region Конструктор

        public ExtendedTimingTogglingService(
            IExtendedGridService gridSRV,
            ExtendedTableFormatter tableFormatter,
            ProjectVM project,
            ObservableCollection<TimingSelectorVM> selectedTimings
        ) {
            this._gridSRV = gridSRV;
            this._tableFormatter = tableFormatter;
            this._project = project;
            this._selectedTimings = selectedTimings;
            this.InitializeHandlers();
        }

        #endregion

        #region Метод инициализации метода обработки события изменения выбранных хронометражей

        public void InitializeHandlers() {
            this._selectedTimings.CollectionChanged -= SelectedTimingsOnCollectionChanged;
            this._selectedTimings.CollectionChanged += SelectedTimingsOnCollectionChanged;
        }

        #endregion

        #region Метод обработки события изменения выбранных хронометражей

        private async void SelectedTimingsOnCollectionChanged(
            object sender, NotifyCollectionChangedEventArgs arguments
        ) {
            if (this._lock == 0) {
                if (!this._gridSRV.IsLoading()) {
                    this._gridSRV.ToggleLoadingPanel();
                }

                this._gridSRV.BeginColumnsUpdate();
                this._gridSRV.BeginDataUpdate();
            }

            this._lock++;

            if (arguments.Action == NotifyCollectionChangedAction.Reset) {
                foreach (var band in _tableFormatter.TimingDistributionBands) {
                    if (!band.Visible) continue;
                    foreach (var column in band.Columns) {
                        if (!column.Visible) continue;
                        column.Visible = false;
                    }

                    band.Visible = false;
                }

                foreach (var region in this._project.Regions) {
                    foreach (var channel in region.Channels) {
                        foreach (var month in channel.Months) {
                            foreach (var week in month.Weeks) {
                                foreach (var timing in week.Timings) {
                                    if (!timing.IsActive) continue;
                                    timing.IsActive = false;
                                }
                            }
                        }
                    }
                }
            } else {
                var isActive = arguments.Action == NotifyCollectionChangedAction.Add;
                var timingSelector = isActive
                    ? arguments.NewItems[0] as TimingSelectorVM
                    : arguments.OldItems[0] as TimingSelectorVM;
                if (timingSelector == null)
                    throw new NullReferenceException("Не удалось определить выбранный хронометраж");

                foreach (var region in this._project.Regions) {
                    foreach (var channel in region.Channels) {
                        foreach (var month in channel.Months) {
                            foreach (var week in month.Weeks) {
                                foreach (var timing in week.Timings) {
                                    if (timing.Value    != timingSelector.Value) continue;
                                    if (timing.IsActive == isActive) continue;
                                    this.AddOrUpdateTiming(timing, isActive);
                                }
                            }
                        }
                    }
                }

                foreach (var band in _tableFormatter.TimingDistributionBands) {
                    foreach (var column in band.Columns) {
                        if (column is not ExtendedColumn extendedColumn) continue;
                        if ((int?)extendedColumn.UID != timingSelector.Value) continue;
                        this.AddOrUpdateColumn(column, isActive);
                    }
                }

                // Генерируем токен
                var token = this.RegenerateToken();

                // Ждём 750 милисекунды перед тем как начать
                try {
                    await Task.Delay(750, token);
                } catch {
                    this._lock--;
                    return;
                }

                foreach (var column in this._columns) {
                    column.Key.Visible = column.Value;
                }

                foreach (var timing in this._timings) {
                    timing.Key.IsActive = timing.Value;
                }

                this._tableFormatter.ChannelTimingDistributionBand.Visible =
                    this._selectedTimings.Count > 0 &&
                    this._project.ViewPreferences.IsShowChannelTimingDistributionBand;

                foreach (var band in this._tableFormatter.MonthsTimingDistributionBands) {
                    band.Visible =
                        this._selectedTimings.Count > 0 &&
                        this._project.ViewPreferences.IsShowMonthTimingDistributionBand;
                }

                foreach (var band in this._tableFormatter.WeeksTimingDistributionBands) {
                    band.Visible =
                        this._selectedTimings.Count > 0 &&
                        this._project.ViewPreferences.IsShowWeekTimingDistributionBand;
                }
            }

            this._lock--;
            if (this._lock != 0) return;

            this.ClearAllCollections();
            this._gridSRV.EndColumnsUpdate();
            this._gridSRV.EndDataUpdate();

            if (this._gridSRV.IsLoading()) {
                this._gridSRV.ToggleLoadingPanel();
            }
        }

        #endregion

        #region Методы

        #region Управление коллекциями

        /// <summary>
        ///     Метод очистки всех коллекций
        /// </summary>
        private void ClearAllCollections() {
            _columns.Clear();
            _timings.Clear();
        }

        /// <summary>
        ///     Метод добавления или обновления признака активности хронометража проекта
        /// </summary>
        /// <param name="timing">Хронометраж</param>
        /// <param name="value">Значение признака активности</param>
        private void AddOrUpdateTiming(
            TimingVM timing,
            bool value
        ) => _timings.AddOrUpdate(timing, value, (_, _) => value);

        /// <summary>
        ///     Метод добавления или обновления признака активности столбца хронометража
        /// </summary>
        /// <param name="column">Столбец хронометража</param>
        /// <param name="value">Значение признака активности</param>
        private void AddOrUpdateColumn(
            GridColumn column,
            bool value
        ) => _columns.AddOrUpdate(column, value, (_, _) => value);

        #endregion

        #region Управление задачей

        /// <summary>
        ///     Метод отмены выполняемой задачи и создания нового токена отмены выполняемой задачи
        /// </summary>
        /// <returns>Новый токен для отмены выполняемой задачи</returns>
        private CancellationToken RegenerateToken() {
            // Если источник токена отмены выполняемой задачи существует
            if (_cancellationTokenSource != null) {
                // Отменяем задачу, если она выполняется
                _cancellationTokenSource.Cancel();
                // Уничтожаем источник токена отмены выполняемой задачи
                _cancellationTokenSource.Dispose();
            }

            // Создаём новый источник токена отмены выполняемой задачи
            _cancellationTokenSource = new CancellationTokenSource();

            // Возвращаем новый токен отмены выполняемой задачи
            return _cancellationTokenSource.Token;
        }

        #endregion

        #endregion
    }
}
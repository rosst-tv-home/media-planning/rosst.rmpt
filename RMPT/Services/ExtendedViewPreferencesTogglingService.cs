﻿using RMPT.COMMON.Dictionaries;
using RMPT.DATA.ViewModels.Models;
using RMPT.Models;
using RMPT.Utils;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RMPT.Services {
    public class ExtendedViewPreferencesTogglingService {
        #region Свойства

        /// <summary>
        ///     Сервис управления таблицей
        /// </summary>
        private readonly IExtendedGridService _gridSRV;

        /// <summary>
        ///     Генератор таблицы проекта
        /// </summary>
        private readonly ExtendedTableFormatter _tableFormatter;

        /// <summary>
        ///     Настройки представления проекта
        /// </summary>
        private readonly ViewPreferencesVM _viewPreferencesVM;

        /// <summary>
        ///     Выбранные недели
        /// </summary>
        private readonly ObservableCollection<WeekSelectorVM> _selectedWeeks;

        /// <summary>
        ///     Счётчик блокировок таблицы
        /// </summary>
        private int _counter;

        #endregion

        #region Конструктор

        public ExtendedViewPreferencesTogglingService(
            IExtendedGridService gridSRV,
            ExtendedTableFormatter tableFormatter,
            ViewPreferencesVM viewPreferencesVM,
            ObservableCollection<WeekSelectorVM> selectedWeeks
        ) {
            this._gridSRV = gridSRV;
            this._tableFormatter = tableFormatter;
            this._viewPreferencesVM = viewPreferencesVM;
            this._selectedWeeks = selectedWeeks;
            this._viewPreferencesVM.PropertyChanged += OnPropertyChanged;
        }

        #endregion

        #region Метод обработки событий изменений свойств настроек представления проекта

        private async void OnPropertyChanged(object sender, PropertyChangedEventArgs arguments) {
            // Если источник событий не настройки представления проекта
            if (sender is not ViewPreferencesVM) {
                throw new ArgumentException(
                    @"Источником событий должна являться модель настроек представления проекта",
                    nameof(sender)
                );
            }

            var isAnyTimingDistributionColumnVisible = _tableFormatter.ChannelTimingDistributionBand.Columns
               .Any(x => x.Visible);

            if (this._counter == 0) {
                if (!this._gridSRV.IsLoading()) {
                    this._gridSRV.ToggleLoadingPanel();
                }
                this._gridSRV.BeginColumnsUpdate();
            }
            this._counter++;

            switch (arguments.PropertyName) {
                // Группа столбцов "Данные сырья"
                case nameof(ViewPreferencesVM.IsShowRawDataBand): {
                    _tableFormatter.ChannelRawDataBand.Visible = this._viewPreferencesVM.IsShowRawDataBand;
                    break;
                }
                // Недельные периоды в месяцах
                case nameof(ViewPreferencesVM.IsShowMonthsWeekBands): {
                    foreach (var band in _tableFormatter.WeeksBands) {
                        if (!this._viewPreferencesVM.IsShowMonthsWeekBands) {
                            band.Visible = false;
                            continue;
                        }

                        if (this._selectedWeeks.All(x => (DateTime?)band.UID != x.DateFrom)) {
                            continue;
                        }

                        band.Visible = true;
                    }

                    break;
                }
                // Группа столбцов "Распределение"
                case nameof(ViewPreferencesVM.IsShowChannelDistributionBands): {
                    _tableFormatter.ChannelDistributionBand.Visible =
                        this._viewPreferencesVM.IsShowChannelDistributionBands;
                    break;
                }
                // Группа столбцов "Распределение" в месяцах
                case nameof(ViewPreferencesVM.IsShowMonthDistributionBands): {
                    foreach (var band in _tableFormatter.MonthsDistributionBands) {
                        band.Visible = this._viewPreferencesVM.IsShowMonthDistributionBands;
                    }

                    break;
                }
                // Группа столбцов "Распределение хронометражей" каналов
                case nameof(ViewPreferencesVM.IsShowChannelTimingDistributionBand): {
                    _tableFormatter.ChannelTimingDistributionBand.Visible =
                        isAnyTimingDistributionColumnVisible &&
                        this._viewPreferencesVM.IsShowChannelTimingDistributionBand;
                    break;
                }
                // Группа столбцов "Распределение хронометражей" месяцев
                case nameof(ViewPreferencesVM.IsShowMonthTimingDistributionBand): {
                    foreach (var band in _tableFormatter.MonthsTimingDistributionBands) {
                        band.Visible =
                            isAnyTimingDistributionColumnVisible &&
                            this._viewPreferencesVM.IsShowMonthTimingDistributionBand;
                    }

                    break;
                }
                // Группа столбцов "Распределение хронометражей" недель
                case nameof(ViewPreferencesVM.IsShowWeekTimingDistributionBand): {
                    foreach (var band in _tableFormatter.WeeksTimingDistributionBands) {
                        band.Visible =
                            isAnyTimingDistributionColumnVisible &&
                            this._viewPreferencesVM.IsShowWeekTimingDistributionBand;
                    }

                    break;
                }

                #region Настраиваемые столбцы и группы столбцов

                case nameof(ViewPreferencesVM.IsShowCustomizableAll): {
                    bool? value = this._viewPreferencesVM.IsShowCustomizableAll;
                    if (value == null) break;
                    this._viewPreferencesVM.IsShowCustomizableInventoryBand = value;
                    this._viewPreferencesVM.IsShowCustomizableOutputsBand = value;
                    this._viewPreferencesVM.IsShowCustomizableBudgetBand = value;
                    break;
                }

                case nameof(ViewPreferencesVM.IsShowCustomizableInventoryBand): {
                    bool? value = this._viewPreferencesVM.IsShowCustomizableInventoryBand;
                    if (value == null) {
                        this._viewPreferencesVM.IsShowCustomizableAll = value;
                        foreach (var band in _tableFormatter.InventoryBands) {
                            band.Visible = true;
                        }
                        break;
                    }

                    foreach (var band in _tableFormatter.InventoryBands) {
                        band.Visible = (bool)value;
                    }

                    this._viewPreferencesVM.IsShowCustomizableTrpColumn = (bool)value;
                    this._viewPreferencesVM.IsShowCustomizableGivenTrpColumn = (bool)value;
                    this._viewPreferencesVM.IsShowCustomizableGrpColumn = (bool)value;
                    this._viewPreferencesVM.IsShowCustomizableGivenGrpColumn = (bool)value;
                    this._viewPreferencesVM.IsShowCustomizableMinutesColumn = (bool)value;
                    
                    bool allVisible =
                        this._viewPreferencesVM.IsShowCustomizableInventoryBand == true &&
                        this._viewPreferencesVM.IsShowCustomizableOutputsBand   == true &&
                        this._viewPreferencesVM.IsShowCustomizableBudgetBand    == true;
                    bool allHidden =
                        this._viewPreferencesVM.IsShowCustomizableInventoryBand == false &&
                        this._viewPreferencesVM.IsShowCustomizableOutputsBand   == false &&
                        this._viewPreferencesVM.IsShowCustomizableBudgetBand    == false;

                    bool? bandVisible;
                    if (!allVisible && !allHidden) bandVisible = null;
                    else bandVisible = allVisible;
                    
                    this._viewPreferencesVM.IsShowCustomizableAll = bandVisible;
                    
                    break;
                }

                case nameof(ViewPreferencesVM.IsShowCustomizableTrpColumn):
                case nameof(ViewPreferencesVM.IsShowCustomizableGivenTrpColumn):
                case nameof(ViewPreferencesVM.IsShowCustomizableGrpColumn):
                case nameof(ViewPreferencesVM.IsShowCustomizableGivenGrpColumn):
                case nameof(ViewPreferencesVM.IsShowCustomizableMinutesColumn): {
                    PropertyInfo? property = _viewPreferencesVM.GetType().GetProperty(arguments.PropertyName);
                    if (property == null) throw new NullReferenceException("Не удалось определить свойство");
                    bool value = (bool)property.GetValue(_viewPreferencesVM);

                    string header =
                        arguments.PropertyName.Equals(nameof(ViewPreferencesVM.IsShowCustomizableTrpColumn))
                            ? ColumnHeaders.TRP
                            : arguments.PropertyName.Equals(nameof(ViewPreferencesVM.IsShowCustomizableGivenTrpColumn))
                                ? $"{ColumnHeaders.TRP} {_viewPreferencesVM.Project.BaseTiming}"
                                : arguments.PropertyName.Equals(nameof(ViewPreferencesVM.IsShowCustomizableGrpColumn))
                                    ? ColumnHeaders.GRP
                                    : arguments.PropertyName.Equals(nameof(ViewPreferencesVM
                                       .IsShowCustomizableGivenGrpColumn))
                                        ? $"{ColumnHeaders.GRP} {_viewPreferencesVM.Project.BaseTiming}"
                                        : arguments.PropertyName.Equals(
                                            nameof(ViewPreferencesVM.IsShowCustomizableMinutesColumn))
                                            ? ColumnHeaders.Minutes
                                            : throw new ArgumentException(
                                                @"Не удалось определить искомый столбец",
                                                nameof(arguments)
                                            );

                    foreach (var column in _tableFormatter.InventoryBands
                       .SelectMany(b => b.Columns)
                       .Where(c => c.Header.Equals(header))
                    ) {
                        column.Visible = value;
                    }

                    bool allVisible =
                        this._viewPreferencesVM.IsShowCustomizableTrpColumn      &&
                        this._viewPreferencesVM.IsShowCustomizableGivenTrpColumn &&
                        this._viewPreferencesVM.IsShowCustomizableGrpColumn      &&
                        this._viewPreferencesVM.IsShowCustomizableGivenGrpColumn &&
                        this._viewPreferencesVM.IsShowCustomizableMinutesColumn;
                    bool allHidden =
                        !this._viewPreferencesVM.IsShowCustomizableTrpColumn      &&
                        !this._viewPreferencesVM.IsShowCustomizableGivenTrpColumn &&
                        !this._viewPreferencesVM.IsShowCustomizableGrpColumn      &&
                        !this._viewPreferencesVM.IsShowCustomizableGivenGrpColumn &&
                        !this._viewPreferencesVM.IsShowCustomizableMinutesColumn;

                    bool? bandVisible;
                    if (!allVisible && !allHidden) bandVisible = null;
                    else bandVisible = allVisible;

                    this._viewPreferencesVM.IsShowCustomizableInventoryBand = bandVisible;
                    
                    break;
                }

                case nameof(ViewPreferencesVM.IsShowCustomizableOutputsBand): {
                    bool? value = this._viewPreferencesVM.IsShowCustomizableOutputsBand;
                    if (value == null) {
                        this._viewPreferencesVM.IsShowCustomizableAll = value;
                        foreach (var band in _tableFormatter.OutputsBands) {
                            band.Visible = true;
                        }
                        break;
                    }
                    
                    foreach (var band in _tableFormatter.OutputsBands) {
                        band.Visible = (bool)value;
                    }

                    this._viewPreferencesVM.IsShowCustomizablePrimeOutputsColumn = (bool)value;
                    this._viewPreferencesVM.IsShowCustomizableOffPrimeOutputsColumn = (bool)value;
                    this._viewPreferencesVM.IsShowCustomizableTotalOutputsColumn = (bool)value;
                    this._viewPreferencesVM.IsShowCustomizablePerDayOutputsColumn = (bool)value;
                    
                    bool allVisible =
                        this._viewPreferencesVM.IsShowCustomizableInventoryBand == true &&
                        this._viewPreferencesVM.IsShowCustomizableOutputsBand   == true &&
                        this._viewPreferencesVM.IsShowCustomizableBudgetBand    == true;
                    bool allHidden =
                        this._viewPreferencesVM.IsShowCustomizableInventoryBand == false &&
                        this._viewPreferencesVM.IsShowCustomizableOutputsBand   == false &&
                        this._viewPreferencesVM.IsShowCustomizableBudgetBand    == false;

                    bool? bandVisible;
                    if (!allVisible && !allHidden) bandVisible = null;
                    else bandVisible = allVisible;
                    
                    this._viewPreferencesVM.IsShowCustomizableAll = bandVisible;
                    
                    break;
                }

                case nameof(ViewPreferencesVM.IsShowCustomizablePrimeOutputsColumn):
                case nameof(ViewPreferencesVM.IsShowCustomizableOffPrimeOutputsColumn):
                case nameof(ViewPreferencesVM.IsShowCustomizableTotalOutputsColumn):
                case nameof(ViewPreferencesVM.IsShowCustomizablePerDayOutputsColumn): {
                    PropertyInfo? property = _viewPreferencesVM.GetType().GetProperty(arguments.PropertyName);
                    if (property == null) throw new NullReferenceException("Не удалось определить свойство");
                    bool value = (bool)property.GetValue(_viewPreferencesVM);

                    string header =
                        arguments.PropertyName.Equals(nameof(ViewPreferencesVM.IsShowCustomizablePrimeOutputsColumn))
                            ? ColumnHeaders.Prime
                            : arguments.PropertyName.Equals(nameof(ViewPreferencesVM
                               .IsShowCustomizableOffPrimeOutputsColumn))
                                ? ColumnHeaders.OffPrime
                                : arguments.PropertyName.Equals(nameof(ViewPreferencesVM
                                   .IsShowCustomizableTotalOutputsColumn))
                                    ? ColumnHeaders.Total
                                    : arguments.PropertyName.Equals(nameof(ViewPreferencesVM
                                       .IsShowCustomizablePerDayOutputsColumn))
                                        ? ColumnHeaders.PerDay
                                        : throw new ArgumentException(
                                            @"Не удалось определить искомый столбец",
                                            nameof(arguments)
                                        );

                    foreach (var column in _tableFormatter.OutputsBands
                       .SelectMany(b => b.Columns)
                       .Where(c => c.Header.Equals(header))
                    ) {
                        column.Visible = value;
                    }
                    
                    bool allVisible =
                        this._viewPreferencesVM.IsShowCustomizablePrimeOutputsColumn    &&
                        this._viewPreferencesVM.IsShowCustomizableOffPrimeOutputsColumn &&
                        this._viewPreferencesVM.IsShowCustomizableTotalOutputsColumn    &&
                        this._viewPreferencesVM.IsShowCustomizablePerDayOutputsColumn;
                    bool allHidden =
                        !this._viewPreferencesVM.IsShowCustomizablePrimeOutputsColumn    &&
                        !this._viewPreferencesVM.IsShowCustomizableOffPrimeOutputsColumn &&
                        !this._viewPreferencesVM.IsShowCustomizableTotalOutputsColumn    &&
                        !this._viewPreferencesVM.IsShowCustomizablePerDayOutputsColumn;

                    bool? bandVisible;
                    if (!allVisible && !allHidden) bandVisible = null;
                    else bandVisible = allVisible;

                    this._viewPreferencesVM.IsShowCustomizableOutputsBand = bandVisible;

                    break;
                }

                case nameof(ViewPreferencesVM.IsShowCustomizableBudgetBand): {
                    bool? value = this._viewPreferencesVM.IsShowCustomizableBudgetBand;
                    if (value == null) {
                        this._viewPreferencesVM.IsShowCustomizableAll = value;
                        foreach (var band in _tableFormatter.BudgetBands) {
                            band.Visible = true;
                        }
                        break;
                    }
                    
                    foreach (var band in _tableFormatter.BudgetBands) {
                        band.Visible = (bool)value;
                    }

                    this._viewPreferencesVM.IsShowCustomizablePriceColumn = (bool)value;
                    this._viewPreferencesVM.IsShowCustomizablePriceWithMarginsColumn = (bool)value;
                    this._viewPreferencesVM.IsShowCustomizableBudgetColumn = (bool)value;
                    
                    bool allVisible =
                        this._viewPreferencesVM.IsShowCustomizableInventoryBand == true &&
                        this._viewPreferencesVM.IsShowCustomizableOutputsBand   == true &&
                        this._viewPreferencesVM.IsShowCustomizableBudgetBand    == true;
                    bool allHidden =
                        this._viewPreferencesVM.IsShowCustomizableInventoryBand == false &&
                        this._viewPreferencesVM.IsShowCustomizableOutputsBand   == false &&
                        this._viewPreferencesVM.IsShowCustomizableBudgetBand    == false;

                    bool? bandVisible;
                    if (!allVisible && !allHidden) bandVisible = null;
                    else bandVisible = allVisible;
                    
                    this._viewPreferencesVM.IsShowCustomizableAll = bandVisible;
                    
                    break;
                }

                case nameof(ViewPreferencesVM.IsShowCustomizablePriceColumn):
                case nameof(ViewPreferencesVM.IsShowCustomizablePriceWithMarginsColumn):
                case nameof(ViewPreferencesVM.IsShowCustomizableBudgetColumn): {
                    PropertyInfo? property = _viewPreferencesVM.GetType().GetProperty(arguments.PropertyName);
                    if (property == null) throw new NullReferenceException("Не удалось определить свойство");
                    bool value = (bool)property.GetValue(_viewPreferencesVM);

                    string header =
                        arguments.PropertyName.Equals(nameof(ViewPreferencesVM.IsShowCustomizablePriceColumn))
                            ? ColumnHeaders.PriceNra
                            : arguments.PropertyName.Equals(nameof(ViewPreferencesVM
                               .IsShowCustomizablePriceWithMarginsColumn))
                                ? ColumnHeaders.PriceWithMargins
                                : arguments.PropertyName.Equals(
                                    nameof(ViewPreferencesVM.IsShowCustomizableBudgetColumn))
                                    ? ColumnHeaders.Budget
                                    : throw new ArgumentException(
                                        @"Не удалось определить искомый столбец",
                                        nameof(arguments)
                                    );

                    foreach (var column in _tableFormatter.BudgetBands
                       .SelectMany(b => b.Columns)
                       .Where(c => c.Header.Equals(header))
                    ) {
                        column.Visible = value;
                    }
                    
                    bool allVisible =
                        this._viewPreferencesVM.IsShowCustomizablePriceColumn            &&
                        this._viewPreferencesVM.IsShowCustomizablePriceWithMarginsColumn &&
                        this._viewPreferencesVM.IsShowCustomizableBudgetColumn;
                    bool allHidden =
                        !this._viewPreferencesVM.IsShowCustomizablePriceColumn            &&
                        !this._viewPreferencesVM.IsShowCustomizablePriceWithMarginsColumn &&
                        !this._viewPreferencesVM.IsShowCustomizableBudgetColumn;

                    bool? bandVisible;
                    if (!allVisible && !allHidden) bandVisible = null;
                    else bandVisible = allVisible;

                    this._viewPreferencesVM.IsShowCustomizableBudgetBand = bandVisible;

                    break;
                }

                #endregion
            }

            this._counter--;
            if (this._counter != 0) return;
            
            this._gridSRV.EndColumnsUpdate();
            if (this._gridSRV.IsLoading()) {
                await Task.Delay(150);
                this._gridSRV.ToggleLoadingPanel();
            }
        }

        #endregion
    }
}
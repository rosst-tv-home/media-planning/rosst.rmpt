﻿using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Grid;
#if DEBUG
using System.Diagnostics;

#endif

namespace RMPT.Services {
    /// <summary>
    ///     Сервис управления таблицей
    /// </summary>
    public class ExtendedGridService : ServiceBaseGeneric<GridControl>, IExtendedGridService {
        public GridControl GridInstance() => AssociatedObject;

        /// <summary>
        ///     Первая видимая строка таблицы
        /// </summary>
        private int topRowIndex = -1;

        /// <summary>
        ///     Индекс строки в фокусе
        /// </summary>
        private int focusedRowIndex = -1;

        #region Индикатор загрузки

        /// <summary>
        ///     Значение признака "Индикатор загрузки отображается"
        /// </summary>
        /// <returns>Значение признака "Индикатор загрузки отображается"</returns>
        public bool IsLoading() => AssociatedObject.ShowLoadingPanel;

        /// <summary>
        ///     Метод переключения видимости индикатора загрузки
        /// </summary>
        public void ToggleLoadingPanel() => AssociatedObject.ShowLoadingPanel = !AssociatedObject.ShowLoadingPanel;

        #endregion

        #region Данные

        /// <summary>
        ///     Метод обновления данных таблицы
        /// </summary>
        public void RefreshData() {
            #if DEBUG
            // Запускаем измерение времени выполнения метода
            var watch = Stopwatch.StartNew();
            #endif

            topRowIndex = AssociatedObject.View.TopRowIndex;
            focusedRowIndex = AssociatedObject.View.FocusedRowHandle;

            // Обновляем данные таблицы
            AssociatedObject.RefreshData();
            AssociatedObject.View.TopRowIndex = topRowIndex;
            AssociatedObject.View.FocusedRowHandle = focusedRowIndex;

            #if DEBUG
            // Выводим время выполнения метода в консоль
            Debug.WriteLine($"{nameof(ExtendedGridService)}:{nameof(RefreshData)}: {watch.ElapsedMilliseconds}ms");
            // Останавливаем измерение времени выполнения метода
            watch.Stop();
            #endif
        }

        /// <summary>
        ///     Метод начала обновления данных таблицы
        /// </summary>
        public void BeginDataUpdate() {
            topRowIndex = AssociatedObject.View.TopRowIndex;
            focusedRowIndex = AssociatedObject.View.FocusedRowHandle;
            // Приостанавливаем реагирование таблицы на обновление данных
            AssociatedObject.BeginDataUpdate();
        }

        /// <summary>
        ///     Метод завершения обновления данных таблицы
        /// </summary>
        public void EndDataUpdate() {
            #if DEBUG
            // Запускаем измерение времени выполнения метода
            var watch = Stopwatch.StartNew();
            #endif

            // Возобновляем реагирование таблицы на обновление данных
            AssociatedObject.EndDataUpdate();
            AssociatedObject.View.TopRowIndex = topRowIndex;
            AssociatedObject.View.FocusedRowHandle = focusedRowIndex;

            #if DEBUG
            // Выводим время выполнения метода в консоль
            Debug.WriteLine($"{nameof(ExtendedGridService)}:{nameof(EndDataUpdate)}: {watch.ElapsedMilliseconds}ms");
            // Останавливаем измерение времени выполнения метода
            watch.Stop();
            #endif
        }

        #endregion

        #region Разметка

        /// <summary>
        ///     Метод обновления разметки таблицы
        /// </summary>
        public void RefreshLayout() {
            #if DEBUG
            // Запускаем измерение времени выполнения метода
            var watch = Stopwatch.StartNew();
            #endif

            topRowIndex = AssociatedObject.View.TopRowIndex;
            focusedRowIndex = AssociatedObject.View.FocusedRowHandle;
            // Обновляем разметку таблицы
            AssociatedObject.UpdateLayout();
            AssociatedObject.View.TopRowIndex = topRowIndex;
            AssociatedObject.View.FocusedRowHandle = focusedRowIndex;

            #if DEBUG
            // Выводим время выполнения метода в консоль
            Debug.WriteLine($"{nameof(ExtendedGridService)}:{nameof(RefreshLayout)}: {watch.ElapsedMilliseconds}ms");
            // Останавливаем измерение времени выполнения метода
            watch.Stop();
            #endif
        }

        /// <summary>
        ///     Метод начала обновления групп столбцов таблицы
        /// </summary>
        public void BeginBandsUpdate() {
            topRowIndex = AssociatedObject.View.TopRowIndex;
            focusedRowIndex = AssociatedObject.View.FocusedRowHandle;
            // Приостанавливаем реагирование таблицы на обновление групп столбцов
            AssociatedObject.Bands.BeginUpdate();
        }

        /// <summary>
        ///     Метод завершения обновления групп столбцов таблицы
        /// </summary>
        public void EndBandsUpdate() {
            #if DEBUG
            // Запускаем измерение времени выполнения метода
            var watch = Stopwatch.StartNew();
            #endif

            // Возобновляем реагирование таблицы на обновление групп столбцов
            AssociatedObject.Bands.EndUpdate();
            AssociatedObject.View.TopRowIndex = topRowIndex;
            AssociatedObject.View.FocusedRowHandle = focusedRowIndex;

            #if DEBUG
            // Выводим время выполнения метода в консоль
            Debug.WriteLine($"{nameof(ExtendedGridService)}:{nameof(EndBandsUpdate)}: {watch.ElapsedMilliseconds}ms");
            // Останавливаем измерение времени выполнения метода
            watch.Stop();
            #endif
        }

        /// <summary>
        ///     Метод начала обновления столбцов таблицы
        /// </summary>
        public void BeginColumnsUpdate() {
            topRowIndex = AssociatedObject.View.TopRowIndex;
            focusedRowIndex = AssociatedObject.View.FocusedRowHandle;
            // Приостанавливаем реагирование таблицы на обновление столбцов
            AssociatedObject.Columns.BeginUpdate();
        }

        /// <summary>
        ///     Метод завершения обновления столбцов таблицы
        /// </summary>
        public void EndColumnsUpdate() {
            #if DEBUG
            // Запускаем измерение времени выполнения метода
            var watch = Stopwatch.StartNew();
            #endif

            // Возобновляем реагирование таблицы на обновление столбцов
            AssociatedObject.Columns.EndUpdate();
            AssociatedObject.View.TopRowIndex = topRowIndex;
            AssociatedObject.View.FocusedRowHandle = focusedRowIndex;

            #if DEBUG
            // Выводим время выполнения метода в консоль
            Debug.WriteLine($"{nameof(ExtendedGridService)}:{nameof(EndColumnsUpdate)}: {watch.ElapsedMilliseconds}ms");
            // Останавливаем измерение времени выполнения метода
            watch.Stop();
            #endif
        }

        #endregion
    }

    /// <summary>
    ///     Интерфес сервиса управления таблицей
    /// </summary>
    public interface IExtendedGridService {

        public GridControl GridInstance();

        #region Индикатор загрузки

        /// <summary>
        ///     Значение признака "Индикатор загрузки отображается"
        /// </summary>
        /// <returns>Значение признака "Индикатор загрузки отображается"</returns>
        bool IsLoading();

        /// <summary>
        ///     Метод переключения видимости индикатора загрузки
        /// </summary>
        void ToggleLoadingPanel();

        #endregion

        #region Данные

        /// <summary>
        ///     Метод обновления данных таблицы
        /// </summary>
        void RefreshData();

        /// <summary>
        ///     Метод начала обновления данных таблицы
        /// </summary>
        void BeginDataUpdate();

        /// <summary>
        ///     Метод завершения обновления данных таблицы
        /// </summary>
        void EndDataUpdate();

        #endregion

        #region Разметка

        /// <summary>
        ///     Метод обновления разметки таблицы
        /// </summary>
        void RefreshLayout();

        /// <summary>
        ///     Метод начала обновления групп столбцов таблицы
        /// </summary>
        void BeginBandsUpdate();

        /// <summary>
        ///     Метод завершения обновления групп столбцов таблицы
        /// </summary>
        void EndBandsUpdate();

        /// <summary>
        ///     Метод начала обновления столбцов таблицы
        /// </summary>
        void BeginColumnsUpdate();

        /// <summary>
        ///     Метод завершения обновления столбцов таблицы
        /// </summary>
        void EndColumnsUpdate();

        #endregion
    }
}
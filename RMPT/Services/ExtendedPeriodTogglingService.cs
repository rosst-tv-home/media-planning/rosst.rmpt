﻿using DevExpress.Xpf.Grid;
using RMPT.DATA.ViewModels.Models;
using RMPT.GRID.Elements;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

#if DEBUG
using System.Diagnostics;

#endif

namespace RMPT.Services {
    /// <summary>
    ///     Сервис переключения периодов проекта
    /// </summary>
    public class ExtendedPeriodTogglingService {
        #region Свойства

        /// <summary>
        ///     Сервис управления таблицей проекта
        /// </summary>
        public IExtendedGridService? GridService;

        #region Синхронизация данных таблицы

        /// <summary>
        ///     Количество блокировок данных проекта
        /// </summary>
        private int _dataUpdateLockCounter;

        /// <summary>
        ///     Объект синхронизации данных проекта
        /// </summary>
        private readonly object _dataUpdateLock = new();

        #endregion

        #region Синхронизация столбцов таблицы

        /// <summary>
        ///     Количество блокировок таблицы проекта
        /// </summary>
        private int _columnUpdateLockCounter;

        /// <summary>
        ///     Объект синхронизации столбцов таблицы проекта
        /// </summary>
        private readonly object _columnUpdateLock = new();

        #endregion

        #region Данные

        /// <summary>
        ///     Коллекция недельных периодов проекта
        /// </summary>
        private readonly ConcurrentDictionary<WeekVM, bool> _weeks = new();

        /// <summary>
        ///     Коллекция месячных периодов проекта
        /// </summary>
        private readonly ConcurrentDictionary<MonthVM, bool> _months = new();

        #endregion

        #region Столбцы

        /// <summary>
        ///     Источник токена отмены задачи переключения видимости групп столбцов периодов проекта
        /// </summary>
        private CancellationTokenSource? _togglingColumnsTokenSource;

        /// <summary>
        ///     Коллекция групп столбцов периодов проекта
        /// </summary>
        private readonly ConcurrentDictionary<GridControlBand, bool> _periodBands = new();

        #endregion

        #endregion

        #region Методы

        #region Периоды

        /// <summary>
        ///     Метод очистки коллекции группы столбцов недельных и месячных периодов
        /// </summary>
        private void ClearCollections() {
            _weeks.Clear();
            _months.Clear();
            _periodBands.Clear();
        }

        /// <summary>
        ///     Метод добавления или обновления недельного периода и признака его активности
        /// </summary>
        /// <param name="week">Недельный период</param>
        /// <param name="value">Значение признака активности</param>
        public void AddOrUpdateWeek(WeekVM week, bool value) => _weeks.AddOrUpdate(week, value, (_, _) => value);

        /// <summary>
        ///     Метод добавления или обновления месячного периода и признака его активности
        /// </summary>
        /// <param name="month">Месячный период</param>
        /// <param name="value">Значение признака активности</param>
        public void AddOrUpdateMonth(MonthVM month, bool value) => _months.AddOrUpdate(month, value, (_, _) => value);

        /// <summary>
        ///     Метод добавления группы столбцов в коллекцию группы столбцов периодов проекта
        /// </summary>
        public void AddOrUpdatePeriodBand(GridControlBand band, bool value) {
            // Добавляем или обновляем значение в коллекции
            _periodBands.AddOrUpdate(band, value, (_, _) => value);
            // Запускаем задачу
            StartTogglingPeriodBands();
        }

        /// <summary>
        ///     Метод определения необходимости деактивации группы столбцов месячного периода проекта
        /// </summary>
        /// <param name="band">Группа столбцов месячного периода</param>
        /// <returns>Результат проверки</returns>
        public bool IsRequireMonthBandDeactivate(ExtendedBand band) {
            // Группы столбцов недельных периодов группы столбцов месячного периода
            var weeksBands = _periodBands.Where(x =>
                (ExtendedBandTag)x.Key.Tag == ExtendedBandTag.Week &&
                band.Bands.Contains(x.Key)
            ).ToList();

            // Если есть хотя бы одна группа столбцов недельного периода, который будет отображён
            if (weeksBands.Any(x => x.Value)) return false;

            // Группы столбцов недельных периодов, которые буду скрыты
            var hiddenWeeksBands = weeksBands.Where(x => !x.Value).Select(x => x.Key).ToList();

            // Группы столбцов недельных периодов группы столбцов месячного периода, которые требуется проверить
            var checking = band.Bands.Where(x =>
                (ExtendedBandTag)x.Tag == ExtendedBandTag.Week &&
                !hiddenWeeksBands.Contains(x)
            ).ToList();

            // Если нет активных групп столбцов недельных периодов или они все деактивированы,
            // тогда требуется деактивация группы столбцов месячного периода
            return checking.All(x => !x.Visible) || checking.Count == 0;
        }

        /// <summary>
        ///     Метод начала задачи переключения групп столбцов периодов проекта
        /// </summary>
        private void StartTogglingPeriodBands() {
            // Если источник токена отмены задачи существует
            if (_togglingColumnsTokenSource != null) {
                // Отменяем выполняемую задачу
                _togglingColumnsTokenSource.Cancel();
                // Уничтожаем источник токена отмены задачи
                _togglingColumnsTokenSource.Dispose();
            }

            // Создаём источник токена отмены задачи
            _togglingColumnsTokenSource = new CancellationTokenSource();

            // Токен отмены задачи
            var token = _togglingColumnsTokenSource.Token;

            // Если сервис управления таблицей проекта отсутсвует
            if (GridService == null) {
                throw new ArgumentNullException(
                    nameof(GridService),
                    @"Не удалось определить сервис управления таблицей проекта"
                );
            }

            try {
                // Запускаем задачу переключения видимости групп столбцов периодов проекта
                Task.Run(async () => {
                    try {
                        #if DEBUG
                        Debug.WriteLine("StartTogglingWeeks: awaiting delay");
                        #endif

                        // Делаем задержку, чтобы подхватить изменения пользователя, если он их всё ещё вносит
                        await Task.Delay(1000, token);

                        #if DEBUG
                        Debug.WriteLine("StartTogglingWeeks: started");
                        #endif

                        // Блокируем таблицу проекта
                        if (Application.Current.Dispatcher.CheckAccess()) {
                            GridService.BeginColumnsUpdate();
                        } else {
                            Application.Current.Dispatcher.Invoke(GridService.BeginColumnsUpdate);
                        }

                        // Синхронизируем потоки
                        lock (_columnUpdateLock) {
                            // Инкрементируем количество блокировок
                            _columnUpdateLockCounter = _columnUpdateLockCounter + 1;
                        }

                        // Выбрасываем исключение, если был запрос омены задачи
                        token.ThrowIfCancellationRequested();

                        foreach (var tuple in _periodBands) {
                            // Выбрасываем исключение, если был запрос омены задачи
                            token.ThrowIfCancellationRequested();

                            // Переключаем видимость группы столбцов
                            if (Application.Current.Dispatcher.CheckAccess()) {
                                tuple.Key.Visible = tuple.Value;
                            } else {
                                await Application.Current.Dispatcher.BeginInvoke(
                                    DispatcherPriority.Normal,
                                    (ThreadStart)delegate { tuple.Key.Visible = tuple.Value; }
                                );
                            }
                        }

                        // Разблокируем таблицу проекта
                        if (Application.Current.Dispatcher.CheckAccess()) {
                            GridService.EndColumnsUpdate();
                        } else {
                            Application.Current.Dispatcher.Invoke(GridService.EndColumnsUpdate);
                        }

                        // Синхронизируем потоки
                        lock (_columnUpdateLock) {
                            // Дикрементируем количество блокировок
                            _columnUpdateLockCounter = _columnUpdateLockCounter - 1;
                            // Выбрасываем исключение, если был запрос омены задачи
                            token.ThrowIfCancellationRequested();
                        }

                        // Блокируем данные таблицы
                        if (Application.Current.Dispatcher.CheckAccess()) {
                            GridService.BeginDataUpdate();
                        } else {
                            Application.Current.Dispatcher.Invoke(GridService.BeginDataUpdate);
                        }

                        // Синхронизируем потоки
                        lock (_dataUpdateLock) {
                            // Инкриментируем количество блокировок
                            _dataUpdateLockCounter = _dataUpdateLockCounter + 1;
                        }

                        // Выбрасываем исключение, если был запрос омены задачи
                        token.ThrowIfCancellationRequested();

                        // Итерируемся по недельным периодам проекта
                        foreach (var tuple in _weeks) {
                            // Выбрасываем исключение, если был запрос омены задачи
                            token.ThrowIfCancellationRequested();

                            // Переключаем активность недельного периода
                            tuple.Key.IsActive = tuple.Value;
                        }

                        // Итерируемся по месячным периодам проекта
                        foreach (var tuple in _months) {
                            // Выбрасываем исключение, если был запрос омены задачи
                            token.ThrowIfCancellationRequested();

                            // Переключаем активность месячного периода
                            tuple.Key.IsActive = tuple.Value;
                        }

                        // Выбрасываем исключение, если был запрос омены задачи
                        token.ThrowIfCancellationRequested();

                        // Разблокируем данные таблицы
                        if (Application.Current.Dispatcher.CheckAccess()) {
                            GridService.EndDataUpdate();
                            GridService.RefreshData();
                        } else {
                            Application.Current.Dispatcher.Invoke(GridService.EndDataUpdate);
                            Application.Current.Dispatcher.Invoke(GridService.RefreshData);
                        }

                        // Синхронизируем потоки
                        lock (_dataUpdateLock) {
                            // Дикрементируем количество блокировок
                            _dataUpdateLockCounter = _dataUpdateLockCounter - 1;
                            // Выбрасываем исключение, если был запрос омены задачи
                            token.ThrowIfCancellationRequested();
                        }

                        // Выбрасываем исключение, если был запрос омены задачи
                        token.ThrowIfCancellationRequested();

                        // Очищаем коллекцию групп столбцов периодов проекта
                        ClearCollections();
                    } catch (TaskCanceledException) {
                        #if DEBUG
                        Debug.WriteLine("StartTogglingWeeks: canceled");
                        #endif

                        // Синхронизируем потоки
                        lock (_columnUpdateLock) {
                            // Если есть блокировки таблицы проекта
                            if (_columnUpdateLockCounter != 0) {
                                // Разблокируем таблицу проекта
                                if (Application.Current.Dispatcher.CheckAccess()) {
                                    GridService.EndColumnsUpdate();
                                } else {
                                    Application.Current.Dispatcher.Invoke(GridService.EndColumnsUpdate);
                                }

                                // Дикрементируем количество блокирово
                                _columnUpdateLockCounter = _columnUpdateLockCounter - 1;
                            }
                        }

                        // Синхронизируем потоки
                        lock (_dataUpdateLock) {
                            // Есои есть блокировки данных таблицы
                            if (_dataUpdateLockCounter != 0) {
                                // Разблокируем данные таблицы
                                if (Application.Current.Dispatcher.CheckAccess()) {
                                    GridService.EndDataUpdate();
                                } else {
                                    Application.Current.Dispatcher.Invoke(GridService.EndDataUpdate);
                                }

                                // Дикрементируем количество блокировок
                                _dataUpdateLockCounter = _dataUpdateLockCounter - 1;
                            }
                        }
                    }
                }, token);
            } catch (TaskCanceledException) {
                #if DEBUG
                Debug.WriteLine("StartTogglingWeeks: canceled");
                #endif

                // Синхронизируем потоки
                lock (_columnUpdateLock) {
                    // Если есть блокировки таблицы проекта
                    if (_columnUpdateLockCounter != 0) {
                        // Разблокируем таблицу проекта
                        if (Application.Current.Dispatcher.CheckAccess()) {
                            GridService.EndColumnsUpdate();
                        } else {
                            Application.Current.Dispatcher.Invoke(GridService.EndColumnsUpdate);
                        }

                        // Дикрементируем количество блокирово
                        _columnUpdateLockCounter = _columnUpdateLockCounter - 1;
                    }
                }

                // Синхронизируем потоки
                lock (_dataUpdateLock) {
                    // Есои есть блокировки данных таблицы
                    if (_dataUpdateLockCounter != 0) {
                        // Разблокируем данные таблицы
                        if (Application.Current.Dispatcher.CheckAccess()) {
                            GridService.EndDataUpdate();
                        } else {
                            Application.Current.Dispatcher.Invoke(GridService.EndDataUpdate);
                        }

                        // Дикрементируем количество блокировок
                        _dataUpdateLockCounter = _dataUpdateLockCounter - 1;
                    }
                }
            }
        }

        #endregion

        #endregion
    }
}
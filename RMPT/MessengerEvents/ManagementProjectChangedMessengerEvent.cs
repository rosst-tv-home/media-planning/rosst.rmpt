﻿using RMPT.DATA.ViewModels.Models;

namespace RMPT.MessengerEvents {
    /// <summary>
    /// Сообщение события выбранного проекта для управления вкладками
    /// </summary>
    public class ManagementProjectChangedMessengerEvent {
        /// <summary>
        /// Проект
        /// </summary>
        public ProjectVM? Project { get; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="project">Проект</param>
        public ManagementProjectChangedMessengerEvent(ProjectVM? project) {
            Project = project;
        }
    }
}
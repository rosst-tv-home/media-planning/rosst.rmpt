﻿namespace RMPT.MessengerEvents {
    public class ToggleProjectCatalogToolBarME {
        public bool IsVisible;

        public ToggleProjectCatalogToolBarME(bool isVisible) {
            this.IsVisible = isVisible;
        }
    }
}
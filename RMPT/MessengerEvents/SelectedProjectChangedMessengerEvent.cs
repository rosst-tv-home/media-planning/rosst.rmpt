﻿using RMPT.DATA.DataBase.Entities.Projects;

namespace RMPT.MessengerEvents {
    /// <summary>
    /// Сообщение события выбора проекта
    /// </summary>
    public class SelectedProjectChangedMessengerEvent {
        /// <summary>
        /// Выбранный проект
        /// </summary>
        public PDProject? SelectedProject { get; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="selectedProject">Выбранный проект</param>
        public SelectedProjectChangedMessengerEvent(PDProject? selectedProject) {
            SelectedProject = selectedProject;
        }
    }
}
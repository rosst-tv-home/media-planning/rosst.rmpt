﻿using RMPT.COMMON.Dictionaries;
using RMPT.COMMON.Models;
using RMPT.EVENT;
using RMPT.FTP;
using RMPT.Initializers;
using RMPT.UI.MainWindow;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.SplashScreen {
    /// <summary>
    ///     Модель окна загрузки приложения
    /// </summary>
    public class SplashScreenVM : AbstractVM {
        #region Свойства

        /// <summary>
        ///     Признак "бесконечный индикатор загрузки"
        /// </summary>
        public bool IsIndeterminate {
            get => GetProperty(() => IsIndeterminate);
            private set => SetProperty(() => IsIndeterminate, value);
        }

        /// <summary>
        ///     Прогресс индикатора загрузки
        /// </summary>
        public double Progress {
            get => GetProperty(() => Progress);
            set => SetProperty(() => Progress, value);
        }

        /// <summary>
        ///     Заголовок
        /// </summary>
        public string? Title {
            get => GetProperty(() => Title);
            private set => SetProperty(() => Title, value);
        }

        /// <summary>
        ///     Подзоголовок
        /// </summary>
        public string? Subtitle {
            get => GetProperty(() => Subtitle);
            private set => SetProperty(() => Subtitle, value);
        }

        /// <summary>
        ///     Статус
        /// </summary>
        public string? Status {
            get => GetProperty(() => Status);
            private set => SetProperty(() => Status, value);
        }

        /// <summary>
        ///     Права копирования
        /// </summary>
        public string? Copyright {
            get => GetProperty(() => Copyright);
            private set => SetProperty(() => Copyright, value);
        }

        #endregion

        #region Методы

        /// <summary>
        ///     Метод обработки события загрузки окна
        /// </summary>
        /// <param name="arguments">Аргументы события</param>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            Title = CommonDictionary.ApplicationName.ToUpperInvariant();
            Status = "Загрузка...";
            Subtitle = "";
            Copyright =
                $"Copyright {(DateTime.Now.Year - 2).ToString()} - {DateTime.Now.Year.ToString()} " +
                $"{CommonDictionary.CompanyName}";
            IsIndeterminate = true;

            HandleMessage<EventInitializerMessage>(OnMessage);

            #if !DEBUG
            var checkUpdateTask = UpdaterFTP.CheckUpdates();
            Status = "Проверка обновлений...";
            var availableVersions = await checkUpdateTask;
            if (availableVersions.Any()) {
                await UpdaterFTP.UpdateFromFTP(availableVersions);
            }
            #endif


            await ApplicationInitializer.Initialize();
            await OpenMainWindow();
        }

        /// <summary>
        ///     Метод обработки события приходящего сообщения статуса инициализации приложения
        /// </summary>
        /// <param name="message"></param>
        private void OnMessage(EventInitializerMessage message) {
            // Если ошибка
            if (message.Type == MessageType.Error) {
                Status = message.Message;
                // Закрываем приложение
                CurrentWindowSRV?.Close();
            }

            Status = message.Message;
        }

        /// <summary>
        ///     Метод открытия нового окна
        /// </summary>
        private Task OpenMainWindow() {
            // Создаём главное окно
            var window = new MainWIN();
            // var window = new TestWindow();
            // Подписываемся на событие "окно загружено"
            window.Loaded += delegate {
                CurrentWindowSRV?.Close();
            };
            // Открываем главное окно
            window.Show();

            return Task.CompletedTask;
        }

        #endregion
    }
}
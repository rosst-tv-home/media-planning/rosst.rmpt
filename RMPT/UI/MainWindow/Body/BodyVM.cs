﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.UI;
using DevExpress.Utils;
using DevExpress.Xpf.Core;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Repositories;
using RMPT.DATA.ViewModels.Models;
using RMPT.EVENT;
using RMPT.MessengerEvents;
using RMPT.UI.MainWindow.Body.ProjectManagement;
using RMPT.UI.MainWindow.Body.ProjectsCatalog;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.MainWindow.Body {
    /// <summary>
    /// Модель представления закладок главного окна
    /// </summary>
    public class BodyVM : AbstractVM {
        #region Свойства

        /// <summary>
        /// Сервис управления вкладками представления
        /// </summary>
        private IDocumentManagerService DocumentUiService => GetService<IDocumentManagerService>();

        /// <summary>
        /// Команда вызова метода обработки события закрытия вкладки
        /// </summary>
        public AsyncCommand<TabControlTabHidingEventArgs> OnTabHidingCMD => new(OnTabHiding);

        #endregion

        /// <summary>
        /// Метод обработки события закрытия вкладки
        /// </summary>
        /// <returns></returns>
        private async Task OnTabHiding(TabControlTabHidingEventArgs argument) {
            // Документ который закрывается
            var document = DocumentUiService.Documents.ToList()[argument.TabIndex];
            // Если вкладка не модель окна проекта
            if (document.Content is not BodyVProjectManagementTabM vm) return;
            argument.Cancel = true;
            // Проверка есть ли изменение проекта
            if (!(await CheckRequireSaveProject(vm.Project))) {
                // Закрываем документ
                document.Close();
                return;
            }

            // Метод обновления данных проекта
            await RepositoryProject.UpdateProject(vm.Project.ToEntity());
            // Закрываем документ
            document.Close();
        }

        /// <summary>
        /// Метод обработки события закрытия главного окна
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private async void ActualWindowOnClosing(object sender, CancelEventArgs argument) {
            // Если сервис текущего окна
            if (CurrentWindowSRV is not CurrentWindowService currentWindowSRV) {
                throw new NullReferenceException("Подключите сервис текущего окна");
            }
            // Отменяем закрытие окна
            // argument.Cancel = true;

            // Отписываемся от события закрытия главного окна
            currentWindowSRV.ActualWindow.Closing -= ActualWindowOnClosing;

            Collection<Task> tasks = new();

            // Итерируемся по открытым вкладкам
            foreach (var document in DocumentUiService.Documents.ToList()) {
                // Если вкладка не модель окна проекта
                if (document.Content is not BodyVProjectManagementTabM vm) continue;
                // Проверка есть ли изменение проекта
                if (!(await CheckRequireSaveProject(vm.Project))) continue;
                // Метод обновления данных проекта
                tasks.Add(RepositoryProject.UpdateProject(vm.Project.ToEntity()));
            }

            if (tasks.Count != 0) {
                argument.Cancel = true;
                await Task.WhenAll(tasks);
            }
        }

        /// <summary>
        /// Метод проверки есть ли изменения в проекте
        /// </summary>
        /// <param name="project"></param>
        /// <returns><see cref="bool"/></returns>
        private async Task<bool> CheckRequireSaveProject(ProjectVM project) {
            // Ищем проект в ДБ по его индентификатору
            var db = RepositoryProject.FindFullByID(project.ID);

            // Если проект не изменен
            if (project.GetSerializeString() == (await db).ToViewModel().GetSerializeString()) {
                return false;
            }

            // Сообщение
            var result = MessageBox.Show(
                $"Сохранить изменение в проекте: {project.Brand.Name} ?",
                "Сохранить ?",
                MessageBoxButton.YesNo,
                MessageBoxImage.Question
            );

            // Возвращаем
            return result == MessageBoxResult.Yes;
        }

        /// <summary>
        /// Метод обработки события загрузки окна
        /// </summary>
        /// <returns></returns>
        protected override Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Подписываемся на событие открытия выбранного проекта
            HandleMessage<EventOpenProject>(message => {
                // Если объект сообщения не является проектом - выбрасываем исключение
                if (message.Project is not ProjectVM project) {
                    throw new ArgumentException("Объект не является проектом");
                }

                foreach (var document in DocumentUiService.Documents) {
                    if (document.Content is not BodyVProjectManagementTabM vm) continue;
                    if (vm.Project.ID != project.ID) continue;

                    DocumentUiService.ActiveDocument = document;
                    return;
                }

                InflateTabView(
                    $"№{project.ID} {project.Brand.Name} {project.PlaningYear}",
                    nameof(BodyVProjectManagementTab),
                    message.Project,
                    DefaultBoolean.True
                );
            });
            // Подписываемся на событие изменения открытой вкладки
            DocumentUiService.ActiveDocumentChanged += OnActiveTabChanged;
            // Раздуваем представление списка проектов
            InflateTabView("Проекты", nameof(BodyVProjectsCatalogTab));
            // Если сервис текущего окна
            if (CurrentWindowSRV is CurrentWindowService currentWindowSRV) {
                // Подписываемся на событие закрытие гланвого окна
                currentWindowSRV.ActualWindow.Closing += ActualWindowOnClosing;
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Метод обработки события изменения открытой вкладки
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private void OnActiveTabChanged(object sender, ActiveDocumentChangedEventArgs argument) {
            if (argument.NewDocument is TabbedWindowDocumentUIService.TabbedWindowDocument tab) {
                switch (tab.DocumentType) {
                    case nameof(BodyVProjectsCatalogTab): {
                        SendMessage(new ToggleProjectCatalogToolBarME(true));
                        break;
                    }
                    default: {
                        SendMessage(new ToggleProjectCatalogToolBarME(false));
                        break;
                    }
                }
            }

            // Если контекст открытой вкладки является модель представлния окна проекта
            if (argument.NewDocument?.Content is BodyVProjectManagementTabM viewModel) {
                // Сообщение с управляемым в данный момент проектом во вкладку кнопок управления проектом
                SendMessage(new ManagementProjectChangedMessengerEvent(viewModel.Project));
            } else SendMessage(new ManagementProjectChangedMessengerEvent(null));
        }

        /// <summary>
        /// Метод раздутия представления во вкладке
        /// </summary>
        /// <param name="tabHeader">Наименование вкладки</param>
        /// <param name="viewName">Наименование представления, которое требуется раздуть</param>
        /// <param name="parameters">Параметры, которые требуется передать в раздутое представление</param>
        /// <param name="allowHide">Признак "вкладку можно закрыть"</param>
        private void InflateTabView(
            string tabHeader,
            string viewName,
            object? parameters = null,
            DefaultBoolean allowHide = DefaultBoolean.Default
        ) {
            // Инициализируем указанное представление с указанными параметрами
            var document = DocumentUiService.CreateDocument(viewName, parameters, this);
            // Устанавливаем наименование вкладки
            document.Title = tabHeader;
            // Раздутое представление должно быть уничтожено, а не оставаться в памяти после закрытия вкладки
            document.DestroyOnClose = true;
            // Отображаем раздутое представление
            document.Show();

            // Если раздутое представление не является вкладкой - завершаем выполнение метода
            if (document is not TabbedWindowDocumentUIService.TabbedWindowDocument tabbedDocument) return;

            // Устанавливаем признак "можно закрывать вкладку"
            tabbedDocument.Tab.AllowHide = allowHide;
        }
    }
}
﻿using DevExpress.Mvvm.Native;
using DevExpress.Xpf.Grid;
using RMPT.COMMON.Models;
using RMPT.DATA.ViewModels.Handlers;
using RMPT.DATA.ViewModels.Models;
using RMPT.EVENT;
using RMPT.GRID.Elements;
using RMPT.Models;
using RMPT.Services;
using RMPT.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.MainWindow.Body.ProjectManagement {
    /// <summary>
    ///     Модель представления окна проекта
    /// </summary>
    public class BodyVProjectManagementTabM : AbstractVM {
        #region Свойства

        private ExtendedTableFormatter? _tableFormatter;

        /// <summary>
        ///     Проект, данные которого отображаются в представлении
        /// </summary>
        public ProjectVM Project {
            get => GetProperty(() => Project);
            set => SetProperty(() => Project, value);
        }

        /// <summary>
        ///     Коллекция каналов проекта
        /// </summary>
        public ObservableCollection<ChannelVM> Channels {
            get => GetProperty(() => Channels);
            private set => SetProperty(() => Channels, value);
        }

        /// <summary>
        ///     Коллекция активных каналов проекта
        /// </summary>
        public ObservableCollection<ChannelVM> SelectedChannels { get; } = new();

        /// <summary>
        ///     Коллекция недельных периодов для выборки
        /// </summary>
        public ObservableCollection<WeekSelectorVM> SelectionWeeks { get; } = new();

        /// <summary>
        ///     Коллекция выбранных недельных периодов
        /// </summary>
        public ObservableCollection<WeekSelectorVM> SelectedWeeks { get; } = new();

        /// <summary>
        ///     Коллекция хронометражей для выборки
        /// </summary>
        public ObservableCollection<TimingSelectorVM> SelectionTimings { get; } = new();

        /// <summary>
        ///     Коллекция выбранных хронометражей
        /// </summary>
        public ObservableCollection<TimingSelectorVM> SelectedTimings { get; } = new();

        /// <summary>
        ///     Коллекция групп столбцов таблицы
        /// </summary>
        public ObservableCollection<ExtendedBand>? Bands {
            get => GetProperty(() => Bands);
            private set => SetProperty(() => Bands, value);
        }

        public ObservableCollection<FormatConditionBase> FormatConditions { get; } = new();

        #region Сервисы

        /// <summary>
        ///     Сервис управления таблицей проекта
        /// </summary>
        private IExtendedGridService GridService => GetService<IExtendedGridService>();

        /// <summary>
        ///     Сервис переключения выбранных периодов проекта
        /// </summary>
        private ExtendedPeriodTogglingService? _extendedPeriodTogglingService;

        /// <summary>
        ///     Сервис переключения выбранных хронометражей
        /// </summary>
        private ExtendedTimingTogglingService? _extendedTimingTogglingService;

        /// <summary>
        ///     Сервис переключения видимых столбцов проекта
        /// </summary>
        private ExtendedViewPreferencesTogglingService? _extendedViewPreferencesTogglingService;

        #endregion

        #endregion

        #region Методы

        #region Метод обновления источника данных таблицы

        private void RefreshChannelsCollection() {
            GridService.BeginDataUpdate();
            Channels = Project.Regions
               .SelectMany(x => x.Channels)
               .Where(x =>
                    !Project.ViewPreferences.IsShowOnlyActiveChannels ||
                    (Project.ViewPreferences.IsShowOnlyActiveChannels && x.IsActive)
                )
               .ToObservableCollection();
            GridService.EndDataUpdate();
        }

        #endregion

        #region Методы генерации моделей выборки

        /// <summary>
        ///     Метод генерации недельных периодов для выборки
        /// </summary>
        private void GenerateSelectingWeeks() {
            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine("Начало: генерация недельных периодов для выборки");

            // Запускаем замер времени выполнение метода
            var measure = Stopwatch.StartNew();
            #endif

            // Если нет проекта
            if (Project == null) {
                throw new InvalidOperationException(
                    "Не удалось сгенерировать периоды для выборки, " +
                    "так как отсутсвует ссылка на проект"
                );
            }

            // Активные недельные периоды проекта
            var activeWeeks = Project.Regions
               .SelectMany(region => region.Channels)
               .SelectMany(channel => channel.Months.Where(x => x.IsActive))
               .SelectMany(month => month.Weeks.Where(x => x.IsActive))
               .Select(week => week.DateFrom)
               .ToList();

            // Определяем месяца, которые есть в сырье
            var allowedMonths = Project.Regions
               .SelectMany(r => r.Channels)
               .SelectMany(c => c.Audiences)
               .Select(a => a.Date.Month)
               .ToHashSet();

            var monthIndex = 0;
            // Генерируем периоды на планируемый год и следующий
            for (var y = 0; y < 2; y++) {
                // Генерируем все месячные периоды года
                for (var m = 1; m <= 12; m++) {
                    if (!allowedMonths.Contains(m)) {
                        monthIndex++;
                        continue;
                    }

                    // Создаём месячный период
                    var month = new MonthSelectorVM(monthIndex, new DateTime(Project.PlaningYear + y, m, 1));
                    monthIndex++;

                    var weekIndex = 0;
                    // Генерируем недельные периоды месяца
                    foreach (var (dateFrom, dateBefore) in WeeksGenerator.Generate(month.Date)) {
                        // Создаём недельный период
                        var week = new WeekSelectorVM(weekIndex, month, dateFrom, dateBefore);
                        weekIndex++;

                        // Добавляем недельный период в коллекцию
                        SelectionWeeks.Add(week);

                        // Если неделя не содержится в коллекции активных - переходим к следующей
                        if (!activeWeeks.Contains(week.DateFrom)) {
                            continue;
                        }

                        // Добавляем недельный период в коллекцию выбранных
                        SelectedWeeks.Add(week);
                    }
                }
            }

            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine($"Завершение: генерация недельных периодов для выборки: {measure.ElapsedMilliseconds} ms");

            // Завершаем замер времени выполнения метода
            measure.Stop();
            #endif
        }

        /// <summary>
        ///     Метод генерации коллекции хронометражей для выборки
        /// </summary>
        private void GenerateSelectingTimings() {
            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine("Начало: генерация хронометражей для выборки");

            // Запускаем замер времени выполнение метода
            var measure = Stopwatch.StartNew();
            #endif

            // Если нет проекта
            if (Project == null) {
                throw new InvalidOperationException(
                    "Не удалось сгенерировать периоды для выборки, " +
                    "так как отсутсвует ссылка на проект"
                );
            }

            // Активные хронометражи проекта
            var activeTimings = Project
               .Regions.FirstOrDefault()
              ?.Channels.FirstOrDefault()
              ?.Months.FirstOrDefault(x => x.IsActive)
              ?.Weeks.FirstOrDefault(x => x.IsActive)
              ?.Timings
               .Where(x => x.IsActive)
               .Select(timing => timing.Value)
               .ToList() ?? new List<int>();

            // Генерируем хронометражи
            for (var value = 5; value <= 60; value += 5) {
                // Создаём хронометраж выборки
                var timing = new TimingSelectorVM(value);

                // Добавляем хронометраж в коллекцию
                SelectionTimings.Add(timing);

                // Если хронометраж не содержится в коллекции активных - переходим к слудющему
                if (!activeTimings.Contains(value)) {
                    continue;
                }

                // Добавляем хронометраж в коллекцию выбранных
                SelectedTimings.Add(timing);
            }

            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine($"Завершение: генерация хронометражей для выборки: {measure.ElapsedMilliseconds} ms");

            // Завершаем замер времени выполнения метода
            measure.Stop();
            #endif
        }

        #endregion

        #region Методы инициализации обработчиков событий изменений значений свойств объектов проекта

        /// <summary>
        ///     Метод инициализации обработчиков событий изменений значений свойств объектов проекта
        /// </summary>
        private void ReinitializeHandlers() {
            Project.PropertyChanged -= OnProjectPropertiesChanged;
            Project.PropertyChanged += OnProjectPropertiesChanged;

            SelectedWeeks.CollectionChanged -= OnWeeksChanged;
            SelectedWeeks.CollectionChanged += OnWeeksChanged;

            this._extendedTimingTogglingService?.InitializeHandlers();

            Project.ViewPreferences.PropertyChanged -= OnViewPreferencesChanged;
            Project.ViewPreferences.PropertyChanged += OnViewPreferencesChanged;

            Project.Regions.ForEach(region => {
                region.Channels.ForEach(channel => {
                    channel.Months.ForEach(month => {
                        month.Weeks.ForEach(week => {
                            week.Timings.ForEach(timing => {
                                timing.OnPropertyChangedCore -= TimingEventHandler.OnPropertyChanged;
                                timing.OnPropertyChangedCore += TimingEventHandler.OnPropertyChanged;
                            });

                            week.OnPropertyChangedCore -= WeekEventHandler.OnPropertyChanged;
                            week.OnPropertyChangedCore += WeekEventHandler.OnPropertyChanged;
                        });

                        month.OnPropertyChangedCore -= MonthEventHandler.OnPropertyChanged;
                        month.OnPropertyChangedCore += MonthEventHandler.OnPropertyChanged;
                    });

                    channel.PropertyChanged -= OnChannelsChanged;
                    channel.PropertyChanged += OnChannelsChanged;

                    channel.OnPropertyChangedCore -= ChannelEventHandler.OnPropertyChanged;
                    channel.OnPropertyChangedCore += ChannelEventHandler.OnPropertyChanged;
                });

                region.OnPropertyChangedCore -= RegionEventHandler.OnPropertyChanged;
                region.OnPropertyChangedCore += RegionEventHandler.OnPropertyChanged;
            });
        }

        private void CheckAllEnabledInNotMeasurableChannels() {
            var selectedWeeks = new HashSet<DateTime>();
            var selectedMonths = new HashSet<DateTime>();
            var selectedTimings = new HashSet<int>();

            foreach (var week in SelectedWeeks) {
                selectedWeeks.Add(week.DateFrom);
                selectedMonths.Add(week.Month.Date);
            }

            foreach (var timing in SelectedTimings) {
                selectedTimings.Add(timing.Value);
            }

            foreach (var channel in Channels) {
                if (channel.Region.IsMeasurable) continue;
                foreach (var month in channel.Months) {
                    foreach (var week in month.Weeks) {
                        foreach (var timing in week.Timings) {
                            if (!selectedTimings.Contains(timing.Value)) continue;
                            timing.IsActive = true;
                        }

                        if (!selectedWeeks.Contains(week.DateFrom)) continue;
                        week.IsActive = true;
                    }

                    if (!selectedMonths.Contains(month.Date)) continue;
                    month.IsActive = true;
                }
            }
        }

        #endregion

        #region Методы обработки событий

        /// <summary>
        ///     Метод вызывается, когда в модель представления передаются параметры
        /// </summary>
        /// <param name="parameter">Параметр, который был передан в эту модель представления</param>
        protected override void OnParameterChanged(object? parameter) {
            // Если параметр не является моделью проекта - завершаем выполнение метода
            if (parameter is not ProjectVM project) {
                return;
            }

            HandleMessage<EventRefreshProjectTable>(message => {
                if (message.ID != Project.ID) {
                    return;
                }

                GridService.RefreshData();
            });

            HandleMessage<EventRefreshProjectTableSource>(message => {
                if (message.ID != Project.ID) {
                    return;
                }

                RefreshChannelsCollection();
                ReinitializeHandlers();
                CheckAllEnabledInNotMeasurableChannels();
                GridService.RefreshData();
            });

            HandleMessage<EventReloadProject>(message => {
                if (message.Project is not ProjectVM project) {
                    return;
                }

                ReloadProject(project);
            });

            // Вызываем метод загрузки проекта
            ReloadProject(project);
            // Вызываем метод генерации периодов для выборки
            GenerateSelectingWeeks();
            // Вызываем метод генерации хронометражей для выборки
            GenerateSelectingTimings();
            // Вызываем метод инициализации обработчиков событий изменения значений свойств объектов проекта
            ReinitializeHandlers();
        }

        private void ReloadProject(ProjectVM project) {
            // Приостанавливаем реагировние таблицы на обновление данных
            GridService.BeginDataUpdate();

            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine("Начало: присваивание данных таблицы");
            // Запускаем замер времени выполнение метода
            var measure = Stopwatch.StartNew();
            #endif

            Project = project;

            // Переопределяем коллекцию каналов для таблицы
            RefreshChannelsCollection();

            // Наполняем коллекцию активных каналов
            Project.Regions
               .SelectMany(region => region.Channels)
               .Where(channel => channel.IsActive)
               .ForEach(SelectedChannels.Add);

            // Возобновляем реагирование таблицы на обновление данных
            GridService.EndDataUpdate();

            if (_tableFormatter != null) {
                ReinitializeHandlers();
                this.InitializeTimingTogglingService(Project, SelectedTimings, GridService, _tableFormatter);
                this.InitializeViewPreferencesTogglingService(Project, SelectedWeeks, GridService, _tableFormatter);
            }

            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine($"Завершение: присваивание данных таблицы: {measure.ElapsedMilliseconds} ms");
            // Завершаем замер времени выполнения метода
            measure.Stop();
            #endif
        }

        /// <summary>
        ///     Метод обработки события загрузки таблицы
        /// </summary>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            if (_tableFormatter != null) {
                return;
            }

            _tableFormatter = new ExtendedTableFormatter();

            // Опредляем коллекцию хронометражей
            var timings = SelectionTimings
               .Select(timing => new Tuple<int, bool>(timing.Value, SelectedTimings.Contains(timing)))
               .ToList();

            // Определяем коллекцию недельных периодов
            var weeks = SelectionWeeks
               .Select(week => new Tuple<DateTime, bool>(week.DateFrom, SelectedWeeks.Contains(week)))
               .ToList();

            await _tableFormatter.GenerateTable(
                Project.PlaningYear,
                Project.Frequency, weeks, timings,
                Project.BaseTiming,
                Project.IsFederal,
                Project.ViewPreferences.IsShowMonthsWeekBands,
                Project.ViewPreferences.IsShowRawDataBand,
                Project.ViewPreferences.IsShowChannelDistributionBands,
                Project.ViewPreferences.IsShowMonthDistributionBands
            );

            GridService.BeginBandsUpdate();
            Bands = _tableFormatter.Bands.ToObservableCollection();
            _tableFormatter.FormatConditions.ForEach(FormatConditions.Add);
            _tableFormatter.GroupSummary.ForEach(GridService.GridInstance().GroupSummary.Add);
            GridService.EndBandsUpdate();

            this.InitializeTimingTogglingService(Project, SelectedTimings, GridService, _tableFormatter);
            this.InitializeViewPreferencesTogglingService(Project, SelectedWeeks, GridService, _tableFormatter);
            _extendedPeriodTogglingService = new ExtendedPeriodTogglingService();
        }
        
        /// <summary>
        ///     
        /// </summary>
        /// <param name="project"></param>
        /// <param name="weeks"></param>
        /// <param name="gridSRV"></param>
        /// <param name="tableFormatter"></param>
        private void InitializeViewPreferencesTogglingService(
            ProjectVM project,
            ObservableCollection<WeekSelectorVM> weeks,
            IExtendedGridService gridSRV,
            ExtendedTableFormatter tableFormatter
        ) => _extendedViewPreferencesTogglingService = new ExtendedViewPreferencesTogglingService(
            gridSRV,
            tableFormatter,
            project.ViewPreferences,
            weeks
        );

        /// <summary>
        ///     
        /// </summary>
        /// <param name="project"></param>
        /// <param name="timings"></param>
        /// <param name="gridSRV"></param>
        /// <param name="tableFormatter"></param>
        private void InitializeTimingTogglingService(
            ProjectVM project,
            ObservableCollection<TimingSelectorVM> timings,
            IExtendedGridService gridSRV,
            ExtendedTableFormatter tableFormatter
        ) => _extendedTimingTogglingService = new ExtendedTimingTogglingService(
            gridSRV, tableFormatter, project, timings
        );

        /// <summary>
        ///     Метод обработки события изменения свойства проекта
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="arguments">Аргументы события</param>
        private void OnProjectPropertiesChanged(object sender, PropertyChangedEventArgs arguments) {
            switch (arguments.PropertyName) {
                // Если изменилась НДС или маржа
                case nameof(ProjectVM.Vat):
                case nameof(ProjectVM.Margin): {
                    // Обновляем таблицу проекта
                    GridService.RefreshData();
                    break;
                }
            }
        }

        /// <summary>
        ///     Метод обработки события изменения настроек представления проекта
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="arguments">Аргументы события</param>
        private void OnViewPreferencesChanged(object sender, PropertyChangedEventArgs arguments) {
            if (_tableFormatter == null) {
                throw new NullReferenceException();
            }


            switch (arguments.PropertyName) {
                // Отображать только активные каналы
                case nameof(ViewPreferencesVM.IsShowOnlyActiveChannels): {
                    RefreshChannelsCollection();
                    break;
                }
            }
        }

        /// <summary>
        ///     Метод обработки события изменения коллекции выбранных недельных периодов проекта
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private void OnWeeksChanged(object sender, NotifyCollectionChangedEventArgs argument) {
            #if DEBUG
            // Запускаем измерение времени выполнения метода
            var watch = Stopwatch.StartNew();
            #endif

            if (_tableFormatter == null) {
                #if DEBUG
                // Выводим время выполнения метода в консоль
                Debug.WriteLine(
                    $"{nameof(BodyVProjectManagementTabM)}:{nameof(OnWeeksChanged)}: {watch.ElapsedMilliseconds}ms");
                // Останавливаем измерение времени выполнения метода
                watch.Stop();
                #endif
                throw new NullReferenceException();
            }

            if (argument.Action == NotifyCollectionChangedAction.Reset) {
                foreach (var channel in Channels) {
                    foreach (var month in channel.Months) {
                        foreach (var week in month.Weeks) {
                            week.IsActive = false;
                        }
                    }
                }

                foreach (var band in _tableFormatter.MonthsBands) {
                    band.Visible = false;
                }

                foreach (var band in _tableFormatter.WeeksBands) {
                    band.Visible = false;
                }

                GridService.RefreshData();

                #if DEBUG
                // Выводим время выполнения метода в консоль
                Debug.WriteLine(
                    $"{nameof(BodyVProjectManagementTabM)}:{nameof(OnWeeksChanged)}: {watch.ElapsedMilliseconds}ms");
                // Останавливаем измерение времени выполнения метода
                watch.Stop();
                #endif

                return;
            }

            var isActive = argument.Action == NotifyCollectionChangedAction.Add;
            var selector = isActive ? argument.NewItems[0] as WeekSelectorVM : argument.OldItems[0] as WeekSelectorVM;
            if (selector == null) {
                #if DEBUG
                // Выводим время выполнения метода в консоль
                Debug.WriteLine(
                    $"{nameof(BodyVProjectManagementTabM)}:{nameof(OnWeeksChanged)}: {watch.ElapsedMilliseconds}ms");
                // Останавливаем измерение времени выполнения метода
                watch.Stop();
                #endif
                throw new NullReferenceException();
            }

            var isAnyWeekActive = false;
            foreach (var region in Project.Regions) {
                foreach (var channel in region.Channels) {
                    channel.Months[selector.Month.Index].Weeks[selector.Index].IsActive = isActive;
                    isAnyWeekActive = channel.Months[selector.Month.Index].Weeks.Any(x => x.IsActive);
                }
            }

            _tableFormatter.WeeksBands.First(x => (DateTime?)x.UID == selector.DateFrom).Visible =
                isActive && Project.ViewPreferences.IsShowMonthsWeekBands;

            _tableFormatter.MonthsBands.First(x => (DateTime?)x.UID == selector.Month.Date).Visible =
                isAnyWeekActive;

            GridService.RefreshData();

            #if DEBUG
            // Выводим время выполнения метода в консоль
            Debug.WriteLine(
                $"{nameof(BodyVProjectManagementTabM)}:{nameof(OnWeeksChanged)}: {watch.ElapsedMilliseconds}ms");
            // Останавливаем измерение времени выполнения метода
            watch.Stop();
            #endif
        }

        /// <summary>
        ///     Метод обработки события изменения коллекции выбранных хронометражей проекта
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private void OnTimingsChanged(object sender, NotifyCollectionChangedEventArgs argument) {
            #if DEBUG
            // Запускаем измерение времени выполнения метода
            var watch = Stopwatch.StartNew();
            #endif

            if (_tableFormatter == null) {
                #if DEBUG
                // Выводим время выполнения метода в консоль
                Debug.WriteLine(
                    $"{nameof(BodyVProjectManagementTabM)}:{nameof(OnTimingsChanged)}: {watch.ElapsedMilliseconds}ms");
                // Останавливаем измерение времени выполнения метода
                watch.Stop();
                #endif
                throw new NullReferenceException();
            }

            if (argument.Action == NotifyCollectionChangedAction.Reset) {
                foreach (var band in _tableFormatter.TimingDistributionBands) {
                    band.Visible = false;
                    foreach (var column in band.Columns) {
                        column.Visible = false;
                    }
                }

                foreach (var region in Project.Regions) {
                    foreach (var channel in region.Channels) {
                        foreach (var month in channel.Months) {
                            foreach (var week in month.Weeks) {
                                foreach (var timing in week.Timings) {
                                    timing.IsActive = false;
                                }
                            }
                        }
                    }
                }

                GridService.RefreshData();

                #if DEBUG
                // Выводим время выполнения метода в консоль
                Debug.WriteLine(
                    $"{nameof(BodyVProjectManagementTabM)}:{nameof(OnTimingsChanged)}: {watch.ElapsedMilliseconds}ms");
                // Останавливаем измерение времени выполнения метода
                watch.Stop();
                #endif

                return;
            }

            var isActive = argument.Action == NotifyCollectionChangedAction.Add;
            var selector = isActive
                ? argument.NewItems[0] as TimingSelectorVM
                : argument.OldItems[0] as TimingSelectorVM;
            if (selector == null) {
                #if DEBUG
                // Выводим время выполнения метода в консоль
                Debug.WriteLine(
                    $"{nameof(BodyVProjectManagementTabM)}:{nameof(OnTimingsChanged)}: {watch.ElapsedMilliseconds}ms");
                // Останавливаем измерение времени выполнения метода
                watch.Stop();
                #endif
                throw new NullReferenceException();
            }

            var columns = _tableFormatter.TimingDistributionBands
               .SelectMany(b => b.Columns.Cast<ExtendedColumn>())
               .Where(c => (int?)c.UID == selector.Value);

            var timings = Project.Regions
               .SelectMany(r => r.Channels)
               .SelectMany(c => c.Months)
               .SelectMany(m => m.Weeks)
               .SelectMany(w => w.Timings)
               .Where(t => t.Value == selector.Value);

            foreach (var timing in timings) {
                timing.IsActive = isActive;
            }

            foreach (var column in columns) {
                column.Visible = isActive;
            }

            var isAnyTimingActive = _tableFormatter.ChannelTimingDistributionBand.Columns.Any(x => x.Visible);

            _tableFormatter.ChannelTimingDistributionBand.Visible =
                isAnyTimingActive &&
                Project.ViewPreferences.IsShowChannelTimingDistributionBand;

            foreach (var band in _tableFormatter.MonthsTimingDistributionBands) {
                band.Visible =
                    isAnyTimingActive &&
                    Project.ViewPreferences.IsShowMonthTimingDistributionBand;
            }

            foreach (var band in _tableFormatter.WeeksTimingDistributionBands) {
                band.Visible =
                    isAnyTimingActive &&
                    Project.ViewPreferences.IsShowWeekTimingDistributionBand;
            }

            // GridService.RefreshData();

            #if DEBUG
            // Выводим время выполнения метода в консоль
            Debug.WriteLine(
                $"{nameof(BodyVProjectManagementTabM)}:{nameof(OnTimingsChanged)}: {watch.ElapsedMilliseconds}ms");
            // Останавливаем измерение времени выполнения метода
            watch.Stop();
            #endif
        }

        /// <summary>
        ///     Метод обработки события изменения коллекции выбранных каналов
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="arguments">Аргумент события</param>
        private void OnChannelsChanged(object sender, PropertyChangedEventArgs arguments) {
            if (sender is not ChannelVM || arguments.PropertyName != nameof(ChannelVM.IsActive)) {
                return;
            }

            RefreshChannelsCollection();
        }

        #endregion

        #endregion
    }
}
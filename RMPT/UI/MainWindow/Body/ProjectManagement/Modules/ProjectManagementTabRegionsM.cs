﻿using DevExpress.Mvvm.Native;
using DevExpress.Xpf.Editors;
using RMPT.COMMON;
using RMPT.COMMON.Dictionaries;
using RMPT.COMMON.Models;
using RMPT.DATA.ViewModels.Models;
using RMPT.DATA.ViewModels.Models.Timing;
using RMPT.GRID.Elements;
using RMPT.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace RMPT.UI.MainWindow.Body.ProjectManagement.Modules {
    /// <summary>
    ///     Модель представления таблицы целевых рейтингов регионов
    /// </summary>
    public class ProjectManagementTabRegionsM : AbstractVM {
        #region Сервисы

        /// <summary>
        ///     Сервис управления таблицей
        /// </summary>
        private IExtendedGridService GridService => GetService<IExtendedGridService>();

        #endregion

        /// <summary>
        ///     Метод генерации столбцов таблицы
        /// </summary>
        private void GenerateColumns() {
            GridService.BeginColumnsUpdate();

            var band = new ExtendedBand("Распределение по регионам");
            band.Columns.Add(new ExtendedColumn(
                ColumnHeaders.Region,
                nameof(RegionVM.Name),
                isReadOnly: true,
                tag: ExtendedColumnTag.RegionTRPTableName,
                width: 100D
            ));
            Bands.Add(band);

            var bands = Regions
               .FirstOrDefault(x => x.IsMeasurable)?.Months
               .Select((month, mIndex) => {
                    var nestedBand = new ExtendedBand(month.Date.ToString("Y"), month.IsActive);
                    nestedBand.Columns.Add(new ExtendedColumn(
                        ColumnHeaders.TRP,
                        $"{nameof(RegionVM.Months)}[{mIndex.ToString()}].{nameof(RegionMonthVM.TRP)}",
                        nestedBand,
                        tag: ExtendedColumnTag.RegionTRPTableMonth,
                        displayMask: "n2"
                    ));
                    var columns = month.Timings.Select((timing, tIndex) => new ExtendedColumn(
                        $"{timing.Value} {ColumnHeaders.Sec}",
                        $"{nameof(RegionVM.Months)}[{mIndex.ToString()}]." +
                        $"{nameof(RegionMonthVM.Timings)}[{tIndex.ToString()}]."             +
                        $"{nameof(RegionTimingVM.Portion)}",
                        nestedBand, isVisible: timing.IsActive,
                        displayMask: "p0", editMask: "p", editMaskType: MaskType.Numeric
                    ));
                    foreach (var column in columns) {
                        nestedBand.Columns.Add(column);
                    }

                    return nestedBand;
                }).ToList() ?? new List<ExtendedBand>();

            foreach (var item in bands) {
                Bands.Add(item);
            }

            GridService.EndColumnsUpdate();
        }

        #region Коллекции

        /// <summary>
        ///     Регионы
        /// </summary>
        public ObservableCollection<RegionVM> Regions { get; } = new();

        public ObservableCollection<ExtendedBand> Bands { get; set; } = new();

        #endregion

        #region Методы обработки событий

        /// <summary>
        ///     Метод обработки события изменения родительской модели представления
        /// </summary>
        /// <param name="parentViewModel">Родительская модель представления</param>
        /// <exception cref="Exception">Ошибка, если родительская модель представления неверного типа</exception>
        protected override void OnParentViewModelChanged(object parentViewModel) {
            // Если модель представления неверного типа
            if (parentViewModel is not BodyVProjectManagementTabM viewModel) {
                // Выбрасываем исключение
                throw new Exception(
                    "Родительской моделью представления должна " +
                    $"быть: {nameof(BodyVProjectManagementTabM)}"
                );
            }

            // Вызываем базовый метод обработки события изменения родительской модели представления
            base.OnParentViewModelChanged(viewModel);

            // Подписываемся на событие изменения значения свойства родительской модели представления
            viewModel.PropertyChanged += OnParentViewModelPropertyChanged;
        }

        /// <summary>
        ///     Метод обработки события изменения значения свойства родительской модели представления
        /// </summary>
        /// <param name="source">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private void OnParentViewModelPropertyChanged(
            object source,
            PropertyChangedEventArgs argument
        ) {
            // Если модель представления неверного типа
            if (source is not BodyVProjectManagementTabM viewModel) {
                // Выбрасываем исключение
                throw new Exception(
                    "Родительской моделью представления должна " +
                    $"быть: {nameof(BodyVProjectManagementTabM)}"
                );
            }

            // Если наименование свойства не совпадает
            if (!argument.PropertyName.Equals(nameof(BodyVProjectManagementTabM.Project))) {
                // Завершаем выполнение метода
                return;
            }

            // Пытаемся наполнить коллекцию регионов
            viewModel.Project.Regions.Where(x => x.IsMeasurable).OrderBy(x => x.Name).ForEach(Regions.Add);
            // Отписываемся от события изменения значения свйоства родительской модели представления
            viewModel.PropertyChanged -= OnParentViewModelPropertyChanged;

            // Вызываем метод генерации столбцов таблицы
            GenerateColumns();

            // Подписываемся на событие изменения значения свойства целевого рейтинга месячного периода региона
            Regions.FirstOrDefault()?.Months.ForEach(month => {
                month.Timings.ForEach(timing => timing.PropertyChanged += OnRegionTimingPropertyChanged);
                month.PropertyChanged += OnRegionMonthTargetRatingPointsPropertyChanged;
            });

            foreach (var region in Regions) {
                foreach (var channel in region.Channels) {
                    foreach (var month in channel.Months) {
                        foreach (var week in month.Weeks) {
                            foreach (var timing in week.Timings) {
                                timing.PropertyChanged -= OnChildModelsPropertyChanged;
                                timing.PropertyChanged += OnChildModelsPropertyChanged;
                            }

                            week.PropertyChanged -= OnChildModelsPropertyChanged;
                            week.PropertyChanged += OnChildModelsPropertyChanged;
                        }

                        month.PropertyChanged -= OnChildModelsPropertyChanged;
                        month.PropertyChanged += OnChildModelsPropertyChanged;
                    }

                    channel.PropertyChanged -= OnChildModelsPropertyChanged;
                    channel.PropertyChanged += OnChildModelsPropertyChanged;
                }

                region.PropertyChanged -= OnChildModelsPropertyChanged;
                region.PropertyChanged += OnChildModelsPropertyChanged;
            }
        }

        private void OnChildModelsPropertyChanged(object sender, PropertyChangedEventArgs e) {
            switch (e.PropertyName) {
                case nameof(TimingVM.TRP):
                case nameof(TimingVM.Outputs):
                case nameof(TimingVM.GivenGRP):
                case nameof(MonthVM.OutputsPerDay): {
                    Regions
                       .Where(r => r.Channels.Any(c => c.IsActive))
                       .SelectMany(r => r.Months)
                       .Where(m => m.IsActive)
                       .SelectMany(m => m.Timings)
                       .Where(t => t.IsActive)
                       .ForEach(t => t.NotifyUI(nameof(t.Portion)));
                    break;
                }
            }
        }

        /// <summary>
        ///     Метод обработки события изменения значения свойства целевого рейтинга месячного периода региона
        /// </summary>
        /// <param name="source">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        /// <exception cref="ArgumentException">Ошибка, если неверный тип источника события</exception>
        private void OnRegionMonthTargetRatingPointsPropertyChanged(object source, PropertyChangedEventArgs argument) {
            // Если источник события не является целевым рейтингом месячного периода региона
            if (source is not RegionMonthVM month) {
                // Выбрасываем исключение
                throw new ArgumentException(
                    $@"Метод {nameof(OnRegionMonthTargetRatingPointsPropertyChanged)} предназначен для " +
                    $@"обработки событий изменения значений свойств {nameof(RegionMonthVM)} моделей",
                    nameof(source)
                );
            }

            // Если изменилось значение не признака активности
            if (!argument.PropertyName.Equals(nameof(RegionMonthVM.IsActive))) {
                // Завершаем выполнение метода
                return;
            }

            Bands.First(b => b.Header.Equals(month.Date.ToString("Y"))).Visible = month.IsActive;
        }

        private void OnRegionTimingPropertyChanged(object source, PropertyChangedEventArgs argument) {
            // Если источник события не является
            if (source is not RegionTimingVM timing) {
                // Выбрасываем исключение
                throw new ArgumentException(
                    $@"Метод {nameof(OnRegionTimingPropertyChanged)} предназначен для " +
                    $@"обработки событий изменения значений свойств {nameof(RegionTimingVM)} моделей",
                    nameof(source)
                );
            }

            // Если изменилось значение не признака активности
            if (!argument.PropertyName.Equals(nameof(RegionTimingVM.IsActive))) {
                return;
            }

            Bands
               .SelectMany(b => b.Columns)
               .Where(c => c.Header.Equals($"{timing.Value} {ColumnHeaders.Sec}"))
               .ForEach(c => c.Visible = timing.IsActive);
        }

        #endregion
    }
}
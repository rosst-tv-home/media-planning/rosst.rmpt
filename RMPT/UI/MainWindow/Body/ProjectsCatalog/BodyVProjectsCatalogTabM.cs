﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.DataBase.Repositories;
using RMPT.EVENT;
using RMPT.MessengerEvents;
using RMPT.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace RMPT.UI.MainWindow.Body.ProjectsCatalog {
    /// <summary>
    ///     Модель представления каталога проектов
    /// </summary>
    public class BodyVProjectsCatalogTabM : AbstractVM {
        /// <summary>
        ///     Сервис управления таблицей проекта
        /// </summary>
        private IExtendedGridService GridService => GetService<IExtendedGridService>();

        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        public BodyVProjectsCatalogTabM() {
            // Подписываем на событие загрузки
            HandleMessage<ILoadingTableMessengerEvent>(_ => GridService?.ToggleLoadingPanel());
            // Подписываемся на событие получения коллекции проектов
            Messenger.Default.Register<List<PDProject>>(this, projects => {
                Projects.Clear();
                projects.ForEach(Projects.Add);
                if (GridService.IsLoading()) {
                    GridService.ToggleLoadingPanel();
                }
            });
            // Отправляем сообщение запроса обновления коллекции проекта
            SendMessage<IEventRefreshProjectsCatalog>(null!);
        }

        #endregion

        #region Делегаты

        /// <summary>
        ///     Команда вызова метода обработки события открытия выбранного проекта
        /// </summary>
        public AsyncCommand OpenSelectedProjectCommand => new(OpenSelectedProject);

        #endregion

        #region Методы

        /// <summary>
        ///     Метод обработки события открытия выбранного проекта
        /// </summary>
        private async Task OpenSelectedProject() {
            // Если нет выбранного проекта - завершаем выполнение метода
            if (SelectedProject == null) {
                return;
            }

            // Отправляем сообщение родительской модели представления,
            // для раздутия представления для выбранного проекта
            SendMessage(new EventOpenProject(
                (await RepositoryProject.FindFullByID(SelectedProject.ID)).ToViewModel()
            ));
        }

        #endregion

        #region Свойства

        /// <summary>
        ///     Выбранный проект
        /// </summary>
        public PDProject? SelectedProject {
            get => GetValue<PDProject?>();
            set => SetProperty(
                () => SelectedProject,
                value,
                () => SendMessage(new SelectedProjectChangedMessengerEvent(value))
            );
        }

        /// <summary>
        ///     Коллекция проектов
        /// </summary>
        public ObservableCollection<PDProject> Projects { get; } = new();

        #endregion
    }
}
﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.DataBase.Repositories;
using RMPT.EVENT;
using RMPT.MessengerEvents;
using RMPT.UI.DialogWindows.Dictionaries.CategoryAccessRights.Roles;
using RMPT.UI.DialogWindows.Dictionaries.CategoryAccessRights.Users;
using RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Audiences;
using RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Channels;
using RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Regions;
using RMPT.UI.DialogWindows.Dictionaries.CategoryProjects.Advertisers;
using RMPT.UI.DialogWindows.Dictionaries.CategoryProjects.Strategists;
using RMPT.UI.DialogWindows.Project;
using System;
using System.Threading.Tasks;

namespace RMPT.UI.MainWindow.Header {
    /// <summary>
    ///     Модель представления меню главного окна приложения
    /// </summary>
    public class HeaderVM : AbstractVM {
        public bool IsVisible {
            get => GetProperty(() => IsVisible);
            set => SetProperty(() => IsVisible, value);
        }

        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        public HeaderVM() {
            HandleMessage<ToggleProjectCatalogToolBarME>(message => IsVisible = message.IsVisible);
            // Подписываемся на событие изменения выбранного проекта в коллекции проектов
            HandleMessage<SelectedProjectChangedMessengerEvent>(message =>
                SelectedProject = message.SelectedProject
            );
            // Подписываемся на событие запроса обновления коллекции проектов
            HandleMessage<IEventRefreshProjectsCatalog>(async _ => await RefreshProjectsAsync());
        }

        #endregion

        #region Свойства

        /// <summary>
        ///     Дата начала периода
        /// </summary>
        public DateTime DateFrom { get; set; } = DateTime.Now.AddDays(-14);

        /// <summary>
        ///     Дата окончания периода
        /// </summary>
        public DateTime DateBefore { get; set; } = DateTime.Now.AddDays(1).AddSeconds(-1);

        /// <summary>
        ///     Выбранный проект
        /// </summary>
        public PDProject? SelectedProject {
            get => GetProperty(() => SelectedProject);
            set => SetProperty(
                () => SelectedProject,
                value,
                () => RaisePropertyChanged(nameof(IsProjectGroupButtonsEnabled))
            );
        }

        /// <summary>
        ///     Признак "кнопки \"группы проект\" активированы"
        /// </summary>
        public bool IsProjectGroupButtonsEnabled => SelectedProject != null;

        #endregion

        #region Делегаты

        /// <summary>
        ///     Комманда обновления коллекции проектов
        /// </summary>
        public AsyncCommand RefreshProjectsAsyncCommand => new(RefreshProjectsAsync);

        /// <summary>
        ///     Команда создания проекта
        /// </summary>
        public AsyncCommand CreateProjectAsyncCommand =>
            new(() => ShowDialogWindowAsync<ProjectPropertiesDW>());

        /// <summary>
        ///     Команда открытия диалогового окна свойств выбранного проекта
        /// </summary>
        public AsyncCommand ShowProjectPropertiesWindowAsyncCommand => new(() =>
            ShowDialogWindowAsync<ProjectPropertiesDW>(SelectedProject));

        /// <summary>
        ///     Команда удаления выбранного проекта
        /// </summary>
        public AsyncCommand RemoveSelectedProjectAsyncCommand => new(RemoveSelectedProjectAsync);

        /// <summary>
        ///     Команда создания копии выбранного проекта
        /// </summary>
        public AsyncCommand CloneSelectedProjectAsyncCommand => new(CloneSelectedProjectAsync);

        /// <summary>
        ///     Команда открытия диалогового окна справочника пользовательских ролей
        /// </summary>
        public AsyncCommand ShowRolesDialogWindowAsyncCommand => new(() => ShowDialogWindowAsync<RolesDW>());

        /// <summary>
        ///     Команда открытия диалогового окна справочника пользователей
        /// </summary>
        public AsyncCommand ShowUsersDialogWindowAsyncCommand => new(() => ShowDialogWindowAsync<UsersDW>());

        /// <summary>
        ///     Команда открытия диалогового окна справочника рекламодателей
        /// </summary>
        public AsyncCommand ShowAdvertisersDialogWindowAsyncCommand =>
            new(() => ShowDialogWindowAsync<AdvertisersDW>());

        /// <summary>
        ///     Команда открытия диалогового окна справочника стратегов
        /// </summary>
        public AsyncCommand ShowStrategistsDialogWindowAsyncCommand =>
            new(() => ShowDialogWindowAsync<StrategistsDW>());

        #region Диалоговые окна справочников каналов

        /// <summary>
        ///     Команда открытия диалогового окна справочника регионов
        /// </summary>
        public AsyncCommand ShowRegionsDialogWindowAsyncCommand =>
            new(() => ShowDialogWindowAsync<RegionsDW>());

        #endregion

        public AsyncCommand ShowChannelsDialogWindowAsyncCommand =>
            new(() => ShowDialogWindowAsync<ChannelsDW>());

        /// <summary>
        ///     Команда открытия диалогового окна базовой аудитории
        /// </summary>
        public AsyncCommand ShowAudiencesDialogWindowAsyncCommand =>
            new(() => ShowDialogWindowAsync<AudiencesDW>());

        #endregion

        #region Методы

        /// <summary>
        ///     Метод поиска проектов по указанному периоду
        /// </summary>
        private async Task RefreshProjectsAsync() {
            Messenger.Default.Send<ILoadingTableMessengerEvent>(null!);
            // Коллекция проектов
            var projects = await RepositoryProject.FindAllByPeriodLight(DateFrom, DateBefore);
            // Отправляем сообщение с коллекцией проектов другим моделям представлений
            Messenger.Default.Send(projects);
        }

        /// <summary>
        ///     Метод удаления выбранного в таблице проекта
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        private async Task RemoveSelectedProjectAsync() {
            // Если есть ссылка на выбранный проект
            if (SelectedProject != null) {
                await RepositoryProject.DeleteAsync(SelectedProject);
                await RefreshProjectsAsync();
            }
        }

        /// <summary>
        ///     Метод клонирования выбранного проекта
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        private async Task CloneSelectedProjectAsync() {
            if (SelectedProject != null) {
                var project =  await RepositoryProject.FindFullByID(SelectedProject.ID);
                project.ID = 0;
                foreach (var region in project.Regions) {
                    region.ID = 0;
                    region.ProjectID = 0;
                    foreach (var channel in region.Channels) {
                        channel.ID = 0;
                        channel.RegionID = 0;
                        foreach (var month in channel.Months) {
                            month.ID = 0;
                            month.ChannelID = 0;
                            foreach (var week in month.Weeks) {
                                week.ID = 0;
                                week.MonthID = 0;
                                foreach (var timing in week.Timings) {
                                    timing.ID = 0;
                                    timing.WeekID = 0;
                                }
                            }
                        }
                    }
                }

                await RepositoryProject.InsertAsync(project, project.IsFirstInitialization);
                await RefreshProjectsAsync();
            }
        }

        #endregion
    }
}
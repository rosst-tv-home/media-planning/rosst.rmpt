﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using DevExpress.Spreadsheet;
using Microsoft.Win32;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Repositories;
using RMPT.DATA.ViewModels.Models;
using RMPT.EVENT;
using RMPT.MessengerEvents;
using RMPT.NRA;
using RMPT.UI.DialogWindows.ProjectManagement.CategoryManagement;
using RMPT.UI.DialogWindows.ProjectManagement.CategoryPlanning;
using RMPT.UI.DialogWindows.ProjectManagement.CategoryReports.FlowChart;
using RMPT.UI.DialogWindows.ProjectManagement.CategoryReports.MediaPlan;
using RMPT.UI.DialogWindows.ProjectManagement.CategoryViewPreferences;
using RMPT.Utils;
using RMPT.VALIDATE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace RMPT.UI.MainWindow.Header.Tabs {
    /// <summary>
    ///     Модель представления вкладки кнопок управления открытым проектом
    /// </summary>
    public class HeaderVProjectManagementTabM : AbstractVM {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        public HeaderVProjectManagementTabM() {
            // Подписываемся на событие изменения управляемого проекта
            HandleMessage<ManagementProjectChangedMessengerEvent>(message => {
                Project = message.Project;
                // Если проект есть то
                if (Project != null) {
                    // Выбираем вкладку управление проектом
                    IsSelected = true;
                    // Завершаем выполнение метода
                    return;
                }

                // Иначе вкладка не выбирается
                IsSelected = false;
            });

            var dispatcher = new DispatcherTimer {
                Interval = new TimeSpan(0, 0, 2)
            };
            dispatcher.Tick += (_, _) => RaisePropertiesChanged(() => ProjectBudgetText, () => ProjectBudgetWithAgencyCommissionText);
            dispatcher.Start();
        }

        #endregion

        #region Свойства

        /// <summary>
        ///     Признак "Вкладка отображается"
        /// </summary>
        public bool IsVisible => Project != null;

        /// <summary>
        ///     Признак "Выбрана ли вкладка"
        /// </summary>
        public bool IsSelected {
            get => GetProperty(() => IsSelected);
            set => SetProperty(() => IsSelected, value);
        }

        /// <summary>
        ///     Наименование вкладки
        /// </summary>
        public string Caption => Project == null ? "" : $"№{Project.ID} {Project!.Brand.Name} {Project.PlaningYear}";

        /// <summary>
        ///     Проект
        /// </summary>
        public ProjectVM? Project {
            get => GetProperty(() => Project);
            private set => SetProperty(
                () => Project,
                value,
                () => RaisePropertiesChanged(
                    nameof(IsVisible),
                    nameof(Caption),
                    nameof(ProjectBudgetText),
                    nameof(ProjectBudgetWithAgencyCommissionText)
                )
            );
        }

        public string ProjectBudgetText =>
            $"Бюджет проекта: {Project?.Regions.Sum(x => x.Channels.Sum(y => y.Months.Sum(z => z.Budget))):C}";
        public string ProjectBudgetWithAgencyCommissionText =>
            $"Бюджет проекта + АК: {Project?.Regions.Sum(x => x.Channels.Sum(y => y.Months.Sum(z => z.Budget + (z.Budget * z.Channel.Region.Project.AgencyCommission)))):C}";

        #endregion

        #region Делегаты

        /// <summary>
        ///     Команда сохранения проекта
        /// </summary>
        public AsyncCommand SaveProjectChangedCommand => new(OnSaveProjectChanged);

        /// <summary>
        ///     Команда отображения диалогового окна неизмеряемых каналов
        /// </summary>
        public AsyncCommand ShowNotMonitoringChannelsDialogWindowCommand => new(
            () => ShowDialogWindowAsync<NotMonitoringChannelsDW>(Project)
        );

        /// <summary>
        ///     Команда экспорта заявки НРА Считалки
        /// </summary>
        public AsyncCommand ExportOrderCommand => new(OnExportOrder);

        /// <summary>
        ///     Команда импорта заявки НРА Считалки
        /// </summary>
        public AsyncCommand ImportOrderCommand => new(ImportOrder);

        /// <summary>
        ///     Команда расчёта цен в НРА Считалке
        /// </summary>
        public AsyncCommand GetPricesCommand => new(GetPrices);

        /// <summary>
        ///     Команда вызова метода обработки события нажатия кнопки "Выборка каналов"
        /// </summary>
        public AsyncCommand RawDataChartCMD => new(OnRawDataChart);

        /// <summary>
        ///     Команда открытия диалогового окна настроек флоучарта
        /// </summary>
        public AsyncCommand FlowChartSettingsCommand => new(() => ShowDialogWindowAsync<FlowChartSettingsDW>(Project));

        /// <summary>
        ///     Команда открытия диалогового окна настроек медиаплана
        /// </summary>
        public AsyncCommand MediaPlanChartSettingsCommand =>
            new(() => ShowDialogWindowAsync<MediaPlanSettingsDW>(Project));

        /// <summary>
        ///     Команда открытия диалогового окна настроек представления проекта
        /// </summary>
        public AsyncCommand ShowViewPreferences => new(() =>
            ShowDialogWindowAsync<ViewPreferencesDW>(Project!.ViewPreferences));

        public AsyncCommand ShowPlanningByWeeks => new(() => ShowDialogWindowAsync<PlanningByWeeksDW>(Project));

        #endregion

        #region Методы

        /// <summary>
        ///     Метод сохранения проекта
        /// </summary>
        private async Task OnSaveProjectChanged() {
            // Если нет проекта завершаем метод
            if (Project == null) {
                return;
            }

            SendMessage(new EventStatusBarMessage("Сохранение проекта..."));
            // Ждем выполнение задачи по обновлению самого проекта в базу данных
            await RepositoryProject.UpdateProject(Project.ToEntity());
            // Меняем статусную строку приложения
            SendMessage(new EventStatusBarMessage("Данные успешно сохранены"));
            // Ждем секунду
            await Task.Delay(1000);
            // var project = (await RepositoryProject.FindFullByID(Project.ID)).ToViewModel();
            // SendMessage(new ManagementProjectChangedMessengerEvent(project));
            // SendMessage(new EventReloadProject(project));
            // Меняем статусную строку приложения
            SendMessage(new EventStatusBarMessage(""));
        }

        #region Методы кнопок группы "НРА Считалка"

        /// <summary>
        ///     Метод экспорта заявки НРА Считалки
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private async Task OnExportOrder() {
            // Если нет проекта завершаем метод
            if (Project == null) {
                return;
            }

            // Формируем диалоговое окно
            var dialog = new SaveFileDialog {
                Title = "Выберите место сохранения заявки НРА Считалки",
                Filter = "Заявка НРА Считалка (*.mz;*.mzn)|*.mz;*.mzn|Все файлы (*.*)|*.*",
                DefaultExt = "mzn",
                FileName = $"{Project.Brand.Advertiser.Name}_{Project.Brand.Name}_{DateTime.Now:g}".Replace(':', '-')
            };

            // Если диалоговое окно не подтверждено - завершаем выполнение метода
            if (dialog.ShowDialog() == false) {
                return;
            }

            var channels = Project.Regions.SelectMany(x => x.Channels).Where(x => x.IID != null).ToList();

            // Заново инициализируем ядро считалки (для очистки)
            await CalculateWrapper.ReInitKernel();
            // Очищаем заявку
            CalculateWrapper.ClearStorage();
            // Устанавливаем наименование агенства
            await CalculateWrapper.SetAgencyName("РА РОССТ");
            // Устанавливаем наименование рекламодателя
            await CalculateWrapper.SetAdvertiserName($"{Project.Brand.Advertiser.Name} [{Project.Brand.Name}]");

            var tasks = new List<Task>();

            if (!Project.IsFederal) {
                foreach (var region in Project.Regions.Where(x =>
                    x.Channels.Any(y => y.IsActive && (y.GivenGRP > 0.0 || y.Minutes > 0.0)))) {
                    tasks.Add(CalculateWrapper.EnableGeoDistribution(region.Name));
                }
            }

            foreach (var channel in channels.Where(x => (x.IsActive && x.GivenGRP > 0.0) || x.Minutes > 0.0)) {
                // Активируем инвентарь канала в заявке НРА Считалки
                var task = CalculateWrapper.EnableChannel(
                    Project.IsFederal,
                    channel.IID ?? -1,
                    channel.Months
                       .Where(x => x.IsActive)
                       .GroupBy(x => x.Date.Month)
                       .Select(x => new Tuple<int, double>(
                            x.Key - 1,
                            channel.SaleType == SaleType.Minutes
                                ? x.Sum(y => y.Minutes)
                                : x.Sum(y => y.GivenGRP)
                        ))
                       .ToReadOnlyCollection(),
                    channel.Months
                       .Where(x => x.IsActive)
                       .GroupBy(x => x.Date.Month)
                       .Select(x => new KeyValuePair<int, double[]>(x.Key - 1, new[] {
                            channel.SaleType == SaleType.Minutes
                                ? ValuesValidator.NanOrZero(
                                    x.Sum(y => y.Minutes * y.SuperFixOrFixPortion) / x.Sum(y => y.Minutes)
                                )
                                : ValuesValidator.NanOrZero(
                                    x.Sum(y => y.GivenGRP * y.SuperFixOrFixPortion) / x.Sum(y => y.GivenGRP)
                                ),
                            channel.SaleType == SaleType.Minutes
                                ? ValuesValidator.NanOrZero(
                                    x.Sum(y => y.Minutes * y.SuperFixOrFixPrimePortion) / x.Sum(y => y.Minutes)
                                )
                                : ValuesValidator.NanOrZero(
                                    x.Sum(y => y.GivenGRP * y.SuperFixOrFixPrimePortion) / x.Sum(y => y.GivenGRP)
                                ),
                            channel.SaleType == SaleType.Minutes
                                ? ValuesValidator.NanOrZero(
                                    x.Sum(y => y.Minutes * y.FixOrFloatPrimePortion) / x.Sum(y => y.Minutes)
                                )
                                : ValuesValidator.NanOrZero(
                                    x.Sum(y => y.GivenGRP * y.FixOrFloatPrimePortion) / x.Sum(y => y.GivenGRP)
                                )
                        })).ToDictionary(x => x.Key, x => x.Value)
                );
                tasks.Add(task);
            }

            await Task.WhenAll(tasks);

            // Формируем заявку НРА Считалки в файл
            await CalculateWrapper.ExportOrder(dialog.FileName);
            CalculateWrapper.ClearStorage();
        }

        /// <summary>
        ///     Метод импорта заявки НРА Считалки
        /// </summary>
        private async Task ImportOrder() {
            // Если нет проекта завершаем метод
            if (Project == null) {
                return;
            }

            // Формируем диалоговое окно
            var dialog = new OpenFileDialog {
                Title = "Выберите место сохранения заявки НРА Считалки",
                Filter = "Заявка НРА Считалка (*.mz;*.mzn)|*.mz;*.mzn|Все файлы (*.*)|*.*",
                Multiselect = false
            };

            // Если диалоговое окно не подтверждено - завершаем выполнение метода
            if (dialog.ShowDialog() == false) {
                return;
            }

            var channels = Project.Regions.SelectMany(x => x.Channels).Where(x => x.IID != null).ToList();

            var storage = await CalculateWrapper.ImportOrder(dialog.FileName);
            try {
                await CalculateWrapper.InjectImportedOrder(storage);
            } catch (Exception exception) {
                MessageBox.Show(exception.Message, "Ошибка ядра считалки", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // Итерируемся по рассчитанным инвентарям
            foreach (var inventory in CalculateWrapper.ClaimPricedInventories()) {
                // Определяем канал, которому принадлежит инвентарь
                var channel = channels.FirstOrDefault(x => x.IID == inventory.ID);
                // Если канал не был определён - переходим к следующему инвентарю
                if (channel == null) {
                    continue;
                }

                // Группируем месячные периоды канала по месяцам
                foreach (var monthGroup in channel.Months.GroupBy(x => x.Date.Month)) {
                    // Устанавливаем цены месячных периодов
                    monthGroup.ForEach(x => {
                        x.Price = inventory.PeriodPrice[monthGroup.Key - 1].Value;
                    });
                }
            }

            SendMessage(new EventRefreshProjectTable(Project.ID));
        }

        private async Task GetPrices() {
            // Если нет проекта завершаем метод
            if (Project == null) {
                return;
            }

            var channels = Project.Regions.SelectMany(x => x.Channels).Where(x => x.IID != null).ToList();

            // Заново инициализируем ядро считалки (для очистки)
            await CalculateWrapper.ReInitKernel();
            // Очищаем заявку
            CalculateWrapper.ClearStorage();
            // Устанавливаем наименование агенства
            await CalculateWrapper.SetAgencyName("РА РОССТ");
            // Устанавливаем наименование рекламодателя
            await CalculateWrapper.SetAdvertiserName($"{Project.Brand.Advertiser.Name} [{Project.Brand.Name}]");

            var tasks = new List<Task>();

            if (!Project.IsFederal) {
                foreach (var region in Project.Regions.Where(x =>
                    x.Channels.Any(y => y.IsActive && (y.GivenGRP > 0.0 || y.Minutes > 0.0)))) {
                    tasks.Add(CalculateWrapper.EnableGeoDistribution(region.Name));
                }
            }

            foreach (var channel in channels.Where(x => (x.IsActive && x.GivenGRP > 0.0) || x.Minutes > 0.0)) {
                // Активируем инвентарь канала в заявке НРА Считалки
                var task = CalculateWrapper.EnableChannel(
                    Project.IsFederal,
                    channel.IID ?? -1,
                    channel.Months
                       .Where(x => x.IsActive)
                       .GroupBy(x => x.Date.Month)
                       .Select(x => new Tuple<int, double>(
                            x.Key - 1,
                            channel.SaleType == SaleType.Minutes
                                ? x.Sum(y => y.Minutes)
                                : x.Sum(y => y.GivenGRP)
                        ))
                       .ToReadOnlyCollection(),
                    channel.Months
                       .Where(x => x.IsActive)
                       .GroupBy(x => x.Date.Month)
                       .Select(x => new KeyValuePair<int, double[]>(x.Key - 1, new[] {
                            channel.SaleType == SaleType.Minutes
                                ? ValuesValidator.NanOrZero(
                                    x.Sum(y => y.Minutes * y.SuperFixOrFixPortion) / x.Sum(y => y.Minutes)
                                )
                                : ValuesValidator.NanOrZero(
                                    x.Sum(y => y.GivenGRP * y.SuperFixOrFixPortion) / x.Sum(y => y.GivenGRP)
                                ),
                            channel.SaleType == SaleType.Minutes
                                ? ValuesValidator.NanOrZero(
                                    x.Sum(y => y.Minutes * y.SuperFixOrFixPrimePortion) / x.Sum(y => y.Minutes)
                                )
                                : ValuesValidator.NanOrZero(
                                    x.Sum(y => y.GivenGRP * y.SuperFixOrFixPrimePortion) / x.Sum(y => y.GivenGRP)
                                ),
                            channel.SaleType == SaleType.Minutes
                                ? ValuesValidator.NanOrZero(
                                    x.Sum(y => y.Minutes * y.FixOrFloatPrimePortion) / x.Sum(y => y.Minutes)
                                )
                                : ValuesValidator.NanOrZero(
                                    x.Sum(y => y.GivenGRP * y.FixOrFloatPrimePortion) / x.Sum(y => y.GivenGRP)
                                )
                        })).ToDictionary(x => x.Key, x => x.Value)
                );
                tasks.Add(task);
            }

            await Task.WhenAll(tasks);

            // Формируем заявку и внедряем её в ядро НРА Считалки
            try {
                await CalculateWrapper.InjectImportedOrder(await CalculateWrapper.GetStorageInstance());
            } catch (Exception exception) {
                MessageBox.Show(exception.Message, "Ошибка ядра считалки", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            // Итерируемся по рассчитанным инвентарям
            foreach (var inventory in CalculateWrapper.ClaimPricedInventories()) {
                // Определяем канал, которому принадлежит инвентарь
                var channel = channels.FirstOrDefault(x => x.IID == inventory.ID);
                // Если канал не был определён - переходим к следующему инвентарю
                if (channel == null) {
                    continue;
                }

                // Группируем месячные периоды канала по месяцам
                foreach (var monthGroup in channel.Months.GroupBy(x => x.Date.Month)) {
                    // Устанавливаем цены месячных периодов
                    monthGroup.ForEach(x => {
                        x.Price = inventory.PeriodPrice[monthGroup.Key - 1].Value;
                    });
                }
            }

            SendMessage(new EventRefreshProjectTable(Project.ID));
        }

        #endregion

        #region Метод обработки события нажатия на кнопку "Выборка каналов"

        /// <summary>
        ///     Метод обработки события нажатия на кнопку "Выборка каналов"
        /// </summary>
        private async Task OnRawDataChart() {
            if (Project == null) {
                throw new NullReferenceException("Нет ссылки на проект");
            }

            var formatter = new RawDataChartFormatter(Project);
            var document = await formatter.CreateDocument();

            // Формируем диалоговое окно
            var dialog = new SaveFileDialog {
                Title = "Выберите место сохранения медиаплана",
                Filter = "Медиаплан (*.xlsx)|*.xlsx|Все файлы (*.*)|*.*",
                DefaultExt = "xlsx",
                FileName =
                    $"Выборка каналов_{Project.Brand.Advertiser.Name}_{Project.Brand.Name}_{DateTime.Now:g}"
                       .Replace(':', '-')
            };

            // Если диалоговое окно не подтверждено - завершаем выполнение метода
            if (dialog.ShowDialog() == false) {
                return;
            }

            document.SaveDocument(dialog.FileName, DocumentFormat.Xlsx);
        }

        #endregion

        #endregion
    }
}
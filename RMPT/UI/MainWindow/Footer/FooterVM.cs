﻿using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.EVENT;
using RMPT.MessengerEvents;

namespace RMPT.UI.MainWindow.Footer {
    /// <summary>
    ///     Модель представления статусной строки приложения
    /// </summary>
    public class FooterVM : AbstractVM {
        /// <summary>
        ///     Конструктор
        /// </summary>
        public FooterVM() {
            // Подписываемся на сообщение события изменения статусной строки
            HandleMessage<EventStatusBarMessage>(message => Status = message.Status);
        }

        /// <summary>
        ///     Строка статуса
        /// </summary>
        public string Status {
            get => GetProperty(() => Status);
            set => SetProperty(() => Status, value);
        }
    }
}
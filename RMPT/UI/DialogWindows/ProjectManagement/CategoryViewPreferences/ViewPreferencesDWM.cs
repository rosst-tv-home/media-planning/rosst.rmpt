﻿using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.ViewModels.Models;

namespace RMPT.UI.DialogWindows.ProjectManagement.CategoryViewPreferences {
    class ViewPreferencesDWM : AbstractVM {
        /// <summary>
        ///     Настройки представления проекта
        /// </summary>
        public ViewPreferencesVM ViewPreferences {
            get => GetProperty(() => ViewPreferences);
            set => SetProperty(() => ViewPreferences, value);
        }

        protected override void OnParameterChanged(object parameter) {
            if (parameter is not ViewPreferencesVM viewPreferences) {
                return;
            }

            ViewPreferences = viewPreferences;

            base.OnParameterChanged(parameter);
        }
    }
}
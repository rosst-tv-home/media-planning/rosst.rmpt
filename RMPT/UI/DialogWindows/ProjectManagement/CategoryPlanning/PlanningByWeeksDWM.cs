﻿using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using RMPT.COMMON.Dictionaries;
using RMPT.COMMON.Models;
using RMPT.DATA.ViewModels.Models;
using RMPT.DATA.ViewModels.Models.Timing;
using RMPT.GRID.Elements;
using RMPT.Services;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.ProjectManagement.CategoryPlanning {
    public class PlanningByWeeksDWM : AbstractVM {
        #region Свойства

        /// <summary>
        ///     Проект
        /// </summary>
        private ProjectVM? _project;

        /// <summary>
        ///     Группы столбцов
        /// </summary>
        public ObservableCollection<BandBase> Bands { get; } = new();

        /// <summary>
        ///     Источник данных
        /// </summary>
        public ObservableCollection<RegionVM> Items { get; } = new();

        #endregion

        #region Сервисы

        /// <summary>
        ///     Сервис доступа к таблице
        /// </summary>
        protected IExtendedGridService? ExtendedGridSRV => GetService<IExtendedGridService>();

        #endregion

        #region Методы обработки событий

        #region Метод обработки события изменения параметров

        /// <summary>
        ///     Метод обработки события изменения параметров
        /// </summary>
        /// <param name="parameter">Новые параметры</param>
        protected override void OnParameterChanged(object parameter) {
            // Если это не проект - завершаем выполнение метода
            if (parameter is not ProjectVM project) return;

            // Запоминаем проект
            this._project = project;
        }

        #endregion

        #region Метод обработки события загрузки представления

        /// <summary>
        ///     Метод обработки события загрузки представления
        /// </summary>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            if (this.ExtendedGridSRV == null) throw new NullReferenceException();

            if (!this.ExtendedGridSRV.IsLoading()) {
                this.ExtendedGridSRV.ToggleLoadingPanel();
                await Task.Delay(100);
            }

            await this.GenerateBands();
            foreach (var region in this._project!.Regions.ToList()) {
                this.Items.Add(region);
                await Task.Delay(50);
            }

            if (this.ExtendedGridSRV.IsLoading()) {
                await Task.Delay(100);
                this.ExtendedGridSRV.ToggleLoadingPanel();
            }
        }

        private async Task GenerateBands() {
            var band = new ExtendedBand("");
            this.Bands.Add(band);
            await Task.Delay(25);

            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.Region,
                nameof(RegionVM.Name),
                band, true, width: 100D
            ));
            await Task.Delay(25);

            await GenerateMonthBands();
        }

        private async Task GenerateMonthBands() {
            int index = 0;
            foreach (var month in this._project!.Regions.First().Months.ToList()) {
                if (!month.IsActive) {
                    index++;
                    continue;
                }

                var monthBand = new ExtendedBand(month.Date.ToString("Y"));
                this.Bands.Add(monthBand);
                await Task.Delay(25);

                await GenerateWeekBands(monthBand, month, index);
                index++;
            }
        }

        private async Task GenerateWeekBands(GridControlBand monthBand, RegionMonthVM month, int monthIndex) {
            int index = 0;
            foreach (var week in month.Weeks.ToList()) {
                if (!week.IsActive) {
                    index++;
                    continue;
                }

                var weekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
                    week.DateFrom,
                    CalendarWeekRule.FirstDay,
                    DayOfWeek.Monday
                );
                var weekBand = new ExtendedBand($"№{weekNumber.ToString()} [{week.DateFrom.Day.ToString()} число]");
                monthBand.Bands.Add(weekBand);
                await Task.Delay(25);

                await GenerateWeekColumns(weekBand, week, monthIndex, index);
                index++;
            }
        }

        private async Task GenerateWeekColumns(
            ExtendedBand weekBand,
            RegionWeekVM week,
            int monthIndex,
            int weekIndex
        ) {
            var column = new ExtendedUnboundColumn(
                ColumnHeaders.ActiveDays,
                $"{nameof(RegionVM.Months)}[{monthIndex}]."    +
                $"{nameof(RegionMonthVM.Weeks)}[{weekIndex}]." +
                $"{nameof(RegionWeekVM.ActiveDays)}",
                weekBand, displayMask: "n2", width: 55
            );
            weekBand.Columns.Add(column);
            await Task.Delay(25);

            column = new ExtendedUnboundColumn(
                ColumnHeaders.TRP,
                $"{nameof(RegionVM.Months)}[{monthIndex}]."    +
                $"{nameof(RegionMonthVM.Weeks)}[{weekIndex}]." +
                $"{nameof(RegionWeekVM.TRP)}",
                weekBand, displayMask: "n2", width: 55
            );
            weekBand.Columns.Add(column);
            await Task.Delay(25);

            int index = 0;
            foreach (var timing in week.Timings.ToList()) {
                if (!timing.IsActive) {
                    index++;
                    continue;
                }

                column = new ExtendedUnboundColumn(
                    timing.Value.ToString(),
                    $"{nameof(RegionVM.Months)}[{monthIndex}]."    +
                    $"{nameof(RegionMonthVM.Weeks)}[{weekIndex}]." +
                    $"{nameof(RegionWeekVM.Timings)}[{index}]."    +
                    $"{nameof(RegionWeekTimingVM.Portion)}",
                    weekBand, displayMask: "p0", editMask: "p", editMaskType: MaskType.Numeric
                );

                weekBand.Columns.Add(column);
                index++;
                await Task.Delay(25);
            }
        }

        #endregion

        #endregion
    }
}
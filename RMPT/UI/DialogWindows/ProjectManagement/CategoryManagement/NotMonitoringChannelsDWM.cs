﻿using DevExpress.Mvvm.Native;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.DataBase.Repositories;
using RMPT.DATA.ViewModels.Models;
using RMPT.EVENT;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.ProjectManagement.CategoryManagement {
    public class NotMonitoringChannelsDWM : AbstractVM {
        #region Свойства

        /// <summary>
        ///     Проект
        /// </summary>
        private ProjectVM? _project;

        /// <summary>
        ///     Неизмеряемые каналы, присутвующие в проекте
        /// </summary>
        private readonly List<ChannelVM> _alreadySelected = new();

        /// <summary>
        ///     Неизмеряемые каналы
        /// </summary>
        public BindingList<DCChannel> Channels { get; } = new();

        /// <summary>
        ///     Выбранные неизмеряемые каналы
        /// </summary>
        public BindingList<DCChannel> Selected { get; } = new();

        #endregion

        #region Методы обработки событий

        /// <summary>
        ///     Метод обработки события изменения параметров
        /// </summary>
        /// <param name="parameter">Новые параметры</param>
        protected override void OnParameterChanged(object parameter) {
            // Если это не проект - завершаем выполнение метода
            if (parameter is not ProjectVM project) return;

            // Запоминаем проект
            _project = project;
        }

        /// <summary>
        ///     Метод обработки события загрузки диалогового окна
        /// </summary>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Заполняем список неизмеряемых каналов
            (await RepositoryChannels.FindAllNotMeasurable())
               .OrderBy(c => c.Region!.Name)
               .ThenBy(c => c.Name)
               .ToList()
               .ForEach(Channels.Add);

            // Заполняем уже выбранные каналы
            _project!.Regions
               .Where(r => !r.IsMeasurable)
               .SelectMany(r => r.Channels)
               .ForEach(c => _alreadySelected.Add(c));

            // Идентификаторы выбранных до
            var selectedIDs = _alreadySelected.Select(c => c.DictionaryID).ToHashSet();

            // Заполняем выбранные для отображения
            Channels.Where(c => selectedIDs.Contains(c.ID)).ForEach(Selected.Add);
        }

        protected override async Task OnUnloadedAsync(RoutedEventArgs? arguments) {
            // Неизмеряемые регионы в проекте
            var notMeasurableRegions = _project!.Regions.Where(r => !r.IsMeasurable).ToList();
            // Идентификаторы выбранных неизмеряемых каналов сейчас
            var selectedIDs = Selected.Select(c => c.ID).ToHashSet();
            // Идентификаторы выбранных неизмеряемых каналов до изменений
            var selectedBeforeIDs = _alreadySelected.Select(c => c.DictionaryID).ToHashSet();

            // Удалённые каналы
            var channels = _project!.Regions
               .Where(r => !r.IsMeasurable)
               .SelectMany(r => r.Channels)
               .Where(c => !selectedIDs.Contains(c.DictionaryID))
               .ToList();

            // Удаляем из модели представления
            foreach (var region in notMeasurableRegions) {
                foreach (var channel in channels) {
                    region.Channels.Remove(channel);
                }
            }

            // Определяем новые выбранные каналы
            var newChannels = Selected
               .Where(c => !selectedBeforeIDs.Contains(c.ID))
               .GroupBy(c => c.Region!.ID);

            var regions = new List<PDRegion>();
            foreach (var group in newChannels) {
                var region =
                    // Ищем в самом проекте
                    _project.Regions.FirstOrDefault(r => r.DictionaryID == group.Key)?.ToEntity() ??
                    // Ищем в заполненной коллекции
                    regions.FirstOrDefault(r => r.DictionaryID == group.Key);
                // Если нигде не нашли - создаём
                if (region == null) {
                    region = new PDRegion {
                        ID = 0,
                        DictionaryID = group.Key,
                        ProjectID = _project.ID
                    };
                    regions.Add(region);
                }

                region.Channels.AddRange(group.Select(c => new PDChannel {
                    ID = 0,
                    RegionID = region.ID,
                    DictionaryID = c.ID,
                    IsActive = true,
                    Region = region.ID == 0 ? region : null
                }));

                regions.Add(region);
            }

            notMeasurableRegions.ForEach(r => {
                if (r.Channels.Count == 0) {
                    r.Project.Regions.Remove(r);
                }
            });

            // Добавляем в модель представления
            foreach (var region in regions) {
                region.Region = await RepositoryRegions.FindByID(region.DictionaryID);
                foreach (var channel in region.Channels) {
                    channel.Channel = await RepositoryChannels.FindByID(channel.DictionaryID);
                }
            }

            regions.Select(r => r.ToViewModel(_project))
               .ToList()
               .ForEach(r => {
                    var region = _project.Regions.FirstOrDefault(x => x.DictionaryID == r.DictionaryID);
                    if (region == null) {
                        region = r;
                        _project.Regions.Add(region);
                    }

                    r.Channels.ToList().ForEach(c => {
                        var channel = region.Channels.FirstOrDefault(x => x.DictionaryID == c.DictionaryID);
                        if (channel == null) {
                            channel = c;
                            region.Channels.Add(channel);
                        }
                    });
                });

            // Обновляем таблицу проекта
            SendMessage(new EventRefreshProjectTableSource(_project.ID));
        }

        #endregion
    }
}
﻿using DevExpress.Mvvm;
using DevExpress.Spreadsheet;
using Microsoft.Win32;
using RMPT.COMMON.Models;
using RMPT.DATA.ViewModels.Models;
using RMPT.Models;
using RMPT.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace RMPT.UI.DialogWindows.ProjectManagement.CategoryReports.FlowChart {
    public class FlowChartSettingsDWM : AbstractVM {
        #region Свойства

        /// <summary>
        ///     Проект
        /// </summary>
        private ProjectVM? _project;

        /// <summary>
        ///     Типы флоучартов
        /// </summary>
        public IList<FlowChartType> Types { get; } = new List<FlowChartType>();

        /// <summary>
        ///     Выбранный тип флоучарта
        /// </summary>
        public FlowChartType SelectedType {
            get => GetProperty(() => SelectedType);
            set => SetProperty(() => SelectedType, value);
        }

        /// <summary>
        ///     Признак "отображать номер недели"
        /// </summary>
        public bool IsShowWeekNumber {
            get => GetProperty(() => IsShowWeekNumber);
            set => SetProperty(() => IsShowWeekNumber, value);
        }

        /// <summary>
        ///     Признак "отображать число понедельника"
        /// </summary>
        public bool IsShowMondayNumber {
            get => GetProperty(() => IsShowMondayNumber);
            set => SetProperty(() => IsShowMondayNumber, value);
        }

        /// <summary>
        ///     Признак "отображать активные дни недели"
        /// </summary>
        public bool IsShowActiveDays {
            get => GetProperty(() => IsShowActiveDays);
            set => SetProperty(() => IsShowActiveDays, value);
        }

        /// <summary>
        ///     Признак "отображать TRP"
        /// </summary>
        public bool IsShowTRP {
            get => GetProperty(() => IsShowTRP);
            set => SetProperty(() => IsShowTRP, value);
        }

        /// <summary>
        ///     Признак "отображать GivenTRP"
        /// </summary>
        public bool IsShowGivenTRP {
            get => GetProperty(() => IsShowGivenTRP);
            set => SetProperty(() => IsShowGivenTRP, value);
        }

        /// <summary>
        ///     Признак "отображать GRP"
        /// </summary>
        public bool IsShowGRP {
            get => GetProperty(() => IsShowGRP);
            set => SetProperty(() => IsShowGRP, value);
        }

        /// <summary>
        ///     Признак "отображать GivenGRP"
        /// </summary>
        public bool IsShowGivenGRP {
            get => GetProperty(() => IsShowGivenGRP);
            set => SetProperty(() => IsShowGivenGRP, value);
        }

        /// <summary>
        ///     Признак "отображать Minutes"
        /// </summary>
        public bool IsShowMinutes {
            get => GetProperty(() => IsShowMinutes);
            set => SetProperty(() => IsShowMinutes, value);
        }

        /// <summary>
        ///     Признак "отображать Outputs"
        /// </summary>
        public bool IsShowOutputs {
            get => GetProperty(() => IsShowOutputs);
            set => SetProperty(() => IsShowOutputs, value);
        }

        /// <summary>
        ///     Признак "отображать Affinity"
        /// </summary>
        public bool IsShowAffinity {
            get => GetProperty(() => IsShowAffinity);
            set => SetProperty(() => IsShowAffinity, value);
        }

        /// <summary>
        ///     Признак "отображать средний хр-ж"
        /// </summary>
        public bool IsShowAverageTiming {
            get => GetProperty(() => IsShowAverageTiming);
            set => SetProperty(() => IsShowAverageTiming, value);
        }

        /// <summary>
        ///     Признак "отображать Бюджет без НДС"
        /// </summary>
        public bool IsShowBudget {
            get => GetProperty(() => IsShowBudget);
            set => SetProperty(() => IsShowBudget, value);
        }

        #endregion

        #region Команды

        /// <summary>
        ///     Команда отображения флоучарта
        /// </summary>
        public DelegateCommand OnShowCommand => new(OnShow);

        /// <summary>
        ///     Команда сохранения флоучарта
        /// </summary>
        public DelegateCommand OnSaveCommand => new(OnSave);

        #endregion

        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        public FlowChartSettingsDWM() {
            // Создаём тип флоучарта "Реализация"
            var flowChart = new FlowChartType("Реализация");
            // Добавляем в список
            Types.Add(flowChart);

            // Создаём тип флоучарта "Планирование"
            flowChart = new FlowChartType("Планирование");
            // Добавляем в список
            Types.Add(flowChart);

            // Подписываем на события изменения значений свойств модели представления
            this.PropertyChanged += OnPropertyChanged;

            // Выбираем тип "Планирование" типом по умолчанию
            SelectedType = flowChart;
        }

        #endregion

        #region Методы обработки событий

        /// <summary>
        ///     Метод обработки события изменения параметров
        /// </summary>
        /// <param name="parameter">Новые параметры</param>
        protected override void OnParameterChanged(object parameter) {
            // Если это не проект - завершаем выполнение метода
            if (parameter is not ProjectVM project) return;

            // Запоминаем проект
            _project = project;
        }

        /// <summary>
        ///     Метод обработки события изменения значения свойства модели представления
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="arguments"></param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs arguments) {
            // Если изменилось значение не выбранного типа флоучарта - завершаем выполнение метода
            if (!arguments.PropertyName.Equals(nameof(SelectedType))) return;

            // Переключаем флаги
            var isPlaning = SelectedType.Name.Equals("Планирование");

            IsShowWeekNumber = false;
            IsShowMondayNumber = true;
            IsShowActiveDays = true;
            IsShowTRP = isPlaning;
            IsShowGivenTRP = false;
            IsShowGRP = false;
            IsShowGivenGRP = true;
            IsShowMinutes = true;
            IsShowOutputs = isPlaning;
            IsShowAffinity = false;
            IsShowAverageTiming = isPlaning;
            IsShowBudget = false;
        }

        /// <summary>
        ///     Метод обработки события нажатия кнопки "Отобразить"
        /// </summary>
        private void OnShow() {
            if (_project == null) throw new NullReferenceException();
            new FlowChartFormatter(
                _project,
                SelectedType,
                IsShowWeekNumber,
                IsShowMondayNumber,
                IsShowActiveDays,
                IsShowTRP,
                IsShowGivenTRP,
                IsShowGRP,
                IsShowGivenGRP,
                IsShowMinutes,
                IsShowOutputs,
                IsShowAffinity,
                IsShowAverageTiming,
                IsShowBudget
            );
            CurrentWindowSRV?.Close();
        }

        /// <summary>
        ///     Метод обработки события нажатия кнопки "Сохранить"
        /// </summary>
        private void OnSave() {
            if (_project == null) throw new NullReferenceException();
            var workbook = new FlowChartFormatter(
                _project,
                SelectedType,
                IsShowWeekNumber,
                IsShowMondayNumber,
                IsShowActiveDays,
                IsShowTRP,
                IsShowGivenTRP,
                IsShowGRP,
                IsShowGivenGRP,
                IsShowMinutes,
                IsShowOutputs,
                IsShowAffinity,
                IsShowAverageTiming,
                IsShowBudget
            ).CreateDocument();

            // Формируем диалоговое окно
            var dialog = new SaveFileDialog {
                Title = "Выберите место сохранения флоучарта",
                Filter = "Флоучарт (*.xlsx)|*.xlsx|Все файлы (*.*)|*.*",
                DefaultExt = "xlsx",
                FileName =
                    $"Флоучарт_{_project.Brand.Advertiser.Name}_{_project.Brand.Name}_{DateTime.Now:g}"
                       .Replace(':', '-')
            };

            // Если диалоговое окно не подтверждено - завершаем выполнение метода
            if (dialog.ShowDialog() == false) {
                return;
            }

            workbook.SaveDocument(dialog.FileName, DocumentFormat.Xlsx);
            CurrentWindowSRV?.Close();
        }

        #endregion
    }
}
﻿using DevExpress.Mvvm;
using DevExpress.Spreadsheet;
using Microsoft.Win32;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.ViewModels.Models;
using RMPT.Models;
using RMPT.Utils;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace RMPT.UI.DialogWindows.ProjectManagement.CategoryReports.MediaPlan {
    public class DwmMediaPlanChartSettings : AbstractVM {
        #region Свойства

        /// <summary>
        ///     Проект
        /// </summary>
        private ProjectVM? _project;

        #region Свойства привязки

        #region Свойства привязки заголовков канала

        /// <summary>
        ///     Признак отображения "Название региона"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowRegion {
            get => GetProperty(() => IsShowRegion);
            set => SetProperty(() => IsShowRegion, value);
        }

        /// <summary>
        ///     Признак отображения "Название канала"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowChannel {
            get => GetProperty(() => IsShowChannel);
            set => SetProperty(() => IsShowChannel, value);
        }

        /// <summary>
        ///     Признак отображения "Тип продажи"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowChannelSaleType {
            get => GetProperty(() => IsShowChannelSaleType);
            set => SetProperty(() => IsShowChannelSaleType, value);
        }

        #endregion

        #region Свойства привязки заголовков распределения канала

        /// <summary>
        ///     Признак отображения "% Канала"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowChannelPortionDistribution {
            get => GetProperty(() => IsShowChannelPortionDistribution);
            set => SetProperty(() => IsShowChannelPortionDistribution, value);
        }

        /// <summary>
        ///     Признак отображения "% Float, %Float P., % Fix, % Fix P., % S.Fix, % S.Fix P." канала
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowChannelQualityDistribution {
            get => GetProperty(() => IsShowChannelQualityDistribution);
            set => SetProperty(() => IsShowChannelQualityDistribution, value);
        }

        #endregion

        #region Свойства привязки заголовков распределения месяца

        /// <summary>
        ///     Признак отображения "Affinity"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowMonthAffinity {
            get => GetProperty(() => IsShowMonthAffinity);
            set => SetProperty(() => IsShowMonthAffinity, value);
        }

        /// <summary>
        ///     Признак отображения "% Float, %Float P., % Fix, % Fix P., % S.Fix, % S.Fix P." месяца
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowMonthQualityDistribution {
            get => GetProperty(() => IsShowMonthQualityDistribution);
            set => SetProperty(() => IsShowMonthQualityDistribution, value);
        }

        #endregion

        #region Свойства привязки заголовков инвентаря месяца

        /// <summary>
        ///     Признак отображения "TRP"
        /// </summary>
        [DefaultValue(true)]
        public bool IsMonthTrp {
            get => GetProperty(() => IsMonthTrp);
            set => SetProperty(() => IsMonthTrp, value);
        }

        /// <summary>
        ///     Признак отображения "GivenTrp"
        /// </summary>
        [DefaultValue(true)]
        public bool IsMonthGivenTrp {
            get => GetProperty(() => IsMonthGivenTrp);
            set => SetProperty(() => IsMonthGivenTrp, value);
        }

        /// <summary>
        ///     Признак отображения "Grp"
        /// </summary>
        [DefaultValue(true)]
        public bool IsMonthGrp {
            get => GetProperty(() => IsMonthGrp);
            set => SetProperty(() => IsMonthGrp, value);
        }

        /// <summary>
        ///     Признак отображения "GivenGrp"
        /// </summary>
        [DefaultValue(true)]
        public bool IsMonthGivenGrp {
            get => GetProperty(() => IsMonthGivenGrp);
            set => SetProperty(() => IsMonthGivenGrp, value);
        }

        /// <summary>
        ///     Признак отображения "Minutes"
        /// </summary>
        [DefaultValue(true)]
        public bool IsMonthMinutes {
            get => GetProperty(() => IsMonthMinutes);
            set => SetProperty(() => IsMonthMinutes, value);
        }

        #endregion

        #region Свойсва привязки заголовков выходов месяца

        /// <summary>
        ///     Признак отображения "По хронометражам"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowOutputsByTimings {
            get => GetProperty(() => IsShowOutputsByTimings);
            set => SetProperty(() => IsShowOutputsByTimings, value);
        }

        /// <summary>
        ///     Признак отображения "По качеству"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowOutputsByQuality {
            get => GetProperty(() => IsShowOutputsByQuality);
            set => SetProperty(() => IsShowOutputsByQuality, value);
        }

        /// <summary>
        ///     Признак отображения "Всего"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowOutputsTotal {
            get => GetProperty(() => IsShowOutputsTotal);
            set => SetProperty(() => IsShowOutputsTotal, value);
        }

        /// <summary>
        ///     Признак отображения "Дни (активные дни)"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowActiveDays {
            get => GetProperty(() => IsShowActiveDays);
            set => SetProperty(() => IsShowActiveDays, value);
        }

        /// <summary>
        ///     Признак отображения "Выходы в день"
        /// </summary>
        [DefaultValue(true)]
        public bool IsShowOutputsPerDay {
            get => GetProperty(() => IsShowOutputsPerDay);
            set => SetProperty(() => IsShowOutputsPerDay, value);
        }

        #endregion

        #region Свойства привязки заголовков бюджетов месяцев

        /// <summary>
        ///     Признак отображения "Бюджет агентский"
        /// </summary>
        [DefaultValue(false)]
        public bool IsShowBudgetAgency {
            get => GetProperty(() => IsShowBudgetAgency);
            set => SetProperty(() => IsShowBudgetAgency, value);
        }

        #endregion

        #endregion

        #endregion

        #region Команды

        /// <summary>
        ///     Команда нажатия на кнопку "Отобразить"
        /// </summary>
        public AsyncCommand OnShowCommand => new(OnShow);

        /// <summary>
        ///     Команда нажатия на кнопку "Сохранить"
        /// </summary>
        public AsyncCommand OnSaveCommand => new(OnSave);

        #endregion

        #region Методы

        #region Методы обработки событий

        #region Метод обработки события изменения параметров модели представления

        /// <summary>
        ///     Метод обработки события измененеия параметров модели представления
        /// </summary>
        /// <param name="parameter">Новые параметры</param>
        protected override void OnParameterChanged(object parameter) {
            // Если новые параметры не являются проектом - завершаем выполнение метода
            if (parameter is not ProjectVM project) return;
            _project = project;
        }

        #endregion

        #region Метод обработки команды нажатия на кнопку "Отобразить"

        /// <summary>
        ///     Метод обработки команды нажатия на кнопку "Отобразить"
        /// </summary>
        private async Task OnShow() {
            if (_project == null) {
                throw new NullReferenceException($"Нет данных о проекте");
            }

            // Создаём документ
            var document = await new MediaPlanFormatter(
                _project, new MediaPlanType(""), IsShowRegion, IsShowChannel, IsShowChannelSaleType,
                IsShowChannelPortionDistribution, IsShowChannelQualityDistribution, IsShowMonthAffinity,
                IsShowMonthQualityDistribution, IsMonthTrp, IsMonthGivenTrp, IsMonthGrp, IsMonthGivenGrp,
                IsMonthMinutes, IsShowOutputsByTimings, IsShowOutputsByQuality, IsShowOutputsTotal, IsShowActiveDays,
                IsShowOutputsPerDay, IsShowBudgetAgency
            ).CreateDocument();
            CurrentWindowSRV?.Close();
        }

        #endregion

        #region Метод обработки команды нажатия на кнопку "Сохранить"

        /// <summary>
        ///     Метод обработки команды нажатия на кнпоку "Сохранить"
        /// </summary>
        /// <returns></returns>
        private async Task OnSave() {
            if (_project == null) {
                throw new NullReferenceException($"Нет данных о проекте");
            }

            // Создаём документ
            var document = await new MediaPlanFormatter(
                _project, new MediaPlanType(""), IsShowRegion, IsShowChannel, IsShowChannelSaleType,
                IsShowChannelPortionDistribution, IsShowChannelQualityDistribution, IsShowMonthAffinity,
                IsShowMonthQualityDistribution, IsMonthTrp, IsMonthGivenTrp, IsMonthGrp, IsMonthGivenGrp,
                IsMonthMinutes, IsShowOutputsByTimings, IsShowOutputsByQuality, IsShowOutputsTotal, IsShowActiveDays,
                IsShowOutputsPerDay, IsShowBudgetAgency
            ).CreateDocument();

            // Формируем диалоговое окно
            var dialog = new SaveFileDialog {
                Title = "Выберите место сохранения медиаплана",
                Filter = "Медиаплан (*.xlsx)|*.xlsx|Все файлы (*.*)|*.*",
                DefaultExt = "xlsx",
                FileName =
                    $"Медиаплан_{_project.Brand.Advertiser.Name}_{_project.Brand.Name}_{DateTime.Now:g}"
                       .Replace(':', '-')
            };

            // Если диалоговое окно не подтверждено - завершаем выполнение метода
            if (dialog.ShowDialog() == false) {
                return;
            }

            document.SaveDocument(dialog.FileName, DocumentFormat.Xlsx);
            CurrentWindowSRV?.Close();
        }

        #endregion

        #endregion

        #endregion
    }
}
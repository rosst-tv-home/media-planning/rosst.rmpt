﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.DATA.DataBase.Repositories;
using RMPT.Services;
using RMPT.VALIDATE;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Channels {
    /// <summary>
    ///     Модель диалогового окна справочника каналов
    /// </summary>
    public class ChannelsDWM : AbstractVM {
        #region Свойства

        /// <summary>
        ///     Выбранный канал
        /// </summary>
        public DCChannel? Channel {
            get => GetProperty(() => Channel);
            set => SetProperty(() => Channel, value, () => Messenger.Default.Send(value));
        }

        #region Коллекции

        /// <summary>
        ///     Регионы
        /// </summary>
        public ObservableCollection<DCRegion> Regions { get; } = new();

        /// <summary>
        ///     Каналы
        /// </summary>
        public ObservableCollection<DCChannel> Channels { get; } = new();

        #endregion

        #endregion

        #region Делегаты

        #region Проверка полей

        /// <summary>
        ///     Команда проверки валидности выбранного региона канала
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnRegionValidateCommand => new(OnRegionValidate);

        /// <summary>
        ///     Команда проверки валидности введённого в ячейку таблицы наименования канала
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnNameValidateCommand => new(OnNameValidate);

        /// <summary>
        ///     Команда проверки валидности введённого в ячейку таблицы наименования канала на английском
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnEnglishNameValidateCommand => new(OnEnglishNameValidate);

        #endregion

        #endregion

        #region Сервисы

        /// <summary>
        ///     Сервис управления таблицей проекта
        /// </summary>
        private IExtendedGridService GridService => GetService<IExtendedGridService>();

        #endregion

        #region Методы

        #region Диалоговое окно

        /// <summary>
        ///     Метод вызывается после загрузки диалогового окна
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Переключаем видимость индикатора загрузки
            GridService.ToggleLoadingPanel();

            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine("Начало: заполнение справочника каналов");

            // Запускаем замер времени выполнение метода
            var measure = Stopwatch.StartNew();
            #endif

            // Задача запроса регионов
            var regionsTask = RepositoryRegions.FindAllOnlyWithChannels();
            // Задача запроса каналов
            var channelsTask = RepositoryChannels.FindAll();

            // Ожидаем выполнение обеих задач
            await Task.WhenAll(regionsTask, channelsTask);

            // Заполняем коллекцию регионов
            regionsTask.Result.ForEach(Regions.Add);
            // Заполняем коллекцию каналов
            channelsTask.Result.ForEach(x => x.Region = Regions.FirstOrDefault(region => region.ID == x.RegionID));
            channelsTask.Result.OrderBy(x => x.Region!.Name).ThenBy(x => x.Name).ForEach(Channels.Add);

            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine($"Завершение: заполнение справочника каналов: {measure.ElapsedMilliseconds} ms");

            // Завершаем замер времени выполнения метода
            measure.Stop();
            #endif

            // Переключаем видимость индикатора загрузки
            GridService.ToggleLoadingPanel();
        }

        /// <summary>
        ///     Методы вызывается перед закрытием диалогового окна
        /// </summary>
        /// <param name="arguments">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        protected override async Task OnUnloadedAsync(RoutedEventArgs? arguments) => await Task.WhenAll(
            // Удаление старых каналов
            RepositoryChannels.DeleteAsync(Channels.ToList()),
            // Обнавление старых каналов
            RepositoryChannels.UpdateAsync(Channels.ToList()),
            // Добавление новых каналов
            RepositoryChannels.InsertAsync(Channels.Where(x => x.ID == 0).ToList())
        );

        #endregion

        #region Проверка полей

        /// <summary>
        ///     Метод базовой проверки валидности вводимого значения в ячейку таблицы каналов
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Признак валидности введённого значения в ячейку таблицы каналов</returns>
        private bool OnBaseValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Возвращаем результат проверки
            return
                // Если строка таблицы не соответсвует заполняемой модели
                GridValidator.IsValidRowType<DCChannel>(argument) &&
                // Если строковое значение ячейки таблицы невалидно
                GridValidator.IsValidStringifyCellValue(argument);
        }

        /// <summary>
        ///     Метод проверки валидности выбранного региона канала в ячейке таблицы каналов
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnRegionValidate(GridCellValidationEventArgs argument) {
            // Ячейка по умолчанию невалидная
            argument.IsValid = false;

            // Если строка таблицы не соответвует заполняемой модели
            if (!GridValidator.IsValidRowType<DCChannel>(argument)) {
                // Завершаем выполнение метода
                return;
            }

            // Если значение ячейки не соответсвует выбираемой модели
            if (!GridValidator.IsValidCellType<DCRegion>(argument)) {
                // Завершаем выполнение метода
                return;
            }

            // Признак наличия дубликата
            var hasDuplicates = Channels.Where(channel =>
                // Наименование канала не пустое
                !string.IsNullOrWhiteSpace(channel.Name.Trim()) &&
                // Канал не является сравниваемым объектом
                !ReferenceEquals(channel, (DCChannel)argument.Row) &&
                // Выбранные регионы совпадают
                ReferenceEquals(channel.Region, (DCRegion)argument.Value)
            ).Any(channel => channel.Name.Trim().Equals(
                ((DCChannel)argument.Row).Name.Trim(),
                StringComparison.InvariantCultureIgnoreCase
            ));

            // Если есть дубликаты
            if (hasDuplicates) {
                // Оповещаем пользователя об ошибке
                argument.ErrorContent = "Регион с таким каналом уже существует";
                // Завершаем выполнение метода
                return;
            }

            // Отмечаем ячейку как валидную
            argument.IsValid = true;
        }

        /// <summary>
        ///     Метод проверки валидности введённого наименования канала в ячейку таблицы каналов
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnNameValidate(GridCellValidationEventArgs argument) {
            // Если базовая проверка не прошла
            if (!OnBaseValidate(argument)) {
                // Завершаем выполнение метода
                return;
            }

            // Признак наличия дубликата
            var hasDuplicates = Channels.Where(channel =>
                // Наименование канала не пустое
                !string.IsNullOrWhiteSpace(channel.Name.Trim()) &&
                // Канал не является сравниваемым объектом
                !ReferenceEquals(channel, (DCChannel)argument.Row) &&
                // Есть выбранный регион
                channel.Region != null &&
                // Выбранные регионы совпадают
                ReferenceEquals(channel.Region, ((DCChannel)argument.Row).Region)
            ).Any(channel => channel.Name.Trim().Equals(
                ((string)argument.Value).Trim(),
                StringComparison.InvariantCultureIgnoreCase
            ));

            // Если есть дубликаты
            if (hasDuplicates) {
                // Оповещаем пользователя об ошибке
                argument.ErrorContent = "Канал с таким наименованием в этом регионе уже есть";
                // Завершаем выполнение метода
                return;
            }

            // Отмечаем ячейку как валидную
            argument.IsValid = true;
        }

        /// <summary>
        ///     Метод проверки валидности введённого наименования канала на анлийском в ячейку таблицы каналов
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnEnglishNameValidate(GridCellValidationEventArgs argument) {
            // Если строка таблицы не соответвует заполняемой модели
            if (!GridValidator.IsValidRowType<DCChannel>(argument)) {
                // Завершаем выполнение метода
                return;
            }

            // Отмечаем ячейку как валидную
            argument.IsValid = true;
        }

        #endregion

        #endregion
    }
}
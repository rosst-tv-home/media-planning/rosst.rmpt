﻿using DevExpress.Mvvm;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.VALIDATE;
using System;
using System.Linq;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Channels.Ratings {
    /// <summary>
    ///     Модель представления вкладки рейтингов в диалоговом окне справочника каналов
    /// </summary>
    public class ChannelsDWRatingsTabM : AbstractVM {
        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        public ChannelsDWRatingsTabM() {
            // Подписываемся на сообщение выбранного канала
            Messenger.Default.Register<DCChannel?>(this, channel => Channel = channel);
        }

        #endregion

        #region Свойства

        /// <summary>
        ///     Выбранный канал
        /// </summary>
        public DCChannel? Channel {
            get => GetProperty(() => Channel);
            set => SetProperty(() => Channel, value);
        }

        #endregion

        #region Делегаты

        /// <summary>
        ///     Команда вызова метода обработки события создания новой строки таблицы
        /// </summary>
        public DelegateCommand<InitNewRowEventArgs> OnNewRowCreatedCommand => new(OnNewRowCreated);

        /// <summary>
        ///     Команда вызова метода обработки события проверки поля периода
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnDateValidateCommand => new(OnDateValidate);

        #endregion

        #region Методы

        /// <summary>
        ///     Метод обработки события создания новой строки таблицы
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnNewRowCreated(InitNewRowEventArgs argument) => Channel?.Ratings
           .ForEach(rating => rating.Channel = Channel);

        /// <summary>
        ///     Метод обработки события проверки поля периода
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnDateValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если строка таблицы не соответсвует типу заполняемой модели - завершаем выполнение метода
            if (!GridValidator.IsValidRowType<DCRating>(argument)) {
                return;
            }

            // Если тип значения ячейки не соответсвует типу свойства заполняемой модели - завершаем выполнение метода
            if (!GridValidator.IsValidCellType<DateTime>(argument)) {
                return;
            }

            // Признак наличия дубликатов
            var hasDuplicates = Channel!.Ratings
                // Объект из коллекции не является сравниваемым объектом
               .Where(rating => !ReferenceEquals(rating, (DCRating)argument.Row))
               .Any(rating =>
                    // Год периода совпадает
                    rating.Date.Year.Equals(((DateTime)argument.Value).Year) &&
                    // Месяц периода совпадает
                    rating.Date.Month.Equals(((DateTime)argument.Value).Month)
                );

            // Если есть дубликаты
            if (hasDuplicates) {
                // Оповещаем пользователя об ошибке
                argument.ErrorContent = "Уже есть запись с рейтингами в указанном периоде";
                // Завершаем выполнение метода
                return;
            }

            // Отмечаем ячейку как валидную
            argument.IsValid = true;
        }

        #endregion
    }
}
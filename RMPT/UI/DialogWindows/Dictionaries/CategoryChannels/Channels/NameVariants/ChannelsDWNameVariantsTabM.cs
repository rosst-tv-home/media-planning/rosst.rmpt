﻿using DevExpress.Mvvm;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.VALIDATE;
using System;
using System.Linq;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Channels.NameVariants {
    /// <summary>
    /// Модель представления вкладки вариантов наименований в диалоговом окне справочника каналов
    /// </summary>
    public class ChannelsDWNameVariantsTabM : AbstractVM {
        #region Свойства

        /// <summary>
        /// Выбранный канал
        /// </summary>
        public DCChannel? Channel {
            get => GetProperty(() => Channel);
            set => SetProperty(() => Channel, value);
        }

        #endregion

        #region Делегаты

        /// <summary>
        /// Команда вызова метода обработки события создания новой строки варианта наименования канала
        /// </summary>
        public DelegateCommand<InitNewRowEventArgs> OnNewNameVariantCreatedCommand =>
            new DelegateCommand<InitNewRowEventArgs>(OnNewNameVariantCreated);

        /// <summary>
        /// Команда вызова метода проверки валидности введённого варианта наименования канала
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnNameValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnNameValidate);

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public ChannelsDWNameVariantsTabM() {
            // Подписываемся на сообщение выбранного канала
            Messenger.Default.Register<DCChannel?>(this, channel => Channel = channel);
        }

        #endregion

        #region Методы

        /// <summary>
        /// Метод обработки события создания новой строки варианта наименования канала
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnNewNameVariantCreated(InitNewRowEventArgs argument) => Channel?.NameVariants
           .ForEach(nameVariant => nameVariant.Channel = Channel);

        #region Проверка полей

        /// <summary>
        /// Метод проверки валидности введённого варианта нименования канала
        /// </summary>
        /// <param name="argument"></param>
        private void OnNameValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если редактируемая строка не является моделью типа - завершаем выполнение метода
            if (!GridValidator.IsValidRowType<DCChannelVariant>(argument)) return;

            // Если значение ячейки невалидно - завершаем выполнение метода
            if (!GridValidator.IsValidStringifyCellValue(argument)) return;

            // Признак наличия дубликатов
            var hasDuplicates = Channel!.NameVariants
               .Where(nameVariant =>
                    // Вариант наименования не является сравниваемым объектом
                    !ReferenceEquals(nameVariant, (DCChannelVariant)argument.Row) &&
                    // Вариант наименования не пустой
                    !string.IsNullOrWhiteSpace(nameVariant.Name.Trim())
                )
               .Any(nameVariant => nameVariant.Name.Trim().Equals(
                    (string)argument.Value,
                    StringComparison.InvariantCultureIgnoreCase
                ));

            // Если есть дубликаты
            if (hasDuplicates) {
                // Оповещаем пользователя об ошибке
                argument.ErrorContent = "Данный вариант наименования уже существует в канале";
                // Завершаем выполнение метода
                return;
            }

            // Отмечаем ячейку как валидную
            argument.IsValid = true;
        }

        #endregion

        #endregion
    }
}
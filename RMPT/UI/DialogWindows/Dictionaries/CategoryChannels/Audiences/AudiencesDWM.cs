﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Repositories;
using RMPT.VALIDATE;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Audiences {
    /// <summary>
    ///     Модель диалогового окна справочника аудиторий
    /// </summary>
    public class AudiencesDWM : AbstractVM {
        #region Свойства

        /// <summary>
        ///     Выбранная аудитория
        /// </summary>
        public DCAudience? Audience {
            get => GetProperty(() => Audience);
            set => SetProperty(() => Audience, value);
        }

        /// <summary>
        ///     Коллекция аудиторий
        /// </summary>
        public ObservableCollection<DCAudience> Audiences { get; set; } = new();

        /// <summary>
        ///     Коллекция удаленных аудиторий
        /// </summary>
        private Collection<DCAudience> RemoveAudiences { get; } = new();

        #endregion

        #region Делегаты

        #region Проверка полей

        /// <summary>
        ///     Команда проверки введённого значения в ячейку наименование
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnNameValidateCommand => new(OnNameValidate);

        #endregion

        #endregion

        #region Методы

        #region Обработка событий коллекции

        /// <summary>
        ///     Метод вызывается после изменения коллекции аудиторий
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private void OnSalePointsCollectionChanged(object sender, NotifyCollectionChangedEventArgs argument) {
            // Если событие не является удалением - завершаем выполнение метода
            if (argument.Action != NotifyCollectionChangedAction.Remove) {
                return;
            }

            // Итерируемся по удаленным аудиториям
            foreach (var item in argument.OldItems) {
                // Добавляем удаленную аудиторию в коллекцию
                if (item is DCAudience audience) {
                    RemoveAudiences.Add(audience);
                }
            }
        }

        #endregion

        #region Диалоговое окно

        /// <summary>
        ///     Метод вызывается после загрузки диалогового окна
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Заполняем коллекцию аудиторий
            (await RepositoryAudiences.FindAll()).OrderBy(x => x.Name).ForEach(Audiences.Add);
            // Подписываемся на событие изменения коллекции аудиторий
            Audiences.CollectionChanged += OnSalePointsCollectionChanged;
        }

        /// <summary>
        ///     Метод вызывается перед закрытием диалогового окна
        /// </summary>
        /// <param name="arguments">Аргумент события</param>
        protected override async Task OnClosingAsync(CancelEventArgs arguments) {
            // Определяем невалидные аудитории
            var invalidAudience = Audiences
               .Where(audience => string.IsNullOrWhiteSpace(audience.Name))
               .ToList();

            // Если невалидные базовой аудитории не были найдены - завершаем выполнение метода
            if (invalidAudience.Count != 0) {
                // Сообщение диалогового окна
                var message = invalidAudience.Aggregate(
                    "Данные следующих базовых аудитории неверны и не буду сохранены:",
                    (messageString, audience) =>
                        messageString + $"\nБазовая аудитория в строке №{Audiences.IndexOf(audience) + 1} таблицы"
                );

                // Результат диалогового окна
                var result = MessageBox.Show(
                    message,
                    "Продолжить?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);

                // Если пользователь не подтвердил продолжение
                if (result != MessageBoxResult.Yes) {
                    // Отменяем событие закрытия диалогового окна
                    arguments.Cancel = true;
                    // Завершаем выполнение метода
                    return;
                }
            }

            // Если пользователь отказался удалять связанные с базовой аудиторией каналы - завершаем выполнение метода
            if (!CheckSafeAudiencesRemoval()) {
                // Отменяем событие закрытия диалогового окна
                arguments.Cancel = true;
                return;
            }

            // Коллекция валидных базовых аудиторий
            var validAudience = Audiences.Where(audience => invalidAudience.All(x => !ReferenceEquals(audience, x)))
               .ToList();

            // Ожидаем выполнения всех задач для обновлений базы данных
            await Task.WhenAll(
                // Удаление старых аудиторий
                RepositoryAudiences.DeleteAsync(validAudience),
                // Обновление старых аудиторий
                RepositoryAudiences.UpdateAsync(validAudience),
                // Добавление новых аудиторий
                RepositoryAudiences.InsertAsync(validAudience)
            );

            // Отписываемся от события изменения коллекции базовой аудитории
            Audiences.CollectionChanged -= OnSalePointsCollectionChanged;
        }

        /// <summary>
        ///     Метод проверки безопасного удаления удалённых базовых аудитории
        /// </summary>
        /// <returns>
        ///     <see cref="bool" />
        /// </returns>
        private bool CheckSafeAudiencesRemoval() {
            // Коллекция которые не безопасно удалять
            var notSafeAudiencesRemoval = RemoveAudiences
               .Where(audience => audience.Channels.Count > 0)
               .ToList();
            // если коллекция пуста, завершаем метод
            if (notSafeAudiencesRemoval.Count == 0) {
                return true;
            }

            // Сообщение диалогового окна
            var message = notSafeAudiencesRemoval
               .Aggregate(
                    "Следующие базовые аудитории небезопасно удалять, т.к. они содержат связанные с ними каналы:",
                    (messageString, audience) =>
                        messageString +
                        $"\nАудитория {audience.Name} содержит в себе {audience.Channels.Count} шт. каналов"
                );
            // Результат диалогового окна
            var result = MessageBox.Show(message, "Продожить?", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            // Если результат не "Да" - возвращаем удалённые базовые аудитории
            if (result != MessageBoxResult.Yes) {
                RemoveAudiences.ForEach(Audiences.Add);
                return false;
            }

            return true;
        }

        #endregion

        #region Проверка полей

        /// <summary>
        ///     Метод базовой проверки поля ввода
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Сообщение об ошибке</returns>
        private bool OnValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если строковое значение ячейки таблицы невалидно
            return GridValidator.IsValidStringifyCellValue(argument)
             && GridValidator.IsValidRowType<DCAudience>(argument);

            // Если строка таблицы не соответсвует заполняемой модели
        }

        /// <summary>
        ///     Метод вызывается после ввода наименования аудитории
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnNameValidate(GridCellValidationEventArgs argument) {
            // Если сообщение является строкой
            if (!OnValidate(argument)) {
                return;
            }

            // Признак наличия пользователя с таким же названием
            var hasDuplicates = Audiences
               .Where(audience => !ReferenceEquals(audience, (DCAudience)argument.Row))
               .Any(audience => audience.Name.Equals(
                    (string)argument.Value,
                    StringComparison.InvariantCultureIgnoreCase)
                );

            // Если нашли то
            if (hasDuplicates) {
                // Выводим сообщение ошибки
                argument.ErrorContent = "Аудитория, с данным наименованием уже существует";
                // Завершаем выполнение метода
                return;
            }

            // Признак ячейки валидна
            argument.IsValid = true;
        }

        #endregion

        #endregion
    }
}
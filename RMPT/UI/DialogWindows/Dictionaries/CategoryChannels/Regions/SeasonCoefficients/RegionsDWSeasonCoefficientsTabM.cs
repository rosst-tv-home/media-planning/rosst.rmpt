﻿using DevExpress.Mvvm;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using System.Collections.Generic;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Regions.SeasonCoefficients {
    /// <summary>
    ///     Модель представления вкладки сезонных коэффициентов в диалоговом окне справочника регионов
    /// </summary>
    public class RegionsDWSeasonCoefficientsTabM : AbstractVM {
        /// <summary>
        ///     Конструктор
        /// </summary>
        public RegionsDWSeasonCoefficientsTabM() {
            // Подписываемся на сообщение выбранного региона
            Messenger.Default.Register<DCRegion?>(this, region => Region = region);
        }

        #region Свойства

        /// <summary>
        ///     Регион
        /// </summary>
        public DCRegion? Region {
            get => GetProperty(() => Region);
            set => SetProperty(() => Region, value, () => RaisePropertyChanged(() => SeasonalOdds));
        }

        public List<DCSeasonalOdd> SeasonalOdds => Region?.SeasonalOddsSet?.SeasonalOdds ?? new List<DCSeasonalOdd>();

        #endregion
    }
}
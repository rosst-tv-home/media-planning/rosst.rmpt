﻿using DevExpress.Mvvm;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.DATA.DataBase.Repositories;
using RMPT.Services;
using RMPT.VALIDATE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Regions {
    /// <summary>
    ///     Модель диалогового окна справочника регионов
    /// </summary>
    public class RegionsDialogWindowModel : AbstractVM {
        #region Сервисы

        /// <summary>
        ///     Сервис управления таблицей проекта
        /// </summary>
        private IExtendedGridService GridService => GetService<IExtendedGridService>();

        #endregion

        #region Свойства

        /// <summary>
        ///     Выбранный регион
        /// </summary>
        public DCRegion? Region {
            get => GetProperty(() => Region);
            set => SetProperty(() => Region, value, () => Messenger.Default.Send(value));
        }

        #region Коллекции

        /// <summary>
        ///     Регионы
        /// </summary>
        public ObservableCollection<DCRegion> Regions { get; } = new();

        /// <summary>
        ///     Удалённые регионы
        /// </summary>
        private List<DCRegion> RemovedRegions { get; } = new();

        #endregion

        #endregion

        #region Делегаты

        #region Проверка полей

        /// <summary>
        ///     Команда вызывается после подтверждения ввода наименования региона
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnNameValidateCommand => new(OnNameValidate);

        #endregion

        #endregion

        #region Методы

        #region Обработка событий коллекции

        /// <summary>
        ///     Метод вызывается после изменения коллекции регионов
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private void OnRegionsCollectionChanged(object sender, NotifyCollectionChangedEventArgs argument) {
            // Если действие не равно удалению объектов из коллекции - завершаем выполнение метода
            if (argument.Action != NotifyCollectionChangedAction.Remove) {
                return;
            }

            // Итерируемся по удалённым регионам
            foreach (var item in argument.OldItems) {
                // Добавляем удалённый регион в коллекцию
                if (item is DCRegion region && region.Channels.Count != 0) {
                    RemovedRegions.Add(region);
                }
            }
        }

        #endregion

        #region Диалоговое окно

        /// <summary>
        ///     Метод вызывается после загрузки диалогового окна
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Переключаем видимость индикатора загрузки
            GridService.ToggleLoadingPanel();

            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine("Начало: заполнение справочника регионов");

            // Запускаем замер времени выполнение метода
            var measure = Stopwatch.StartNew();
            #endif

            // Заполняем коллекцию регионов из базы данных
            (await RepositoryRegions.FindAllWithoutChannels()).ForEach(Regions.Add);

            #if DEBUG
            // Выводим сообщение в консоль
            Debug.WriteLine($"Завершение: заполнение справочника регионов: {measure.ElapsedMilliseconds} ms");

            // Завершаем замер времени выполнения метода
            measure.Stop();
            #endif

            // Переключаем видимость индикатора загрузки
            GridService.ToggleLoadingPanel();

            // Подписываемся на событие изменения коллекции регионов
            Regions.CollectionChanged += OnRegionsCollectionChanged;
        }

        /// <summary>
        ///     Метод вызывается перед закрытием диалогового окна справочника регоинов
        /// </summary>
        /// <param name="arguments">Аргумент события</param>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        protected override async Task OnClosingAsync(CancelEventArgs arguments) {
            // Если есть удалённые регионы с привязанными сущностями
            if (RemovedRegions.Count != 0) {
                // Сообщение диалогового окна с предупреждением
                var errorMessage = RemovedRegions.Aggregate(
                    "Вы пытаетесь удалить регионы, которые связанны с некоторыми каналами:",
                    (message, region) => message + $"\n{region.Name}"
                );

                // Диалогвое сообщение
                var resultRemove = MessageBox.Show(
                    errorMessage,
                    "Выполнить?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил удаление
                if (resultRemove != MessageBoxResult.Yes) {
                    // Возвращаем удалённые регионы
                    RemovedRegions.ForEach(Regions.Add);
                    // Очищаем коллекцию удалённых регионов
                    RemovedRegions.Clear();
                    // Отменяем событие закрытия диалогового окна
                    arguments.Cancel = true;
                    // Завершаем выполнение метода
                    return;
                }
            }

            // Коллекция невалидных регионов
            var invalidRegions = Regions.Where(region =>
                // Наименование пустое
                string.IsNullOrWhiteSpace(region.Name) ||
                // Варианты наименований пустые
                region.NameVariants
                   .Any(variant => string.IsNullOrWhiteSpace(variant.Name)) ||
                // Сезонные коэффициенты с нулевыми значениями
                (region.SeasonalOddsSet?.SeasonalOdds.Any(seasonCoefficient => seasonCoefficient.Value <= 0) ?? false) ||
                // Повторяющиеся сезонные коэффициенты
                (region.SeasonalOddsSet?.SeasonalOdds
                   .GroupBy(seasonCoefficient => seasonCoefficient.Date)
                   .Any(group => group.Count() > 1) ?? false) ||
                // В наценках за хронометраж неправильно указан хронометраж
                region.TimingMargins.Any(timingMargin => timingMargin.Timing % 5 != 0) ||
                // Наценка за хронометраж с нулевым значением
                region.TimingMargins.Any(timingMargin => timingMargin.Value <= 0) ||
                // Повторяющиеся наценки за хронометраж
                region.TimingMargins
                   .GroupBy(timingMargin => new {
                        timingMargin.Timing,
                        timingMargin.Date
                    })
                   .Any(group => group.Count() > 1)
            ).ToList();

            // Если есть невалидные регионы
            if (invalidRegions.Count != 0) {
                // Сообщение диалогового окна с предупреждением
                var errorMessage = invalidRegions.Aggregate(
                    "В таблице есть невалидные данные:",
                    (message, region) => message + $"\nСтрока №{Regions.IndexOf(region) + 1} таблицы"
                );

                // Диалогвое сообщение
                var resultRemove = MessageBox.Show(
                    errorMessage,
                    "Продолжить?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил продолжение
                if (resultRemove != MessageBoxResult.Yes) {
                    // Отменяем событие закрытия диалогового окна
                    arguments.Cancel = true;
                    // Завершаем выполнение метода
                    return;
                }
            }

            // Коллекция валидных регионов
            var validRegions = Regions.Where(region => !string.IsNullOrWhiteSpace(region.Name)).ToList();

            // Убираем дубликаты связанных сущностей
            validRegions.ForEach(region => {
                if (region.SeasonalOddsSet == null && region.SeasonalOddsSetID == 0) {
                    region.SeasonalOddsSetID = 83;
                }
                #region Сезонные коэффициенты

                // Определяем валидные сезонные коэффициенты, убирая дубликаты
                // var validSeasonCoefficients = region.SeasonCoefficientSet.SeasonCoefficients
                //     // Группируем по дате
                //    .GroupBy(seasonCoefficient => seasonCoefficient.Date)
                //     // Выбираем первый элемент из группы с не нулевым идентификатором или просто первый
                //    .Select(group => group.FirstOrDefault(x => x.Id != 0) ?? group.First())
                //    .ToList();
                // // Очищаем старые сезонные коэффициенты
                // region.SeasonCoefficients.Clear();
                // // Заполняем регион валидными сезонными коэффициентам
                // region.SeasonCoefficients.AddRange(validSeasonCoefficients);

                #endregion


                #region Наценки за хронометражи

                // Определяем валидные наценки за хронометражи, убирая дубликаты
                var validTimingMargins = region.TimingMargins
                    // Группируем по дате и хронометражу
                   .GroupBy(timingMargin => new {
                        timingMargin.Timing,
                        timingMargin.Date
                    })
                    // Берём первый элемент с не нулевым идентификатором или просто первый
                   .Select(group => group.FirstOrDefault(x => x.Id != 0) ?? group.First())
                   .ToList();
                // Очищаем старые наценки за хронометражи
                region.TimingMargins.Clear();
                // Заполняем регион валидными наценками за хронометражи
                region.TimingMargins.AddRange(validTimingMargins);

                #endregion
            });

            // Ожидаем выполнения всех задач
            await Task.WhenAll(
                // Удаляем старые регионы
                RepositoryRegions.DeleteAsync(validRegions),
                // Обновляем старые регионы
                RepositoryRegions.UpdateAsync(validRegions),
                // Добавляем новые регионы
                RepositoryRegions.InsertAsync(validRegions)
            );

            // Отписываемся от события изменения коллекции 
            Regions.CollectionChanged -= OnRegionsCollectionChanged;
        }

        #endregion

        #region Проверка полей

        /// <summary>
        ///     Метод базовой проверки валидности вводимого значения в ячейку таблицы регионов
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Признак валидности введённого значения в ячейку таблицы регионов</returns>
        private bool OnBaseValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Возвращаем результат проверки
            return
                // Если строка таблицы не соответсвует заполняемой модели
                GridValidator.IsValidRowType<DCRegion>(argument) &&
                // Если строковое значение ячейки таблицы невалидно
                GridValidator.IsValidStringifyCellValue(argument);
        }

        /// <summary>
        ///     Метод проверки валидности введённого наименования региона в ячейку таблицы регионов
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnNameValidate(GridCellValidationEventArgs argument) {
            // Если базовая проверка не прошла
            if (!OnBaseValidate(argument)) {
                // Завершаем выполнение метода
                return;
            }

            // Признак наличия дубликатов
            var hasDuplicates = Regions.Where(region =>
                !string.IsNullOrWhiteSpace(region.Name) &&
                !ReferenceEquals(region, (DCRegion)argument.Row)
            ).Any(region => region.Name!.Equals(
                (string)argument.Value,
                StringComparison.InvariantCultureIgnoreCase
            ));

            // Если есть дубликаты
            if (hasDuplicates) {
                // Оповещаем пользователя об ошибке
                argument.ErrorContent = "Регион с таким наименованием уже существует";
                // Завершаем выполнение метода
                return;
            }

            // Отмечаем ячейку как валидную
            argument.IsValid = true;
        }

        #endregion

        #endregion
    }
}
﻿using DevExpress.Mvvm;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.VALIDATE;
using System.Linq;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Regions.TimingMargins {
    /// <summary>
    ///     Модель представления вкладки наценок на хронометражи в диалоговом окне справочника регионов
    /// </summary>
    public class RegionsDWTimingMarginsTabM : AbstractVM {
        /// <summary>
        ///     Конструктор
        /// </summary>
        public RegionsDWTimingMarginsTabM() {
            // Подписываемся на сообщение выбранного региона
            Messenger.Default.Register<DCRegion?>(this, region => Region = region);
        }

        #region Свойства

        /// <summary>
        ///     Регион
        /// </summary>
        public DCRegion? Region {
            get => GetProperty(() => Region);
            set => SetProperty(() => Region, value);
        }

        #endregion

        #region Делегаты

        /// <summary>
        ///     Команда вызывается после подтверждения ввода хронометража
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnTimingValidateCommand => new(OnTimingValidate);

        /// <summary>
        ///     Команда вызывается при проверке строки таблицы
        /// </summary>
        public DelegateCommand<GridRowValidationEventArgs> OnRowValidateCommand => new(OnRowValidate);

        #endregion

        #region Методы

        #region Проверка полей

        /// <summary>
        ///     Метод вызывается после подтверждения ввода хронометража
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnTimingValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если значение ячейки не является натуральным числом
            if (!GridValidator.IsValidIntCellValue(argument)) {
                return;
            }

            // Если строка таблицы не соответсвует заполняемой модели
            if (!GridValidator.IsValidRowType<DCTimingMargin>(argument)) {
                return;
            }

            // Если значение не чётное 5
            if ((int)argument.Value % 5 != 0) {
                argument.ErrorContent = "Значение хронометража должно быть чётным 5";
                return;
            }

            argument.IsValid = true;
        }

        /// <summary>
        ///     Метод вызывается при проверке строки таблицы
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnRowValidate(GridRowValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если строка таблицы не соответсвует заполняемой модели
            if (!GridValidator.IsValidRowType<DCTimingMargin>(argument)) {
                return;
            }

            // Признак наличия дубликатов
            var hasDuplicates = Region?.TimingMargins
               .Where(timingMargin =>
                    !ReferenceEquals(timingMargin, (DCTimingMargin)argument.Row)
                )
               .Any(timingMargin =>
                    timingMargin.Timing    == ((DCTimingMargin)argument.Row).Timing &&
                    timingMargin.Value     == ((DCTimingMargin)argument.Row).Value  &&
                    timingMargin.Date.Year == ((DCTimingMargin)argument.Row).Date.Year
                ) ?? false;

            // Если нет дубликатов - отмечаем стоку как валидную и завершаем выполнение метода
            if (!hasDuplicates) {
                argument.IsValid = true;
                return;
            }

            argument.ErrorContent = "Такая наценка на хронометраж уже есть";
        }

        #endregion

        #endregion
    }
}
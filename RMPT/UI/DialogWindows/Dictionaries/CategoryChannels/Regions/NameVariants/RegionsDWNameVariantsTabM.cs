﻿using DevExpress.Mvvm;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Region;
using RMPT.VALIDATE;
using System;
using System.Linq;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryChannels.Regions.NameVariants {
    /// <summary>
    ///     Модель представления вкладки вариантов наименований в диалоговом окне справочника регионов
    /// </summary>
    public class RegionsDWNameVariantsM : AbstractVM {
        /// <summary>
        ///     Конструктор
        /// </summary>
        public RegionsDWNameVariantsM() {
            // Подписываемся на сообщение выбранного региона
            Messenger.Default.Register<DCRegion?>(this, region => Region = region);
        }

        #region Свойства

        /// <summary>
        ///     Регион
        /// </summary>
        public DCRegion? Region {
            get => GetProperty(() => Region);
            set => SetProperty(() => Region, value);
        }

        #endregion

        #region Делегаты

        /// <summary>
        ///     Команда вызывается после подтверждения ввода варианта наименования региона
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnNameVariantValidateCommand => new(OnNameVariantValidate);

        #endregion

        #region Методы

        #region Проверка полей

        /// <summary>
        ///     Метод вызывается после ввода варианта наименования региона
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnNameVariantValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если строковое значение ячейки таблицы невалидно
            if (!GridValidator.IsValidStringifyCellValue(argument)) {
                return;
            }

            // Если строка таблицы не соответсвует заполняемой модели
            if (!GridValidator.IsValidRowType<DCRegionVariant>(argument)) {
                return;
            }

            // Признак наличия дубликатов вариантов наименований региона
            var hasDuplicates = Region?.NameVariants
               .Where(variant =>
                    !string.IsNullOrWhiteSpace(variant.Name) &&
                    !ReferenceEquals(variant, (DCRegionVariant)argument.Row)
                )
               .Any(variant => variant.Name.Equals(
                    (string)argument.Value,
                    StringComparison.InvariantCultureIgnoreCase
                )) ?? true;

            // Если нет дубликатов - отмечаем ячейку как валидную и завершаем выполнение метода
            if (!hasDuplicates) {
                argument.IsValid = true;
                return;
            }

            argument.ErrorContent = "Такой вариант наименования уже есть в данном регионе";
        }

        #endregion

        #endregion
    }
}
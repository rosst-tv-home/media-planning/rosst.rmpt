﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using DevExpress.Xpf.Core.DragDrop.Native;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using RMPT.DATA.DataBase.Repositories;
using RMPT.VALIDATE;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryAccessRights.Roles {
    /// <summary>
    /// Модель представления диалогового окна пользовательских ролей
    /// </summary>
    public class RolesDWM : AbstractVM {
        #region Свойства

        /// <summary>
        /// Коллекция пользовательских ролей
        /// </summary>
        public ObservableCollection<DCRole> Roles { get; } = new ObservableCollection<DCRole>();

        /// <summary>
        /// Коллекция удалённых ролей
        /// </summary>
        private Collection<DCRole> RemovedRoles { get; } = new Collection<DCRole>();

        #endregion

        #region Делегаты

        #region Проверка полей

        /// <summary>
        /// Команда проверки введённого значения в ячейку наименования роли
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnTypeValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnTypeValidate);

        /// <summary>
        /// Команда проверки введённого значения в ячейку описания роли
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnDescriptionValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnDescriptionValidate);

        #endregion

        #endregion

        #region Методы

        #region Диалоговое окно

        /// <summary>
        /// Метод вызывается после загрузки диалогового окна
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Заполняем коллекцию пользовательских ролей
            (await RepositoryRoles.FindAllWithUsersAsync()).ForEach(Roles.Add);
            // Подписываемся на событие изменения коллекции
            Roles.CollectionChanged += OnRolesCollectionChanged;
        }

        /// <summary>
        /// Метод вызывается перед закрытием окна
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        protected override async Task OnClosingAsync(CancelEventArgs arguments) {
            // Если есть удалённые пользовательские роли с привязанными сущностями
            if (RemovedRoles.Count != 0) {
                // Сообщение диалогового окна с предупреждением
                var errorMessage = RemovedRoles.Aggregate(
                    "Вы пытаетесь удалить роли, которые связанны с некоторыми пользователями:",
                    (message, role) => message + $"\n{role.Type}"
                );

                // Диалогвое сообщение
                var resultRemove = MessageBox.Show(
                    errorMessage,
                    "Выполнить?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил удаление
                if (resultRemove != MessageBoxResult.Yes) {
                    // Возвращаем удалённые пользовательские роли
                    RemovedRoles.ForEach(Roles.Add);
                    // Очищаем коллекцию удалённых пользовательских ролей
                    RemovedRoles.Clear();
                    // Отменяем событие закрытия диалогового окна
                    arguments.Cancel = true;
                    // Завершаем выполнение метода
                    return;
                }
            }

            // Коллекция невалидных пользовательских ролей
            var invalidRoles = Roles.Where(role =>
                string.IsNullOrWhiteSpace(role.Type) ||
                string.IsNullOrWhiteSpace(role.Description)
            ).ToList();

            // Если есть невалидные пользовательские роли
            if (invalidRoles.Count != 0) {
                // Сообщение диалогового окна с предупреждением
                var errorMessage = invalidRoles.Aggregate(
                    "В таблице есть невалидные данные:",
                    (message, role) => message + $"\nСтрока №{invalidRoles.IndexOf(role) + 1} таблицы"
                );

                // Диалогвое сообщение
                var resultRemove = MessageBox.Show(
                    errorMessage,
                    "Продолжить?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил продолжение
                if (resultRemove != MessageBoxResult.Yes) {
                    // Удаляем невалидные пользовательские роли из таблицы
                    Roles.RemoveMultiple(invalidRoles.Select(role => Roles.IndexOf(role)).ToArray());
                    // Отменяем событие закрытия диалогового окна
                    arguments.Cancel = true;
                    // Завершаем выполнение метода
                    return;
                }
            }

            // Коллекция валидных пользовательских ролей
            var validRoles = Roles.Where(x => !invalidRoles.Contains(x)).ToList();

            // Ожидаем завершения обновления данных в базе данных
            await Task.WhenAll(
                // Удаляем недействительные пользовательские роли из базы данных
                RepositoryRoles.DeleteAsync(validRoles),
                // Обновляем действительные пользовательские роли в базе данных
                RepositoryRoles.UpdateAsync(validRoles),
                // Добавляем новые пользовательские роли в базу данных
                RepositoryRoles.InsertAsync(validRoles)
            );

            // Отписываемся от события изменения коллекции
            Roles.CollectionChanged -= OnRolesCollectionChanged;
        }

        #endregion

        /// <summary>
        /// Метод вызывается после изменения коллекции пользовательских ролей
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private void OnRolesCollectionChanged(object sender, NotifyCollectionChangedEventArgs argument) {
            // Если действие не является "удаление объекта из коллекции" - завершаем выполнение метода
            if (argument.Action != NotifyCollectionChangedAction.Remove) return;

            // Итерируемся по удаленным пользовательских ролям
            foreach (var item in argument.OldItems) {
                // Если элемент не является пользовательской ролью - переходим к следующей
                if (!(item is DCRole role)) continue;

                // Если есть привязка к пользователю
                if (role.Users.Count > 0) {
                    // Добавляем в коллекцию удалённых пользовательских ролей
                    RemovedRoles.Add(role);
                }
            }
        }

        #region Проверка полей

        /// <summary>
        /// Метод вызывается после ввода типа роли
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnTypeValidate(GridCellValidationEventArgs argument) {
            // Вызываем метод базовой проверки валидности данных
            var isValid = OnValidate(argument);
            // Если данные не валидны - завершаем выполнение метода
            if (!isValid) return;

            // Признак наличия пользовательской роли с таким же типом
            var hasDuplicates = Roles
               .Where(role =>
                    !string.IsNullOrWhiteSpace(role.Type) &&
                    !ReferenceEquals(role, (DCRole)argument.Row)
                )
               .Any(role => role.Type!.Equals(
                    (string)argument.Value,
                    StringComparison.InvariantCultureIgnoreCase
                ));

            // Если нет дубликатов - отмечаем ячейку как валидную и завершаем выполнение метода
            if (!hasDuplicates) {
                argument.IsValid = true;
                return;
            }

            argument.ErrorContent = "Пользовательская роль с данным типом уже существует";
        }

        /// <summary>
        /// Метод вызывается после ввода типа роли
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnDescriptionValidate(GridCellValidationEventArgs argument) {
            // Вызываем метод базовой проверки валидности данных
            var isValid = OnValidate(argument);
            // Если данные не валидны - завершаем выполнение метода
            if (!isValid) return;

            // Признак наличия пользовательской роли с таким же описанием
            var hasDuplicates = Roles
               .Where(role =>
                    !string.IsNullOrWhiteSpace(role.Description) &&
                    !ReferenceEquals(role, (DCRole)argument.Row)
                )
               .Any(role => role.Description!.Equals(
                    (string)argument.Value,
                    StringComparison.InvariantCultureIgnoreCase
                ));

            // Если нет дубликатов - отмечаем ячейку как валидную и завершаем выполнение метода
            if (!hasDuplicates) {
                argument.IsValid = true;
                return;
            }

            argument.ErrorContent = "Пользовательская роль с данным описанием уже существует";
        }

        /// <summary>
        /// Метод вызывается при проверке строковых значений в ячейках
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Признак валидности данных</returns>
        private bool OnValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если строковое значение ячейки таблицы невалидно
            if (!GridValidator.IsValidStringifyCellValue(argument)) return false;

            // Если строка таблицы не соответсвует заполняемой модели
            if (!GridValidator.IsValidRowType<DCRole>(argument)) return false;

            return true;
        }

        #endregion

        #endregion
    }
}
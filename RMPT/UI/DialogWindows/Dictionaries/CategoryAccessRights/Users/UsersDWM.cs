﻿using DevExpress.Mvvm;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.User;
using RMPT.DATA.DataBase.Repositories;
using RMPT.VALIDATE;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryAccessRights.Users {
    /// <summary>
    /// Модель диалогового окна справочника пользователей
    /// </summary>
    public class UsersDWM : AbstractVM {
        #region Свойства

        /// <summary>
        /// Коллекция пользователей
        /// </summary>
        public ObservableCollection<DCUser> Users { get; } = new ObservableCollection<DCUser>();

        /// <summary>
        /// Коллекция пользовательских ролей
        /// </summary>
        public ObservableCollection<DCRole> Roles { get; } = new ObservableCollection<DCRole>();

        #endregion

        #region Делегаты

        #region Проверка полей

        /// <summary>
        /// Команда проверки введённого значения в ячейку логина пользователя
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnLoginValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnLoginValidate);

        /// <summary>
        /// Команда проверки введённого значения в ячейку имени пользователя
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnFirstNameValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnFirstNameValidate);

        /// <summary>
        /// Команда проверки введённой значения в ячейку фамилии пользователя
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnLastNameValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnLastNameValidate);

        /// <summary>
        /// Команда проверки выбранных ролей в ячейке ролей пользователя
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnRolesValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnRolesValidate);

        #endregion

        #endregion

        #region Методы

        #region Диалоговое окно

        /// <summary>
        /// Метод вызывается после загрузки диалогового окна
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Заполняем коллекцию ролей
            (await RepositoryRoles.FindAllWithUsersAsync()).ForEach(Roles.Add);
            // Заполняем коллекцию пользователей
            (await RepositoryUsers.FindAllWithRolesAsync()).ForEach(user => {
                // Определяем коллекцию идентификаторов пользовательских ролей
                var userRoles = user.Roles.Select(link => link.RoleID).ToList();
                // Находим пользовательские роли в коллекции
                var roles = Roles.Where(role => userRoles.Contains(role.ID)).ToList();
                // Если нашли - заменяем
                if (roles.Count != 0) {
                    // Очищаем коллекцию ролей в пользователе
                    user.Roles.Clear();
                    // И заменяем её на новую (требуется для правильного отображения в представлении)
                    roles
                       .Select(role => new DCUserRole {
                            Role = role,
                            RoleID = role.ID,
                            User = user,
                            UserID = user.ID
                        })
                       .ToList()
                       .ForEach(user.Roles.Add);
                }

                // Добавляем пользователя в коллекцию
                Users.Add(user);
            });
        }

        /// <summary>
        /// Метод вызывается перед закрытием диалогового окна
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        protected override async Task OnClosingAsync(CancelEventArgs arguments) {
            // Определяем невалидных пользователей
            var invalidUsers = Users.Where(user =>
                string.IsNullOrWhiteSpace(user.Login)     ||
                string.IsNullOrWhiteSpace(user.FirstName) ||
                string.IsNullOrWhiteSpace(user.LastName)  ||
                user.Roles.Count == 0
            ).ToList();

            // Если есть невалидные пользователи
            if (invalidUsers.Count != 0) {
                // Сообщение диалогового окна с предупреждением
                var errorMessage = invalidUsers.Aggregate(
                    "В таблице есть невалидные пользователи, они не будут сохранены или будут удалены:",
                    (message, user) => message + $"\nПользователь в строке №{Users.IndexOf(user) + 1} таблицы"
                );
                // Диалоговое окно
                var result = MessageBox.Show(
                    errorMessage,
                    "Продолжить?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил продолжение
                if (result != MessageBoxResult.Yes) {
                    // Отменяем событие закрытия диалогового окна
                    arguments.Cancel = true;
                    // Завершаем выполнение метода
                    return;
                }
            }

            // Валидные пользовтели
            var validUsers = Users
               .Where(user => invalidUsers.All(x => !ReferenceEquals(user, x)))
               .ToList();

            // Ожидаем выполнения всех задач
            await Task.WhenAll(
                // Удаляем недейсвтительных пользователей из базы данных
                RepositoryUsers.DeleteAsync(validUsers),
                // Обновляем действительных пользователей в базе данных
                RepositoryUsers.UpdateAsync(validUsers),
                // Добавляем новых пользователей в базу данных
                RepositoryUsers.InsertAsync(validUsers)
            );
        }

        #endregion

        /// <summary>
        /// Метод вызывается после ввода строковых значений в ячейки таблицы
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        /// <returns>Признак валидности вводимых значений</returns>
        private bool OnBaseStringValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если строковое значение ячейки таблицы невалидно
            if (!GridValidator.IsValidStringifyCellValue(argument)) return false;

            // Если строка таблицы не соответсвует заполняемой модели
            if (!GridValidator.IsValidRowType<DCUser>(argument)) return false;

            return true;
        }

        /// <summary>
        /// Метод вызывается после ввода логина пользователя
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnLoginValidate(GridCellValidationEventArgs argument) {
            // Если не прошла базовая проверка - завершаем выполнение метода
            if (!OnBaseStringValidate(argument)) return;

            // Признак наличия пользователя с таким же логином
            var hasDuplicates = Users
               .Where(user =>
                    !string.IsNullOrWhiteSpace(user.Login) &&
                    !ReferenceEquals(user, (DCUser)argument.Row)
                )
               .Any(user => user.Login!.Equals(
                    (string)argument.Value,
                    StringComparison.InvariantCultureIgnoreCase
                ));

            // Если нет дубликатов - отмечаем ячейку как валидную и завершаем выполнение метода
            if (!hasDuplicates) {
                argument.IsValid = true;
                return;
            }

            argument.ErrorContent = "Пользователь с таким логином уже существует";
        }

        /// <summary>
        /// Метод вызывается после ввода имени пользователя
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnFirstNameValidate(GridCellValidationEventArgs argument) {
            // Если не прошла базовая проверка - завершаем выполнение метода
            if (!OnBaseStringValidate(argument)) return;
            // Иначе ячейка валидна
            argument.IsValid = true;
        }

        /// <summary>
        /// Метод вызывается после ввода фамилии пользователя
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnLastNameValidate(GridCellValidationEventArgs argument) {
            // Если не прошла базовая проверка - завершаем выполнение метода
            if (!OnBaseStringValidate(argument)) return;
            // Иначе ячейка валидна
            argument.IsValid = true;
        }

        /// <summary>
        /// Метод вызывается после ввода "Ролей"
        /// </summary>
        /// <param name="argument"></param>
        private void OnRolesValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если значение равно null - выводим сообщение об оишбке и завершаем выполнение метода
            if (argument.Value == null) {
                argument.ErrorContent = "Значение не является коллекцией";
                return;
            }

            // Если есть сообщение об ошибке - выводим и завершаем выполнение метода
            if (!GridValidator.IsValidRowType<DCUser>(argument)) return;

            // Иначе ячейка валидная
            argument.IsValid = true;
        }

        #endregion
    }
}
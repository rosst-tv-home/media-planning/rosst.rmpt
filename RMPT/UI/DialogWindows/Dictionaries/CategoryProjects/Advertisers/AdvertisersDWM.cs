﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Brand;
using RMPT.DATA.DataBase.Repositories;
using RMPT.VALIDATE;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryProjects.Advertisers {
    /// <summary>
    /// Модель диалогового окна рекламодателей
    /// </summary>
    public class AdvertisersDWM : AbstractVM {
        #region Свойства

        /// <summary>
        /// Выбранный рекламодатель
        /// </summary>
        public DCAdvertiser? Advertiser {
            get => GetProperty(() => Advertiser);
            set => SetProperty(() => Advertiser, value);
        }

        /// <summary>
        /// Коллекция рекламодателей
        /// </summary>
        public ObservableCollection<DCAdvertiser> Advertisers { get; } = new ObservableCollection<DCAdvertiser>();

        /// <summary>
        /// Коллекция удаленных рекламодателей
        /// </summary>
        private Collection<DCAdvertiser> RemovedAdvertisers { get; } = new Collection<DCAdvertiser>();

        #endregion

        #region Делегаты

        #region Проверка полей

        /// <summary>
        /// Команда проверки введённого значения в ячейку наименования рекламодателя
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnAdvertiserNameValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnAdvertiserNameValidate);

        /// <summary>
        /// Команда проверки введённого значения в ячейку наименования бренда
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnBrandNameValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnBrandNameValidate);

        #endregion

        #endregion

        #region Методы

        #region Диалоговое окно

        /// <summary>
        /// Метод вызывается после загрузки диалогового окна
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Заполняем коллекцию рекламодателей
            (await RepositoryAdvertisers.FindAllWithBrandsAsync()).ForEach(Advertisers.Add);
            // Подписываемся на событие изменения коллекции 
            Advertisers.CollectionChanged += OnLegalEntitiesChanged;
        }

        /// <summary>
        /// Метод вызывается перед закрытием диалогового окна
        /// </summary>
        /// <param name="arguments">Аргумент события</param>
        /// <returns><see cref="Task"/></returns>
        protected override async Task OnClosingAsync(CancelEventArgs arguments) {
            // Если есть удалённые рекламодатели с привязанными сущностями
            if (RemovedAdvertisers.Count != 0) {
                // Сообщение диалогового окна с предупреждением
                var errorMessage = RemovedAdvertisers.Aggregate(
                    "Вы пытаетесь удалить рекламодателей, которые связанны с некоторыми брендами:",
                    (message, advertiser) => message + $"\n{advertiser.Name}"
                );

                // Диалогвое сообщение
                var resultRemove = MessageBox.Show(
                    errorMessage,
                    "Выполнить?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил удаление
                if (resultRemove != MessageBoxResult.Yes) {
                    // Возвращаем удалённых рекламодателей
                    RemovedAdvertisers.ForEach(Advertisers.Add);
                    // Очищаем коллекцию удалённых рекламодателей
                    RemovedAdvertisers.Clear();
                    // Отменяем событие закрытия диалогового окна
                    arguments.Cancel = true;
                    // Завершаем выполнение метода
                    return;
                }
            }

            // Коллекция невалидных рекламодателей
            var invalidAdvertisers = Advertisers
               .Where(advertiser =>
                    // Наименование рекламодателя невалидно
                    string.IsNullOrWhiteSpace(advertiser.Name.Trim()) ||
                    // Или наименование одного из брендов рекламодателя невалидно
                    advertiser.Brands.Any(brand => string.IsNullOrWhiteSpace(brand.Name.Trim()))
                )
               .ToList();

            // Если есть невалидные рекламодатели
            if (invalidAdvertisers.Count != 0) {
                // Сообщение диалогового окна с предупреждением
                var errorMessage = invalidAdvertisers.Aggregate(
                    "В таблице есть невалидные данные:",
                    (message, advertiser) => message + $"\nСтрока №{Advertisers.IndexOf(advertiser) + 1} таблицы"
                );

                // Диалогвое сообщение
                var resultRemove = MessageBox.Show(
                    errorMessage,
                    "Продолжить?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил продолжение
                if (resultRemove != MessageBoxResult.Yes) {
                    // Отменяем событие закрытия диалогового окна
                    arguments.Cancel = true;
                    // Завершаем выполнение метода
                    return;
                }
            }

            // Коллекция валидных рекламодателей
            var validAdvertisers = Advertisers
               .Where(advertiser => !string.IsNullOrWhiteSpace(advertiser.Name.Trim()))
               .ToList();

            // Итерируемся по коллекции валидных рекламодателей
            foreach (var advertiser in validAdvertisers) {
                // Нормализируем наименования рекламодателя
                advertiser.Name = advertiser.Name.Trim();

                // Определяем коллекцию валидных брендов рекламодателя
                var validBrands = advertiser.Brands
                   .Where(brand => !string.IsNullOrWhiteSpace(brand.Name.Trim()))
                   .GroupBy(brand => brand.Name.Trim())
                   .Select(group => group.First())
                   .ToList();

                // Итерируемся по коллекции валидных брендов
                foreach (var brand in advertiser.Brands) {
                    // Исправляем ссылки на рекламодателя
                    brand.AdvertiserId = advertiser.ID;
                    brand.Advertiser = null;

                    // Нормализируем наименование бренда
                    brand.Name = brand.Name.Trim();
                }

                // Очищаем коллекцию брендов рекламодателя
                advertiser.Brands.Clear();
                // Заполняем коллекцию брендов рекламодателя валидными брендами
                advertiser.Brands.AddRange(validBrands);
            }

            // Ожидаем выполнения всех задач
            await Task.WhenAll(
                // Удаляем старых рекламодателей
                RepositoryAdvertisers.DeleteAsync(validAdvertisers),
                // Обновляем имеющихся рекламодателей
                RepositoryAdvertisers.UpdateAsync(validAdvertisers),
                // Добавляем новых рекламодателей
                RepositoryAdvertisers.InsertAsync(validAdvertisers)
            );

            // Отписываемся от события изменения коллекции 
            Advertisers.CollectionChanged -= OnLegalEntitiesChanged;
        }

        #endregion

        /// <summary>
        /// Метод вызывается после изменения коллекции рекламодателей
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="argument">Аргумент события</param>
        private void OnLegalEntitiesChanged(object sender, NotifyCollectionChangedEventArgs argument) {
            // Если действие не равно удалению объектов из коллекции - завершаем выполнение метода
            if (argument.Action != NotifyCollectionChangedAction.Remove) return;

            // Итерируемся по удалённым рекламодателям
            foreach (var item in argument.OldItems) {
                // Если элемент не является рекламодателем - переходим к следующему
                if (!(item is DCAdvertiser entity)) continue;

                // Если есть привязка к бренду
                if (entity.Brands.Count > 0) {
                    // Добовляем в коллекцию удалённых рекламодателей
                    RemovedAdvertisers.Add(entity);
                }
            }
        }

        #region Проверка полей

        /// <summary>
        /// Метод вызывается после ввода наименования рекламодателя
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnAdvertiserNameValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если строковое значение ячейки таблицы невалидно
            if (!GridValidator.IsValidStringifyCellValue(argument)) return;

            // Если строка таблицы не соответсвует заполняемой модели
            if (!GridValidator.IsValidRowType<DCAdvertiser>(argument)) return;

            // Признак наличия дубликатов
            var hasDuplicates = Advertisers
               .Where(advertiser =>
                    // Наименование рекламодателя валидно
                    !string.IsNullOrWhiteSpace(advertiser.Name.Trim()) &&
                    // Рекламодатель не является сравниваемым объектом
                    !ReferenceEquals(advertiser, (DCAdvertiser)argument.Row)
                )
               .Any(advertiser => advertiser.Name.Trim().Equals(
                    ((string)argument.Value).Trim(),
                    StringComparison.InvariantCultureIgnoreCase
                ));

            // Если есть дубликат рекламодателя
            if (hasDuplicates) {
                // Выводим пользователю сообщение об ошибке
                argument.ErrorContent = "Рекламодатель с данным наименованием уже существует";
                // Завершаем выполнение метода
                return;
            }

            // Отмечаем ячейку как валидную
            argument.IsValid = true;
        }

        /// <summary>
        /// Метод вызывается после ввода наименования бренда
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnBrandNameValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если строковое значение ячейки невалидно - завершаем выполнение метода
            if (!GridValidator.IsValidStringifyCellValue(argument)) return;
            // Если строка таблицы не соответсвует заполняемой модели - завершаем выполнение метода
            if (!GridValidator.IsValidRowType<DCBrand>(argument)) return;

            // Признак наличия дубликатов
            var hasDuplicates = Advertisers
                // Все бренды всех рекламодателей
               .SelectMany(advertiser => advertiser.Brands)
               .Where(brand =>
                    // Наименование бренда валидно
                    !string.IsNullOrWhiteSpace(brand.Name.Trim()) &&
                    // Бренд не является сравниваемым объектом
                    !ReferenceEquals(brand, (DCBrand)argument.Row)
                ).Any(brand => brand.Name.Trim().Equals(
                    ((string)argument.Value).Trim(),
                    StringComparison.InvariantCultureIgnoreCase
                ));

            // Если есть дубликат бренда
            if (hasDuplicates) {
                // Выводим пользователю сообщение об ошибке
                argument.ErrorContent = "Бренд с таким наименованием уже имеется у одного из рекламодателей";
                // Завершаем выполнение метода
                return;
            }

            // Отмечаем ячейку как валидную
            argument.IsValid = true;
        }

        #endregion

        #endregion
    }
}
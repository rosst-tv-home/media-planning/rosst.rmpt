﻿using DevExpress.Mvvm;
using DevExpress.Xpf.Grid;
using RMPT.COMMON;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Other;
using RMPT.DATA.DataBase.Repositories;
using RMPT.VALIDATE;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.Dictionaries.CategoryProjects.Strategists {
    /// <summary>
    /// Модель диалогового окна справочника стратегов
    /// </summary>
    public class StrategistsDWM : AbstractVM {
        #region Свойства

        /// <summary>
        /// Коллекция стратегов
        /// </summary>
        public ObservableCollection<DCStrategist> Strategists { get; } = new ObservableCollection<DCStrategist>();

        #endregion

        #region Делегаты

        #region Проверка полей

        /// <summary>
        /// Команда общей проверки введённого значения в ячейку стратега
        /// </summary>
        public DelegateCommand<GridCellValidationEventArgs> OnValidateCommand =>
            new DelegateCommand<GridCellValidationEventArgs>(OnValidate);

        #endregion

        #endregion

        #region Методы

        #region Диалоговое окно

        /// <summary>
        /// Метод вызывается после загрузки диалогового окна
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Заполняем коллекцию стратегов
            (await RepositoryStrategists.FindAllAsync()).ForEach(Strategists.Add);
        }

        /// <summary>
        /// Метод вызывается перед закрытием диалогового окна
        /// </summary>
        /// <param name="arguments">Аргумент события</param>
        /// <returns><see cref="Task"/></returns>
        protected override async Task OnClosingAsync(CancelEventArgs arguments) {
            // Коллекция невалидных стратегов
            var invalidStrategists = Strategists
               .Where(strategist =>
                    string.IsNullOrWhiteSpace(strategist.FirstName.Trim()) ||
                    string.IsNullOrWhiteSpace(strategist.LastName.Trim())
                ).ToList();

            // Если есть невалидные стратеги
            if (invalidStrategists.Count > 0) {
                // Формируем сообщение предупреждение диалогового окна
                var errorMessage = invalidStrategists.Aggregate(
                    "В таблице есть невалидные данные:",
                    (message, strategist) => message + $"\nСтрока №{Strategists.IndexOf(strategist) + 1} таблицы"
                );
                // Диалогвое сообщение
                var resultRemove = MessageBox.Show(
                    errorMessage,
                    "Продолжить?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил продолжение
                if (resultRemove != MessageBoxResult.Yes) {
                    // Отменяем событие закрытия диалогового окна
                    arguments.Cancel = true;
                    // Завершаем выполнение метода
                    return;
                }
            }

            // Коллекция действительных стратегов
            var strategists = Strategists
               .Where(strategist => !invalidStrategists.Contains(strategist))
               .ToList();

            // Ожидаем выполнения задач
            await Task.WhenAll(
                // Удаляем недейсвтительные
                RepositoryStrategists.DeleteAsync(strategists),
                // Обновляем действительные
                RepositoryStrategists.UpdateAsync(strategists),
                // Добавляем новые
                RepositoryStrategists.InsertAsync(strategists)
            );
        }

        #endregion

        #region Проверка полей

        /// <summary>
        /// Метод базовой проверки введённого значения в ячейку стратега
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnValidate(GridCellValidationEventArgs argument) {
            // По умолчанию ячейка невалидная
            argument.IsValid = false;

            // Если редактируемая строка не является моделью указанного типа - завершаем выполнение метода
            if (!GridValidator.IsValidRowType<DCStrategist>(argument)) return;

            // Если редактируемая ячейка является пустой строкой - завершаем выполнение метода
            if (!GridValidator.IsValidStringifyCellValue(argument)) return;

            argument.IsValid = true;
        }

        #endregion

        #endregion
    }
}
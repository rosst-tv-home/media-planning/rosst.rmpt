﻿using DevExpress.Mvvm;
using DevExpress.Xpf.Editors;
using DevExpress.XtraEditors.DXErrorProvider;
using RMPT.COMMON.Dictionaries;
using RMPT.COMMON.Models;
using RMPT.DATA.DataBase.Entities.Dictionaries.Brand;
using RMPT.DATA.DataBase.Entities.Dictionaries.Channel;
using RMPT.DATA.DataBase.Entities.Dictionaries.Other;
using RMPT.DATA.DataBase.Entities.Projects;
using RMPT.DATA.DataBase.Repositories;
using RMPT.EVENT;
using RMPT.EXCEL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.UI.DialogWindows.Project {
    /// <summary>
    ///     Модель диалогового окна свойств проекта
    /// </summary>
    public class ProjectPropertiesDWM : AbstractVM {
        #region Конструктор

        public ProjectPropertiesDWM() {
            Project = new PDProject();
            Project.ViewPreferences = new PDViewPreferences {
                Project = Project,
                IsShowChannelDistributionBands = true,
                IsShowChannelTimingDistributionBand = true
            };
        }

        #endregion

        /// <summary>
        ///     Проект
        /// </summary>
        public PDProject Project {
            get => GetProperty(() => Project);
            private set => SetProperty(() => Project, value);
        }

        /// <summary>
        ///     Выбранный рекламодатель
        /// </summary>
        public DCAdvertiser? SelectedAdvertiser {
            get => GetProperty(() => SelectedAdvertiser);
            set => SetProperty(() => SelectedAdvertiser, value, () => IsBrandSelectionEnabled = value != null);
        }

        /// <summary>
        ///     Выбранный бренд
        /// </summary>
        public DCBrand? SelectedBrand {
            get => GetProperty(() => SelectedBrand);
            set => SetProperty(() => SelectedBrand, value);
        }

        /// <summary>
        ///     Выбранный стратег
        /// </summary>
        public DCStrategist? SelectedStrategist {
            get => GetProperty(() => SelectedStrategist);
            set => SetProperty(() => SelectedStrategist, value);
        }

        /// <summary>
        ///     Выбранный менеджер
        /// </summary>
        public DCStrategist? SelectedManager {
            get => GetProperty(() => SelectedManager);
            set => SetProperty(() => SelectedManager, value);
        }

        /// <summary>
        ///     Признак "Федеральное ТВ"
        /// </summary>
        public bool IsFederal {
            get => GetProperty(() => Project.IsFederal);
            set => SetProperty(() => IsFederal, value, () => Project.IsFederal = value);
        }

        /// <summary>
        ///     Оригинальный планируемый период
        /// </summary>
        private int OriginPlaningYear {
            get => GetProperty(() => OriginPlaningYear);
            set => SetProperty(() => OriginPlaningYear, value);
        }

        /// <summary>
        ///     Коллекция рекламодателей
        /// </summary>
        public ObservableCollection<DCAdvertiser> Advertisers { get; } = new();

        /// <summary>
        ///     Коллекция стратегов
        /// </summary>
        public ObservableCollection<DCStrategist> Strategists { get; } = new();

        /// <summary>
        ///     Команда вызова метода обработки события "нажатие на кнопку загрузки сырья"
        /// </summary>
        public AsyncCommand OnRawDataClickedAsyncCMD => new(OnRawDataButtonClicked);

        /// <summary>
        ///     Команда вызова метода обработки события "Нажатия кнопки создать проекр"
        /// </summary>
        public AsyncCommand OnCreateOrApplyClickedAsyncCMD => new(OnCreateOrApplyButtonClicked);

        /// <summary>
        ///     Команда проверки введённого значения в поле базового хронометража
        /// </summary>
        public DelegateCommand<ValidationEventArgs> OnBaseTimingValidateCommand => new(OnBaseTimingValidate);

        /// <summary>
        ///     Сервис диалогового окна загруки файлов
        /// </summary>
        private IOpenFileDialogService ServiceFileDialog => GetService<IOpenFileDialogService>();

        /// <summary>
        ///     Метод обработки события изменения параметров модели представления
        /// </summary>
        /// <param name="parameter">Переданные параметры</param>
        protected override void OnParameterChanged(object? parameter) {
            // Если переданные параметры не являются проектом - завершаем выполнение метода
            if (parameter is not PDProject project) {
                return;
            }

            Project = project;
            OriginPlaningYear = Project.PlaningYear;

            CreateOrApplyButtonText = CommonDictionary.Apply;
            IsCreateOrApplyButtonEnabled = true;
        }

        /// <summary>
        ///     Метод обработки события загрузки окна
        /// </summary>
        /// <returns>
        ///     <see cref="Task" />
        /// </returns>
        protected override async Task OnLoadedAsync(RoutedEventArgs? arguments) {
            // Заполняем коллекцию рекламодателей
            (await RepositoryAdvertisers.FindAllWithBrandsAsync()).ForEach(Advertisers.Add);
            // Заполняем коллекцию стратегов
            (await RepositoryStrategists.FindAllAsync()).ForEach(Strategists.Add);

            // Если проект создаётся - завершаем выполнение метода
            if (Project.ID == 0) {
                return;
            }

            // Активируем кнопку применения изменений в проект
            IsCreateOrApplyButtonEnabled = true;

            SelectedManager = Strategists.FirstOrDefault(x => x.ID    == Project.ManagerID);
            SelectedStrategist = Strategists.FirstOrDefault(x => x.ID == Project.StrategistID);
            SelectedAdvertiser = Advertisers.FirstOrDefault(x => x.ID == Project.Brand!.AdvertiserId);
            SelectedBrand = Advertisers
               .FirstOrDefault(x => x.ID == Project.Brand!.AdvertiserId)?.Brands
               .FirstOrDefault(x => x.ID == Project.BrandID);
        }

        /// <summary>
        ///     Метод обработки события закрытия окна
        /// </summary>
        protected override Task OnUnloadedAsync(RoutedEventArgs? arguments) {
            SendMessage<IEventRefreshProjectsCatalog>(null!);
            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки события нажатия кнопки <see cref="CommonDictionary.DownloadRawData" />
        /// </summary>
        private async Task OnRawDataButtonClicked() {
            // Если сырьё уже есть в проекте
            if (Project.ID != 0) {
                var result = MessageBox.Show(
                    "Вы пытаетесь загрузить новое сырьё в старый проект.\nЭто приведёт к сбросу данных проекта\nПродолжить ?",
                    "Внимание !",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил операцию - завершаем выполнение метода
                if (result != MessageBoxResult.Yes) {
                    SendMessage(new EventStatusBarMessage("Операция была отменена"));
                    await Task.Delay(250);
                    return;
                }
            }

            // Деактивируем кнопку загрузки сырья
            IsRawDataButtonEnabled = false;
            // Деактивируем кнопку создания проекта
            IsCreateOrApplyButtonEnabled = false;
            ServiceFileDialog.Filter = "Выгрузка Palomars (*.xlsx)|*.xlsx";

            // Если файл не был выбран
            if (!ServiceFileDialog.ShowDialog()) {
                SendMessage(new EventStatusBarMessage("Операция была отменена"));
                await Task.Delay(500);

                IsRawDataButtonEnabled = true;
                SendMessage(new EventStatusBarMessage(""));
                return;
            }

            SendMessage(new EventStatusBarMessage("Чтение файла"));
            await Task.Delay(250);

            // Поток данных файла
            Stream? stream = null;

            try {
                // Получаем поток данных файла
                stream = ServiceFileDialog.File.OpenRead();
                // Получаем данные из прочтённого файла
                var regions = await PalomarsUtil.ReadFileAsync(stream);

                SendMessage(new EventStatusBarMessage("Данные успешно распознаны"));
                await Task.Delay(250);

                SendMessage(new EventStatusBarMessage("Наполнение проекта данными"));
                await Task.Delay(250);

                // Наполняем проект данными сырья
                await FillProjectWithRawData(regions);

                SendMessage(new EventStatusBarMessage("Проект успешно сформирован"));
                await Task.Delay(250);

                // Активируем кнопку создания проекта
                IsCreateOrApplyButtonEnabled = true;
            } catch (Exception) {
                SendMessage(new EventStatusBarMessage("Ошибка..."));
                await Task.Delay(250);
                throw;
            } finally {
                // Закрываем поток данных файла
                stream?.Close();
                stream?.Dispose();

                // Активируем кнопку загрузки сырья
                IsRawDataButtonEnabled = true;
                SendMessage(new EventStatusBarMessage(""));
            }
        }

        /// <summary>
        ///     Метод наполнения проекта данными сырья
        /// </summary>
        private async Task FillProjectWithRawData(IReadOnlyCollection<PalomarsRegion> regions) {
            // Очищаем старое сырьё проекта
            Project.Regions.Clear();

            // Не найденные регионы
            Collection<string> missingRegions = new();
            // Не найденные каналы
            Collection<string> missingChannels = new();
            // Прочие ошибки
            Collection<string> errors = new();

            // Итерируемся по регионам
            foreach (var region in regions) {
                SendMessage(new EventStatusBarMessage($"Поиск региона {region.Name} в базе данных"));
                await Task.Delay(50);
                // Пытаемся найти регион в базе данных
                var dcRegion = await RepositoryRegions.FindByName(region.Name);

                // Если регион не найден - добавляем в сообщение пользователю и переходим к следующему
                if (dcRegion == null) {
                    missingRegions.Add(region.Name);
                    SendMessage(new EventStatusBarMessage($"Регион {region.Name} не найден - пропускаем"));
                    await Task.Delay(50);
                    continue;
                }

                // Создаём регион для проекта
                var pdRegion = new PDRegion {
                    ID = 0,
                    ProjectID = Project.ID,
                    DictionaryID = dcRegion.ID,
                    Region = dcRegion,
                    Project = Project
                };

                // Итерируемся по каналам
                foreach (var channel in region.Channels) {
                    SendMessage(new EventStatusBarMessage($"Поиск канала {channel.Name} в базе данных"));
                    await Task.Delay(50);

                    // Пытаемся найти канал региона в базе данных
                    var dcChannel = await RepositoryChannels.FindByRegionAndChannelName(region.Name, channel.Name);

                    // Если канал не найден - добавляем в сообщение пользователю и переходим к следующему
                    if (dcChannel == null) {
                        missingChannels.Add(channel.Name);
                        SendMessage(new EventStatusBarMessage($"Канал {channel.Name} не найден - пропускаем"));
                        await Task.Delay(50);
                        continue;
                    }

                    // Данные по базовой аудитории канала
                    Collection<PalomarsAudience> baseAudienceData = new();
                    // Данные по целевой аудитории проекта
                    Collection<PalomarsAudience> targetAudienceData = new();

                    SendMessage(new EventStatusBarMessage($"Поиск нужных данных по аудиториям канала {channel.Name}"));
                    await Task.Delay(50);

                    // Итерируемся по аудиториям в канале
                    foreach (var period in channel.Periods) {
                        foreach (var audience in period.Audiences) {
                            // Если аудитория не ЦА проекта и не БА канала - переходим к следующей
                            if (!audience.IsTarget && !audience.Name.Equals(dcChannel.Audience?.Name)) {
                                continue;
                            }

                            // Если ЦА проекта
                            if (audience.IsTarget) {
                                targetAudienceData.Add(audience);
                                if (dcChannel.Audience.Name.Equals(audience.Name,
                                    StringComparison.InvariantCultureIgnoreCase)) {
                                    baseAudienceData.Add(audience);
                                }
                                continue;
                            }

                            baseAudienceData.Add(audience);
                        }
                    }
                    
                    // Если не удалось найти данные по ЦА или БА канала - переходим к следующему каналу
                    if (baseAudienceData.Count == 0 || targetAudienceData.Count == 0) {
                        errors.Add($"В сырье по каналу {channel.Name} нет данных по БА [{dcChannel.Audience?.Name}] или ЦА [{targetAudienceData.FirstOrDefault()?.Name}]");
                        continue;
                    }

                    // Если количество данных по ЦА и БА не совпадает - переходим к следующему каналу
                    if (baseAudienceData.Count != targetAudienceData.Count) {
                        errors.Add($"В сырье по каналу {channel.Name} не совпадает количество данных по БА [{dcChannel.Audience?.Name}] и ЦА [{targetAudienceData.FirstOrDefault()?.Name}]");
                        continue;
                    }
                    
                    // Создаём канал для проекта
                    var pdChannel = new PDChannel {
                        ID = 0,
                        DictionaryID = dcChannel.ID,
                        RegionID = pdRegion.ID,
                        Region = pdRegion,
                        IsActive = true,
                        Channel = dcChannel
                    };

                    SendMessage(new EventStatusBarMessage($"Формирования данных аудиторий канала {channel.Name}"));
                    await Task.Delay(50);

                    // Создаём аудитории для канала проекта
                    foreach (var audience in baseAudienceData) {
                        // Пытаемся найти аудиторию в базе данных или создаём новую
                        var dcAudience =
                            await RepositoryAudiences.FindByName(audience.Name.Trim()) ??
                            await RepositoryAudiences.InsertSingleAsync(new DCAudience {
                                Name = audience.Name.Trim()
                            });

                        // Создаём аудиторию канала для проекта
                        var pdAudience = new PDAudience {
                            ID = 0,
                            Date = audience.Period.Date,
                            ChannelID = pdChannel.ID,
                            DictionaryID = dcAudience.ID,
                            Channel = pdChannel,
                            Audience = dcAudience,
                            IsTarget = false,
                            Sample = Convert.ToInt32(audience.Sample),
                            FrequencyPercent = audience.FrequencyValue,
                            WeightedTotal = Convert.ToInt32(audience.WeightTotal),
                            CumReachPercent = audience.CumReach,
                            TVR = audience.Tvr
                        };

                        // Добавляем аудиторию в канал
                        pdChannel.Audiences.Add(pdAudience);
                    }

                    foreach (var audience in targetAudienceData) {
                        // Пытаемся найти аудиторию в базе данных или создаём новую
                        var dcAudience =
                            await RepositoryAudiences.FindByName(audience.Name.Trim()) ??
                            await RepositoryAudiences.InsertSingleAsync(new DCAudience {
                                Name = audience.Name.Trim()
                            });

                        // Создаём аудиторию канала для проекта
                        var pdAudience = new PDAudience {
                            ID = 0,
                            ChannelID = pdChannel.ID,
                            DictionaryID = dcAudience.ID,
                            Date = audience.Period.Date,
                            Channel = pdChannel,
                            Audience = dcAudience,
                            IsTarget = audience.IsTarget,
                            Sample = Convert.ToInt32(audience.Sample),
                            FrequencyPercent = audience.FrequencyValue,
                            WeightedTotal = Convert.ToInt32(audience.WeightTotal),
                            CumReachPercent = audience.CumReach,
                            TVR = audience.Tvr
                        };

                        // Добавляем аудиторию в канал
                        pdChannel.Audiences.Add(pdAudience);
                    }

                    // Добавляем канал в регион проекта
                    pdRegion.Channels.Add(pdChannel);
                }

                // Если в регионе нет каналов - переходим к следующему региону
                if (pdRegion.Channels.Count == 0) {
                    continue;
                }

                // Добавляем регион в проект
                Project.Regions.Add(pdRegion);
            }

            SendMessage(new EventStatusBarMessage("Определение целевой аудитории проекта..."));
            await Task.Delay(50);

            // Определеяем целевую аудиторию проекта
            Project.TargetAudienceID = Project
               .Regions.FirstOrDefault()?
               .Channels.FirstOrDefault()?
               .Audiences.FirstOrDefault(a => a.IsTarget)?
               .Audience?.ID;

            SendMessage(new EventStatusBarMessage("Определение типа проекта..."));
            await Task.Delay(50);

            // Определяем тип проекта
            Project.IsFederal = Project.Regions.FirstOrDefault()?.Region?.Name.Equals("Федеральное ТВ") == true;
            IsFederal = Project.IsFederal;

            // Определяем частоту проекта
            Project.Frequency = regions.FirstOrDefault()?
               .Channels.FirstOrDefault()?
               .Periods.FirstOrDefault()?
               .Audiences.FirstOrDefault()?
               .FrequencyName;

            // Если есть пропущенные регионы или каналы, формируем сообщение для пользователя
            if (missingRegions.Count != 0 || missingChannels.Count != 0) {
                string message = "";

                if (missingRegions.Count != 0) {
                    message += "Регионов нет в базе данных: " + string.Join(",", missingRegions);
                    if (missingChannels.Count != 0) {
                        message += "\n\n";
                    }
                }

                if (missingChannels.Count != 0) {
                    message += "Каналов нет в базе данных: " + string.Join(",", missingChannels);
                }

                message += "\n\n";
                if (errors.Count != 0) {
                    message += string.Join("\n", errors);
                }

                // Оповещаем пользователя о не найденных данных
                MessageBox.Show(message, "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        ///     Метод обработки события нажатия кнопки
        ///     <see cref="CommonDictionary.CreateProject" /> или <see cref="CommonDictionary.Apply" />
        /// </summary>
        private async Task OnCreateOrApplyButtonClicked() {
            // Если не выбран рекламодатель
            if (SelectedAdvertiser == null) {
                throw new NullReferenceException("Выберите рекламодателя");
            }

            // Если не выбран бренд
            if (SelectedBrand == null) {
                throw new NullReferenceException("Выберите бренд");
            }

            // Если не выбран стратег
            if (SelectedStrategist == null) {
                throw new NullReferenceException("Выберите стратега");
            }

            // Если не выбран менеджер
            if (SelectedManager == null) {
                throw new NullReferenceException("Выберите менеджера");
            }

            Project.Brand = SelectedBrand;
            Project.Brand.Advertiser = SelectedAdvertiser;
            Project.BrandID = SelectedBrand!.ID;
            Project.ManagerID = SelectedManager!.ID;
            Project.StrategistID = SelectedStrategist!.ID;

            // Если создаётся новый проект
            if (Project.ID == 0) {
                SendMessage(new EventStatusBarMessage("Создание нового проекта..."));
                await RepositoryProject.InsertAsync(Project);
                SendMessage(new EventStatusBarMessage("Проект успешно создан"));

                CurrentWindowSRV?.Close();

                await Task.Delay(100);
                SendMessage(new EventStatusBarMessage(""));

                return;
            }

            // Если планируемы год изменился
            if (Project.PlaningYear != OriginPlaningYear) {
                var result = MessageBox.Show(
                    "Вы пытаетесь изменить планируемый год.\nЭто приведёт к сбросу данных проекта\nСохранить ?",
                    "Внимание !",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning
                );

                // Если пользователь не подтвердил действие - возвращаем старый год
                if (result != MessageBoxResult.Yes) {
                    Project.PlaningYear = OriginPlaningYear;
                } else {
                    SendMessage(new EventStatusBarMessage("Сброс данных проекта..."));
                    foreach (var channel in Project.Regions.SelectMany(region => region.Channels).ToList()) {
                        channel.Months.Clear();
                    }

                    await Task.Delay(250);
                }
            }

            SendMessage(new EventStatusBarMessage("Обновление данных проекта..."));
            await RepositoryProject.UpdateProject(Project, true);
            SendMessage(new EventStatusBarMessage($"Данные проекта № {Project.ID} успешно обновлены"));

            await Task.Delay(250);
            SendMessage(new EventStatusBarMessage(""));

            CurrentWindowSRV?.Close();
        }

        /// <summary>
        ///     Метод проверки введённого значения в поле базового хронометража
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        private void OnBaseTimingValidate(ValidationEventArgs argument) {
            // По умолчанию ячейка невалидная 
            argument.IsValid = false;

            // Если значение не является целочисленным
            if (argument.Value is not int value) {
                argument.ErrorType = ErrorType.Warning;
                argument.ErrorContent = "Значение не является целочисленным";
                return;
            }

            // Если значение равно 0 или не кратно 5(-ти)
            if (value == 0 || value % 5 != 0) {
                argument.ErrorType = ErrorType.Warning;
                argument.ErrorContent = "Хронометраж не должен быть равен 0 и должен быть кратным 5-ти";
                return;
            }

            // Отмечаем ячейку как валидную
            argument.IsValid = true;
        }

        #region Свойства

        #region UI флаги

        /// <summary>
        ///     Признак активности поля выбора бренда
        /// </summary>
        [DefaultValue(false)]
        public bool IsBrandSelectionEnabled {
            get => GetProperty(() => IsBrandSelectionEnabled);
            set => SetProperty(() => IsBrandSelectionEnabled, value);
        }

        /// <summary>
        ///     Признак активности кнопки <see cref="CommonDictionary.DownloadRawData" />
        /// </summary>
        [DefaultValue(true)]
        public bool IsRawDataButtonEnabled {
            get => GetProperty(() => IsRawDataButtonEnabled);
            set => SetProperty(() => IsRawDataButtonEnabled, value);
        }

        /// <summary>
        ///     Признак активности кнопки <see cref="CommonDictionary.CreateProject" />
        /// </summary>
        [DefaultValue(false)]
        public bool IsCreateOrApplyButtonEnabled {
            get => GetProperty(() => IsCreateOrApplyButtonEnabled);
            set => SetProperty(() => IsCreateOrApplyButtonEnabled, value);
        }

        /// <summary>
        ///     Текст кнопки <see cref="CommonDictionary.CreateProject" /> или <see cref="CommonDictionary.Apply" />
        /// </summary>
        [DefaultValue(CommonDictionary.CreateProject)]
        public string CreateOrApplyButtonText {
            get => GetProperty(() => CreateOrApplyButtonText);
            set => SetProperty(() => CreateOrApplyButtonText, value);
        }

        #endregion

        #endregion
    }
}
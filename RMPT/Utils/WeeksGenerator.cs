﻿using System;
using System.Collections.Generic;

namespace RMPT.Utils {
    /// <summary>
    ///     Генератор недельных периодов
    /// </summary>
    public static class WeeksGenerator {
        /// <summary>
        ///     Метод генерации недельных периодов месяца
        /// </summary>
        /// <param name="monthDate">Месяц</param>
        /// <returns>Недельные периоды месяца</returns>
        public static IEnumerable<Tuple<DateTime, DateTime>> Generate(DateTime monthDate) {
            // Наполняемая коллекция недельных периодов
            var weeks = new List<Tuple<DateTime, DateTime>>();

            // Дата окончания месячного периода
            var monthEndDate = monthDate.AddMonths(1).AddDays(-1);
            // Количество дней
            var daysCount = (monthEndDate - monthDate).Days + 1;

            // Дата начала недельного периода
            DateTime? dateFrom = null;

            // Итерируемся по дням месяца
            for (var i = 0; i <= daysCount; i++) {
                // Если начало периода или понедельник
                if (i == 0 || monthDate.AddDays(i).DayOfWeek == DayOfWeek.Monday) {
                    dateFrom = monthDate.AddDays(i);
                }

                // Если вышли за диапазон дат месяца
                if (monthDate.AddDays(i).Month != monthDate.Month) {
                    continue;
                }

                // Если не воскресенье и не окончание периода
                if (i != (monthEndDate - monthDate).Days && monthDate.AddDays(i).DayOfWeek != DayOfWeek.Sunday) {
                    continue;
                }

                // Дата окончания недельного периода
                DateTime? dateBefore = monthDate.AddDays(i);

                // Добавляем недельный период в коллекцию
                weeks.Add(Tuple.Create(dateFrom!.Value, dateBefore.Value));
            }

            return weeks;
        }
    }
}
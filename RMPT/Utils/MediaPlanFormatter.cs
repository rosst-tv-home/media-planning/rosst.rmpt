﻿using DevExpress.Mvvm.Native;
using DevExpress.Spreadsheet;
using RMPT.COMMON.Dictionaries;
using RMPT.COMMON.Models;
using RMPT.DATA.ViewModels.Models;
using RMPT.EVENT;
using RMPT.Models;
using RMPT.VALIDATE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.Utils {
    /// <summary>
    ///     Форматор медиаплана
    /// </summary>
    public class MediaPlanFormatter : AbstractM {
        #region Конструктор

        public MediaPlanFormatter(
            ProjectVM project,
            MediaPlanType type,
            bool isShowRegion,
            bool isShowChannel,
            bool isShowChannelSaleType,
            bool isShowChannelPortionDistribution,
            bool isShowChannelQualityDistribution,
            bool isShowMonthAffinity,
            bool isShowMonthQualityDistribution,
            bool isMonthTrp,
            bool isMonthGivenTrp,
            bool isMonthGrp,
            bool isMonthGivenGrp,
            bool isMonthMinutes,
            bool isShowOutputsByTimings,
            bool isShowOutputsByQuality,
            bool isShowOutputsTotal,
            bool isShowActiveDays,
            bool isShowOutputsPerDay,
            bool isShowBudgetAgency
        ) {
            _project = project;
            _type = type;
            _isShowRegion = isShowRegion;
            _isShowChannel = isShowChannel;
            _isShowChannelSaleType = isShowChannelSaleType;
            _isShowChannelPortionDistribution = isShowChannelPortionDistribution;
            _isShowChannelQualityDistribution = isShowChannelQualityDistribution;
            _isShowMonthAffinity = isShowMonthAffinity;
            _isShowMonthQualityDistribution = isShowMonthQualityDistribution;
            _isMonthTrp = isMonthTrp;
            _isMonthGivenTrp = isMonthGivenTrp;
            _isMonthGrp = isMonthGrp;
            _isMonthGivenGrp = isMonthGivenGrp;
            _isMonthMinutes = isMonthMinutes;
            _isShowOutputsByTimings = isShowOutputsByTimings;
            _isShowOutputsByQuality = isShowOutputsByQuality;
            _isShowOutputsTotal = isShowOutputsTotal;
            _isShowActiveDays = isShowActiveDays;
            _isShowOutputsPerDay = isShowOutputsPerDay;
            _isShowBudgetAgency = isShowBudgetAgency;

            _workbook = new Workbook();
            _worksheet = _workbook.Worksheets.ActiveWorksheet;

            SendMessage(new EventStatusBarMessage("Сохранение отчета: Определение количество строк данных ! 1/3"));

            // Определяем количественные данные
            DefineCounters();

            // Если количество строк данных или количество месяцев или количество хронометражей равно нулю - выбрасываем исключение
            if (
                _rowsCount    == 0 ||
                _monthsCount  == 0 ||
                _timingsCount == 0
            ) {
                throw new ArgumentException("Нет данных для отображения отчёта");
            }

            // Определяем диапазоны ячеек для упрощения навигации
            DefineCellRanges();
        }

        #endregion

        #region Свойства

        #region Данные

        /// <summary>
        ///     Проект
        /// </summary>
        private readonly ProjectVM _project;

        #endregion

        #region Настройки документа

        /// <summary>
        ///     Тип медиаплана
        /// </summary>
        private readonly MediaPlanType _type;

        #region Настройки столбцов канала

        /// <summary>
        ///     Признак отображения столбца "Регион"
        /// </summary>
        private readonly bool _isShowRegion;

        /// <summary>
        ///     Признак отображения столбца "Канал"
        /// </summary>
        private readonly bool _isShowChannel;

        /// <summary>
        ///     Признак отображения столбца "Тип продажи"
        /// </summary>
        private readonly bool _isShowChannelSaleType;

        #endregion

        #region Настройки группы столбцов "Распределение" канала

        /// <summary>
        ///     Признак отображения столбца "% Канала" в группе столбцов "Распределение" канала
        /// </summary>
        private readonly bool _isShowChannelPortionDistribution;

        /// <summary>
        ///     Признак отображения столбцов "% Float, %Float P., % Fix, % Fix P., % S.Fix, % S.Fix P." в группе столбцов
        ///     "Распределение" канала
        /// </summary>
        private readonly bool _isShowChannelQualityDistribution;

        #endregion

        #region Настройки группы столбцов "Распределение" месяца

        /// <summary>
        ///     Признак отображения столбца "Affinity" в группе столбцов "Распределение" месяца
        /// </summary>
        private readonly bool _isShowMonthAffinity;

        /// <summary>
        ///     Признак отображения столбца "% Float, %Float P., % Fix, % Fix P., % S.Fix, % S.Fix P." в группе столбцов
        ///     "Распределение" месяца
        /// </summary>
        private readonly bool _isShowMonthQualityDistribution;

        #endregion

        #region Настройки группы столбцов "Инвентарь" месяца

        /// <summary>
        ///     Признак отображения столбца "TRP" в группе столбцов "Инвентарь" месяца
        /// </summary>
        private readonly bool _isMonthTrp;

        /// <summary>
        ///     Признак отображения столбца "GivenTRP" в группе столбцов "Инвентарь" месяца
        /// </summary>
        private readonly bool _isMonthGivenTrp;

        /// <summary>
        ///     Признак отображения столбца "GRP" в группе столбцов "Инвентарь" месяца
        /// </summary>
        private readonly bool _isMonthGrp;

        /// <summary>
        ///     Признак отображения столбца "GivenGRP" в группе столбцов "Инвентарь" месяца
        /// </summary>
        private readonly bool _isMonthGivenGrp;

        /// <summary>
        ///     Признак отображения столбца "Minutes" в группе столбцов "Инвентарь" месяца
        /// </summary>
        private readonly bool _isMonthMinutes;

        #endregion

        #region Настройки группы столбцов "Выходы" месяца

        /// <summary>
        ///     Признак отображения столбца "Выходы по хронометражам" в группе столбцов "Выходы" месяца
        /// </summary>
        private readonly bool _isShowOutputsByTimings;

        /// <summary>
        ///     Признак отображения столбца "Выходы по качеству" в группе столбцов "Выходы" месяца
        /// </summary>
        private readonly bool _isShowOutputsByQuality;

        /// <summary>
        ///     Признак отображения столбца "Всего" в группе столбцов "Выходы" месяца
        /// </summary>
        private readonly bool _isShowOutputsTotal;

        /// <summary>
        ///     Признак отображения столбца "Активные дни" в группе столбцов "Выходы" месяца
        /// </summary>
        private readonly bool _isShowActiveDays;

        /// <summary>
        ///     Признак отображения столбца "Выходы в день" в группе столбцов "Выходы" месяца
        /// </summary>
        private readonly bool _isShowOutputsPerDay;

        #endregion

        #region Настройки группы столбцов "Бюджеты" месяца

        /// <summary>
        ///     Признак отображения столбца "Бюджет агентский" месяца
        /// </summary>
        private readonly bool _isShowBudgetAgency;

        #endregion

        #endregion

        #region Документ

        /// <summary>
        ///     Книга
        /// </summary>
        private readonly IWorkbook _workbook;

        /// <summary>
        ///     Лист
        /// </summary>
        private readonly Worksheet _worksheet;

        #endregion

        #region Вводные данные

        /// <summary>
        ///     Количество регионов
        /// </summary>
        private int _regionsCount;

        /// <summary>
        ///     Количество строк данных таблицы проекта
        /// </summary>
        private int _rowsCount;

        /// <summary>
        ///     Количество месяцев
        /// </summary>
        private int _monthsCount;

        /// <summary>
        ///     Количество хронометражей
        /// </summary>
        private int _timingsCount;

        /// <summary>
        ///     Индекс строки начала таблицы данных медиаплана
        /// </summary>
        private int _mediaPlanTableStartRow;

        /// <summary>
        ///     Месяца
        /// </summary>
        private readonly HashSet<DateTime> _months = new();

        /// <summary>
        ///     Хронометражи
        /// </summary>
        private readonly HashSet<int> _timings = new();

        #endregion

        #region Диапазоны ячеек

        /// <summary>
        ///     Диапазон ячеек данных проекта
        /// </summary>
        private CellRange? _projectDataCellRange;

        /// <summary>
        ///     Диапазон ячеек канала
        /// </summary>
        private CellRange? _baseChannelDataCellRange;

        /// <summary>
        ///     Диапазон ячеек группы столбцов "Распределение" канала
        /// </summary>
        private CellRange? _channelDistributionCellRange;

        /// <summary>
        ///     Диапазон ячеек группы столбцов "Распределение" месяца
        /// </summary>
        private CellRange? _monthDistributionCellRange;

        /// <summary>
        ///     Диапазон ячеек группы столбцов "Инвентарь" месяца
        /// </summary>
        private CellRange? _monthInventoryCellRange;

        /// <summary>
        ///     Диапазон ячеек группы столбцов "Выходы" месяца
        /// </summary>
        private CellRange? _monthOutputsCellRange;

        /// <summary>
        ///     Диапазон ячеек столбца "Бюджет" месяца
        /// </summary>
        private CellRange? _monthBudgetCellRange;

        /// <summary>
        ///     Диапазон ячеек столбца "бюджет агентский" месяца
        /// </summary>
        private CellRange? _monthBudgetAgencyCellRange;

        /// <summary>
        ///     Диапазон ячеек группы столбцов месяца
        /// </summary>
        private CellRange? _monthCellRange;

        /// <summary>
        ///     Диапазон ячеек группы столбцов "Инвентарь" в группе столбцов "Итого"
        /// </summary>
        private CellRange? _totalInventoryCellRange;

        /// <summary>
        ///     Диапазон ячеек группы столбцов "Выходы" в группе столбцов "Итого"
        /// </summary>
        private CellRange? _totalOutputsCellRange;

        /// <summary>
        ///     Диапазон ячеейки "Бюджет" в группе столбцов "Итого"
        /// </summary>
        private CellRange? _totalBudgetCellRange;

        /// <summary>
        ///     Диапазон ячейки "Бюджет агентский" в группе столбцов "Итого"
        /// </summary>
        private CellRange? _totalBudgetAgencyCellRange;

        /// <summary>
        ///     Диапазон ячеек группы столбцов "Итого"
        /// </summary>
        private CellRange? _totalCellRange;

        /// <summary>
        ///     Диапазон ячеек данных хронометражей регионов в месяцев
        /// </summary>
        private CellRange? _monthRegionToTimingRange;

        /// <summary>
        ///     Ячейки заголовков
        /// </summary>
        private readonly Collection<Cell> _headerCells = new();

        /// <summary>
        ///     Диапазоны ячеек заголовков
        /// </summary>
        private readonly Collection<CellRange> _headerCellRanges = new();

        /// <summary>
        ///     Диапазон ячеек строки "Итого"
        /// </summary>
        private readonly Collection<Cell> _totalSummaryCell = new();

        /// <summary>
        ///     Диапазон ячеек строки "Итого" в которых нет данных
        /// </summary>
        private readonly Collection<Cell> _totalSummaryNotCell = new();

        /// <summary>
        ///     Диапазон ячеек строки "Итого" регионов
        /// </summary>
        private readonly Collection<Cell> _summaryBudgetRowCell = new();

        /// <summary>
        ///     Диапазон ячеек строки "Итого" регионов в которых нет данных
        /// </summary>
        private readonly Collection<Cell> _summaryBudgetNotCell = new();

        /// <summary>
        ///     Ячейки "% Канал" каналов по регионам, для применения стиля
        /// </summary>
        private readonly Dictionary<int, List<Cell>> _regionToChannelsPortionCells = new();

        /// <summary>
        ///     Ячейка "Affinity" месяца по регионам, для применения стиля
        /// </summary>
        private readonly Dictionary<int, Dictionary<DateTime, List<Cell>>> _regionToMonthsAffinityCells = new();

        /// <summary>
        ///     Ячейка "Бюджет" месяца региона, для применения стиля
        /// </summary>
        private readonly Dictionary<int, Dictionary<DateTime, List<Cell>>> _monthsBudgetsCells = new();

        /// <summary>
        ///     Ячейка "Бюджет агентский" месяца региона, для применения стиля
        /// </summary>
        private readonly Dictionary<int, Dictionary<DateTime, List<Cell>>> _monthsAgencyBudgetsCells = new();

        /// <summary>
        ///     Ячейка "Бюджет" группы столбцов "Итого" региона
        /// </summary>
        private readonly Dictionary<int, List<Cell>> _totalBudgetsCells = new();

        /// <summary>
        ///     Ячейка "Бюджет агентский" группы столбцов "Итого" региона
        /// </summary>
        private readonly Dictionary<int, List<Cell>> _totalAgencyBudgetsCells = new();

        #endregion

        #endregion

        #region Методы

        #region Метод определения количественных данных

        private void DefineCounters() {
            // Обнуляем значение количества строк данных
            _rowsCount = 0;
            // Обнуляем значение количетсва месяцев
            _monthsCount = 0;
            // Обнуляем значение количетсва хронометражей
            _timingsCount = 0;

            for (var r = 0; r < _project.Regions.Count; r++) {
                if (_project.Regions[r].Outputs == 0.0) {
                    continue;
                }

                for (var c = 0; c < _project.Regions[r].Channels.Count; c++) {
                    if (
                        !_project.Regions[r].Channels[c].IsActive ||
                        _project.Regions[r].Channels[c].Outputs == 0.0
                    ) {
                        continue;
                    }

                    for (var m = 0; m < _project.Regions[r].Channels[c].Months.Count; m++) {
                        if (
                            !_project.Regions[r].Channels[c].Months[m].IsActive ||
                            _project.Regions[r].Channels[c].Months[m].Outputs == 0.0
                        ) {
                            continue;
                        }

                        for (var w = 0; w < _project.Regions[r].Channels[c].Months[m].Weeks.Count; w++) {
                            if (
                                !_project.Regions[r].Channels[c].Months[m].Weeks[w].IsActive ||
                                _project.Regions[r].Channels[c].Months[m].Weeks[w].Outputs == 0.0
                            ) {
                                continue;
                            }

                            for (var t = 0; t < _project.Regions[r].Channels[c].Months[m].Weeks.Count; t++) {
                                if (
                                    !_project.Regions[r].Channels[c].Months[m].Weeks[w].Timings[t].IsActive ||
                                    _project.Regions[r].Channels[c].Months[m].Weeks[w].Timings[t].Outputs == 0.0
                                ) {
                                    continue;
                                }

                                // Добавляем хронометраж
                                _timings.Add(_project.Regions[r].Channels[c].Months[m].Weeks[w].Timings[t].Value);
                            }
                        }

                        // Добавляем месяц
                        _months.Add(_project.Regions[r].Channels[c].Months[m].Date);
                    }

                    // + строка канала
                    _rowsCount++;
                }

                // + итоговая строка региона
                _rowsCount++;
            }

            // + итоговая строка проекта
            _rowsCount++;

            // Определяем количество месяцев
            _monthsCount = _months.Count;

            // Определяем количество хронометражей
            _timingsCount = _timings.Count;
        }

        #endregion

        #region Методы определения диапазонов ячеек

        /// <summary>
        ///     Метод определения диапазонов ячеек данных таблицы проекта
        /// </summary>
        private void DefineCellRanges() {
            // Определяем диапазон ячеек данных проекта
            DefineProjectDataCellRange();
            // Определяем индекс строки начала формирования таблицы медиаплана
            DefineMediaPlanTableStartRow();
            // Определяем диапазон ячеек данных канала
            DefineChannelCellRange();
            // Определяем диапазон ячеек группы столбцов "Распределение" канала
            DefineChannelDistributionCellRange();
            // Определяем диапазон ячеек группы столбцов "Распределение" месяца
            DefineMonthDistributionCellRange();
            // Определяем диапазон ячеек группы столбцов "Инвентарь" месяца
            DefineMonthInventoryCellRange();
            // Определяем диапазон ячеек группы столбцов "Выходы" месяца
            DefineMonthOutputsCellRange();
            // Определяем диапазон ячеек бюджета месяца
            DefineMonthBudgetCellRange();
            // Определяем диапазон ячеек бюджета агентского месяца
            DefineMonthBudgetAgencyCellRange();
            // Определяем диапазон ячеек месяца
            DefineMonthCellRange();
            // Определям диапазон ячеек группы стобцов "Инвентарь" итого
            DefineTotalInventoryCellRange();
            // Определям диапазон ячеек группы стобцов "Выходы" итого
            DefineTotalOutputsCellRange();
            // Определяем диапазон ячеек бюджета итого
            DefineTotalBudgetCellRange();
            // Определяем диапазон ячеек бюджета агентского итого
            DefineTotalBudgetAgencyCellRange();
            // Определяем диапазон ячеек итого
            DefineTotalCellRange();
            // Определяем диапазон ячеек данных хронометражей месяца региона
            DefineMonthRegionToTimingRange();
        }

        /// <summary>
        ///     Метод определения диапазона ячеек данных проекта
        /// </summary>
        private void DefineProjectDataCellRange() => _projectDataCellRange = _worksheet.Range.FromLTRB(0, 0, 1, 3);

        /// <summary>
        ///     Метод определения индекса строки начала формирования таблицы медиаплана
        /// </summary>
        private void DefineMediaPlanTableStartRow() {
            _mediaPlanTableStartRow = _projectDataCellRange?.BottomRowIndex + 2 ?? 0;

            _regionsCount = _project.Regions.Count(r => r.Outputs > 0.0);
            if (_regionsCount > 2) {
                _mediaPlanTableStartRow = _regionsCount + 2 + 2;
            }
        }

        /// <summary>
        ///     Метод определения диапазона ячеек данных каналов
        /// </summary>
        private void DefineChannelCellRange() {
            if (!_isShowRegion && !_isShowChannel && !_isShowChannelSaleType) {
                return;
            }

            // Количество столбцов данных канала
            var headersCount = 0;

            // Если отображается регион
            if (_isShowRegion) {
                headersCount++;
            }

            // Если отображается канал
            if (_isShowChannel) {
                headersCount++;
            }

            // Если отображается тип продаж
            if (_isShowChannelSaleType) {
                headersCount++;
            }

            // Определяем диапазон ячеек данных канала
            _baseChannelDataCellRange = _worksheet.Range.FromLTRB(
                0, _mediaPlanTableStartRow,
                headersCount - 1, _mediaPlanTableStartRow + 3 - 1
            );
        }

        /// <summary>
        ///     Метод определения диапазона ячеек группы столбцов "Распределение" канала
        /// </summary>
        private void DefineChannelDistributionCellRange() {
            // Если никакие данные не будут отображены в группе столбцов "Распределение" канала - завершаем метод
            if (!_isShowChannelPortionDistribution && !_isShowChannelQualityDistribution) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow;

            // Количество заголовков в группе столбцов "Распределение" канала
            var headersCount = 0;

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex;
            }

            // Если отображается "% Канала"
            if (_isShowChannelPortionDistribution) {
                headersCount++;
            }

            // Если отображается ""% Float, %Float P., % Fix, % Fix P., % S.Fix, % S.Fix P."
            if (_isShowChannelQualityDistribution) {
                headersCount += 4;
            }

            // Определяем диапазон ячеек группы столбцов "Распределение" канала
            _channelDistributionCellRange = _worksheet.Range.FromLTRB(
                startCol, startRow,
                startCol + headersCount - 1, startRow
            );
        }

        /// <summary>
        ///     Метод определения диапазона ячеек группы столбцов "Распределение" месяца
        /// </summary>
        private void DefineMonthDistributionCellRange() {
            // Если никакие данные не буду отображены в группе столбцов "Распределение" месяца - завершаем метод
            if (!_isShowMonthAffinity && !_isShowMonthQualityDistribution) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow + 1; // Потому что сверху будет заголовок названия месяца

            // Количество заголовков в группе столбцов "Распределение" месяца
            var headersCount = 0;

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек данных распределения канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
                startRow = _channelDistributionCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если отображается "Affinity"
            if (_isShowMonthAffinity) {
                headersCount++;
            }

            // Если отображается ""% Float, %Float P., % Fix, % Fix P., % S.Fix, % S.Fix P."
            if (_isShowMonthQualityDistribution) {
                headersCount += 4;
            }

            // Определяем диапазон ячеек группы столбцов "Распределение" месяца
            _monthDistributionCellRange = _worksheet.Range.FromLTRB(
                startCol, startRow,
                startCol + headersCount - 1, startRow
            );
        }

        /// <summary>
        ///     Метод определения диапазона ячеек группы столбцов "Инвентарь" месяца
        /// </summary>
        private void DefineMonthInventoryCellRange() {
            // Если никакие данные не буду отображены в группе столбцов "Инвентарь" месяца - завершаем метод
            if (!_isMonthTrp && !_isMonthGivenTrp && !_isMonthGrp && !_isMonthGivenGrp && !_isMonthMinutes) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow + 1; // Потому что сверху будет заголовок названия месяца

            // Количество заголовков в группе столбцов "Инвентарь" месяца
            var headersCount = 0;

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек данных распределения канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
                startRow = _channelDistributionCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек группы столбцов "Распределение" месяца
            if (_monthDistributionCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthDistributionCellRange.RightColumnIndex + 1;
                startRow = _monthDistributionCellRange.TopRowIndex;
            }

            // Если отображается "TRP"
            if (_isMonthTrp) {
                headersCount++;
            }

            // Если отображается "GivenTRP"
            if (_isMonthGivenTrp) {
                headersCount++;
            }

            // Если отображается "GRP"
            if (_isMonthGrp) {
                headersCount++;
            }

            // Если отображается "GivenGRP"
            if (_isMonthGivenGrp) {
                headersCount++;
            }

            // Если отображается "Minutes"
            if (_isMonthMinutes) {
                headersCount++;
            }

            // Определяем диапазон ячеек группы столбцов "Распределение" месяца
            _monthInventoryCellRange = _worksheet.Range.FromLTRB(
                startCol, startRow,
                startCol + headersCount - 1, startRow
            );
        }

        /// <summary>
        ///     Метод определения диапазона ячеек группы столбцов "Выходы" месяца
        /// </summary>
        private void DefineMonthOutputsCellRange() {
            // Если никакие данные не буду отображены в группе столбцов "Выходы" месяца - завершаем метод
            if (
                !_isShowOutputsByTimings &&
                !_isShowOutputsByQuality &&
                !_isShowOutputsTotal     &&
                !_isShowActiveDays       &&
                !_isShowOutputsPerDay
            ) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow + 1; // Потому что сверху будет заголовок названия месяца

            // Количество заголовков в группе столбцов "Выходы" месяца
            var headersCount = 0;

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек данных распределения канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
                startRow = _channelDistributionCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек группы столбцов "Распределение" месяца
            if (_monthDistributionCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthDistributionCellRange.RightColumnIndex + 1;
                startRow = _monthDistributionCellRange.TopRowIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Инвентарь" месяца
            if (_monthInventoryCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthInventoryCellRange.RightColumnIndex + 1;
                startRow = _monthInventoryCellRange.TopRowIndex;
            }

            // Если отображается "По хронометражам"
            if (_isShowOutputsByTimings) {
                headersCount += _timingsCount;
            }

            // Если отображается "По качеству"
            if (_isShowOutputsByQuality) {
                headersCount += 2;
            }

            // Если отображается "Всего"
            if (_isShowOutputsTotal) {
                headersCount++;
            }

            // Если отображается "Активные дни"
            if (_isShowActiveDays) {
                headersCount++;
            }

            // Если отображается "В день"
            if (_isShowOutputsPerDay) {
                headersCount++;
            }

            // Определяем диапазон ячеек группы столбцов "Выходы" месяца
            _monthOutputsCellRange = _worksheet.Range.FromLTRB(
                startCol, startRow,
                startCol + headersCount - 1, startRow
            );
        }

        /// <summary>
        ///     Определяем диапазон ячеек бюджета месяца
        /// </summary>
        private void DefineMonthBudgetCellRange() {
            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow + 1; // Потому что сверху будет заголовок названия месяца

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек данных распределения канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
                startRow = _channelDistributionCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек группы столбцов "Распределение" месяца
            if (_monthDistributionCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthDistributionCellRange.RightColumnIndex + 1;
                startRow = _monthDistributionCellRange.TopRowIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Инвентарь" месяца
            if (_monthInventoryCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthInventoryCellRange.RightColumnIndex + 1;
                startRow = _monthInventoryCellRange.TopRowIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Выходы" месяца
            if (_monthOutputsCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthOutputsCellRange.RightColumnIndex + 1;
                startRow = _monthOutputsCellRange.TopRowIndex;
            }

            // Определяем диапазон ячеек столбца "Бюджет" месяца
            _monthBudgetCellRange = _worksheet.Range.FromLTRB(startCol, startRow, startCol, startRow + 1);
        }

        /// <summary>
        ///     Определяем диапазон ячеек бюджета агентского месяца
        /// </summary>
        private void DefineMonthBudgetAgencyCellRange() {
            if (!_isShowBudgetAgency) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow + 1; // Потому что сверху будет заголовок названия месяца

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек данных распределения канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
                startRow = _channelDistributionCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек группы столбцов "Распределение" месяца
            if (_monthDistributionCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthDistributionCellRange.RightColumnIndex + 1;
                startRow = _monthDistributionCellRange.TopRowIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Инвентарь" месяца
            if (_monthInventoryCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthInventoryCellRange.RightColumnIndex + 1;
                startRow = _monthInventoryCellRange.TopRowIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Выходы" месяца
            if (_monthOutputsCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthOutputsCellRange.RightColumnIndex + 1;
                startRow = _monthOutputsCellRange.TopRowIndex;
            }

            if (_monthBudgetCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthBudgetCellRange.RightColumnIndex + 1;
                startRow = _monthBudgetCellRange.TopRowIndex;
            }

            // Определяем диапазон ячеек столбца "Бюджет агентский" месяца
            _monthBudgetAgencyCellRange = _worksheet.Range.FromLTRB(startCol, startRow, startCol, startRow + 1);
        }

        /// <summary>
        ///     Метод определения диапазона ячеек группы столбцов месяца
        /// </summary>
        private void DefineMonthCellRange() {
            // Если никакие данные в месяце не будут отображены - завершаем метод
            // TODO дополнить
            if (_monthDistributionCellRange == null &&
                _monthInventoryCellRange    == null &&
                _monthOutputsCellRange      == null &&
                _monthBudgetCellRange       == null &&
                _monthBudgetAgencyCellRange == null
            ) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow;

            // Если определён диапазон ячеек канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
            }

            // Если определён диапазон ячеек группы столбцов "Распределение" канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
            }

            // Индекс последнего столбца вложенных столбцов группы
            var lastCol = 0;

            // Если определён диапазон ячеек группы столбцов "Распределение" месяца
            if (_monthDistributionCellRange != null) {
                lastCol = _monthDistributionCellRange.RightColumnIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Инвентарь" месяца
            if (_monthInventoryCellRange != null) {
                lastCol = _monthInventoryCellRange.RightColumnIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Выходы" месяца
            if (_monthOutputsCellRange != null) {
                lastCol = _monthOutputsCellRange.RightColumnIndex;
            }

            // Если определён диапазон ячеек столбца "Бюджет" месяца
            if (_monthBudgetCellRange != null) {
                lastCol = _monthBudgetCellRange.RightColumnIndex;
            }

            // Если определён диапазон ячеек столбца "Бюджет агентский" месяца
            if (_monthBudgetAgencyCellRange != null) {
                lastCol = _monthBudgetAgencyCellRange.RightColumnIndex;
            }

            // Определяем диапазон ячеек группы столбцов месяца
            _monthCellRange = _worksheet.Range.FromLTRB(startCol, startRow, lastCol, startRow);
        }

        /// <summary>
        ///     Метод определения диапазона ячеек группы столбцов "Инвентарь" в группе столбцов "Итого"
        /// </summary>
        private void DefineTotalInventoryCellRange() {
            // Если никакие данные не будут отображаться в группе "Инвентарь" - завершаем метод;
            if (_monthInventoryCellRange == null) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow + 1; // Потому что сверху будет заголовок "Итого"

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия итого
            }

            // Если определён диапазон ячеек данных распределения канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
                startRow = _channelDistributionCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек данных месяца
            if (_monthCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthCellRange.RightColumnIndex + 1 + ((_monthsCount - 1) * _monthCellRange.ColumnCount);
                startRow = _monthCellRange.TopRowIndex      + 1; // Потому что сверху будет заголовок названия месяца;
            }

            // Определяем диапазон ячеек группы столбцов "Инвентарь" в группе столбцов "Итого"
            _totalInventoryCellRange = _worksheet.Range.FromLTRB(
                startCol,
                startRow,
                startCol + _monthInventoryCellRange.ColumnCount - 1,
                startRow
            );
        }

        /// <summary>
        ///     Метод определения диапозона ячеек группы столбцов "Выходы" в группе столбцов "Итого"
        /// </summary>
        private void DefineTotalOutputsCellRange() {
            // Если никакие данные не буду отображены в группе столбцов "Выходы" - завершаем метод
            if (_monthOutputsCellRange == null) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow + 1; // Потому что сверху будет заголовок "Итого"

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек данных распределения канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
                startRow = _channelDistributionCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия месяца
            }

            // Если определён диапазон ячеек группы столбцов месяца
            if (_monthCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthCellRange.RightColumnIndex + 1 + ((_monthsCount - 1) * _monthCellRange.ColumnCount);
                startRow = _monthCellRange.TopRowIndex      + 1;
            }

            // Если определён диапазон ячеек группы столбцов "Инвентарь" группы столбцов "Итого"
            if (_totalInventoryCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _totalInventoryCellRange.RightColumnIndex + 1;
                startRow = _totalInventoryCellRange.TopRowIndex;
            }

            // Определяем диапазон ячеек группы столбцов "Выходы" в группе столбцов "Итого"
            _totalOutputsCellRange = _worksheet.Range.FromLTRB(
                startCol,
                startRow,
                startCol + _monthOutputsCellRange.ColumnCount - 1,
                startRow
            );
        }

        /// <summary>
        ///     Метод определения диапазон ячейки "Бюджет" в группе столбцов "Итого"
        /// </summary>
        private void DefineTotalBudgetCellRange() {
            // Если никакие данные не буду отображены в группе столбцов "Бюджет" - завершаем метод
            if (_monthBudgetCellRange == null) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow + 1; // Потому что сверху будет заголовок "Итого"

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия "Итого";
            }

            // Если определён диапазон ячеек данных распределения канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
                startRow = _channelDistributionCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия "Итого";
            }

            // Если определён диапазон ячеек группы столбцов "Распределение" месяца
            if (_monthCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthCellRange.RightColumnIndex + 1 + ((_monthsCount - 1) * _monthCellRange.ColumnCount);
                startRow = _monthCellRange.TopRowIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Инвентарь" группы столбцов "Итого"
            if (_totalInventoryCellRange != null) {
                startCol = _totalInventoryCellRange.RightColumnIndex + 1;
                startRow = _totalInventoryCellRange.TopRowIndex;
            }

            //  Если определён диапазон ячеек группы столбцов "Выходы" группы столбцов "Итого"
            if (_totalOutputsCellRange != null) {
                startCol = _totalOutputsCellRange.RightColumnIndex + 1;
                startRow = _totalOutputsCellRange.TopRowIndex;
            }

            _totalBudgetCellRange = _worksheet.Range.FromLTRB(startCol, startRow, startCol, startRow + 1);
        }

        private void DefineTotalBudgetAgencyCellRange() {
            // Если никакие данные не буду отображены в группе столбцов "Бюджет" - завершаем метод
            if (_monthBudgetCellRange == null || !_isShowBudgetAgency) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow + 1; // Потому что сверху будет заголовок "Итого"

            // Если определён диапазон ячеек данных канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
                startRow = _baseChannelDataCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия "Итого";
            }

            // Если определён диапазон ячеек данных распределения канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
                startRow = _channelDistributionCellRange.TopRowIndex +
                    1; // Потому что сверху будет заголовок названия "Итого";
            }

            // Если определён диапазон ячеек группы столбцов "Распределение" месяца
            if (_monthCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthCellRange.RightColumnIndex + 1 + ((_monthsCount - 1) * _monthCellRange.ColumnCount);
                startRow = _monthCellRange.TopRowIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Инвентарь" группы столбцов "Итого"
            if (_totalInventoryCellRange != null) {
                startCol = _totalInventoryCellRange.RightColumnIndex + 1;
                startRow = _totalInventoryCellRange.TopRowIndex;
            }

            //  Если определён диапазон ячеек группы столбцов "Выходы" группы столбцов "Итого"
            if (_totalOutputsCellRange != null) {
                startCol = _totalOutputsCellRange.RightColumnIndex + 1;
                startRow = _totalOutputsCellRange.TopRowIndex;
            }

            // Если определён диапазон ячеек "Бюджет"
            if (_totalBudgetCellRange != null) {
                startCol = _totalBudgetCellRange.RightColumnIndex + 1;
                startRow = _totalBudgetCellRange.TopRowIndex;
            }

            // Определяем ячейку "Бюджет агентский"
            _totalBudgetAgencyCellRange = _worksheet.Range.FromLTRB(startCol, startRow, startCol, startRow + 1);
        }

        /// <summary>
        ///     Метод определения диапазона ячееек группы столбцов "Итого"
        /// </summary>
        private void DefineTotalCellRange() {
            if (_totalInventoryCellRange    == null &&
                _totalOutputsCellRange      == null &&
                _totalBudgetCellRange       == null &&
                _totalBudgetAgencyCellRange == null
            ) {
                return;
            }

            // Индексы начала построения диапазона ячеек
            var startCol = 0;
            var startRow = _mediaPlanTableStartRow;

            // Если определён диапазон ячеек канала
            if (_baseChannelDataCellRange != null) {
                // Переопределям начальные индексы
                startCol = _baseChannelDataCellRange.RightColumnIndex + 1;
            }

            // Если определён диапазон ячеек группы столбцов "Распределение" канала
            if (_channelDistributionCellRange != null) {
                // Переопределям начальные индексы
                startCol = _channelDistributionCellRange.RightColumnIndex + 1;
            }

            // Если определён диапазон ячеек группы столбцов месяца
            if (_monthCellRange != null) {
                // Переопределяем начальные индексы
                startCol = _monthCellRange.RightColumnIndex + 1 + ((_monthsCount - 1) * _monthCellRange.ColumnCount);
            }

            // Индекс последнего столбца вложенных столбцов группы
            var lastCol = 0;

            // Если определён диапазон ячеек группы столбцов "Инвентарь" "Итого"
            if (_totalInventoryCellRange != null) {
                lastCol = _totalInventoryCellRange.RightColumnIndex;
            }

            // Если определён диапазон ячеек группы столбцов "Выходы" "Итого"
            if (_totalOutputsCellRange != null) {
                lastCol = _totalOutputsCellRange.RightColumnIndex;
            }

            // Если определён диапазон ячеек столбца "Бюджет" "Итого"
            if (_totalBudgetCellRange != null) {
                lastCol = _totalBudgetCellRange.RightColumnIndex;
            }

            // Если определён диапазон ячеек столбца "Бюджет агентский" "Итого"
            if (_totalBudgetAgencyCellRange != null) {
                lastCol = _totalBudgetAgencyCellRange.RightColumnIndex;
            }

            // Определяем диапазон ячеек группы столбцов месяца
            _totalCellRange = _worksheet.Range.FromLTRB(startCol, startRow, lastCol, startRow);
        }

        /// <summary>
        ///     Метод определения диапазоная ячеек данных хронометражей месяца региона
        /// </summary>
        private void DefineMonthRegionToTimingRange() {
            // Если диапазон ячеек месяца не определён - завершаем выполнение метода
            if (_monthCellRange == null) {
                return;
            }

            _monthRegionToTimingRange = _worksheet.Range.FromLTRB(
                _monthCellRange.LeftColumnIndex, _monthCellRange.TopRowIndex - 1 - _regionsCount - 2,
                _monthCellRange.LeftColumnIndex + _timingsCount + 2, _monthCellRange.TopRowIndex - 1 - 3 + _regionsCount
            );
        }

        #endregion

        #region Метод инициализации создания документа

        /// <summary>
        ///     Метод инициализации создания документа
        /// </summary>
        /// <returns>Документ медиаплана</returns>
        public async Task<IWorkbook> CreateDocument() {
            // Начало заполнения документа
            _workbook.BeginUpdate();

            SendMessage(new EventStatusBarMessage("Сохранение отчета: заполнение заголовков!"));
            await Task.Delay(100);

            // Запроеяем заголовки данных проекта
            await FillProjectDataHeaders();
            // Заполняем заголовки канала
            await FillChannelHeaders();
            // Заполняем заголовки группы столбцов "Распределение" канала
            await FillChannelDistributionHeaders();
            // Заполняем заголовки групп столбцов месяцев
            await FillMonthsHeaders();
            // Заполняем заголовки групп столбцов Итого
            await FillTotalHeaders();
            // Заполняем заголовки групп столбцов "Распределение" месяцев
            await FillMonthsDistributionHeaders();
            // Заполняем заголовки групп столбцов "Инвентарь" месяцев
            await FillMonthsInventoryHeaders();
            // Заполняем заголвоки групп столбцов "Выходы" месяцев
            await FillMonthsOutputsHeaders();
            // Заполняем заголовки столбцов "Бюджет" месяцев
            await FillMonthsBudgetHeaders();
            // Заполняем заголовки столбцов "Бюджет агентский" месяцев
            await FillMonthsBudgetAgencyHeaders();
            // Заполняем заголовки групп столбцов "Инвентарь" итого
            await FillTotalInventoryHeaders();
            // Заполняем заголовки групп столбцов "Выходы" итого
            await FillTotalOutputsHeaders();
            // Заполняем заголовки столбцов "Бюджет" итого
            await FillTotalBudgetHeaders();
            // Заполняем заголовки столбцов "Бюджет агентский" итого
            await FillTotalBudgetAgencyHeaders();
            // Заполняем заголовки данных хронометражей в месяце регионов
            await FillMonthsRegionsToTimingsHeaders();
            // Применяем стиль на заголовки
            await StyleHeaders();

            // Метод заполнения данных
            await FillData();

            // Прокручиваем в начало документа
            _worksheet.ScrollTo(0, 0);
            // Авто высота строк
            _worksheet.GetDataRange().AutoFitRows();
            // Авто ширина столбцов
            _worksheet.GetDataRange().AutoFitColumns();
            _worksheet[0, 0].ColumnWidthInCharacters = 20;
            // Конец заполнения документа
            _workbook.EndUpdate();

            // Передаём документ
            return _workbook;
        }

        #endregion

        #region Методы заполнения заголовков

        #region Метод заполнения заголовков данных проекта

        /// <summary>
        ///     Метод заполнения заголовков данных проекта
        /// </summary>
        private Task FillProjectDataHeaders() {
            if (_projectDataCellRange == null) {
                return Task.CompletedTask;
            }

            var row = _projectDataCellRange.TopRowIndex;
            var col = _projectDataCellRange.LeftColumnIndex;

            Cell cell = _worksheet[row, col];
            cell.Value = ColumnHeaders.Client;
            _headerCells.Add(cell);

            row++;

            cell = _worksheet[row, col];
            cell.Value = ColumnHeaders.Period;
            _headerCells.Add(cell);

            row++;

            cell = _worksheet[row, col];
            cell.Value = ColumnHeaders.Timings;
            _headerCells.Add(cell);

            row++;

            cell = _worksheet[row, col];
            cell.Value = ColumnHeaders.TargetAudience;
            _headerCells.Add(cell);

            row++;

            cell = _worksheet[row, col];
            cell.Value = ColumnHeaders.IDProject;
            cell.Font.Color = Color.White;


            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков канала

        /// <summary>
        ///     Метод заполнения заголовков канала
        /// </summary>
        private Task FillChannelHeaders() {
            if (_baseChannelDataCellRange == null) {
                return Task.CompletedTask;
            }

            CellRange cellRange;
            var row = _baseChannelDataCellRange.TopRowIndex;
            var col = _baseChannelDataCellRange.LeftColumnIndex;

            if (_isShowRegion) {
                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 2);
                cellRange.Value = ColumnHeaders.Region;
                _headerCellRanges.Add(cellRange);

                col++;
            }

            if (_isShowChannel) {
                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 2);
                cellRange.Value = ColumnHeaders.Channel;
                _headerCellRanges.Add(cellRange);

                col++;
            }

            if (_isShowChannelSaleType) {
                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 2);
                cellRange.Value = ColumnHeaders.SaleType;
                _headerCellRanges.Add(cellRange);
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков группы столбцов "Распределение" канала

        /// <summary>
        ///     Метод заполнения заголовков группы столбцов "Распределение" канала
        /// </summary>
        /// <returns></returns>
        private Task FillChannelDistributionHeaders() {
            if (_channelDistributionCellRange == null) {
                return Task.CompletedTask;
            }

            var row = _channelDistributionCellRange.TopRowIndex;
            var col = _channelDistributionCellRange.LeftColumnIndex;

            CellRange cellRange =
                _worksheet.Range.FromLTRB(col, row, _channelDistributionCellRange.RightColumnIndex, row);
            cellRange.Value = BandHeaders.Distribution;
            _headerCellRanges.Add(cellRange);

            row++;

            if (_isShowChannelPortionDistribution) {
                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 1);
                cellRange.Value = ColumnHeaders.ChannelPortion;
                _headerCellRanges.Add(cellRange);

                col++;
            }

            if (_isShowChannelQualityDistribution) {
                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 1);
                cellRange.Value = _project.IsFederal
                    ? ColumnHeaders.FixPortion
                    : ColumnHeaders.FloatPortion;
                _headerCellRanges.Add(cellRange);

                col++;

                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 1);
                cellRange.Value = _project.IsFederal
                    ? ColumnHeaders.FixPrimePortion
                    : ColumnHeaders.FloatPrimePortion;
                _headerCellRanges.Add(cellRange);

                col++;

                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 1);
                cellRange.Value = _project.IsFederal
                    ? ColumnHeaders.SuperFixPortion
                    : ColumnHeaders.FixPortion;
                _headerCellRanges.Add(cellRange);

                col++;

                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 1);
                cellRange.Value = _project.IsFederal
                    ? ColumnHeaders.SuperFixPrimePortion
                    : ColumnHeaders.FixPrimePortion;
                _headerCellRanges.Add(cellRange);
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков месяцев

        /// <summary>
        ///     Метод заполнения заголовков группы столбцов месяца
        /// </summary>
        private Task FillMonthsHeaders() {
            if (_monthCellRange == null) {
                return Task.CompletedTask;
            }

            for (var month = 0; month < _monthsCount; month++) {
                var increment = _monthCellRange.ColumnCount * month;
                CellRange cellRange = _worksheet.Range.FromLTRB(
                    month == 0 ? _monthCellRange.LeftColumnIndex : _monthCellRange.LeftColumnIndex + increment,
                    _monthCellRange.TopRowIndex,
                    month == 0 ? _monthCellRange.RightColumnIndex : _monthCellRange.RightColumnIndex + increment,
                    _monthCellRange.TopRowIndex
                );
                cellRange.Value = _months.ToList()[month].ToString("Y");
                _headerCellRanges.Add(cellRange);
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков иотого

        private Task FillTotalHeaders() {
            if (_totalCellRange == null) {
                return Task.CompletedTask;
            }

            CellRange cellRange = _worksheet.Range.FromLTRB(
                _totalCellRange.LeftColumnIndex,
                _totalCellRange.TopRowIndex,
                _totalCellRange.RightColumnIndex,
                _totalCellRange.TopRowIndex
            );
            cellRange.Value = BandHeaders.Summary;

            _headerCellRanges.Add(cellRange);

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков групп столбцов "Распределение" месяцев

        /// <summary>
        ///     Метод заполнения заголовков групп столбцов "Распределение" месяцев
        /// </summary>
        private Task FillMonthsDistributionHeaders() {
            if (_monthCellRange == null || _monthDistributionCellRange == null) {
                return Task.CompletedTask;
            }

            for (var month = 0; month < _monthsCount; month++) {
                var increment = _monthCellRange.ColumnCount * month;
                var row = _monthDistributionCellRange.TopRowIndex;
                var leftCol = month == 0
                    ? _monthDistributionCellRange.LeftColumnIndex
                    : _monthDistributionCellRange.LeftColumnIndex + increment;
                var rightCol = month == 0
                    ? _monthDistributionCellRange.RightColumnIndex
                    : _monthDistributionCellRange.RightColumnIndex + increment;
                CellRange cellRange = _worksheet.Range.FromLTRB(leftCol, row, rightCol, row);
                cellRange.Value = BandHeaders.Distribution;
                _headerCellRanges.Add(cellRange);

                row++;

                Cell cell;
                if (_isShowMonthAffinity) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = ColumnHeaders.Affinity;
                    _headerCells.Add(cell);

                    leftCol++;
                }

                if (_isShowMonthQualityDistribution) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = _project.IsFederal
                        ? ColumnHeaders.FixPortion
                        : ColumnHeaders.FloatPortion;
                    _headerCells.Add(cell);

                    leftCol++;

                    cell = _worksheet[row, leftCol];
                    cell.Value = _project.IsFederal
                        ? ColumnHeaders.FixPrimePortion
                        : ColumnHeaders.FloatPrimePortion;
                    _headerCells.Add(cell);

                    leftCol++;

                    cell = _worksheet[row, leftCol];
                    cell.Value = _project.IsFederal
                        ? ColumnHeaders.SuperFixPortion
                        : ColumnHeaders.FixPortion;
                    _headerCells.Add(cell);

                    leftCol++;

                    cell = _worksheet[row, leftCol];
                    cell.Value = _project.IsFederal
                        ? ColumnHeaders.SuperFixPrimePortion
                        : ColumnHeaders.FixPrimePortion;
                    _headerCells.Add(cell);
                }
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков групп столбцов "Инвентарь" месяцев

        /// <summary>
        ///     Метод заполнения заголовков групп столбцов "Инвентарь" месяцев
        /// </summary>
        private Task FillMonthsInventoryHeaders() {
            if (_monthCellRange == null || _monthInventoryCellRange == null) {
                return Task.CompletedTask;
            }

            for (var month = 0; month < _monthsCount; month++) {
                var increment = _monthCellRange.ColumnCount * month;
                var row = _monthInventoryCellRange.TopRowIndex;
                var leftCol = month == 0
                    ? _monthInventoryCellRange.LeftColumnIndex
                    : _monthInventoryCellRange.LeftColumnIndex + increment;
                var rightCol = month == 0
                    ? _monthInventoryCellRange.RightColumnIndex
                    : _monthInventoryCellRange.RightColumnIndex + increment;
                CellRange cellRange = _worksheet.Range.FromLTRB(leftCol, row, rightCol, row);
                cellRange.Value = BandHeaders.Inventory;
                _headerCellRanges.Add(cellRange);

                row++;

                Cell cell;
                if (_isMonthTrp) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = ColumnHeaders.TRP;
                    _headerCells.Add(cell);

                    leftCol++;
                }

                if (_isMonthGivenTrp) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = $"{ColumnHeaders.TRP} {_project.BaseTiming}";
                    _headerCells.Add(cell);

                    leftCol++;
                }

                if (_isMonthGrp) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = ColumnHeaders.GRP;
                    _headerCells.Add(cell);

                    leftCol++;
                }

                if (_isMonthGivenGrp) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = $"{ColumnHeaders.GRP} {_project.BaseTiming}";
                    _headerCells.Add(cell);

                    leftCol++;
                }

                if (_isMonthMinutes) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = ColumnHeaders.Minutes;
                    _headerCells.Add(cell);
                }
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков групп столбцов "Выходы" месяцев

        /// <summary>
        ///     Метод заполнения заголовков групп столбцов "Выходы" месяцев
        /// </summary>
        private Task FillMonthsOutputsHeaders() {
            if (_monthCellRange == null || _monthOutputsCellRange == null) {
                return Task.CompletedTask;
            }

            for (var month = 0; month < _monthsCount; month++) {
                var increment = _monthCellRange.ColumnCount * month;
                var row = _monthOutputsCellRange.TopRowIndex;
                var leftCol = month == 0
                    ? _monthOutputsCellRange.LeftColumnIndex
                    : _monthOutputsCellRange.LeftColumnIndex + increment;
                var rightCol = month == 0
                    ? _monthOutputsCellRange.RightColumnIndex
                    : _monthOutputsCellRange.RightColumnIndex + increment;
                CellRange cellRange = _worksheet.Range.FromLTRB(leftCol, row, rightCol, row);
                cellRange.Value = BandHeaders.Outputs;
                _headerCellRanges.Add(cellRange);

                row++;

                Cell cell;
                if (_isShowOutputsByTimings) {
                    for (var timing = 0; timing < _timings.Count; timing++) {
                        cell = _worksheet[row, leftCol];
                        cell.Value = $"{_timings.ToList()[timing]} {ColumnHeaders.Sec}";
                        _headerCells.Add(cell);

                        leftCol++;
                    }
                }

                if (_isShowOutputsByQuality) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = ColumnHeaders.Prime;
                    _headerCells.Add(cell);

                    leftCol++;

                    cell = _worksheet[row, leftCol];
                    cell.Value = ColumnHeaders.OffPrime;
                    _headerCells.Add(cell);

                    leftCol++;
                }

                if (_isShowOutputsTotal) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = ColumnHeaders.Total;
                    _headerCells.Add(cell);

                    leftCol++;
                }

                if (_isShowActiveDays) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = ColumnHeaders.ActiveDays;
                    _headerCells.Add(cell);

                    leftCol++;
                }

                if (_isShowOutputsPerDay) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = ColumnHeaders.PerDay;
                    _headerCells.Add(cell);
                }
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков столбцов "Бюджет" месяцев

        /// <summary>
        ///     Метод заполнения заголовков столбцов "Бюджет" месяцев
        /// </summary>
        private Task FillMonthsBudgetHeaders() {
            if (_monthCellRange == null || _monthBudgetCellRange == null) {
                return Task.CompletedTask;
            }

            for (var month = 0; month < _monthsCount; month++) {
                var increment = _monthCellRange.ColumnCount * month;
                var leftCol = month == 0
                    ? _monthBudgetCellRange.LeftColumnIndex
                    : _monthBudgetCellRange.LeftColumnIndex + increment;
                var rightCol = month == 0
                    ? _monthBudgetCellRange.RightColumnIndex
                    : _monthBudgetCellRange.RightColumnIndex + increment;
                CellRange cellRange = _worksheet.Range.FromLTRB(
                    leftCol, _monthBudgetCellRange.TopRowIndex,
                    rightCol, _monthBudgetCellRange.BottomRowIndex
                );
                cellRange.Value = BandHeaders.Budget;
                _headerCellRanges.Add(cellRange);
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков столбцов "Бюджет агентский" месяцев

        /// <summary>
        ///     Метод заполнения заголовков столбцов "Бюджет агентский" месяцев
        /// </summary>
        private Task FillMonthsBudgetAgencyHeaders() {
            if (_monthCellRange == null || _monthBudgetAgencyCellRange == null) {
                return Task.CompletedTask;
            }

            for (var month = 0; month < _monthsCount; month++) {
                var increment = _monthCellRange.ColumnCount * month;
                var leftCol = month == 0
                    ? _monthBudgetAgencyCellRange.LeftColumnIndex
                    : _monthBudgetAgencyCellRange.LeftColumnIndex + increment;
                var rightCol = month == 0
                    ? _monthBudgetAgencyCellRange.RightColumnIndex
                    : _monthBudgetAgencyCellRange.RightColumnIndex + increment;
                CellRange cellRange = _worksheet.Range.FromLTRB(
                    leftCol, _monthBudgetAgencyCellRange.TopRowIndex,
                    rightCol, _monthBudgetAgencyCellRange.BottomRowIndex
                );
                cellRange.Value = BandHeaders.BudgetAgency;
                _headerCellRanges.Add(cellRange);
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков групп столбцов "Инвентаря" итого

        private Task FillTotalInventoryHeaders() {
            if (_totalInventoryCellRange == null) {
                return Task.CompletedTask;
            }

            var row = _totalInventoryCellRange.TopRowIndex;
            var leftCol = _totalInventoryCellRange.LeftColumnIndex;
            var rightCol = _totalInventoryCellRange.RightColumnIndex;

            CellRange cellRange = _worksheet.Range.FromLTRB(leftCol, row, rightCol, row);
            cellRange.Value = BandHeaders.Inventory;
            _headerCellRanges.Add(cellRange);

            row++;
            Cell cell;
            if (_isMonthTrp) {
                cell = _worksheet[row, leftCol];
                cell.Value = ColumnHeaders.TRP;
                _headerCells.Add(cell);

                leftCol++;
            }

            if (_isMonthGivenTrp) {
                cell = _worksheet[row, leftCol];
                cell.Value = $"{ColumnHeaders.TRP} {_project.BaseTiming}";
                _headerCells.Add(cell);

                leftCol++;
            }

            if (_isMonthGrp) {
                cell = _worksheet[row, leftCol];
                cell.Value = ColumnHeaders.GRP;
                _headerCells.Add(cell);

                leftCol++;
            }

            if (_isMonthGivenGrp) {
                cell = _worksheet[row, leftCol];
                cell.Value = $"{ColumnHeaders.GRP} {_project.BaseTiming}";
                _headerCells.Add(cell);

                leftCol++;
            }

            if (_isMonthMinutes) {
                cell = _worksheet[row, leftCol];
                cell.Value = ColumnHeaders.Minutes;
                _headerCells.Add(cell);
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков групп столбцов "Выходы" итого

        private Task FillTotalOutputsHeaders() {
            if (_totalOutputsCellRange == null) {
                return Task.CompletedTask;
            }

            var row = _totalOutputsCellRange.TopRowIndex;
            var leftCol = _totalOutputsCellRange.LeftColumnIndex;
            var rightCol = _totalOutputsCellRange.RightColumnIndex;

            CellRange cellRange = _worksheet.Range.FromLTRB(leftCol, row, rightCol, row);
            cellRange.Value = BandHeaders.Outputs;
            _headerCellRanges.Add(cellRange);

            row++;

            Cell cell;
            if (_isShowOutputsByTimings) {
                for (var timing = 0; timing < _timings.Count; timing++) {
                    cell = _worksheet[row, leftCol];
                    cell.Value = $"{_timings.ToList()[timing]} {ColumnHeaders.Sec}";
                    _headerCells.Add(cell);

                    leftCol++;
                }
            }

            if (_isShowOutputsByQuality) {
                cell = _worksheet[row, leftCol];
                cell.Value = ColumnHeaders.Prime;
                _headerCells.Add(cell);

                leftCol++;

                cell = _worksheet[row, leftCol];
                cell.Value = ColumnHeaders.OffPrime;
                _headerCells.Add(cell);

                leftCol++;
            }

            if (_isShowOutputsTotal) {
                cell = _worksheet[row, leftCol];
                cell.Value = ColumnHeaders.Total;
                _headerCells.Add(cell);

                leftCol++;
            }

            if (_isShowActiveDays) {
                cell = _worksheet[row, leftCol];
                cell.Value = ColumnHeaders.ActiveDays;
                _headerCells.Add(cell);

                leftCol++;
            }

            if (_isShowOutputsPerDay) {
                cell = _worksheet[row, leftCol];
                cell.Value = ColumnHeaders.PerDay;
                _headerCells.Add(cell);
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовка столбца "Бюджет" итого

        private Task FillTotalBudgetHeaders() {
            if (_totalBudgetCellRange == null) {
                return Task.CompletedTask;
            }

            CellRange cellRange = _worksheet.Range.FromLTRB(
                _totalBudgetCellRange.LeftColumnIndex, _totalBudgetCellRange.TopRowIndex,
                _totalBudgetCellRange.LeftColumnIndex, _totalBudgetCellRange.BottomRowIndex
            );
            cellRange.Value = BandHeaders.Budget;
            _headerCellRanges.Add(cellRange);
            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовка столбца "Бюджет агентский" итого

        private Task FillTotalBudgetAgencyHeaders() {
            if (_totalBudgetAgencyCellRange == null) {
                return Task.CompletedTask;
            }

            CellRange cellRange = _worksheet.Range.FromLTRB(
                _totalBudgetAgencyCellRange.LeftColumnIndex, _totalBudgetAgencyCellRange.TopRowIndex,
                _totalBudgetAgencyCellRange.LeftColumnIndex, _totalBudgetAgencyCellRange.BottomRowIndex
            );
            cellRange.Value = BandHeaders.BudgetAgency;
            _headerCellRanges.Add(cellRange);
            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения заголовков хронометражей в месяце регионов

        /// <summary>
        ///     Метод заполнения заголовков хронометражей в месяце регионов
        /// </summary>
        private Task FillMonthsRegionsToTimingsHeaders() {
            if (_monthCellRange == null || _monthRegionToTimingRange == null) {
                return Task.CompletedTask;
            }

            for (var month = 0; month < _monthsCount; month++) {
                var increment = _monthCellRange.ColumnCount * month;
                var row = _monthRegionToTimingRange.TopRowIndex;
                var col = month == 0
                    ? _monthRegionToTimingRange.LeftColumnIndex
                    : _monthRegionToTimingRange.LeftColumnIndex + increment;

                CellRange cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 1);
                cellRange.Value = ColumnHeaders.Region;
                _headerCellRanges.Add(cellRange);

                col++;

                cellRange = _worksheet.Range.FromLTRB(col, row, col + _timingsCount - 1, row);
                cellRange.Value = BandHeaders.TimingDistribution;
                _headerCellRanges.Add(cellRange);

                row++;

                for (var tIndex = 0; tIndex < _timingsCount; tIndex++) {
                    Cell cell = _worksheet[row, col];
                    cell.Value = _timings.ToList()[tIndex];
                    _headerCells.Add(cell);
                    col++;
                }

                row--;

                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 1);
                cellRange.Value = ColumnHeaders.AverageTiming;
                _headerCellRanges.Add(cellRange);

                col++;

                cellRange = _worksheet.Range.FromLTRB(col, row, col, row + 1);
                cellRange.Value = ColumnHeaders.TimingMargin;
                _headerCellRanges.Add(cellRange);
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод применения стиля заголовков

        /// <summary>
        ///     Метод применения стиля заголовков
        /// </summary>
        private Task StyleHeaders() {
            foreach (var cellRange in _headerCellRanges) {
                _worksheet.MergeCells(cellRange);
                cellRange.Font.Size = 12;
                cellRange.Font.Bold = true;
                cellRange.FillColor = Color.PowderBlue;
                cellRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cellRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cellRange.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
            }

            foreach (var cell in _headerCells) {
                cell.Font.Size = 12;
                cell.Font.Bold = true;
                cell.FillColor = Color.PowderBlue;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
            }

            return Task.CompletedTask;
        }

        #endregion

        #endregion

        #region Методы заполнения данных

        private async Task FillData() {
            List<Task> tasks = new();

            SendMessage(new EventStatusBarMessage("Сохранение отчета: заполнение данных!"));
            await Task.Delay(100);

            // Заполняем данные проекта
            tasks.Add(FillProjectData());

            // Заполняем данные хронометражей месяцев регионов
            tasks.Add(FillMonthsRegionsToTimingsData());

            var rIndexIncrement = 0;
            var regions = _project.Regions
               .Where(r => r.Outputs > 0.0)
               .OrderBy(r => r.Name)
               .ToList();
            for (var rIndex = 0; rIndex < regions.Count; rIndex++) {
                var channels = regions[rIndex].Channels
                   .Where(c => c.Outputs > 0.0)
                   .OrderByDescending(c => c.Months.Sum(m => m.Budget))
                   .ToList();
                for (var cIndex = 0; cIndex < channels.Count; cIndex++) {
                    // Заполняем базовую информацию о канале
                    tasks.Add(FillChannelBaseData(rIndexIncrement, channels[cIndex]));
                    // Заполняем группу столбцов "Распределение" канала
                    tasks.Add(FillChannelDistributionData(rIndexIncrement, channels[cIndex]));

                    // Если не отображается информация по месяцам
                    if (_monthCellRange == null) {
                        continue;
                    }

                    var months = channels[cIndex].Months
                       .Where(m => _months.Contains(m.Date))
                       .OrderBy(m => m.Date)
                       .ToList();
                    for (var mIndex = 0; mIndex < months.Count; mIndex++) {
                        // Инкремент индекса столбца
                        var cIndexIncrement = _monthCellRange.ColumnCount * mIndex;

                        tasks.AddRange(new[] {
                            // Заполняем группу столбцов "Распределение" месяца
                            FillMonthDistributionData(rIndexIncrement, cIndexIncrement, months[mIndex]),
                            // Заполняем группу столбцов "Инвентарь" месяца
                            FillMonthInventoryData(rIndexIncrement, cIndexIncrement, months[mIndex]),
                            // Заполняем группу столбцов "Выходы" месяца
                            FillMonthOutputsData(rIndexIncrement, cIndexIncrement, months[mIndex]),
                            // Заполняем столбец "Бюджет" месяца
                            FillMonthBudgetData(rIndexIncrement, cIndexIncrement, months[mIndex]),
                            // Заполняем столбец "Бюджет агентский" месяца
                            FillMonthBudgetAgencyData(rIndexIncrement, cIndexIncrement, months[mIndex])
                        });
                    }

                    tasks.AddRange(new[] {
                        // Заполняем групп столбцов "Инвентарь" групп столбцов "Итого"
                        FillTotalInventoryData(rIndexIncrement, channels[cIndex]),
                        // Заполняем групп столбцов "Выходы" групп столбцов "Итого"
                        FillTotalOutputsData(rIndexIncrement, channels[cIndex]),
                        // Заполняем групп столбцов "Бюджет" групп столбцов "Итого"
                        FillTotalBudgetData(rIndexIncrement, channels[cIndex]),
                        // Заполняем групп столбцов "Бюджет агентский" групп столбцов "Итого"
                        FillTotalBudgetAgencyData(rIndexIncrement, channels[cIndex]),
                        // Применяем стиль строки на канал
                        StyleChannelRow(rIndexIncrement)
                    });

                    // Инкремент индекса строки
                    rIndexIncrement++;
                }

                rIndexIncrement++;

                #region заполнения строки "Итого" региона

                // Заполненяем строки "Итого" столбца "Регион" названием "Итого + Region.Name"
                tasks.Add(FillRegionTotalRowData(rIndexIncrement, regions[rIndex]));

                // Заполненяем строки "Итого" группы столбцов "Распределение" канала колонки "% Канал"
                tasks.Add(FillRegionTotalRowChannelDistributionData(rIndexIncrement, regions[rIndex]));

                // Если не отображается информация по месяцам
                if (_monthCellRange == null) {
                    continue;
                }

                for (var mIndex = 0; mIndex < _monthsCount; mIndex++) {
                    // Инкремент индекса столбца
                    var cIndexIncrement = _monthCellRange.ColumnCount * mIndex;

                    var groupMonths = regions[rIndex].Channels
                       .Where(c => c.Outputs > 0.0)
                       .SelectMany(c => c.Months)
                       .Where(m => m.Budget > 0.000)
                       .GroupBy(m => m.Date)
                       .OrderBy(m => m.Key)
                       .ToList();
                    if (!groupMonths.Any()) {
                        continue;
                    }

                    var groupMonth = groupMonths[mIndex];

                    tasks.AddRange(new[] {
                        // Заполненяем строки "Итого" гурпп столбцов "Инвентарь" месяца 
                        FillRegionTotalRowMonthInventoryData(rIndexIncrement, cIndexIncrement, groupMonth),
                        // Заполненяем строки "Итого" гурпп столбцов "Выходы" месяца 
                        FillRegionTotalRowMonthOutputsData(rIndexIncrement, cIndexIncrement, regions[rIndex],
                            groupMonth),
                        // Заполненяем строки "Итого" столбца "Бюджет" месяца 
                        FillRegionTotalRowMonthBudgetData(rIndexIncrement, cIndexIncrement, groupMonth),
                        // Заполненяем строки "Итого" столбца "Бюджет агентский" месяца 
                        FillRegionTotalRowMonthAgencyBudgetData(rIndexIncrement, cIndexIncrement, groupMonth)
                    });
                }

                tasks.AddRange(new[] {
                    // Заполненяем строки "Итого" гурпп столбцов "Инвентарь" канала 
                    FillRegionTotalRowSummaryInventoryData(rIndexIncrement, regions[rIndex]),
                    // Заполненяем строки "Итого" гурпп столбцов "Выходы" канала 
                    FillRegionTotalRowSummaryOutputsData(rIndexIncrement, regions[rIndex]),
                    // Заполненяем строки "Итого" столбца "Бюджета" канала 
                    FillRegionTotalRowSummaryBudgetData(rIndexIncrement, regions[rIndex]),
                    // Заполненяем строки "Итого" столбца "Бюджета агентского" канала 
                    FillRegionTotalRowSummaryAgencyBudgetData(rIndexIncrement, regions[rIndex])
                });

                #endregion
            }

            rIndexIncrement++;

            tasks.AddRange(new[] {
                // Заполняем "Итого" регионов столбца "Регион" название "Итого"
                FillProjectTotalRowData(rIndexIncrement),
                // Заполняем "Итого" регионов столбца "Бюджета" месяца
                FillProjectTotalRowMonthBudgetData(rIndexIncrement, regions),
                // Заполняем "Итого" регионов столбца "Бюджета агентского" месяца
                FillProjectTotalRowMonthAgencyBudgetData(rIndexIncrement, regions),
                // Заполняем "Итого" регионов столбца "Бюджета" каналов
                FillProjectTotalRowSummaryBudgetData(rIndexIncrement, regions),
                // Заполняем "Итого" регионов столбца "Бюджета агентского" каналов
                FillRegionSummaryBudgetAgency(rIndexIncrement, regions)
            });

            rIndexIncrement++;

            // Заполняем бюджетную сноску проекта
            tasks.Add(FillProjectBudgetNote(rIndexIncrement, regions));

            await Task.WhenAll(tasks);

            int rowIndex = this._summaryBudgetRowCell.MaxBy(r => r.RowIndex).RowIndex + 1;

            var cell = _worksheet[rowIndex, 0];
            cell.Value = "* Прогнозное количество выходов";
            cell.Font.Size = 12;
            cell.Font.Bold = true;
            cell.Font.Italic = true;
            rowIndex++;

            cell = _worksheet[rowIndex, 0];
            cell.Value = "Настоящий медиаплан не гарантирует букирование инвентаря на каналах.";
            cell.Font.Size = 12;
            cell.Font.Bold = true;
            cell.Font.Italic = true;
            cell.Font.Color = Color.Red;
            rowIndex++;
            cell = _worksheet[rowIndex, 0];
            cell.Value =
                "Букирование производится в момент подтверждения сделки селлеру и осуществляется по факту наличия свободного инвентаря на каналах.";
            cell.Font.Size = 12;
            cell.Font.Bold = true;
            cell.Font.Italic = true;

            SendMessage(new EventStatusBarMessage("Сохранение отчета: заполнение стилей!"));
            await Task.Delay(100);

            // Применяем стили на данные
            await ApplyDataStyles();
            // Применяем стиль на столбцы бюджетов группы столбцов "Итого"
            await StyleSummaryBudgets();
            // Применяем стиль на итоговые строки регионов
            await StyleRegionTotalRow();
            // Применяем стиль на итоговоую строку проекта
            await StyleProjectTotalRow();
        }

        /// <summary>
        ///     Метод заполнения данных проекта
        /// </summary>
        private Task FillProjectData() {
            // Если диапазон ячеек данных проекта не определён - завершаем выполнение метода
            if (_projectDataCellRange == null) {
                return Task.CompletedTask;
            }

            // Определяем начальные индексы
            var row = _projectDataCellRange.TopRowIndex;
            var col = _projectDataCellRange.LeftColumnIndex + 1;

            _worksheet[row, col].Value = $"{_project.Brand.Advertiser.Name} - {_project.Brand.Name}";
            row++;

            _worksheet[row, col].Value = $"{_months.FirstOrDefault():MMM yy} - {_months.LastOrDefault():MMM yy}";
            row++;

            _worksheet[row, col].Value = string.Join(", ", _timings);
            row++;

            _worksheet[row, col].Value = _project
               .Regions.FirstOrDefault()?
               .Channels.FirstOrDefault()?
               .TargetAudience?.Name;

            row++;

            Cell cell = _worksheet[row, col];
            _worksheet[row, col].Value = _project.ID;
            cell.Font.Color = Color.White;

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения данных хронометражей месяцев регионов
        /// </summary>
        private Task FillMonthsRegionsToTimingsData() {
            if (_monthCellRange == null || _monthRegionToTimingRange == null) {
                return Task.CompletedTask;
            }

            var regions = _project.Regions.Where(r => r.Outputs > 0.0).OrderBy(r => r.Name).ToList();
            for (var month = 0; month < _monthsCount; month++) {
                var increment = _monthCellRange.ColumnCount * month;
                var row = _monthRegionToTimingRange.TopRowIndex + 2;
                var col = month == 0
                    ? _monthRegionToTimingRange.LeftColumnIndex
                    : _monthRegionToTimingRange.LeftColumnIndex + increment;
                for (var rIndex = 0; rIndex < regions.Count; rIndex++) {
                    var regionMonthTimings = regions[rIndex]
                       .Channels.Where(c => c.Outputs > 0.0)
                       .SelectMany(c => c.Months)
                       .Where(m => m.Date == _months.ToList()[month])
                       .SelectMany(m => m.Weeks)
                       .Where(w => w.Outputs > 0.0)
                       .SelectMany(w => w.Timings)
                       .Where(t => _timings.Contains(t.Value))
                       .ToList();

                    _worksheet[row, col].Value = regions[rIndex].Name;
                    _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    col++;

                    for (var tIndex = 0; tIndex < _timingsCount; tIndex++) {
                        _worksheet[row, col].Value = regionMonthTimings
                               .Where(t => t.Value == _timings.ToList()[tIndex])
                               .Sum(t => !regions[rIndex].IsMeasurable ? t.Outputs : t.TRP) /
                            regionMonthTimings.Sum(t => !regions[rIndex].IsMeasurable ? t.Outputs : t.TRP);
                        _worksheet[row, col].NumberFormat = "0.0%";
                        _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        col++;
                    }

                    _worksheet[row, col].Value = regionMonthTimings
                       .GroupBy(t => t.Value)
                       .Sum(g => g.Key * (
                            g.Sum(t => !regions[rIndex].IsMeasurable ? t.Outputs : t.TRP) /
                            regionMonthTimings.Sum(t => !regions[rIndex].IsMeasurable ? t.Outputs : t.TRP)
                        ));
                    _worksheet[row, col].NumberFormat = "0.0#";
                    _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    col++;

                    var timingMargin = 1.0;
                    if (regions[rIndex].IsMeasurable && regions[rIndex].TimingMargins.Count != 0) {
                        timingMargin = regionMonthTimings
                           .GroupBy(t => t.Value)
                           .Select(g => new KeyValuePair<int, double>(g.Key, ValuesValidator.NanOrZero(
                                g.Sum(t => t.GivenGRP) /
                                regionMonthTimings.Sum(t => t.GivenGRP)
                            )))
                           .Sum(p =>
                                p.Value *
                                (regions[rIndex].TimingMargins.FirstOrDefault(t => p.Key == t.Timing)?.Value ?? 0.0)
                            );
                        if (timingMargin == 0.0) {
                            timingMargin = 1.0;
                        }
                    }

                    _worksheet[row, col].Value = timingMargin;
                    _worksheet[row, col].NumberFormat = "0.0#";
                    _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;


                    row++;
                    col = month == 0
                        ? _monthRegionToTimingRange.LeftColumnIndex
                        : _monthRegionToTimingRange.LeftColumnIndex + increment;
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения базовой информации канала
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент к индексу строки</param>
        /// <param name="channel">Канал</param>
        private Task FillChannelBaseData(int rIndexIncrement, ChannelVM channel) {
            // Если базовая информация канала не отображается - завершаем выполнение метода
            if (_baseChannelDataCellRange == null) {
                return Task.CompletedTask;
            }

            // Определяем начальные индексы
            var col = _baseChannelDataCellRange.LeftColumnIndex;
            var row = _baseChannelDataCellRange.BottomRowIndex + 1 + rIndexIncrement;

            // Если отображается столбец "Регион"
            if (_isShowRegion) {
                _worksheet[row, col].Value = channel.Region.Name;
                col++;
            }

            // Если отображается столбец "Канал"
            if (_isShowChannel) {
                _worksheet[row, col].Value = channel.Name;
                col++;
            }

            // Если не отображается столбец "Тип продаж" - завершаем выполнение метода
            if (_isShowChannelSaleType) {
                _worksheet[row, col].Value = channel.SaleType == SaleType.GRP ? "Рейтинги" : "Минуты";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения группы столбцов "Распределение" канала
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="channel">Канал</param>
        private Task FillChannelDistributionData(int rIndexIncrement, ChannelVM channel) {
            // Если группа столбцов "Распределение" не отображается - завершаем выполнение метода
            if (_channelDistributionCellRange == null) {
                return Task.CompletedTask;
            }

            // Определяем начальные индексы
            var col = _channelDistributionCellRange.LeftColumnIndex;
            var row = _channelDistributionCellRange.BottomRowIndex + 3 + rIndexIncrement;

            // Если отображается "% Канал" канала
            if (_isShowChannelPortionDistribution) {
                _worksheet[row, col].Value = channel.Portion;
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                if (!_regionToChannelsPortionCells.ContainsKey(channel.Region.DictionaryID)) {
                    _regionToChannelsPortionCells.Add(channel.Region.DictionaryID, new List<Cell>());
                }

                _regionToChannelsPortionCells[channel.Region.DictionaryID].Add(_worksheet[row, col]);

                col++;
            }

            // Если отображается распределение по качеству
            if (_isShowChannelQualityDistribution) {
                _worksheet[row, col].Value = channel.FixOrFloatPortion;
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;

                _worksheet[row, col].Value = channel.FixOrFloatPrimePortion;
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;

                _worksheet[row, col].Value = channel.SuperFixOrFixPortion;
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;

                _worksheet[row, col].Value = channel.SuperFixOrFixPrimePortion;
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            return Task.CompletedTask;
        }

        #region Методы заполнения групп столбцов "Месяц" канала

        /// <summary>
        ///     Метод заполнения группы столбцов "Распределение" месяца
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="cIndexIncrement">Инкремент индекса столбца</param>
        /// <param name="month">Месяц</param>
        private Task FillMonthDistributionData(int rIndexIncrement, int cIndexIncrement, MonthVM month) {
            // Если группа столбцов "Распределение" месяца не отображается - завершаем выполнение метода
            if (_monthDistributionCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthDistributionCellRange.LeftColumnIndex + cIndexIncrement;
            var row = _monthDistributionCellRange.BottomRowIndex  + 2 + rIndexIncrement;

            // Если отображается Affinity
            if (_isShowMonthAffinity) {
                _worksheet[row, col].Value = month.Affinity * 100;
                _worksheet[row, col].NumberFormat = "0.0";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                if (!_regionToMonthsAffinityCells.ContainsKey(month.Channel.Region.DictionaryID)) {
                    _regionToMonthsAffinityCells.Add(
                        month.Channel.Region.DictionaryID, new Dictionary<DateTime, List<Cell>>()
                    );
                }

                if (!_regionToMonthsAffinityCells[month.Channel.Region.DictionaryID].ContainsKey(month.Date)) {
                    _regionToMonthsAffinityCells[month.Channel.Region.DictionaryID].Add(month.Date, new List<Cell>());
                }

                _regionToMonthsAffinityCells[month.Channel.Region.DictionaryID][month.Date].Add(_worksheet[row, col]);

                col++;
            }

            // Если отображается распределение по качеству
            if (_isShowMonthQualityDistribution) {
                _worksheet[row, col].Value = month.FixOrFloatPortion;
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;

                _worksheet[row, col].Value = month.FixOrFloatPrimePortion;
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;

                _worksheet[row, col].Value = month.SuperFixOrFixPortion;
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;

                _worksheet[row, col].Value = month.SuperFixOrFixPrimePortion;
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения группы столбцов "Инвентарь" месяца
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="cIndexIncrement">Инкремент индекса столбца</param>
        /// <param name="month">Месяц</param>
        private Task FillMonthInventoryData(int rIndexIncrement, int cIndexIncrement, MonthVM month) {
            // Если группа столбцов "Распределение" месяца не отображается - завершаем выполнение метода
            if (_monthInventoryCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthInventoryCellRange.LeftColumnIndex + cIndexIncrement;
            var row = _monthInventoryCellRange.BottomRowIndex  + 2 + rIndexIncrement;

            // Если отображается "TRP"
            if (_isMonthTrp) {
                _worksheet[row, col].Value = month.TRP;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;
            }

            // Если отображается "GivenTRP"
            if (_isMonthGivenTrp) {
                _worksheet[row, col].Value = month.GivenTRP;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;
            }

            // Если отображается "GRP"
            if (_isMonthGrp) {
                _worksheet[row, col].Value = month.GRP;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;
            }

            // Если отображается "GivenGRP"
            if (_isMonthGivenGrp) {
                _worksheet[row, col].Value = month.GivenGRP;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;
            }

            // Если отображается "Minutes"
            if (_isMonthMinutes) {
                _worksheet[row, col].Value = month.Minutes;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнение группы столбцов "Выходы" месяца
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="cIndexIncrement">Инкремент индекса столбца</param>
        /// <param name="month">Месяц</param>
        private Task FillMonthOutputsData(int rIndexIncrement, int cIndexIncrement, MonthVM month) {
            // Если группа столбцов "Выходы" месяца не отображается - завершаем выполнение метода
            if (_monthOutputsCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthOutputsCellRange.LeftColumnIndex + cIndexIncrement;
            var row = _monthOutputsCellRange.BottomRowIndex  + 2 + rIndexIncrement;

            // Если отображается "По хронометражам"
            if (_isShowOutputsByTimings) {
                var timings = month.Timings
                   .Where(t => _timings.Contains(t.Value))
                   .OrderBy(t => t.Value)
                   .ToList();
                for (var tIndex = 0; tIndex < timings.Count; tIndex++) {
                    _worksheet[row, col].Value = timings[tIndex].Outputs;
                    _worksheet[row, col].NumberFormat = "0.00";
                    _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                    col++;
                }
            }

            // Если отображается "По качеству"
            if (_isShowOutputsByQuality) {
                _worksheet[row, col].Value = month.OutputsPrime;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;

                _worksheet[row, col].Value = month.OutputsOffPrime;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;
            }

            // Если отображается "Всего"
            if (_isShowOutputsTotal) {
                _worksheet[row, col].Value = month.Outputs;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;
            }

            // Если отображается "Активные дни"
            if (_isShowActiveDays) {
                _worksheet[row, col].Value = month.ActiveDays;
                _worksheet[row, col].NumberFormat = "0";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;
            }

            // Если отображается "В день"
            if (_isShowOutputsPerDay) {
                _worksheet[row, col].Value = month.OutputsPerDay;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнение столбца "Бюджет" месяца
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="cIndexIncrement">Инкремент индекса столбца</param>
        /// <param name="month">Месяц</param>
        private Task FillMonthBudgetData(int rIndexIncrement, int cIndexIncrement, MonthVM month) {
            // Если столбец "Бюджет" месяца не отображается - завершаем выполнение метода
            if (_monthBudgetCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthBudgetCellRange.LeftColumnIndex + cIndexIncrement;
            var row = _monthBudgetCellRange.BottomRowIndex  + 1 + rIndexIncrement;

            _worksheet[row, col].Value = month.Budget;
            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            if (!_monthsBudgetsCells.ContainsKey(month.Channel.Region.DictionaryID)) {
                _monthsBudgetsCells.Add(
                    month.Channel.Region.DictionaryID, new Dictionary<DateTime, List<Cell>>()
                );
            }

            if (!_monthsBudgetsCells[month.Channel.Region.DictionaryID].ContainsKey(month.Date)) {
                _monthsBudgetsCells[month.Channel.Region.DictionaryID].Add(month.Date, new List<Cell>());
            }

            _monthsBudgetsCells[month.Channel.Region.DictionaryID][month.Date].Add(_worksheet[row, col]);

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнение столбца "Бюджет агентский" месяца
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="cIndexIncrement">Инкремент индекса столбца</param>
        /// <param name="month">Месяц</param>
        private Task FillMonthBudgetAgencyData(int rIndexIncrement, int cIndexIncrement, MonthVM month) {
            // Если столбец "Бюджет" месяца не отображается - завершаем выполнение метода
            if (_monthBudgetAgencyCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthBudgetAgencyCellRange.LeftColumnIndex + cIndexIncrement;
            var row = _monthBudgetAgencyCellRange.BottomRowIndex  + 1 + rIndexIncrement;

            _worksheet[row, col].Value = month.Budget * (1.0 - _project.Margin);
            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            if (!_monthsAgencyBudgetsCells.ContainsKey(month.Channel.Region.DictionaryID)) {
                _monthsAgencyBudgetsCells.Add(
                    month.Channel.Region.DictionaryID, new Dictionary<DateTime, List<Cell>>()
                );
            }

            if (!_monthsAgencyBudgetsCells[month.Channel.Region.DictionaryID].ContainsKey(month.Date)) {
                _monthsAgencyBudgetsCells[month.Channel.Region.DictionaryID].Add(month.Date, new List<Cell>());
            }

            _monthsAgencyBudgetsCells[month.Channel.Region.DictionaryID][month.Date].Add(_worksheet[row, col]);

            return Task.CompletedTask;
        }

        #endregion

        #region Методы заполнения групп столбцов "Итого" канала

        /// <summary>
        ///     Метод заполнения групп столбцов "Инвентарь" группы столбца "Итого"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="channel">Канал</param>
        /// <returns>
        ///     <see cref=" Task.CompletedTask" />
        /// </returns>
        private Task FillTotalInventoryData(int rIndexIncrement, ChannelVM channel) {
            if (_totalInventoryCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _totalInventoryCellRange.LeftColumnIndex;
            var row = _totalInventoryCellRange.BottomRowIndex + 2 + rIndexIncrement;

            // Если отображается "TRP"
            if (_isMonthTrp) {
                _worksheet[row, col].Value = channel.TRP;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;
            }

            // Если отображается "GivenTRP"
            if (_isMonthGivenTrp) {
                _worksheet[row, col].Value = channel.GivenTRP;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;
            }

            // Если отображается "GRP"
            if (_isMonthGrp) {
                _worksheet[row, col].Value = channel.GRP;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;
            }

            // Если отображается "GivenGRP"
            if (_isMonthGivenGrp) {
                _worksheet[row, col].Value = channel.GivenGRP;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                col++;
            }

            // Если отображается "Minutes"
            if (_isMonthMinutes) {
                _worksheet[row, col].Value = channel.Minutes;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения групп столбцов "Выходы" группы столбца "Итого"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="channel">Канал</param>
        /// <returns></returns>
        private Task FillTotalOutputsData(int rIndexIncrement, ChannelVM channel) {
            if (_totalOutputsCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _totalOutputsCellRange.LeftColumnIndex;
            var row = _totalOutputsCellRange.BottomRowIndex + 2 + rIndexIncrement;

            // Если отображается "По хронометражам"
            if (_isShowOutputsByTimings) {
                foreach (var timing in _timings) {
                    _worksheet[row, col].Value = channel.Timings.Where(t => t.Value == timing).Sum(t => t.Outputs);
                    _worksheet[row, col].NumberFormat = "0.00";
                    _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    col++;
                }
            }

            // Если отображается "По качеству"
            if (_isShowOutputsByQuality) {
                _worksheet[row, col].Value = channel.Months.Sum(x => x.OutputsPrime);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;

                _worksheet[row, col].Value = channel.Months.Sum(x => x.OutputsOffPrime);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;
            }

            // Если отображается "Всего"
            if (_isShowOutputsTotal) {
                _worksheet[row, col].Value = channel.Outputs;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;
            }

            // Если отображается "Активные дни"
            if (_isShowActiveDays) {
                _worksheet[row, col].Value = channel.ActiveDays;
                _worksheet[row, col].NumberFormat = "0";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                col++;
            }

            // Если отображается "В день"
            if (_isShowOutputsPerDay) {
                _worksheet[row, col].Value = channel.Outputs / channel.ActiveDays;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения столбца "Бюджет" группы столбцов "Итого"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="channel">Канал</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillTotalBudgetData(int rIndexIncrement, ChannelVM channel) {
            if (_totalBudgetCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _totalBudgetCellRange.LeftColumnIndex;
            var row = _totalBudgetCellRange.BottomRowIndex + 1 + rIndexIncrement;

            _worksheet[row, col].Value = channel.Months.Sum(m => m.Budget);
            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            if (!_totalBudgetsCells.ContainsKey(channel.Region.DictionaryID)) {
                _totalBudgetsCells.Add(
                    channel.Region.DictionaryID, new List<Cell>()
                );
            }

            _totalBudgetsCells[channel.Region.DictionaryID].Add(_worksheet[row, col]);

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения столбца "Бюджет агентский" группы столбцов "Итого"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="channel">Канал</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillTotalBudgetAgencyData(int rIndexIncrement, ChannelVM channel) {
            if (_totalBudgetAgencyCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _totalBudgetAgencyCellRange.LeftColumnIndex;
            var row = _totalBudgetAgencyCellRange.BottomRowIndex + 1 + rIndexIncrement;
            _worksheet[row, col].Value = channel.Months.Sum(m => m.Budget * (1.0 - _project.Margin));
            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            if (!_totalAgencyBudgetsCells.ContainsKey(channel.Region.DictionaryID)) {
                _totalAgencyBudgetsCells.Add(
                    channel.Region.DictionaryID, new List<Cell>()
                );
            }

            _totalAgencyBudgetsCells[channel.Region.DictionaryID].Add(_worksheet[row, col]);


            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод применения стиля на строку канала
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task StyleChannelRow(int rIndexIncrement) {
            var cellIndex = 0;
            var rowIndex = 0;

            // Если есть диапазон ячеек "Канала"
            if (_baseChannelDataCellRange != null) {
                cellIndex = _baseChannelDataCellRange.RightColumnIndex + 1;
                rowIndex = _baseChannelDataCellRange.BottomRowIndex    + rIndexIncrement + 3;
            }

            // Если есть диапазон ячеек "Итого"
            if (_totalCellRange != null) {
                cellIndex = _totalCellRange.RightColumnIndex + 1;
                rowIndex = _totalCellRange.BottomRowIndex    + rIndexIncrement + 3;
            }

            for (var cellI = 0; cellI < cellIndex; cellI++) {
                // Сохраянем ячейку.
                Cell cell = _worksheet[rowIndex, cellI];
                cell.Font.Size = 12;
                if (rowIndex % 2 == 0) {
                    cell.FillColor = Color.White;
                } else {
                    cell.FillColor = Color.FromArgb(240, 253, 255);
                }

                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод применения стиля на столбцы бюджетов месяцев регионов и итоговых бюджетов регионов
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        private Task StyleSummaryBudgets() {
            foreach (var group in _totalBudgetsCells) {
                var col = group.Value.FirstOrDefault()?.ColumnIndex ?? 999;
                var top = group.Value.FirstOrDefault()?.RowIndex    ?? 999;
                var bottom = group.Value.LastOrDefault()?.RowIndex  ?? 999;
                _worksheet.ConditionalFormattings.AddDataBarConditionalFormatting(
                    _worksheet.Range.FromLTRB(col, top, col, bottom),
                    _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                    _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                    Color.CadetBlue
                );
            }

            foreach (var group in _totalAgencyBudgetsCells) {
                var col = group.Value.FirstOrDefault()?.ColumnIndex ?? 999;
                var top = group.Value.FirstOrDefault()?.RowIndex    ?? 999;
                var bottom = group.Value.LastOrDefault()?.RowIndex  ?? 999;
                _worksheet.ConditionalFormattings.AddDataBarConditionalFormatting(
                    _worksheet.Range.FromLTRB(col, top, col, bottom),
                    _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                    _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                    Color.CadetBlue
                );
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Методы заполнения строки "Итого" региона

        /// <summary>
        ///     Метод заполнения итоговой строки региона столбца "Регион"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="region">Регион</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowData(int rIndexIncrement, RegionVM region) {
            // Если базовая информация канала не отображается - завершаем выполнение метода
            if (_baseChannelDataCellRange == null) {
                return Task.CompletedTask;
            }

            // Определяем начальные индексы
            var col = _baseChannelDataCellRange.LeftColumnIndex;
            var row = _baseChannelDataCellRange.BottomRowIndex + rIndexIncrement;

            // Если отображается столбец "Регион"
            if (!_isShowRegion) return Task.CompletedTask;

            _worksheet[row, col].Value = BandHeaders.Summary + " " + $"{region.Name}:";
            Cell cell = _worksheet[row, col];
            _totalSummaryCell.Add(cell);

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки региона группы столбцов "Распределение" каналов региона
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="region">Регион</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowChannelDistributionData(int rIndexIncrement, RegionVM region) {
            // Если группа столбцов "Распределение" не отображается - завершаем выполнение метода
            if (_channelDistributionCellRange == null) {
                return Task.CompletedTask;
            }

            // Определяем начальные индексы
            var col = _channelDistributionCellRange.LeftColumnIndex;
            var row = _channelDistributionCellRange.BottomRowIndex + 2 + rIndexIncrement;

            // Если отображается "% Канал" канала
            if (_isShowChannelPortionDistribution) {
                _worksheet[row, col].Value = region.Channels.Where(c => c.Outputs > 0.0).Sum(c => c.Portion);
                _worksheet[row, col].NumberFormat = "0.0%";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                Cell cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки региона группы столбцов "Инвентарь" месяцев региона
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="cIndexIncrement">Инкремент индекса ячейки</param>
        /// <param name="groupMonth"></param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowMonthInventoryData(
            int rIndexIncrement,
            int cIndexIncrement, IGrouping<DateTime, MonthVM> groupMonth
        ) {
            // Если группа столбцов "Распределение" месяца не отображается - завершаем выполнение метода
            if (_monthInventoryCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthInventoryCellRange.LeftColumnIndex + cIndexIncrement;
            var row = _monthInventoryCellRange.BottomRowIndex  + 1 + rIndexIncrement;

            Cell cell;

            // Если отображается "TRP"
            if (_isMonthTrp) {
                _worksheet[row, col].Value = groupMonth.Sum(m => m.TRP);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "GivenTRP"
            if (_isMonthGivenTrp) {
                _worksheet[row, col].Value = groupMonth.Sum(m => m.GivenTRP);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "GRP"
            if (_isMonthGrp) {
                _worksheet[row, col].Value = groupMonth.Sum(m => m.GRP);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "GivenGRP"
            if (_isMonthGivenGrp) {
                _worksheet[row, col].Value = groupMonth.Sum(m => m.GivenGRP);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);


                col++;
            }

            // Если отображается "Minutes"
            if (_isMonthMinutes) {
                _worksheet[row, col].Value = groupMonth.Sum(m => m.Minutes);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки региона группы столбцов "Выходы" месяцев региона
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="cIndexIncrement">Инкремент индекса ячейки</param>
        /// <param name="region">Регион</param>
        /// <param name="groupMonth"></param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowMonthOutputsData(
            int rIndexIncrement,
            int cIndexIncrement,
            RegionVM region, IGrouping<DateTime, MonthVM> groupMonth
        ) {
            // Если группа столбцов "Выходы" месяца не отображается - завершаем выполнение метода
            if (_monthOutputsCellRange == null) {
                return Task.CompletedTask;
            }

            Cell cell;

            var col = _monthOutputsCellRange.LeftColumnIndex + cIndexIncrement;
            var row = _monthOutputsCellRange.BottomRowIndex  + 1 + rIndexIncrement;


            // Если отображается "По хронометражам"
            if (_isShowOutputsByTimings) {
                for (var tIndex = 0; tIndex < _timings.Count; tIndex++) {
                    var timings = groupMonth.SelectMany(m => m.Timings)
                       .Where(t => _timings.Contains(t.Value))
                       .OrderBy(t => t.Value)
                       .GroupBy(t => t.Value)
                       .OrderBy(t => t.Key)
                       .ToList()[tIndex];
                    _worksheet[row, col].Value = timings.Sum(t => t.Outputs);
                    _worksheet[row, col].NumberFormat = "0.00";
                    _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                    cell = _worksheet[row, col];
                    _totalSummaryCell.Add(cell);

                    col++;
                }
            }

            // Если отображается "По качеству"
            if (_isShowOutputsByQuality) {
                _worksheet[row, col].Value = groupMonth.Sum(m => m.OutputsPrime);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;

                _worksheet[row, col].Value = groupMonth.Sum(m => m.OutputsOffPrime);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "Всего"
            if (_isShowOutputsTotal) {
                _worksheet[row, col].Value = groupMonth.Sum(m => m.Outputs);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "Активные дни"
            if (_isShowActiveDays) {
                _worksheet[row, col].Value = groupMonth.Sum(m => m.ActiveDays) /
                    region.Channels.Where(c => c.Outputs > 0.0).ToList().Count;
                _worksheet[row, col].NumberFormat = "0";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }


            // Если отображается "В день"
            if (_isShowOutputsPerDay) {
                _worksheet[row, col].Value = groupMonth.Sum(m => m.Outputs) / groupMonth.Sum(m => m.ActiveDays);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки региона группы столбцов "Бюджет" месяцев региона
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="cIndexIncrement">Инкремент индекса ячейки</param>
        /// <param name="groupMonth"></param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowMonthBudgetData(
            int rIndexIncrement,
            int cIndexIncrement, IGrouping<DateTime, MonthVM> groupMonth
        ) {
            // Если столбец "Бюджет" месяца не отображается - завершаем выполнение метода
            if (_monthBudgetCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthBudgetCellRange.LeftColumnIndex + cIndexIncrement;
            var row = _monthBudgetCellRange.BottomRowIndex  + rIndexIncrement;

            _worksheet[row, col].Value = groupMonth.Sum(m => m.Budget);
            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            Cell cell = _worksheet[row, col];
            _totalSummaryCell.Add(cell);
            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки региона группы столбцов "Бюджет агентский" месяцев региона
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="cIndexIncrement">Инкремент индекса ячейки</param>
        /// <param name="groupMonth"></param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowMonthAgencyBudgetData(
            int rIndexIncrement,
            int cIndexIncrement, IGrouping<DateTime, MonthVM> groupMonth
        ) {
            // Если столбец "Бюджет агентский" месяца не отображается - завершаем выполнение метода
            if (_monthBudgetAgencyCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthBudgetAgencyCellRange.LeftColumnIndex + cIndexIncrement;
            var row = _monthBudgetAgencyCellRange.BottomRowIndex  + rIndexIncrement;

            _worksheet[row, col].Value = groupMonth.Sum(m => m.Budget * (1.0 - _project.Margin));
            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            Cell cell = _worksheet[row, col];
            _totalSummaryCell.Add(cell);
            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки региона по группе столбцов "Итого" - "Инвентарь"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="region">Регион</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowSummaryInventoryData(int rIndexIncrement, RegionVM region) {
            if (_totalInventoryCellRange == null) {
                return Task.CompletedTask;
            }

            Cell cell;

            var col = _totalInventoryCellRange.LeftColumnIndex;
            var row = _totalInventoryCellRange.BottomRowIndex + 1 + rIndexIncrement;

            var channels = region.Channels
               .Where(c => c.Outputs > 0.0)
               .ToList();

            // Если отображается "TRP"
            if (_isMonthTrp) {
                _worksheet[row, col].Value = channels.Sum(c => c.TRP);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "GivenTRP"
            if (_isMonthGivenTrp) {
                _worksheet[row, col].Value = channels.Sum(c => c.GivenTRP);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "GRP"
            if (_isMonthGrp) {
                _worksheet[row, col].Value = channels.Sum(c => c.GRP);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "GivenGRP"
            if (_isMonthGivenGrp) {
                _worksheet[row, col].Value = channels.Sum(c => c.GivenGRP);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "Minutes"
            if (_isMonthMinutes) {
                _worksheet[row, col].Value = channels.Sum(c => c.Minutes);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки региона по группе столбцов "Итого" - "Выходы"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="region">Регион</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowSummaryOutputsData(int rIndexIncrement, RegionVM region) {
            if (_totalOutputsCellRange == null) {
                return Task.CompletedTask;
            }

            Cell cell;

            var col = _totalOutputsCellRange.LeftColumnIndex;
            var row = _totalOutputsCellRange.BottomRowIndex + 1 + rIndexIncrement;

            var channels = region.Channels
               .Where(c => c.Outputs > 0.0)
               .ToList();

            // Если отображается "По хронометражам"
            if (_isShowOutputsByTimings) {
                foreach (var timing in _timings) {
                    _worksheet[row, col].Value = channels.SelectMany(c => c.Timings.Where(t => t.Value == timing))
                       .Sum(t => t.Outputs);
                    _worksheet[row, col].NumberFormat = "0.00";
                    _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                    cell = _worksheet[row, col];
                    _totalSummaryCell.Add(cell);

                    col++;
                }
            }

            // Если отображается "По качеству"
            if (_isShowOutputsByQuality) {
                _worksheet[row, col].Value = channels.SelectMany(c => c.Months).Sum(m => m.OutputsPrime);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;

                _worksheet[row, col].Value = channels.SelectMany(c => c.Months).Sum(m => m.OutputsOffPrime);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "Всего"
            if (_isShowOutputsTotal) {
                _worksheet[row, col].Value = channels.SelectMany(c => c.Months).Sum(m => m.Outputs);
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "Активные дни"
            if (_isShowActiveDays) {
                _worksheet[row, col].Value = channels.SelectMany(c => c.Months).Sum(m => m.ActiveDays) /
                    region.Channels.Where(c => c.Outputs > 0.0).OrderBy(c => c.Name).ToList().Count;
                _worksheet[row, col].NumberFormat = "0";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);

                col++;
            }

            // Если отображается "В день"
            if (_isShowOutputsPerDay) {
                _worksheet[row, col].Value = channels.Sum(c => c.Outputs / c.ActiveDays) / channels.Count;
                _worksheet[row, col].NumberFormat = "0.00";
                _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                cell = _worksheet[row, col];
                _totalSummaryCell.Add(cell);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки региона по группе столбцов "Итого" - "Бюджет"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="region">Регион</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowSummaryBudgetData(int rIndexIncrement, RegionVM region) {
            // Если столбец "Бюджет" месяца не отображается - завершаем выполнение метода
            if (_totalBudgetCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _totalBudgetCellRange.LeftColumnIndex;
            var row = _totalBudgetCellRange.BottomRowIndex + rIndexIncrement;

            var channels = region.Channels
               .Where(c => c.Outputs > 0.0)
               .ToList();

            _worksheet[row, col].Value = channels.SelectMany(c => c.Months).Sum(m => m.Budget);
            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            Cell cell = _worksheet[row, col];
            _totalSummaryCell.Add(cell);

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки региона по группе столбцов "Итого" - "Бюджет агентский"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="region">Регион</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionTotalRowSummaryAgencyBudgetData(int rIndexIncrement, RegionVM region) {
            // Если столбец "Бюджет агентский" месяца не отображается - завершаем выполнение метода
            if (_totalBudgetAgencyCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _totalBudgetAgencyCellRange.LeftColumnIndex;
            var row = _totalBudgetAgencyCellRange.BottomRowIndex + rIndexIncrement;

            var channels = region.Channels
               .Where(c => c.Outputs > 0.0)
               .ToList();

            _worksheet[row, col].Value =
                channels.SelectMany(c => c.Months).Sum(m => m.Budget * (1.0 - _project.Margin));
            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            Cell cell = _worksheet[row, col];
            _totalSummaryCell.Add(cell);

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод применения стиля на итоговую строку региона
        /// </summary>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task StyleRegionTotalRow() {
            foreach (var cell in _totalSummaryCell) {
                cell.Font.Size = 12;
                cell.Font.Bold = true;
                cell.FillColor = Color.FromArgb(189, 241, 255);
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
                if (cell.ColumnIndex == 0) {
                    continue;
                }

                for (var i = 0; i < cell.ColumnIndex; i++) {
                    Cell cellNot = _worksheet[cell.RowIndex, i];
                    var firstOrDefault = _totalSummaryCell.FirstOrDefault(f => f.ColumnIndex == i);
                    if (firstOrDefault != null || _totalSummaryNotCell.Contains(cellNot)) {
                        continue;
                    }

                    _totalSummaryNotCell.Add(cellNot);
                }
            }

            foreach (var cell in _totalSummaryNotCell) {
                cell.Font.Size = 12;
                cell.Font.Bold = true;
                cell.FillColor = Color.FromArgb(189, 241, 255);
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.TopBorder.Color = Color.Black;
                cell.Borders.TopBorder.LineStyle = BorderLineStyle.Thin;
                cell.Borders.BottomBorder.Color = Color.Black;
                cell.Borders.BottomBorder.LineStyle = BorderLineStyle.Thin;
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Методы заполнения строки "Итого" проекта

        /// <summary>
        ///     Метод заполнения итоговой строки проекта в группе столбцов "Регион"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillProjectTotalRowData(int rIndexIncrement) {
            // Если базовая информация канала не отображается - завершаем выполнение метода
            if (_baseChannelDataCellRange == null) {
                return Task.CompletedTask;
            }


            // Определяем начальные индексы
            var col = _baseChannelDataCellRange.LeftColumnIndex;
            var row = _baseChannelDataCellRange.BottomRowIndex + rIndexIncrement;

            // Если отображается столбец "Регион"
            if (_isShowRegion) {
                _worksheet[row, col].Value = BandHeaders.Summary;
                Cell cell = _worksheet[row, col];
                _summaryBudgetRowCell.Add(cell);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки проекта в группе столбцов "Месяц" - "Бюджет"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="regions">Регионы</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillProjectTotalRowMonthBudgetData(int rIndexIncrement, List<RegionVM> regions) {
            if (_monthBudgetCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthBudgetCellRange.LeftColumnIndex;
            var row = _monthBudgetCellRange.BottomRowIndex + rIndexIncrement;

            // Если не отображается информация по месяцам
            if (_monthCellRange == null) {
                return Task.CompletedTask;
            }

            for (var mIndex = 0; mIndex < _monthsCount; mIndex++) {
                // Инкремент индекса столбца
                var cIndexIncrement = _monthCellRange.ColumnCount * mIndex;

                _worksheet[row, col + cIndexIncrement].Value =
                    regions.SelectMany(r => r.Channels)
                       .Where(c => c.Outputs != 0)
                       .SelectMany(c => c.Months)
                       .Where(m => m.Budget > 0.0001)
                       .GroupBy(m => m.Date)
                       .OrderBy(m => m.Key)
                       .ToList()[mIndex]
                       .Sum(m => m.Budget);

                _worksheet[row, col + cIndexIncrement].NumberFormat = "#,##0.00 ₽";
                _worksheet[row, col + cIndexIncrement].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col + cIndexIncrement].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

                Cell cell = _worksheet[row, col + cIndexIncrement];
                _summaryBudgetRowCell.Add(cell);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки проекта в группе столбцов "Месяц" - "Бюджет агентский"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="regions">Регионы</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillProjectTotalRowMonthAgencyBudgetData(int rIndexIncrement, List<RegionVM> regions) {
            // Если не отображается информация по месяцам
            if (_monthCellRange == null || _monthBudgetAgencyCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _monthBudgetAgencyCellRange.LeftColumnIndex;
            var row = _monthBudgetAgencyCellRange.BottomRowIndex + rIndexIncrement;

            for (var mIndex = 0; mIndex < _monthsCount; mIndex++) {
                // Инкремент индекса столбца
                var cIndexIncrement = _monthCellRange.ColumnCount * mIndex;

                _worksheet[row, col + cIndexIncrement].Value =
                    regions.SelectMany(r => r.Channels)
                       .Where(c => c.Outputs != 0)
                       .SelectMany(c => c.Months)
                       .Where(m => m.Budget > 0.0001)
                       .GroupBy(m => m.Date)
                       .OrderBy(m => m.Key)
                       .ToList()[mIndex]
                       .Sum(m => m.Budget * (1.0 - _project.Margin));

                _worksheet[row, col + cIndexIncrement].NumberFormat = "#,##0.00 ₽";
                _worksheet[row, col + cIndexIncrement].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                _worksheet[row, col + cIndexIncrement].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

                Cell cell = _worksheet[row, col + cIndexIncrement];
                _summaryBudgetRowCell.Add(cell);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки проекта в группе столбцов "Итого" - "Бюджет"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="regions">Регионы</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillProjectTotalRowSummaryBudgetData(int rIndexIncrement, List<RegionVM> regions) {
            if (_totalBudgetCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _totalBudgetCellRange.LeftColumnIndex;
            var row = _totalBudgetCellRange.BottomRowIndex + rIndexIncrement;

            _worksheet[row, col].Value = regions
               .SelectMany(r => r.Channels)
               .Where(c => c.Outputs != 0)
               .SelectMany(c => c.Months)
               .Where(m => m.Budget > 0.0001)
               .Sum(m => m.Budget);

            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            Cell cell = _worksheet[row, col];
            _summaryBudgetRowCell.Add(cell);

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения итоговой строки проекта в группе столбцов "Итого" - "Бюджет агентский"
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="regions">Регионы</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillRegionSummaryBudgetAgency(int rIndexIncrement, List<RegionVM> regions) {
            if (_totalBudgetAgencyCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _totalBudgetAgencyCellRange.LeftColumnIndex;
            var row = _totalBudgetAgencyCellRange.BottomRowIndex + rIndexIncrement;

            _worksheet[row, col].Value = regions
               .SelectMany(r => r.Channels)
               .Where(c => c.Outputs != 0)
               .SelectMany(c => c.Months)
               .Where(m => m.Budget > 0.0001)
               .Sum(m => m.Budget * (1.0 - _project.Margin));

            _worksheet[row, col].NumberFormat = "#,##0.00 ₽";
            _worksheet[row, col].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[row, col].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;

            Cell cell = _worksheet[row, col];
            _summaryBudgetRowCell.Add(cell);

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод применения стиля на строку "Итого" регионов.
        /// </summary>
        /// <returns></returns>
        private Task StyleProjectTotalRow() {
            foreach (var cellRange in _summaryBudgetRowCell) {
                _worksheet.MergeCells(cellRange);
                cellRange.Font.Size = 12;
                cellRange.Font.Bold = true;
                cellRange.FillColor = Color.FromArgb(189, 241, 255);
                cellRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cellRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;
                cellRange.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
                if (cellRange.ColumnIndex == 0) {
                    continue;
                }

                for (var i = 0; i < cellRange.ColumnIndex; i++) {
                    Cell cell = _worksheet[cellRange.RowIndex, i];
                    var firstOrDefault = _summaryBudgetRowCell.FirstOrDefault(f => f.ColumnIndex == i);
                    if (firstOrDefault != null || _summaryBudgetNotCell.Contains(cell)) {
                        continue;
                    }

                    _summaryBudgetNotCell.Add(cell);
                }
            }

            foreach (var cell in _summaryBudgetNotCell) {
                cell.Font.Size = 12;
                cell.Font.Bold = true;
                cell.FillColor = Color.FromArgb(189, 241, 255);
                cell.Borders.TopBorder.Color = Color.Black;
                cell.Borders.TopBorder.LineStyle = BorderLineStyle.Thin;
                cell.Borders.BottomBorder.Color = Color.Black;
                cell.Borders.BottomBorder.LineStyle = BorderLineStyle.Thin;
            }

            return Task.CompletedTask;
        }

        #endregion

        /// <summary>
        ///     Метод заполнения бюджетной сноски проекта
        /// </summary>
        /// <param name="rIndexIncrement">Инкремент индекса строки</param>
        /// <param name="regions">Регионы</param>
        /// <returns>
        ///     <see cref="Task.CompletedTask" />
        /// </returns>
        private Task FillProjectBudgetNote(int rIndexIncrement, List<RegionVM> regions) {
            // Если определён диапазон ячеек "Бюджет"
            if (_totalBudgetCellRange == null) {
                return Task.CompletedTask;
            }

            var col = _totalBudgetCellRange.LeftColumnIndex;
            var row = _totalBudgetCellRange.BottomRowIndex + rIndexIncrement + 1;

            var budgetProjectVat = regions
               .SelectMany(r => r.Channels)
               .Where(c => c.Outputs != 0)
               .SelectMany(c => c.Months)
               .Where(m => m.Budget > 0.000)
               .Sum(m => m.Budget);

            // Диапазон ячеек
            CellRange cellRange;
            // Ячейка
            Cell cell;
            // Значение для ячейки
            var cellValue = 0.0;

            #region Итого без НДС

            // Определяем 3 ячейки для наименовании столбца
            cellRange = _worksheet.Range.FromLTRB(col - 3, row, col - 1, row);
            // Обьеденяем 3 ячейки для наименовании столбца
            _worksheet.MergeCells(cellRange);
            // Задаем наименование обьеденёной ячейки
            cellRange.Value = BandHeaders.Summary + " без НДС:";

            // Ячейка
            cell = _worksheet[row, col];

            // Значение "Итого без НДС"
            cellValue = budgetProjectVat / (_project.Vat + 1);

            // Вызываем метод заполнения бюджета
            WorkSheetAllBudget(row, col, cellValue);

            // Вызываем метод применения стиля для ячейки
            StyleCellHLeftBSThin(cell, Color.FromArgb(255, 214, 182));
            // Вызываем метод применения стиля для диапазона ячеек
            StyleCellRangeHLeftBSThin(cellRange, Color.FromArgb(255, 214, 182));

            // Если отображать столбец "Бюджет агентский"
            if (_isShowBudgetAgency) {
                // Ячейка
                cell = _worksheet[row, col + 1];

                // Значение бюджета без НДС "Бюджета агенсткий"
                cellValue = budgetProjectVat * (1.0 - _project.Margin) / (_project.Vat + 1);

                // Вызываем метод заполнения бюджета
                WorkSheetAllBudget(row, col + 1, cellValue);

                // Вызываем метод применения стиля для ячейки
                StyleCellHLeftBSThin(cell, Color.FromArgb(255, 214, 182));
            }

            row++;

            #endregion

            #region НДС проекта

            // Определяем 3 ячейки для наименовании столбца
            cellRange = _worksheet.Range.FromLTRB(col - 3, row, col - 1, row);
            // Обьеденяем 3 ячейки для наименовании столбца
            _worksheet.MergeCells(cellRange);
            // Задаем наименование обьеденёной ячейки
            cellRange.Value = $"НДС {_project.Vat * 100} %:";

            // Ячейка
            cell = _worksheet[row, col];

            // Значение "НДС проекта"
            cellValue = budgetProjectVat - (budgetProjectVat / (_project.Vat + 1));

            // Вызываем метод заполнения бюджета
            WorkSheetAllBudget(row, col, cellValue);

            // Вызываем метод применения стиля для ячейки
            StyleCellHLeftBSThin(cell, Color.White);
            // Вызываем метод применения стиля для диапазона ячеек
            StyleCellRangeHLeftBSThin(cellRange, Color.White);

            // Если отображать столбец "Бюджет агентский"
            if (_isShowBudgetAgency) {
                // Ячейка
                cell = _worksheet[row, col + 1];

                // Значение бюджета без НДС "Бюджета агенсткий"
                cellValue = (budgetProjectVat                   * (1.0          - _project.Margin))
                  - (budgetProjectVat * (1.0 - _project.Margin) / (_project.Vat + 1));

                // Вызываем метод заполнения бюджета
                WorkSheetAllBudget(row, col + 1, cellValue);

                // Вызываем метод применения стиля для ячейки
                StyleCellHLeftBSThin(cell, Color.White);
            }

            row++;

            #endregion

            #region Итого с НДС

            // Определяем 3 ячейки для наименовании столбца
            cellRange = _worksheet.Range.FromLTRB(col - 3, row, col - 1, row);
            // Обьеденяем 3 ячейки для наименовании столбца
            _worksheet.MergeCells(cellRange);
            // Задаем наименование обьеденёной ячейки
            cellRange.Value = BandHeaders.Summary + " c НДС:";

            // Ячейка
            cell = _worksheet[row, col];

            // Значение "Итого с НДС"
            cellValue = budgetProjectVat;

            // Вызываем метод заполнения бюджета
            WorkSheetAllBudget(row, col, cellValue);

            // Вызываем метод применения стиля для ячейки
            StyleCellHLeftBSThin(cell, Color.FromArgb(240, 253, 255));
            // Вызываем метод применения стиля для диапазона ячеек
            StyleCellRangeHLeftBSThin(cellRange, Color.FromArgb(240, 253, 255));

            // Если отображать столбец "Бюджет агентский"
            if (_isShowBudgetAgency) {
                // Ячейка
                cell = _worksheet[row, col + 1];

                // Значение "Итого с НДС" "Бюджета агенсткий"
                cellValue = budgetProjectVat * (1.0 - _project.Margin);

                // Вызываем метод заполнения бюджета
                WorkSheetAllBudget(row, col + 1, cellValue);

                // Вызываем метод применения стиля для ячейки
                StyleCellHLeftBSThin(cell, Color.FromArgb(240, 253, 255));
            }

            row++;

            #endregion

            #region Комиссия агенства без НДС

            // Определяем 3 ячейки для наименовании столбца
            cellRange = _worksheet.Range.FromLTRB(col - 3, row, col - 1, row);
            // Обьеденяем 3 ячейки для наименовании столбца
            _worksheet.MergeCells(cellRange);
            // Задаем наименование обьеденёной ячейки
            cellRange.Value = "Комиссия агенства " + (_project.AgencyCommission * 100) + "% без НДС:";

            // Ячейка
            cell = _worksheet[row, col];

            // Значение "Комиссия агенства без НДС"
            cellValue = budgetProjectVat / (_project.Vat + 1) * _project.AgencyCommission;

            // Вызываем метод заполнения бюджета
            WorkSheetAllBudget(row, col, cellValue);

            // Вызываем метод применения стиля для ячейки
            StyleCellHLeftBSThin(cell, Color.White);
            // Вызываем метод применения стиля для диапазона ячеек
            StyleCellRangeHLeftBSThin(cellRange, Color.White);

            row++;

            #endregion

            #region Комиссия агенства с НДС

            // Определяем 3 ячейки для наименовании столбца
            cellRange = _worksheet.Range.FromLTRB(col - 3, row, col - 1, row);
            // Обьеденяем 3 ячейки для наименовании столбца
            _worksheet.MergeCells(cellRange);
            // Задаем наименование обьеденёной ячейки
            cellRange.Value = "Комиссия агенства " + (_project.AgencyCommission * 100) + "% с уч. НДС:";

            // Ячейка
            cell = _worksheet[row, col];

            // Значение "Комиссия агенства с НДС"
            cellValue = budgetProjectVat * _project.AgencyCommission;

            // Вызываем метод заполнения бюджета
            WorkSheetAllBudget(row, col, cellValue);

            // Вызываем метод применения стиля для ячейки
            StyleCellHLeftBSThin(cell, Color.FromArgb(240, 253, 255));
            // Вызываем метод применения стиля для диапазона ячеек
            StyleCellRangeHLeftBSThin(cellRange, Color.FromArgb(240, 253, 255));

            row++;

            #endregion

            #region Итоговый бюджет + комиссия агенства без НДС

            // Определяем 3 ячейки для наименовании столбца
            cellRange = _worksheet.Range.FromLTRB(col - 3, row, col - 1, row);
            // Обьеденяем 3 ячейки для наименовании столбца
            _worksheet.MergeCells(cellRange);
            // Задаем наименование обьеденёной ячейки
            cellRange.Value = "Итоговый бюджет + АК без НДС:";

            //Ячейка
            cell = _worksheet[row, col];

            // Значение "Итоговый бюджет + комиссия агенства без НДС"
            cellValue = (budgetProjectVat              / (_project.Vat + 1)) +
                (budgetProjectVat / (_project.Vat + 1) * _project.AgencyCommission);

            // Вызываем метод заполнения бюджета
            WorkSheetAllBudget(row, col, cellValue);

            // Вызываем метод применения стиля для ячейки
            StyleCellHLeftBSThin(cell, Color.White);
            // Вызываем метод применения стиля для диапазона ячеек
            StyleCellRangeHLeftBSThin(cellRange, Color.White);

            row++;

            #endregion

            #region Итоговый бюджет + комиссия агенства с НДС

            // Определяем 3 ячейки для наименовании столбца
            cellRange = _worksheet.Range.FromLTRB(col - 3, row, col - 1, row);
            // Обьеденяем 3 ячейки для наименовании столбца
            _worksheet.MergeCells(cellRange);
            // Задаем наименование обьеденёной ячейки
            cellRange.Value = "Итоговый бюджет + АК с уч. НДС:";

            // Ячейка
            cell = _worksheet[row, col];

            // Значение "Итоговый бюджет + комиссия агенства с НДС"
            cellValue = budgetProjectVat + (budgetProjectVat * _project.AgencyCommission);

            // Вызываем метод заполнения бюджета
            WorkSheetAllBudget(row, col, cellValue);

            // Вызываем метод применения стиля для ячейки
            StyleCellHLeftBSThin(cell, Color.FromArgb(245, 174, 174));
            // Вызываем метод применения стиля для диапазона ячеек
            StyleCellRangeHLeftBSThin(cellRange, Color.FromArgb(245, 174, 174));

            #endregion

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод применения стиля для диапазона ячеек
        /// </summary>
        /// <param name="cellRange">Диапазон ячеек</param>
        /// <param name="color">Цвет</param>
        private void StyleCellRangeHLeftBSThin(CellRange cellRange, Color color) {
            cellRange.Font.Size = 12;
            cellRange.Font.Bold = true;
            cellRange.FillColor = color;
            cellRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            cellRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;
            cellRange.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
        }

        /// <summary>
        ///     Метод применения стиля для ячейки
        /// </summary>
        /// <param name="cell">Ячейка</param>
        /// <param name="color">Цвет</param>
        private void StyleCellHLeftBSThin(Cell cell, Color color) {
            cell.Font.Size = 12;
            cell.Font.Bold = true;
            cell.FillColor = color;
            cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;
            cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
        }

        /// <summary>
        ///     Метод применения стилей на данные
        /// </summary>
        private Task ApplyDataStyles() {
            foreach (var group in _regionToChannelsPortionCells) {
                var col = group.Value.FirstOrDefault()?.ColumnIndex ?? 999;
                var top = group.Value.FirstOrDefault()?.RowIndex    ?? 999;
                var bottom = group.Value.LastOrDefault()?.RowIndex  ?? 999;
                _worksheet.ConditionalFormattings.AddDataBarConditionalFormatting(
                    _worksheet.Range.FromLTRB(col, top, col, bottom),
                    _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                    _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                    Color.CadetBlue
                );
            }

            foreach (var group in _regionToMonthsAffinityCells) {
                foreach (var innerGroup in group.Value) {
                    var col = innerGroup.Value.FirstOrDefault()?.ColumnIndex ?? 999;
                    var top = innerGroup.Value.FirstOrDefault()?.RowIndex    ?? 999;
                    var bottom = innerGroup.Value.LastOrDefault()?.RowIndex  ?? 999;
                    _worksheet.ConditionalFormattings.AddColorScale2ConditionalFormatting(
                        _worksheet.Range.FromLTRB(col, top, col, bottom),
                        _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.MinMax),
                        Color.Moccasin,
                        _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.MinMax),
                        Color.CadetBlue
                    );
                }
            }

            foreach (var group in _monthsBudgetsCells) {
                foreach (var innerGroup in group.Value) {
                    var col = innerGroup.Value.FirstOrDefault()?.ColumnIndex ?? 999;
                    var top = innerGroup.Value.FirstOrDefault()?.RowIndex    ?? 999;
                    var bottom = innerGroup.Value.LastOrDefault()?.RowIndex  ?? 999;
                    _worksheet.ConditionalFormattings.AddDataBarConditionalFormatting(
                        _worksheet.Range.FromLTRB(col, top, col, bottom),
                        _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                        _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                        Color.CadetBlue
                    );
                }
            }

            foreach (var group in _monthsAgencyBudgetsCells) {
                foreach (var innerGroup in group.Value) {
                    var col = innerGroup.Value.FirstOrDefault()?.ColumnIndex ?? 999;
                    var top = innerGroup.Value.FirstOrDefault()?.RowIndex    ?? 999;
                    var bottom = innerGroup.Value.LastOrDefault()?.RowIndex  ?? 999;
                    _worksheet.ConditionalFormattings.AddDataBarConditionalFormatting(
                        _worksheet.Range.FromLTRB(col, top, col, bottom),
                        _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                        _worksheet.ConditionalFormattings.CreateValue(ConditionalFormattingValueType.Auto),
                        Color.CadetBlue
                    );
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод заполнения для отображения "Бюджета".
        /// </summary>
        /// <param name="rowIndex">Индекс строки</param>
        /// <param name="colIndex">Индекс ячейки</param>
        /// <param name="value">Значение</param>
        private void WorkSheetAllBudget(int rowIndex, int colIndex, double value) {
            _worksheet[rowIndex, colIndex].Value = value;
            _worksheet[rowIndex, colIndex].NumberFormat = "#,##0.00 ₽";
            _worksheet[rowIndex, colIndex].Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            _worksheet[rowIndex, colIndex].Alignment.Horizontal = SpreadsheetHorizontalAlignment.Left;
        }

        #endregion

        #endregion
    }
}
﻿using DevExpress.Mvvm.Native;
using DevExpress.Spreadsheet;
using RMPT.COMMON.Dictionaries;
using RMPT.DATA.ViewModels.Models;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace RMPT.Utils {
    public class RawDataChartFormatter {
        #region Свойства

        #region Данные

        /// <summary>
        ///     Проект
        /// </summary>
        private readonly ProjectVM _project;

        #endregion

        #region Документ

        /// <summary>
        ///     Книга
        /// </summary>
        private readonly IWorkbook _workbook;

        /// <summary>
        ///     Лист
        /// </summary>
        private readonly Worksheet _worksheet;

        /// <summary>
        ///     Ячейки заголовков
        /// </summary>
        private readonly Collection<Cell> _headerCells = new();

        #endregion

        #endregion

        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="project">Проект, для которого создаётся отчёт</param>
        public RawDataChartFormatter(ProjectVM project) {
            this._project = project;
            _workbook = new Workbook();
            _worksheet = _workbook.Worksheets.ActiveWorksheet;
        }

        #endregion

        #region Метод инициализации

        public async Task<IWorkbook> CreateDocument() {
            this._workbook.BeginUpdate();

            await this.FillHeaders();

            var fillDataTask = this.FillData();
            var applyHeadersStyleTask = this.ApplyHeadersStyle();

            await Task.WhenAll(fillDataTask, applyHeadersStyleTask);

            this._worksheet.ScrollTo(0, 0);
            this._worksheet.GetDataRange().AutoFitRows();
            this._worksheet.GetDataRange().AutoFitColumns();

            this._workbook.EndUpdate();

            return this._workbook;
        }

        #endregion

        #region Метод заполнения заголовков

        private Task FillHeaders() {
            int col = 0;
            const int row = 0;

            Cell cell = this._worksheet[row, col];
            cell.Value = ColumnHeaders.Region;
            this._headerCells.Add(cell);
            col++;

            cell = this._worksheet[row, col];
            cell.Value = ColumnHeaders.Channel;
            this._headerCells.Add(cell);
            col++;

            cell = this._worksheet[row, col];
            cell.Value = ColumnHeaders.TvrTargetAudience;
            this._headerCells.Add(cell);
            col++;

            cell = this._worksheet[row, col];
            cell.Value = ColumnHeaders.CumReach;
            this._headerCells.Add(cell);
            col++;

            cell = this._worksheet[row, col];
            cell.Value = ColumnHeaders.Affinity;
            this._headerCells.Add(cell);
            col++;

            cell = this._worksheet[row, col];
            cell.Value = $"{ColumnHeaders.CumReach} {this._project.Frequency}";
            this._headerCells.Add(cell);
            col++;

            cell = this._worksheet[row, col];
            cell.Value = ColumnHeaders.CostTRP;
            this._headerCells.Add(cell);
            col++;

            cell = this._worksheet[row, col];
            cell.Value = ColumnHeaders.CostReachCPT;
            this._headerCells.Add(cell);

            return Task.CompletedTask;
        }

        #endregion

        #region Метод заполнения данных

        private Task FillData() {
            int row = 1;
            // Итерируемся только по измеряемым регионам
            foreach (var region in this._project.Regions.Where(r => r.IsMeasurable).ToList()) {
                foreach (var channel in region.Channels) {
                    int col = 0;

                    // Если у канала нет аффинитивности или рейтингов - переходим к следующему
                    if (channel.Months.All(m =>
                        !m.IsActive || m.Affinity == 0.0 || (m.RatingPrime == 0 && m.RatingOffPrime == 0))) {
                        continue;
                    }

                    this._worksheet[row, col].Value = region.Name;
                    col++;

                    this._worksheet[row, col].Value = channel.Name;
                    col++;

                    this._worksheet[row, col].Value = channel.Months
                       .Where(m => m.IsActive && m.Affinity != 0.0 && (m.RatingPrime != 0 || m.RatingOffPrime != 0))
                       .Average(m => (m.RatingPrime + m.RatingOffPrime) / 2 * m.Affinity);
                    this._worksheet[row, col].NumberFormat = "0.0#";
                    col++;

                    this._worksheet[row, col].Value = channel.Audiences
                           .Where(a => a.IsTarget)
                           .GroupBy(a => a.Date.Month)
                           .Select(g => g.MaxBy(a => a.Date.Year))
                           .Average(a => a.CumReachPercent / 100 * a.WeightedTotal) /
                        channel.Audiences.Where(a => a.IsTarget).MaxBy(a => a.Date).WeightedTotal;
                    this._worksheet[row, col].NumberFormat = "0.0%";
                    col++;

                    this._worksheet[row, col].Value = channel.Months
                       .Where(m => m.IsActive && m.Affinity != 0.0)
                       .Average(m => m.Affinity);
                    this._worksheet[row, col].NumberFormat = "0.00";
                    col++;

                    var averageFrequencyCumReach = channel.Audiences
                       .Where(a => a.IsTarget)
                       .GroupBy(a => a.Date.Month)
                       .Select(g => g.MaxBy(a => a.Date.Year))
                       .Average(a => a.FrequencyPercent / 100 * a.WeightedTotal);

                    this._worksheet[row, col].Value = averageFrequencyCumReach / channel.Audiences
                       .Where(a => a.IsTarget)
                       .MaxBy(a => a.Date).WeightedTotal;
                    this._worksheet[row, col].NumberFormat = "0.0%";
                    col++;

                    this._worksheet[row, col].Value = channel.Months.Sum(m =>
                        m.Budget + (m.Budget * m.Channel.Region.Project.AgencyCommission)
                    ) / channel.TRP;
                    this._worksheet[row, col].NumberFormat = "#,##0.00 ₽";
                    col++;

                    this._worksheet[row, col].Value = channel.Months.Sum(m =>
                        m.Budget + (m.Budget * m.Channel.Region.Project.AgencyCommission)
                    ) / averageFrequencyCumReach;
                    this._worksheet[row, col].NumberFormat = "#,##0.00 ₽";

                    row++;
                }
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Метод применения стилей заголовков

        private Task ApplyHeadersStyle() {
            foreach (var cell in this._headerCells) {
                cell.Font.Size = 12;
                cell.Font.Bold = true;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Thin);
            }

            return Task.CompletedTask;
        }

        #endregion
    }
}
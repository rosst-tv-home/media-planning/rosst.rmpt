﻿using DevExpress.Data;
using DevExpress.Xpf.Core.ConditionalFormatting;
using DevExpress.Xpf.Core.Native;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using RMPT.COMMON.Dictionaries;
using RMPT.DATA.ViewModels.Models;
using RMPT.DATA.ViewModels.Models.Timing;
using RMPT.GRID.Elements;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace RMPT.Utils {
    public class ExtendedTableFormatter {
        #region Свойства

        #region Группы столбцов

        /// <summary>
        ///     Группа столбцов <see cref="BandHeaders.RawData"/> "Данные сырья" каналов
        /// </summary>
        public ExtendedBand ChannelRawDataBand { get; private set; } = null!;

        /// <summary>
        ///     Группа столбцов <see cref="BandHeaders.Distribution"/> "Распределение" каналов
        /// </summary>
        public ExtendedBand ChannelDistributionBand { get; private set; } = null!;

        /// <summary>
        ///     Группа столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей" каналов
        /// </summary>
        public ExtendedBand ChannelTimingDistributionBand { get; private set; } = null!;

        /// <summary>
        ///     Группы столбцов
        /// </summary>
        public readonly List<ExtendedBand> Bands = new();

        /// <summary>
        ///     Группы столбцов каналов
        /// </summary>
        public readonly List<ExtendedBand> ChannelsBands = new();

        /// <summary>
        ///     Группы столбцов месяцев
        /// </summary>
        public readonly List<ExtendedBand> MonthsBands = new();

        /// <summary>
        ///     Группы столбцов недель
        /// </summary>
        public readonly List<ExtendedBand> WeeksBands = new();

        /// <summary>
        ///     Группы столбцов <see cref="BandHeaders.Distribution"/> "Распределение" месяцев
        /// </summary>
        public readonly List<ExtendedBand> MonthsDistributionBands = new();

        /// <summary>
        ///     Группы столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей"
        /// </summary>
        public readonly List<ExtendedBand> TimingDistributionBands = new();

        /// <summary>
        ///     Группы столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей" месяцев
        /// </summary>
        public readonly List<ExtendedBand> MonthsTimingDistributionBands = new();

        /// <summary>
        ///     Группы столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей" недель
        /// </summary>
        public readonly List<ExtendedBand> WeeksTimingDistributionBands = new();

        /// <summary>
        ///     Группы столбцов <see cref="BandHeaders.Inventory"/> "Инвентарь"
        /// </summary>
        public readonly List<ExtendedBand> InventoryBands = new();

        /// <summary>
        ///     Группы столбцов <see cref="BandHeaders.Budget"/> "Бюджет"
        /// </summary>
        public readonly List<ExtendedBand> BudgetBands = new();

        /// <summary>
        ///     Группа столбцов <see cref="BandHeaders.Outputs"/> "Выходы"
        /// </summary>
        public readonly List<ExtendedBand> OutputsBands = new();

        #endregion

        #region Столбцы

        /// <summary>
        ///     Столбцы таблицы
        /// </summary>
        public readonly List<ExtendedColumn> Columns = new();

        #endregion

        #region Условия форматирования

        /// <summary>
        ///     Условия форматирования
        /// </summary>
        public readonly List<FormatConditionBase> FormatConditions = new();

        #endregion

        #region Сводки по группам

        /// <summary>
        ///     Сводки по группам
        /// </summary>
        public readonly List<GridSummaryItem> GroupSummary = new();

        #endregion

        #endregion

        #region Методы

        /// <summary>
        ///     Метод генерирования таблицы проекта
        /// </summary>
        public async Task GenerateTable(
            int projectPlanningYear,
            string frequency,
            IReadOnlyCollection<Tuple<DateTime, bool>> weeks,
            IReadOnlyCollection<Tuple<int, bool>> timings,
            int baseTiming = 20,
            bool isFederal = false,
            bool isWeeksBandsVisible = false,
            bool isChannelRawDataBandVisible = false,
            bool isChannelDistributionBandVisible = true,
            bool isMonthDistributionBandVisible = false
        ) {
            #if DEBUG
            Debug.WriteLine($"Начало: {nameof(GenerateTable)}");
            var measure = Stopwatch.StartNew();
            #endif

            var channelBaseInfoBandTask = GenerateChannelBaseInfoBand();
            var channelRawDataBandTask = GenerateChannelRawDataBand(isChannelRawDataBandVisible, frequency);
            var channelDistributionBandTask =
                GenerateChannelDistributionBand(isFederal, isChannelDistributionBandVisible);
            var channelTimingDistributionBandTask = GenerateChannelTimingDistributionBand(timings);
            var channelInventoryBandTask = GenerateChannelInventoryBand(baseTiming);
            var monthsBandsTask = GenerateMonthsBands(projectPlanningYear, weeks, timings, baseTiming, isFederal,
                isWeeksBandsVisible, isMonthDistributionBandVisible);

            await Task.WhenAll(
                channelBaseInfoBandTask,
                channelRawDataBandTask,
                channelDistributionBandTask,
                channelTimingDistributionBandTask,
                channelInventoryBandTask,
                monthsBandsTask
            );

            Bands.Add(channelBaseInfoBandTask.Result);
            Bands.Add(channelRawDataBandTask.Result);
            Bands.Add(channelDistributionBandTask.Result);
            Bands.Add(channelTimingDistributionBandTask.Result);
            Bands.Add(channelInventoryBandTask.Result);
            Bands.AddRange(monthsBandsTask.Result);

            #if DEBUG
            Debug.WriteLine($"Завершение: {nameof(GenerateTable)}: {measure.ElapsedMilliseconds.ToString()} ms");
            measure.Stop();
            #endif
        }

        #region Методы генерирования части таблицы, относящейся к каналам

        /// <summary>
        ///    Метод генерирования группы столбцов <see cref="BandHeaders.BaseInfo"/> "Базовая информация"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.BaseInfo"/> "Базовая информация"</returns>
        private Task<ExtendedBand> GenerateChannelBaseInfoBand() {
            // Группа стобцов "Базовая информация"
            var band = new ExtendedBand(BandHeaders.BaseInfo, fixedStyle: FixedStyle.Left);

            var checkEdit = new FrameworkElementFactory(typeof(CheckEdit));
            checkEdit.SetValue(FrameworkElement.NameProperty, "PART_Editor");
            checkEdit.SetValue(FrameworkElement.VerticalAlignmentProperty, VerticalAlignment.Center);
            checkEdit.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Center);
            checkEdit.SetBinding(
                CheckEdit.IsCheckedProperty,
                new Binding {
                    Mode = BindingMode.TwoWay,
                    Path = new PropertyPath($"RowData.Row.{nameof(ChannelVM.IsActive)}")
                }
            );
            var column = new ExtendedColumn(
                ColumnHeaders.Active, nameof(ChannelVM.IsActive),
                band, width: 20
            ) {
                CellTemplate = new DataTemplate {
                    VisualTree = checkEdit
                }
            };
            band.Columns.Add(column);

            // Столбец "Регион"
            band.Columns.Add(new ExtendedColumn(
                ColumnHeaders.Region, $"{nameof(ChannelVM.Region)}.{nameof(RegionVM.Name)}",
                band, true, groupIndex: 0, width: 100
            ));

            // Столбец "Канал"
            band.Columns.Add(new ExtendedColumn(
                ColumnHeaders.Channel, nameof(ChannelVM.Name),
                band, true, width: 100
            ));

            // Столбец "Точка продаж"
            band.Columns.Add(new ExtendedColumn(
                ColumnHeaders.SalePoint, nameof(ChannelVM.SalePoint),
                band, true, width: 35
            ));

            // Столбец "Тип продаж"
            band.Columns.Add(new ExtendedColumn(
                ColumnHeaders.SaleType, nameof(ChannelVM.SaleType),
                band, true, width: 55
            ));

            ChannelsBands.Add(band);

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.RawData"/> "Данные сырья"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.RawData"/> "Данные сырья"</returns>
        private Task<ExtendedBand> GenerateChannelRawDataBand(
            bool isChannelRawDataBandVisible, string frequency
        ) {
            // Группа столбцов "Данные сырья"
            var band = new ExtendedBand(BandHeaders.RawData, isChannelRawDataBandVisible, tag: ExtendedBandTag.RawData);

            // Столбец "TVR ЦА"
            band.Columns.Add(new ExtendedColumn(
                ColumnHeaders.TvrTargetAudience, $"{nameof(ChannelVM.TargetAudience)}.{nameof(AudienceVM.TVR)}",
                band, true, "n2"
            ));

            // Столбец "TVR БА"
            band.Columns.Add(new ExtendedColumn(
                ColumnHeaders.TvrBaseAudience, $"{nameof(ChannelVM.BaseAudience)}.{nameof(AudienceVM.TVR)}",
                band, true, "n2"
            ));

            // Столбец "Cum. Reach ЦА"
            band.Columns.Add(new ExtendedColumn(
                ColumnHeaders.CumReachTargetAudiencePortion,
                $"{nameof(ChannelVM.TargetAudience)}.{nameof(AudienceVM.CumReachPercent)}",
                band, true, "{0:n2}%"
            ));

            // Столбец "Sample"
            band.Columns.Add(new ExtendedColumn(
                ColumnHeaders.Sample,
                $"{nameof(ChannelVM.BaseAudience)}.{nameof(AudienceVM.Sample)}",
                band, true
            ));

            // Столбец "Частота проекта"
            band.Columns.Add(new ExtendedColumn(
                frequency,
                $"{nameof(ChannelVM.BaseAudience)}.{nameof(AudienceVM.FrequencyPercent)}",
                band, true, "n2", width: 43
            ));

            ChannelsBands.Add(band);
            ChannelRawDataBand = band;

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.Distribution"/> "Распределение"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.Distribution"/> "Распределение"</returns>
        private Task<ExtendedBand> GenerateChannelDistributionBand(bool isFederal, bool isVisible) {
            // Создаём "Распределение"
            var band = new ExtendedBand(BandHeaders.Distribution, isVisible);

            // Столбец "% Канал"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.ChannelPortion, nameof(ChannelVM.Portion),
                band, displayMask: "p2", editMask: "p", editMaskType: MaskType.Numeric, width: 63,
                type: UnboundColumnType.Decimal
            ));
            
            // Условное форматирование
            FormatConditions.Add(new FormatCondition {
                FieldName = nameof(ChannelVM.Portion),
                Expression = $"Round([{nameof(ChannelVM.Region)}.{nameof(RegionVM.TotalActiveChannelsPortion)}], 2) <> 1.0",
                PredefinedFormatName = "LightRedFillWithDarkRedText"
            });

            // Столбец "% Корректировки Affinity"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.AffinityCorrectivePercent,
                nameof(ChannelVM.AffinityCorrectivePercent),
                band, true, "p2", width: 60, type: UnboundColumnType.Decimal
            ));

            // Столбец "Affinity"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.Affinity, nameof(ChannelVM.Affinity),
                band, displayMask: "n2", width: 60,
                type: UnboundColumnType.Decimal
            ));

            // Условное форматирование
            FormatConditions.Add(
                new ColorScaleFormatCondition {
                    FieldName = $"{nameof(ChannelVM.Affinity)}",
                    PredefinedFormatName = "GreenYellowRedColorScale"
                }
            );

            // Стобец "% Fix" или "% Float"
            band.Columns.Add(new ExtendedUnboundColumn(
                isFederal ? ColumnHeaders.FixPortion : ColumnHeaders.FloatPortion,
                nameof(ChannelVM.FixOrFloatPortion),
                band, displayMask: "p2", editMask: "p", editMaskType: MaskType.Numeric, width: 60,
                type: UnboundColumnType.Decimal
            ));

            // Стобец "% Fix P." или "% Float P."
            band.Columns.Add(new ExtendedUnboundColumn(
                isFederal ? ColumnHeaders.FixPrimePortion : ColumnHeaders.FloatPrimePortion,
                nameof(ChannelVM.FixOrFloatPrimePortion),
                band, displayMask: "p2", editMask: "p", editMaskType: MaskType.Numeric, width: 60,
                type: UnboundColumnType.Decimal
            ));

            // Стобец "% S.Fix" или "% Fix"
            band.Columns.Add(new ExtendedUnboundColumn(
                isFederal ? ColumnHeaders.SuperFixPortion : ColumnHeaders.FixPortion,
                nameof(ChannelVM.SuperFixOrFixPortion),
                band, true, "p2", "p", MaskType.Numeric, width: 60, type: UnboundColumnType.Decimal
            ));

            // Стобец "% S.Fix P." или "% Fix P."
            band.Columns.Add(new ExtendedUnboundColumn(
                isFederal ? ColumnHeaders.SuperFixPrimePortion : ColumnHeaders.FixPrimePortion,
                nameof(ChannelVM.SuperFixOrFixPrimePortion),
                band, displayMask: "p2", editMask: "p", editMaskType: MaskType.Numeric, width: 60,
                type: UnboundColumnType.Decimal
            ));

            ChannelsBands.Add(band);
            // Запоминаем группу столбцов "Распределение" в переменной
            ChannelDistributionBand = band;

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей"</returns>
        private Task<ExtendedBand> GenerateChannelTimingDistributionBand(
            IReadOnlyCollection<Tuple<int, bool>> timings
        ) {
            // Группа столбцов "Распределение хронометражей"
            var band = new ExtendedBand(
                BandHeaders.TimingDistribution,
                timings.Any(timing => timing.Item2),
                tag: ExtendedBandTag.TimingDistribution
            );

            band.Columns.Add(new ExtendedUnboundColumn(
                nameof(ChannelVM.TotalTimingPortion),
                nameof(ChannelVM.TotalTimingPortion),
                band, true, "p0", isVisible: false
            ));

            var timingIndex = 0;
            for (var timing = 5; timing <= 60; timing += 5) {
                // Столбец "{Хронометраж} сек."
                band.Columns.Add(new ExtendedUnboundColumn(
                    $"{timing} {ColumnHeaders.Sec}",
                    $"{nameof(ChannelVM.Timings)}[{timingIndex}]." +
                    $"{nameof(ChannelTimingVM.Portion)}",
                    band, displayMask: "p0", editMask: "p", editMaskType: MaskType.Numeric,
                    isVisible: timings.First(x => x.Item1 == timing).Item2, type: UnboundColumnType.Decimal, uid: timing
                ));

                // FormatConditions.Add(new FormatCondition {
                //     FieldName = $"{nameof(ChannelVM.Timings)}[{timingIndex}].{nameof(ChannelTimingVM.Portion)}",
                //     Expression = $"Round([{nameof(ChannelVM.TotalTimingPortion)}], 1) <> 1",
                //     PredefinedFormatName = "LightRedFillWithDarkRedText",
                // });

                timingIndex += 1;
            }

            ChannelsBands.Add(band);
            TimingDistributionBands.Add(band);
            ChannelTimingDistributionBand = band;

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.Inventory"/> "Инвентарь"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.Inventory"/> "Инвентарь"</returns>
        private Task<ExtendedBand> GenerateChannelInventoryBand(int baseTiming) {
            // Гуппа столбцов "Инвентарь"
            var band = new ExtendedBand(BandHeaders.Inventory);

            // Столбец "TRP"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.TRP, nameof(ChannelVM.TRP),
                band, displayMask: "n2", width: 55, type: UnboundColumnType.Decimal
            ));

            // Столбец "TRP [базовый хронометраж проекта]"
            band.Columns.Add(new ExtendedUnboundColumn(
                $"{ColumnHeaders.TRP} {baseTiming.ToString()}", nameof(ChannelVM.GivenTRP),
                band, true, "n2", width: 55, type: UnboundColumnType.Decimal
            ));

            // Столбец "GRP"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.GRP, nameof(ChannelVM.GRP),
                band, true, "n2", width: 55, type: UnboundColumnType.Decimal
            ));

            // Столбец "GRP [базовый хронометраж проекта]"
            band.Columns.Add(new ExtendedUnboundColumn(
                $"{ColumnHeaders.GRP} {baseTiming.ToString()}", nameof(ChannelVM.GivenGRP),
                band, true, "n2", width: 55, type: UnboundColumnType.Decimal
            ));

            // Столбец "Минуты"
            band.Columns.Add(new ExtendedUnboundColumn(
                $"{ColumnHeaders.Minutes}", nameof(ChannelVM.Minutes),
                band, true, "n2", width: 55, type: UnboundColumnType.Decimal
            ));

            ChannelsBands.Add(band);
            InventoryBands.Add(band);

            return Task.FromResult(band);
        }

        #endregion

        #region Методы генерирования части таблицы, относящейся к месяцам

        /// <summary>
        ///     Метод генерирования групп столбцов месяцев
        /// </summary>
        /// <returns>Группы столбцов месяцев</returns>
        private async Task<List<ExtendedBand>> GenerateMonthsBands(
            int projectPlanningYear,
            IReadOnlyCollection<Tuple<DateTime, bool>> weeks,
            IReadOnlyCollection<Tuple<int, bool>> timings,
            int baseTiming, bool isFederal, bool isWeeksBandsVisible, bool isMonthDistributionBandVisible
        ) {
            // Группы столбцов месяцев
            var bands = new List<ExtendedBand>();

            for (var y = 0; y <= 1; y++) {
                for (var m = 1; m <= 12; m++) {
                    var monthIndex = y == 0 ? m - 1 : m + 11;
                    var monthDate = new DateTime(projectPlanningYear + y, m, 1);

                    // Группа столбцов "Месяц"
                    var band = new ExtendedBand(
                        monthDate.ToString("Y"),
                        weeks.Any(w =>
                            w.Item1.Year  == monthDate.Year &&
                            w.Item1.Month == monthDate.Month &&
                            w.Item2
                        ),
                        tag: ExtendedBandTag.Month, uid: monthDate
                    );
                    // Строка привзки месяца
                    var monthBindingString = $"{nameof(ChannelVM.Months)}[{monthIndex.ToString()}].";

                    var monthDaysBandTask = GenerateMonthDaysBand(monthBindingString);
                    var monthRatingsBandTask = GenerateMonthRatingsBand(monthBindingString);
                    var monthDistributionBandTask = GenerateMonthDistributionBand(monthBindingString, isFederal,
                        isMonthDistributionBandVisible);
                    var monthTimingDistributionBandTask =
                        GenerateMonthTimingDistributionBand(monthBindingString, timings);
                    var monthInventoryBandTask = GenerateMonthInventoryBand(monthBindingString, baseTiming);
                    var monthOutputsBandTask = GenerateMonthOutputsBand(monthBindingString);
                    var monthBudgetBandTask = GenerateMonthBudgetBand(monthBindingString);
                    var weeksBandsTask = GenerateWeeksBands(monthBindingString, monthDate, weeks, timings, baseTiming,
                        isWeeksBandsVisible);

                    await Task.WhenAll(
                        monthDaysBandTask,
                        monthRatingsBandTask,
                        monthDistributionBandTask,
                        monthTimingDistributionBandTask,
                        monthInventoryBandTask,
                        monthOutputsBandTask,
                        monthBudgetBandTask,
                        weeksBandsTask
                    );

                    band.Bands.Add(monthDaysBandTask.Result);
                    band.Bands.Add(monthRatingsBandTask.Result);
                    band.Bands.Add(monthDistributionBandTask.Result);
                    band.Bands.Add(monthTimingDistributionBandTask.Result);
                    band.Bands.Add(monthInventoryBandTask.Result);
                    band.Bands.Add(monthOutputsBandTask.Result);
                    band.Bands.Add(monthBudgetBandTask.Result);
                    foreach (var weekBand in weeksBandsTask.Result) {
                        band.Bands.Add(weekBand);
                    }

                    bands.Add(band);
                }
            }

            MonthsBands.AddRange(bands);

            return bands;
        }

        /// <summary>
        ///     Метод генерирования группы стобцов <see cref="BandHeaders.Days"/> "Дни"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.Days"/> "Дни"</returns>
        private Task<ExtendedBand> GenerateMonthDaysBand(string binding) {
            // Группа столбцов "Дни"
            var band = new ExtendedBand(BandHeaders.Days);

            // Столбец "Акт. дни"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.ActiveDays, $"{binding}{nameof(MonthVM.ActiveDays)}",
                band, true, type: UnboundColumnType.Integer
            ));

            // Cтолбец "Всего"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.Total, $"{binding}{nameof(MonthVM.Days)}",
                band, true, type: UnboundColumnType.Integer
            ));

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.Ratings"/> "Рейтинги"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.Ratings"/> "Рейтинги"</returns>
        private Task<ExtendedBand> GenerateMonthRatingsBand(string binding) {
            // Группа столбцов "Рейтинги"
            var band = new ExtendedBand(BandHeaders.Ratings);

            // Столбец "Prime"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.Prime, $"{binding}{nameof(MonthVM.RatingPrime)}",
                band, displayMask: "n2"
            ));

            // Столбец "Off Prime"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.OffPrime, $"{binding}{nameof(MonthVM.RatingOffPrime)}",
                band, displayMask: "n2"
            ));

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.Distribution"/> "Распределение"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.Distribution"/> "Распределение"</returns>
        private Task<ExtendedBand> GenerateMonthDistributionBand(
            string binding, bool isFederal, bool isMonthDistributionBandVisible
        ) {
            // Группа столбцов "Распределение"
            var band = new ExtendedBand(
                BandHeaders.Distribution,
                isMonthDistributionBandVisible,
                tag: ExtendedBandTag.MonthDistribution
            );

            // Столбец "Affinity"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.Affinity, $"{binding}{nameof(MonthVM.Affinity)}",
                band, displayMask: "n2", width: 60,
                type: UnboundColumnType.Decimal
            ));

            // Условие форматирования
            FormatConditions.Add(
                new ColorScaleFormatCondition {
                    FieldName = $"{binding}{nameof(MonthVM.Affinity)}",
                    PredefinedFormatName = "GreenYellowRedColorScale"
                }
            );

            // Столбец "% Fix" или "% Float"
            band.Columns.Add(new ExtendedUnboundColumn(
                isFederal ? ColumnHeaders.FixPortion : ColumnHeaders.FloatPortion,
                $"{binding}{nameof(MonthVM.FixOrFloatPortion)}",
                band, displayMask: "p2", editMask: "p", editMaskType: MaskType.Numeric, width: 60
            ));

            // Столбец "% Fix P." или "% Float P."
            band.Columns.Add(new ExtendedUnboundColumn(
                isFederal ? ColumnHeaders.FixPrimePortion : ColumnHeaders.FloatPrimePortion,
                $"{binding}{nameof(MonthVM.FixOrFloatPrimePortion)}",
                band, displayMask: "p2", editMask: "p", editMaskType: MaskType.Numeric, width: 60
            ));

            // Столбец "% S.Fix" или "% Fix"
            band.Columns.Add(new ExtendedUnboundColumn(
                isFederal ? ColumnHeaders.SuperFixPortion : ColumnHeaders.FixPortion,
                $"{binding}{nameof(MonthVM.SuperFixOrFixPortion)}",
                band, true, "p2", "p", MaskType.Numeric, width: 60
            ));

            // Столбец "% S.Fix P." или "% Fix P."
            band.Columns.Add(new ExtendedUnboundColumn(
                isFederal ? ColumnHeaders.SuperFixPrimePortion : ColumnHeaders.FixPrimePortion,
                $"{binding}{nameof(MonthVM.SuperFixOrFixPrimePortion)}",
                band, displayMask: "p2", editMask: "p", editMaskType: MaskType.Numeric, width: 60
            ));

            MonthsDistributionBands.Add(band);

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей"</returns>
        private Task<ExtendedBand> GenerateMonthTimingDistributionBand(
            string binding,
            IReadOnlyCollection<Tuple<int, bool>> timings
        ) {
            // Группа столбцов "Распределение хронометражей"
            var band = new ExtendedBand(
                BandHeaders.TimingDistribution,
                timings.Any(timing => timing.Item2),
                tag: ExtendedBandTag.TimingDistribution
            );

            var timingIndex = 0;
            for (var timing = 5; timing <= 60; timing += 5) {
                // Столбец "{Хронометраж} сек."
                band.Columns.Add(new ExtendedUnboundColumn(
                    $"{timing.ToString()} {ColumnHeaders.Sec}",
                    $"{binding}{nameof(MonthVM.Timings)}[{timingIndex.ToString()}].{nameof(MonthTimingVM.Portion)}",
                    band, displayMask: "p0", editMask: "p", editMaskType: MaskType.Numeric,
                    isVisible: timings.First(x => x.Item1 == timing).Item2, uid: timing
                ));

                timingIndex += 1;
            }

            TimingDistributionBands.Add(band);
            MonthsTimingDistributionBands.Add(band);

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.Inventory"/> "Инвентарь"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.Inventory"/> "Инвентарь"</returns>
        private Task<ExtendedBand> GenerateMonthInventoryBand(string binding, int baseTiming) {
            // Группа столбцов "Инвентарь"
            var band = new ExtendedBand(BandHeaders.Inventory);

            // Столбец "TRP"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.TRP, $"{binding}{nameof(MonthVM.TRP)}",
                band, displayMask: "n2", width: 55
            ));

            // Столбец "TRP [Базовый хронометраж проекта]"
            band.Columns.Add(new ExtendedUnboundColumn(
                $"{ColumnHeaders.TRP} {baseTiming.ToString()}", $"{binding}{nameof(MonthVM.GivenTRP)}",
                band, true, "n2", width: 55
            ));

            // Столбец "GRP"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.GRP, $"{binding}{nameof(MonthVM.GRP)}",
                band, true, "n2", width: 55
            ));

            // Столбец "GRP [Базовый хронометраж проекта]"
            band.Columns.Add(new ExtendedUnboundColumn(
                $"{ColumnHeaders.GRP} {baseTiming.ToString()}", $"{binding}{nameof(MonthVM.GivenGRP)}",
                band, displayMask: "n2", width: 55
            ));

            // Столбец "Минуты"
            band.Columns.Add(new ExtendedUnboundColumn(
                $"{ColumnHeaders.Minutes}", $"{binding}{nameof(MonthVM.Minutes)}",
                band, true, "n2", width: 55
            ));

            InventoryBands.Add(band);

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.Outputs"/> "Выходы"
        /// </summary>
        /// <returns>Гурппа столбцов <see cref="BandHeaders.Outputs"/> "Выходы"</returns>
        private Task<ExtendedBand> GenerateMonthOutputsBand(string binding) {
            // Группа столбцов "Выходы"
            var band = new ExtendedBand(BandHeaders.Outputs);

            // Столбец "Prime"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.Prime, $"{binding}{nameof(MonthVM.OutputsPrime)}",
                band, true, "n2", width: 55
            ));

            // Столбец "Off Prime"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.OffPrime, $"{binding}{nameof(MonthVM.OutputsOffPrime)}",
                band, true, "n2", width: 55
            ));

            // Столбец "Всего"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.Total, $"{binding}{nameof(MonthVM.Outputs)}",
                band, true, "n2"
            ));

            // Столбец "В день"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.PerDay, $"{binding}{nameof(MonthVM.OutputsPerDay)}",
                band, displayMask: "n2"
            ));

            OutputsBands.Add(band);

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.Budget"/> "Бюджет"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.Budget"/> "Бюджет"</returns>
        private Task<ExtendedBand> GenerateMonthBudgetBand(string binding) {
            // Группа столбцов "Бюджет"
            var band = new ExtendedBand(BandHeaders.Budget);

            // Столбец "Цена НРА"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.PriceNra, $"{binding}{nameof(MonthVM.Price)}",
                band, false, "c2", width: 80D
            ));

            // Условное форматирование столбца "Цена НРА"
            FormatConditions.Add(
                new DataBarFormatCondition {
                    FieldName = $"{binding}{nameof(MonthVM.Price)}",
                    PredefinedFormatName = "GreenGradientDataBar",
                    AnimationSettings = new ConditionalFormattingAnimationSettings {
                        Duration = new Duration(TimeSpan.Parse("0:0:6")),
                        EasingMode = AnimationEasingMode.EaseOut
                    }
                }
            );

            // Столбец "Цена с наценками"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.PriceWithMargins, $"{binding}{nameof(MonthVM.PriceWithMarkups)}",
                band, true, "c2", width: 80D
            ));

            // Условное форматирование столбца "Цена с наценками"
            FormatConditions.Add(
                new DataBarFormatCondition {
                    FieldName = $"{binding}{nameof(MonthVM.PriceWithMarkups)}",
                    PredefinedFormatName = "GreenGradientDataBar",
                    AnimationSettings = new ConditionalFormattingAnimationSettings {
                        Duration = new Duration(TimeSpan.Parse("0:0:6")),
                        EasingMode = AnimationEasingMode.EaseOut
                    }
                }
            );

            // Столбец "Бюджет"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.Budget, $"{binding}{nameof(MonthVM.Budget)}",
                band, true, "c2", width: 120D
            ));

            // Условное форматирование столбца "Бюджет"
            FormatConditions.Add(
                new DataBarFormatCondition {
                    FieldName = $"{binding}{nameof(MonthVM.Budget)}",
                    PredefinedFormatName = "GreenGradientDataBar",
                    AnimationSettings = new ConditionalFormattingAnimationSettings {
                        Duration = new Duration(TimeSpan.Parse("0:0:6")),
                        EasingMode = AnimationEasingMode.EaseOut
                    }
                }
            );

            BudgetBands.Add(band);

            return Task.FromResult(band);
        }

        #endregion

        #region Методы генерирования части таблицы, относящиеся к неделям

        /// <summary>
        ///     Метод генерирования групп столбцов недель
        /// </summary>
        /// <returns>Группы столбцов недель</returns>
        private async Task<List<ExtendedBand>> GenerateWeeksBands(
            string binding,
            DateTime monthDate,
            IReadOnlyCollection<Tuple<DateTime, bool>> weeks,
            IReadOnlyCollection<Tuple<int, bool>> timings,
            int baseTiming, bool isWeeksBandsVisible
        ) {
            // Группы столбцов недель
            var bands = new List<ExtendedBand>();

            var weekIndex = 0;
            foreach (var tuple in WeeksGenerator.Generate(monthDate)) {
                var weekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
                    tuple.Item1,
                    CalendarWeekRule.FirstDay,
                    DayOfWeek.Monday
                );

                var isWeekActive = weeks.FirstOrDefault(x => x.Item1 == tuple.Item1)?.Item2 ?? false;

                // Группа столбцов "Неделя"
                var band = new ExtendedBand(
                    $"Неделя №{weekNumber.ToString()} [{tuple.Item1.Date.Day.ToString()} число]",
                    isWeeksBandsVisible && isWeekActive,
                    tag: ExtendedBandTag.Week,
                    uid: tuple.Item1
                );

                var weekBindingString = $"{binding}{nameof(MonthVM.Weeks)}[{weekIndex.ToString()}].";

                var weekDaysBandTask = GenerateWeekDaysBand(weekBindingString);
                var weekInventoryBandTask = GenerateWeekInventoryBand(weekBindingString, baseTiming);
                var weekTimingDistributionBandTask = GenerateWeekTimingDistributionBand(weekBindingString, timings);

                await Task.WhenAll(
                    weekDaysBandTask,
                    weekInventoryBandTask,
                    weekTimingDistributionBandTask
                );

                band.Bands.Add(weekDaysBandTask.Result);
                band.Bands.Add(weekInventoryBandTask.Result);
                band.Bands.Add(weekTimingDistributionBandTask.Result);

                bands.Add(band);

                weekIndex++;
            }

            WeeksBands.AddRange(bands);

            return bands;
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.Days"/> "Дни"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.Days"/> "Дни"</returns>
        private Task<ExtendedBand> GenerateWeekDaysBand(string binding) {
            // Группа столбцов "Дни"
            var band = new ExtendedBand(BandHeaders.Days);

            // Столбец "Активные дни"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.ActiveDays, $"{binding}{nameof(WeekVM.ActiveDays)}",
                band, type: UnboundColumnType.Integer
            ));

            // Столбец "Всего"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.Total, $"{binding}{nameof(WeekVM.Days)}",
                band, true, type: UnboundColumnType.Integer
            ));

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.Inventory"/> "Инвентарь"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.Inventory"/> "Инвентарь"</returns>
        private Task<ExtendedBand> GenerateWeekInventoryBand(string binding, int baseTiming) {
            // Группа столбцов "Инвентарь"
            var band = new ExtendedBand(BandHeaders.Inventory);

            // Столбец "TRP"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.TRP, $"{binding}{nameof(WeekVM.TRP)}",
                band, displayMask: "n2", width: 55
            ));

            // Столбец "TRP [Базоывй хронометраж проекта]"
            band.Columns.Add(new ExtendedUnboundColumn(
                $"{ColumnHeaders.TRP} {baseTiming.ToString()}",
                $"{binding}{nameof(WeekVM.GivenTRP)}",
                band, true, "n2", width: 55
            ));

            // Столбец "GRP"
            band.Columns.Add(new ExtendedUnboundColumn(
                ColumnHeaders.GRP, $"{binding}{nameof(WeekVM.GRP)}",
                band, true, "n2", width: 55
            ));

            // Столбец"GRP [Базоывй хронометраж проекта]"
            band.Columns.Add(new ExtendedUnboundColumn(
                $"{ColumnHeaders.GRP} {baseTiming.ToString()}", $"{binding}{nameof(WeekVM.GivenGRP)}",
                band, displayMask: "n2", width: 55
            ));

            // Столбец "Минуты"
            band.Columns.Add(new ExtendedUnboundColumn(
                $"{ColumnHeaders.Minutes}", $"{binding}{nameof(WeekVM.Minutes)}",
                band, true, "n2", width: 55
            ));

            InventoryBands.Add(band);

            return Task.FromResult(band);
        }

        /// <summary>
        ///     Метод генерирования группы столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей"
        /// </summary>
        /// <returns>Группа столбцов <see cref="BandHeaders.TimingDistribution"/> "Распределение хронометражей"</returns>
        private Task<ExtendedBand> GenerateWeekTimingDistributionBand(
            string binding, IReadOnlyCollection<Tuple<int, bool>> timings
        ) {
            // Группа столбцов "Распределение хронометражей"
            var band = new ExtendedBand(
                BandHeaders.TimingDistribution,
                timings.Any(timing => timing.Item2),
                tag: ExtendedBandTag.TimingDistribution
            );

            var timingIndex = 0;
            for (var timing = 5; timing <= 60; timing += 5) {
                // Столбец "{Хронометраж} сек."
                band.Columns.Add(new ExtendedUnboundColumn(
                    $"{timing} {ColumnHeaders.Sec}",
                    $"{binding}{nameof(WeekVM.Timings)}[{timingIndex}].{nameof(TimingVM.Portion)}",
                    band, displayMask: "p0", editMask: "p", editMaskType: MaskType.Numeric,
                    isVisible: timings.First(x => x.Item1 == timing).Item2, type: UnboundColumnType.Object, uid: timing
                ));

                timingIndex += 1;
            }

            TimingDistributionBands.Add(band);
            WeeksTimingDistributionBands.Add(band);

            return Task.FromResult(band);
        }

        #endregion

        #endregion
    }
}
﻿using DevExpress.Export.Xl;
using DevExpress.Spreadsheet;
using RMPT.COMMON.Dictionaries;
using RMPT.DATA.ViewModels.Models;
using RMPT.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;

namespace RMPT.Utils {
    /// <summary>
    ///     Форматор флоучарта
    /// </summary>
    public class FlowChartFormatter {
        /// <summary>
        ///     Проект
        /// </summary>
        private readonly ProjectVM _project;

        /// <summary>
        ///     Тип флоучарта
        /// </summary>
        private readonly FlowChartType _type;

        /// <summary>
        ///     Признак "отображать номер недели"
        /// </summary>
        private readonly bool _isShowWeekNumber;

        /// <summary>
        ///     Признак "отображать число понедельника"
        /// </summary>
        private readonly bool _isShowMondayNumber;

        /// <summary>
        ///     Признак "отображать активные дни недели"
        /// </summary>
        private readonly bool _isShowActiveDays;

        /// <summary>
        ///     Признак "отображать TRP"
        /// </summary>
        private readonly bool _isShowTRP;

        /// <summary>
        ///     Признак "отображать GivenTRP"
        /// </summary>
        private readonly bool _isShowGivenTRP;

        /// <summary>
        ///     Признак "отображать GRP"
        /// </summary>
        private readonly bool _isShowGRP;

        /// <summary>
        ///     Признак "отображать GivenGRP"
        /// </summary>
        private readonly bool _isShowGivenGRP;

        /// <summary>
        ///     Признак "отображать Minutes"
        /// </summary>
        private readonly bool _isShowMinutes;

        /// <summary>
        ///     Признак "отображать Outputs"
        /// </summary>
        private readonly bool _isShowOutputs;

        /// <summary>
        ///     Признак "отображать Affinity"
        /// </summary>
        private readonly bool _isShowAffinity;

        /// <summary>
        ///     Признак "отображать средний хр-ж"
        /// </summary>
        private readonly bool _isShowAverageTiming;

        /// <summary>
        ///     Признак "отображать Бюджет с НДС"
        /// </summary>
        private readonly bool _isShowBudget;

        /// <summary>
        ///     Книга
        /// </summary>
        private readonly IWorkbook _workbook;

        /// <summary>
        ///     Лист
        /// </summary>
        private readonly Worksheet _worksheet;

        /// <summary>
        ///     Тип флоучарта - планирование
        /// </summary>
        private readonly bool _isPlaningType;

        /// <summary>
        ///     Начальный индекс столбца заголовков
        /// </summary>
        private readonly int _headersColumnStartIndex;

        /// <summary>
        ///     Начальный индекс наименований регионов
        /// </summary>
        private int _regionsNamesRowStartIndex;

        /// <summary>
        ///     Количество строк с данными
        /// </summary>
        private int _dataRowsCount;

        /// <summary>
        ///     Текущий индекс столбца
        /// </summary>
        private int _column;

        /// <summary>
        ///     Текущий индекс строки
        /// </summary>
        private int _row;

        /// <summary>
        ///     Список хронометражей с выходами
        /// </summary>
        private readonly List<int> _timingsWithOutputs = new();

        #region Конструктор

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="project">Проект</param>
        /// <param name="type">Тип флоучарта</param>
        /// <param name="isShowWeekNumber">Признак "отображать номер недели"</param>
        /// <param name="isShowMondayNumber">Признак "отображать число понедельника"</param>
        /// <param name="isShowActiveDays">Признак "отображать активные дни недели"</param>
        /// <param name="isShowTRP">Признак "отображать TRP"</param>
        /// <param name="isShowGivenTRP">Признак "отображать GivenTRP"</param>
        /// <param name="isShowGRP">Признак "отображать GRP"</param>
        /// <param name="isShowGivenGRP">Признак "отображать GivenGRP"</param>
        /// <param name="isShowMinutes">Признак "отображать Minutes"</param>
        /// <param name="isShowOutputs">Признак "отображать Outputs"</param>
        /// <param name="isShowAffinity">Признак "отображать Affinity"</param>
        /// <param name="isShowAverageTiming">Признак "отображать средний хр-ж"</param>
        /// <param name="isShowBudget">Признак "отображать Бюджет с НДС"</param>
        public FlowChartFormatter(
            ProjectVM project, FlowChartType type,
            bool isShowWeekNumber, bool isShowMondayNumber, bool isShowActiveDays,
            bool isShowTRP, bool isShowGivenTRP,
            bool isShowGRP, bool isShowGivenGRP,
            bool isShowMinutes, bool isShowOutputs,
            bool isShowAffinity,
            bool isShowAverageTiming,
            bool isShowBudget
        ) {
            _project = project;
            _type = type;
            _isPlaningType = _type.Name.Equals("Планирование");

            _isShowWeekNumber = isShowWeekNumber;
            _isShowMondayNumber = isShowMondayNumber;
            _isShowActiveDays = isShowActiveDays;
            _isShowTRP = isShowTRP;
            _isShowGivenTRP = isShowGivenTRP;
            _isShowGRP = isShowGRP;
            _isShowGivenGRP = isShowGivenGRP;
            _isShowMinutes = isShowMinutes;
            _isShowOutputs = isShowOutputs;
            _isShowAffinity = isShowAffinity;
            _isShowAverageTiming = isShowAverageTiming;
            _isShowBudget = isShowBudget;

            _workbook = new Workbook();
            _worksheet = _workbook.Worksheets.ActiveWorksheet;

            // Определяем начальный индекс столбца заголовков
            _headersColumnStartIndex = _isPlaningType ? 2 : 3;

            // Устанавливаем текущее значение индекса столбца
            _column = _headersColumnStartIndex;

            // Определяем начальный индекс наименований регионов
            DefineRegionsNamesStartIndex();
            // Определяем количество строк с данными
            DefineDataRowsCount();
        }

        /// <summary>
        ///     Метод определения начального индекса строки наименований регионов
        /// </summary>
        private void DefineRegionsNamesStartIndex() {
            // Счётчик индекса
            var indexCounter = 1;
            // Если будут указаны номера недель - прибавляем к счётчику индекса
            if (_isShowWeekNumber) indexCounter++;
            // Если будут указаны числа понедельника - прибавляем к счётчику индекса
            if (_isShowMondayNumber) indexCounter++;
            // Если будут указаны активные дни - прибавляем к счётчику индекса
            if (_isShowActiveDays) indexCounter++;

            // Задаём начальный индекс строки наименований регионов
            _regionsNamesRowStartIndex = indexCounter;
        }

        /// <summary>
        ///     Метод определения количества строк с данными
        /// </summary>
        private void DefineDataRowsCount() {
            // Если будут отображены данные по TRP
            if (_isShowTRP) _dataRowsCount++;
            // Если будут отображены данные по TRP 20
            if (_isShowGivenTRP) _dataRowsCount++;
            // Если будут отображены данные по GRP
            if (_isShowGRP) _dataRowsCount++;
            // Если будут отображены данные по GRP 20
            if (_isShowGivenGRP) _dataRowsCount++;
            // Если будут отображены данные по Minutes
            if (_isShowMinutes) _dataRowsCount++;
            // Если буду отображены данные по Outputs
            if (_isShowOutputs) _dataRowsCount++;
            // Если будут отображены данные по Affinity
            if (_isShowAffinity) _dataRowsCount++;
            // Если будут отображены данные по Среднему хронометражу
            if (_isShowAverageTiming) _dataRowsCount++;
            // Если будут отображены данные по Бюджету
            if (_isShowBudget) _dataRowsCount++;

            // Хронометражи с выходами
            _timingsWithOutputs.AddRange(_project.Regions
               .SelectMany(r => r.Channels)
               .SelectMany(c => c.Months)
               .SelectMany(m => m.Weeks)
               .SelectMany(w => w.Timings)
               .Where(t => t.Outputs > 0.0)
               .GroupBy(t => t.Value)
               .OrderBy(t=>t.Key)
               .Select(g => g.Key)
               .ToHashSet()
            );

            // Если будет отображено TRP или GivenTRP
            if (_isShowTRP || _isShowGivenTRP) {
                _dataRowsCount += _timingsWithOutputs.Count;
            }

            // Если будет отображено GRP или GivenGRP
            if (_isShowGRP || _isShowGivenGRP) {
                _dataRowsCount += _timingsWithOutputs.Count;
                // Если флоучарт для реализации
                if (!_isPlaningType) {
                    _dataRowsCount++;
                }
            }

            // Если будет отображено Outputs
            if (_isShowOutputs) {
                _dataRowsCount += _timingsWithOutputs.Count;
            }
        }

        /// <summary>
        ///     Метод начала формирования флоучарта
        /// </summary>
        /// <returns>Документ флоучарта</returns>
        public IWorkbook CreateDocument() {
            // Начало заполнения отчёта
            _workbook.BeginUpdate();

            // Формируем верхние заголовки
            CreateHeaders();
            // Заполняем данными
            CreateData();

            _worksheet.ScrollTo(0, 0);
            _worksheet.GetDataRange().AutoFitRows();
            _worksheet.GetDataRange().AutoFitColumns();

            // Завершение заполнение отчёта
            _workbook.EndUpdate();

            return _workbook;
        }

        /// <summary>
        ///     Метод формирования верхних заголовков отчёта
        /// </summary>
        private void CreateHeaders() {
            // Вставляем заголовок "№"
            if (_isShowWeekNumber) {
                _row += 1;
                var cell = _worksheet[_row, _column - 1];
                cell.Value = "№";
                cell.Font.Size = 10;
                cell.Font.Bold = true;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            // Вставляем заголовок "ПН"
            if (_isShowMondayNumber) {
                _row += 1;
                var cell = _worksheet[_row, _column - 1];
                cell.Value = "ПН";
                cell.Font.Size = 10;
                cell.Font.Bold = true;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            // Вставляем заголовок "Активные дни"
            if (_isShowActiveDays) {
                _row += 1;
                var cell = _worksheet[_row, _column - 1];
                cell.Value = "А.Д.";
                cell.Font.Size = 10;
                cell.Font.Bold = true;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }

            // Обнуляем индекс строки
            _row = 0;

            // Месяца с выходами
            var groupedMonths = _project.Regions
               .SelectMany(r => r.Channels)
               .SelectMany(c => c.Months)
               .GroupBy(m => m.Date)
               .Where(g => g.Sum(m => m.Budget) > 0.000)
               .OrderBy(g => g.Key)
               .ToList();

            foreach (var monthGroup in groupedMonths) {
                // Недели с выходами
                var groupedWeeks = monthGroup
                   .SelectMany(m => m.Weeks)
                   .GroupBy(w => w.DateFrom)
                   .Where(g => g.Sum(w => w.Outputs) > 0.0)
                   .OrderBy(g => g.Key)
                   .ToList();

                // Наименование месяца
                var monthRange = _worksheet.Range.FromLTRB(_column, _row, _column + groupedWeeks.Count() - 1, _row);
                monthRange.Value = monthGroup.Key.ToString("Y");
                monthRange.Font.Size = 10;
                monthRange.Font.Bold = true;
                monthRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                monthRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                monthRange.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);
                _worksheet.MergeCells(monthRange);

                foreach (var weekGroup in groupedWeeks) {
                    // Понедельник в прошлом месяце
                    var isWeekMondayFromPreviousMonth = false;
                    // Определяем дату понедельника недели
                    var weekStartDate = new DateTime(weekGroup.Key.Year, weekGroup.Key.Month, weekGroup.Key.Day);
                    if (weekGroup.Key.DayOfWeek != DayOfWeek.Monday) {
                        isWeekMondayFromPreviousMonth = true;
                        while (weekStartDate.DayOfWeek != DayOfWeek.Monday) {
                            weekStartDate = weekStartDate.AddDays(-1);
                        }
                    }

                    // Если требуется отобразить номер недели
                    if (_isShowWeekNumber) {
                        // Номер недели
                        var weekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
                            weekGroup.Key,
                            CalendarWeekRule.FirstDay,
                            DayOfWeek.Monday
                        );
                        _row += 1;

                        // Ячейка номера недели
                        var cell = _worksheet[
                            _row,
                            monthRange.LeftColumnIndex + groupedWeeks.IndexOf(weekGroup)
                        ];
                        cell.Value = weekNumber;
                        cell.Font.Size = 8;
                        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        // Окрашиваем в серый цвет если понедельник недели в прошлом месяце
                        if (isWeekMondayFromPreviousMonth) cell.Font.Color = Color.DarkGray;
                    }

                    // Если требуется отобразить число понедельника
                    if (_isShowMondayNumber) {
                        _row += 1;

                        // Ячейка числа понедельника
                        var cell = _worksheet[
                            _row,
                            monthRange.LeftColumnIndex + groupedWeeks.IndexOf(weekGroup)
                        ];
                        cell.Value = weekStartDate.Day;
                        cell.Font.Size = 8;
                        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        // Окрашиваем в серый цвет если понедельник недели в прошлом месяце
                        if (isWeekMondayFromPreviousMonth) cell.Font.Color = Color.DarkGray;
                    }

                    // Если требуется отобразить количество активных дней
                    if (_isShowActiveDays) {
                        _row += 1;

                        // Ячейка количества активных дней
                        var cell = _worksheet[
                            _row,
                            monthRange.LeftColumnIndex + groupedWeeks.IndexOf(weekGroup)
                        ];
                        cell.Value = weekGroup.First().ActiveDays;
                        cell.Font.Size = 8;
                        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        // Окрашиваем в серый цвет если понедельник недели в прошлом месяце
                        if (isWeekMondayFromPreviousMonth) cell.Font.Color = Color.DarkGray;
                    }

                    // Обнуляем индекс строки
                    _row = 0;
                }

                _column += groupedWeeks.Count();
            }

            // Обнуляем индекс строки
            _row = 0;

            // Итоговый столбец
            var totalCellRange = _worksheet.Range.FromLTRB(_column, _row, _column, _regionsNamesRowStartIndex - 1);
            totalCellRange.Value = ColumnHeaders.Total;
            totalCellRange.Font.Size = 8;
            totalCellRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
            totalCellRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            totalCellRange.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);
            _worksheet.MergeCells(totalCellRange);

            // Обнуляем индекс столбца
            _column = _headersColumnStartIndex;
        }

        /// <summary>
        ///     Метод заполнения данными флоучарта
        /// </summary>
        private void CreateData() {
            // Обнуляем индекс столбца
            _column = 0;
            // Устанавливаем индекс строки
            var row = _regionsNamesRowStartIndex;

            // Регионы с выходами
            var regions = _project.Regions
               .Where(r => r.Channels.Any(c => c.Months.Any(m => m.Budget > 0.000)))
               .OrderBy(r => r.Name)
               .ToList();

            foreach (var region in regions) {
                // Каналы с выходами в регионе
                var channels = region.Channels
                   .Where(c => c.Months.Any(m => m.Budget > 0.000))
                   .OrderBy(c => c.Name)
                   .ToList();

                // Определяем размер диапазона ячеек региона
                var regionRangeIncrement = _dataRowsCount;

                // Если флоучарт не для планирования
                if (!_isPlaningType) {
                    // Умножаем инкремент на количество каналов
                    regionRangeIncrement *= channels.Count();
                }

                var regionNameRange = _worksheet.Range.FromLTRB(
                    _column, row,
                    _column, row + regionRangeIncrement - 1
                );
                regionNameRange.Value = region.Name.Aggregate(
                    "",
                    (accumulator, character) => accumulator + $"{character}\n"
                ).Trim();
                regionNameRange.Font.Size = 10;
                regionNameRange.Font.Bold = true;
                regionNameRange.Alignment.WrapText = true;
                regionNameRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                regionNameRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                _worksheet.MergeCells(regionNameRange);

                // Если это флоучарт для планирования
                if (_isPlaningType) {
                    FillDataHeaders(row, _column + 1);
                    FillData(row, _column        + 1, region);
                    row += regionRangeIncrement;
                    continue;
                }

                foreach (var channel in channels) {
                    var channelStartRowIndex = row + (channels.IndexOf(channel) * _dataRowsCount);
                    var channelNameRange = _worksheet.Range.FromLTRB(
                        _column + 1, channelStartRowIndex,
                        _column + 1, channelStartRowIndex + _dataRowsCount - 1
                    );
                    channelNameRange.Value = channel.Name;
                    channelNameRange.Font.Size = 8;
                    channelNameRange.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    channelNameRange.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    _worksheet.MergeCells(channelNameRange);
                    FillDataHeaders(channelStartRowIndex, _column + 2);
                    FillData(channelStartRowIndex, _column        + 2, channel: channel);
                }

                row += regionRangeIncrement;
            }
        }

        private void FillDataHeaders(int startRow, int startCol) {
            var row = startRow;
            var col = startCol;

            if (_isShowTRP) {
                var cell = _worksheet[row, col];
                cell.Value = ColumnHeaders.TRP;
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                row++;
            }

            if (_isShowGivenTRP) {
                var cell = _worksheet[row, col];
                cell.Value = $"{ColumnHeaders.TRP} {_project.BaseTiming}";
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                row++;
            }

            if (_isShowTRP || _isShowGivenTRP) {
                foreach (var timing in _timingsWithOutputs) {
                    var cell = _worksheet[row, col];
                    cell.Value = $"{timing} (по {ColumnHeaders.TRP})";
                    cell.Font.Size = 8;
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                    row++;
                }
            }

            if (_isShowGRP) {
                var cell = _worksheet[row, col];
                cell.Value = ColumnHeaders.GRP;
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                row++;
            }

            if (_isShowGivenGRP) {
                var cell = _worksheet[row, col];
                cell.Value = $"{ColumnHeaders.GRP} {_project.BaseTiming}";
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                row++;
            }

            if (_isShowGRP || _isShowGivenGRP) {
                foreach (var timing in _timingsWithOutputs) {
                    var cell = _worksheet[row, col];
                    cell.Value = $"{timing} (по {ColumnHeaders.GRP} {_project.BaseTiming})";
                    cell.Font.Size = 8;
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                    row++;
                }
            }

            if (_isShowMinutes) {
                var cell = _worksheet[row, col];
                cell.Value = ColumnHeaders.Minutes;
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                row++;
            }

            if (_isShowOutputs) {
                var cell = _worksheet[row, col];
                cell.Value = ColumnHeaders.Outputs;
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                row++;

                foreach (var timing in _timingsWithOutputs) {
                    cell = _worksheet[row, col];
                    cell.Value = $"{timing} (по {ColumnHeaders.Outputs})";
                    cell.Font.Size = 8;
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                    row++;
                }
            }

            if ((_isShowGRP || _isShowGivenGRP) && !_isPlaningType) {
                var cell = _worksheet[row, col];
                cell.Value = $"% {ColumnHeaders.Prime}";
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                row++;
            }

            if (_isShowAffinity) {
                var cell = _worksheet[row, col];
                cell.Value = ColumnHeaders.Affinity;
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                row++;
            }

            if (_isShowAverageTiming) {
                var cell = _worksheet[row, col];
                cell.Value = "Средний хр-ж";
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;

                row++;
            }

            if (_isShowBudget) {
                var cell = _worksheet[row, col];
                cell.Value = ColumnHeaders.BudgetWithVat;
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
            }
        }

        private void FillData(int startRow, int startCol, RegionVM? region = null, ChannelVM? channel = null) {
            var col = startCol;
            var row = startRow;

            // Признак региона "Измеряемый"
            var isMeasurable = region?.IsMeasurable
             ?? channel?.Region.IsMeasurable
             ?? throw new NullReferenceException();

            // Месяца с выходами
            var groupedMonths = region?.Channels
               .Where(c => c.Months.Any(m => m.Budget > 0.000))
               .SelectMany(c => c.Months)
               .Where(m => m.Budget > 0.000)
               .GroupBy(m => m.Date)
               .OrderBy(g => g.Key)
               .ToList() ?? channel?.Months
               .Where(m => m.Budget > 0.000)
               .GroupBy(m => m.Date)
               .OrderBy(g => g.Key)
               .ToList();

            if (groupedMonths == null) return;

            foreach (var monthGroup in groupedMonths) {
                // Недели с выходами
                var groupedWeeks = monthGroup
                   .SelectMany(m => m.Weeks)
                   .GroupBy(w => w.DateFrom)
                   .Where(g => g.Sum(w => w.Outputs) > 0.0)
                   .OrderBy(g => g.Key)
                   .ToList();

                foreach (var weekGroup in groupedWeeks) {
                    // Хронометражи недели
                    var groupedTimings = weekGroup
                       .SelectMany(g => g.Timings)
                       .Where(t => _timingsWithOutputs.Contains(t.Value))
                       .GroupBy(t => t.Value)
                       .OrderBy(t=>t.Key)
                       .ToList();

                    col++;

                    if (_isShowTRP) {
                        var cell = _worksheet[row, col];
                        cell.Value = isMeasurable ? weekGroup.Sum(x => x.TRP) : 0.0;
                        cell.Font.Size = 8;
                        cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        row++;
                    }

                    if (_isShowGivenTRP) {
                        var cell = _worksheet[row, col];
                        cell.Value = isMeasurable ? weekGroup.Sum(x => x.GivenTRP) : 0.0;
                        cell.Font.Size = 8;
                        cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        row++;
                    }

                    if (_isShowTRP || _isShowGivenTRP) {
                        foreach (var timingGroup in groupedTimings) {
                            var cell = _worksheet[row, col];
                            cell.Value = isMeasurable
                                ? timingGroup.Sum(g => g.TRP) / groupedTimings.Sum(g => g.Sum(t => t.TRP))
                                : 0.0;
                            cell.Font.Size = 8;
                            cell.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                            cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                            cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                            row++;
                        }
                    }

                    if (_isShowGRP) {
                        var cell = _worksheet[row, col];
                        cell.Value = isMeasurable ? weekGroup.Sum(x => x.GRP) : 0.0;
                        cell.Font.Size = 8;
                        cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        row++;
                    }

                    if (_isShowGivenGRP) {
                        var cell = _worksheet[row, col];
                        cell.Value = isMeasurable ? weekGroup.Sum(x => x.GivenGRP) : 0.0;
                        cell.Font.Size = 8;
                        cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        row++;
                    }

                    if (_isShowGRP || _isShowGivenGRP) {
                        foreach (var timingGroup in groupedTimings) {
                            var cell = _worksheet[row, col];
                            cell.Value = isMeasurable
                                ? timingGroup.Sum(g => g.GivenGRP) / groupedTimings.Sum(g => g.Sum(t => t.GivenGRP))
                                : 0.0;
                            cell.Font.Size = 8;
                            cell.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                            cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                            cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                            row++;
                        }
                    }

                    if (_isShowMinutes) {
                        var cell = _worksheet[row, col];
                        cell.Value = weekGroup.Sum(x => x.Minutes);
                        cell.Font.Size = 8;
                        cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        row++;
                    }

                    if (_isShowOutputs) {
                        var cell = _worksheet[row, col];
                        cell.Value = weekGroup.Sum(x => x.Outputs);
                        cell.Font.Size = 8;
                        cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                        cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        row++;

                        foreach (var timingGroup in groupedTimings) {
                            cell = _worksheet[row, col];
                            cell.Value =
                                timingGroup.Sum(g => g.Outputs) /
                                groupedTimings.Sum(g => g.Sum(t => t.Outputs));
                            cell.Font.Size = 8;
                            cell.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                            cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                            cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                            cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                            row++;
                        }
                    }

                    // Если индекс недели не нулевой - значит значения ниже уже внесены
                    if (groupedWeeks.IndexOf(weekGroup) != 0) {
                        row = startRow;
                        continue;
                    }

                    if ((_isShowGRP || _isShowGivenGRP) && !_isPlaningType) {
                        var range = _worksheet.Range.FromLTRB(
                            col, row, col + groupedWeeks.Count - 1, row
                        );
                        range.Value = monthGroup.First().UnionPrimePortion;
                        range.Font.Size = 8;
                        range.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                        range.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        range.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        range.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        _worksheet.MergeCells(range);

                        row++;
                    }

                    if (_isShowAffinity) {
                        var range = _worksheet.Range.FromLTRB(
                            col, row, col + groupedWeeks.Count - 1, row
                        );
                        range.Value = isMeasurable ? monthGroup.Sum(g => g.Affinity) / monthGroup.Count() : 0.0;
                        range.Font.Size = 8;
                        range.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                        range.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        range.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        range.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        _worksheet.MergeCells(range);

                        row++;
                    }

                    if (_isShowAverageTiming) {
                        var range = _worksheet.Range.FromLTRB(
                            col, row, col + groupedWeeks.Count - 1, row
                        );
                        range.Value = monthGroup.First().AverageTiming;
                        range.Font.Size = 8;
                        range.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        range.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        range.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        _worksheet.MergeCells(range);

                        row++;
                    }

                    if (_isShowBudget) {
                        var range = _worksheet.Range.FromLTRB(
                            col, row, col + groupedWeeks.Count - 1, row
                        );
                        range.Value = monthGroup.Sum(x => x.Budget);
                        range.Font.Size = 8;
                        range.NumberFormat = "₽#,#";
                        range.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                        range.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                        range.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                        _worksheet.MergeCells(range);
                    }

                    row = startRow;
                }
            }

            col++;

            var timingsGroups = groupedMonths
               .SelectMany(g => g.ToList())
               .SelectMany(m => m.Weeks)
               .Where(w => w.Outputs > 0.0)
               .SelectMany(w => w.Timings)
               .Where(t => _timingsWithOutputs.Contains(t.Value))
               .GroupBy(t => t.Value)
               .ToList();

            if (_isShowTRP) {
                var cell = _worksheet[row, col];
                cell.Value = isMeasurable ? groupedMonths.Sum(g => g.Sum(m => m.TRP)) : 0.0;
                cell.Font.Size = 8;
                cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                row++;
            }

            if (_isShowGivenTRP) {
                var cell = _worksheet[row, col];
                cell.Value = isMeasurable ? groupedMonths.Sum(g => g.Sum(m => m.GivenTRP)) : 0.0;
                cell.Font.Size = 8;
                cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                row++;
            }

            if (_isShowTRP || _isShowGivenTRP) {
                foreach (var timingGroup in timingsGroups) {
                    var cell = _worksheet[row, col];
                    cell.Value = isMeasurable
                        ? timingGroup.Sum(g => g.TRP) / timingsGroups.Sum(g => g.Sum(t => t.TRP))
                        : 0.0;
                    cell.Font.Size = 8;
                    cell.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                    row++;
                }
            }

            if (_isShowGRP) {
                var cell = _worksheet[row, col];
                cell.Value = isMeasurable ? groupedMonths.Sum(g => g.Sum(m => m.GRP)) : 0.0;
                cell.Font.Size = 8;
                cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                row++;
            }

            if (_isShowGivenGRP) {
                var cell = _worksheet[row, col];
                cell.Value = isMeasurable ? groupedMonths.Sum(g => g.Sum(m => m.GivenGRP)) : 0.0;
                cell.Font.Size = 8;
                cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                row++;
            }

            if (_isShowGRP || _isShowGivenGRP) {
                foreach (var timingGroup in timingsGroups) {
                    var cell = _worksheet[row, col];
                    cell.Value = isMeasurable
                        ? timingGroup.Sum(g => g.GivenGRP) / timingsGroups.Sum(g => g.Sum(t => t.GivenGRP))
                        : 0.0;
                    cell.Font.Size = 8;
                    cell.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                    row++;
                }
            }

            if (_isShowMinutes) {
                var cell = _worksheet[row, col];
                cell.Value = groupedMonths.Sum(g => g.Sum(m => m.Minutes));
                cell.Font.Size = 8;
                cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                row++;
            }

            if (_isShowOutputs) {
                var cell = _worksheet[row, col];
                cell.Value = groupedMonths.Sum(g => g.Sum(m => m.Outputs));
                cell.Font.Size = 8;
                cell.NumberFormat = XlNumberFormat.Number2.FormatCode;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                row++;

                foreach (var timingGroup in timingsGroups) {
                    cell = _worksheet[row, col];
                    cell.Value = timingGroup.Sum(g => g.Outputs) / timingsGroups.Sum(g => g.Sum(t => t.Outputs));
                    cell.Font.Size = 8;
                    cell.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                    cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                    cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                    cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                    row++;
                }
            }

            if (!_isPlaningType) {
                var cell = _worksheet[row, col];
                cell.Value = isMeasurable
                    ? groupedMonths.Sum(g => g.Sum(m => m.UnionPrimePortion * m.GivenGRP) / g.Sum(m => m.GivenGRP))
                    : 0.0;
                cell.Font.Size = 8;
                cell.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                row++;
            }

            if (_isShowAffinity) {
                var cell = _worksheet[row, col];
                cell.Value = isMeasurable
                    ? groupedMonths.Sum(g => g.Sum(m => m.Affinity) / g.Count()) / groupedMonths.Count
                    : 0.0;
                cell.Font.Size = 8;
                cell.NumberFormat = XlNumberFormat.Percentage2.FormatCode;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                row++;
            }

            if (_isShowAverageTiming) {
                var cell = _worksheet[row, col];
                cell.Value = isMeasurable
                    ? timingsGroups.Sum(g => g.Key * (g.Sum(t => t.TRP) / timingsGroups.Sum(gg => gg.Sum(t => t.TRP))))
                    : 0.0;
                cell.Font.Size = 8;
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);

                _worksheet.MergeCells(cell);

                row++;
            }

            if (_isShowBudget) {
                var cell = _worksheet[row, col];
                cell.Value = groupedMonths.Sum(g => g.Sum(m => m.Budget));
                cell.Font.Size = 8;
                cell.NumberFormat = "₽#,#";
                cell.Alignment.Vertical = SpreadsheetVerticalAlignment.Center;
                cell.Alignment.Horizontal = SpreadsheetHorizontalAlignment.Center;
                cell.Borders.SetAllBorders(Color.Black, BorderLineStyle.Dotted);
            }
        }

        #endregion
    }
}
﻿namespace RMPT.COMMON.Dictionaries {
    /// <summary>
    ///     Заголовки групп столбцов таблицы проекта
    /// </summary>
    public static class BandHeaders {
        /// <summary>
        ///     Заголовок "Общее"
        /// </summary>
        public const string General = "Общее";

        /// <summary>
        ///     Заголовок "Данные сырья"
        /// </summary>
        public const string RawData = "Данные сырья";

        /// <summary>
        ///     Заголовок "Базовая информация"
        /// </summary>
        public const string BaseInfo = "Базовая информация";

        /// <summary>
        ///     Заголовок "Частоты"
        /// </summary>
        public const string Frequencies = "Частоты";

        /// <summary>
        ///     Заголовок "Дни"
        /// </summary>
        public const string Days = "Дни";

        /// <summary>
        ///     Заголовок "Распределение"
        /// </summary>
        public const string Distribution = "Распределение";

        /// <summary>
        ///     Заголовок "Распределение хронометражей"
        /// </summary>
        public const string TimingDistribution = "Распределение хронометражей";

        /// <summary>
        ///     Заголовок "Рейтинги"
        /// </summary>
        public const string Ratings = "Рейтинги";

        /// <summary>
        ///     Заголовок "Инвентарь"
        /// </summary>
        public const string Inventory = "Инвентарь";

        /// <summary>
        ///     Заголовок "Выходы"
        /// </summary>
        public const string Outputs = "Выходы";

        /// <summary>
        ///     Загаловок "Бюджет"
        /// </summary>
        public const string Budget = "Бюджет";

        /// <summary>
        ///     Заголовок "Бюджет агентский"
        /// </summary>
        public const string BudgetAgency = "Бюджет агентский";

        /// <summary>
        ///     Заголовок "Итого"
        /// </summary>
        public const string Summary = "Итого";
    }
}
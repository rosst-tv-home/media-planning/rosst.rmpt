﻿namespace RMPT.COMMON.Dictionaries {
    /// <summary>
    ///     Заголовки столбцов таблицы проекта
    /// </summary>
    public static class ColumnHeaders {
        /// <summary>
        ///     Заголовок "Активен"
        /// </summary>
        public const string Active = "Активен";
        
        /// <summary>
        ///     Заголовок "Регион"
        /// </summary>
        public const string Region = "Регион";

        /// <summary>
        ///     Заголовок "Канал"
        /// </summary>
        public const string Channel = "Канал";

        /// <summary>
        ///     Заголовок "Точка продаж"
        /// </summary>
        public const string SalePoint = "Точка продаж";

        /// <summary>
        ///     Заголовок "Тип продаж"
        /// </summary>
        public const string SaleType = "Тип продаж";

        /// <summary>
        ///     Заголовок "TVR ЦА"
        /// </summary>
        public const string TvrTargetAudience = "TVR ЦА";

        /// <summary>
        ///     Заголовок "TVR БА"
        /// </summary>
        public const string TvrBaseAudience = "TVR БА";

        /// <summary>
        ///     Заголовок "Affinity"
        /// </summary>
        public const string Affinity = "Affinity";

        /// <summary>
        ///     Заголовок "% Корректировки Affinity"
        /// </summary>
        public const string AffinityCorrectivePercent = "% Корректировки Affinity";

        /// <summary>
        ///     Заголовок "% Cum. Reach ЦА"
        /// </summary>
        public const string CumReachTargetAudiencePortion = "% Cum. Reach ЦА";

        /// <summary>
        ///     Заголовок "Cum. Reach"
        /// </summary>
        public const string CumReach = "Cum. Reach";

        /// <summary>
        ///     Заголовок "Sample"
        /// </summary>
        public const string Sample = "Sample";

        /// <summary>
        ///     Заголовок "Cost TRP"
        /// </summary>
        public const string CostTRP = "Cost TRP";

        /// <summary>
        ///     Заголовок "Cost Reach CPT"
        /// </summary>
        public const string CostReachCPT = "Cost Reach CPT";

        /// <summary>
        ///     Заголовок "% Канал"
        /// </summary>
        public const string ChannelPortion = "% Канал";

        /// <summary>
        ///     Заголовок "% Float"
        /// </summary>
        public const string FloatPortion = "% Float";

        /// <summary>
        ///     Заголовок "% Float P."
        /// </summary>
        public const string FloatPrimePortion = "% Float P.";

        /// <summary>
        ///     Заголовок "% Fix"
        /// </summary>
        public const string FixPortion = "% Fix";

        /// <summary>
        ///     Заголовок "% Fix P."
        /// </summary>
        public const string FixPrimePortion = "% Fix P.";

        /// <summary>
        ///     Заголовок "% S.Fix"
        /// </summary>
        public const string SuperFixPortion = "% S.Fix";

        /// <summary>
        ///     Заголовок "% S.Fix P."
        /// </summary>
        public const string SuperFixPrimePortion = "% S.Fix P.";

        /// <summary>
        ///     Заголовок "Акт. дни"
        /// </summary>
        public const string ActiveDays = "Акт. дни";

        /// <summary>
        ///     Заголовок "Всего"
        /// </summary>
        public const string Total = "Всего";

        /// <summary>
        ///     Заголовок "сек."
        /// </summary>
        public const string Sec = "сек.";

        /// <summary>
        ///     Заголовок "Цена НРА"
        /// </summary>
        public const string PriceNra = "Цена НРА";

        /// <summary>
        ///     Заголовок "Цена с наценками"
        /// </summary>
        public const string PriceWithMargins = "Цена с наценками";

        /// <summary>
        ///     Заголовок "Бюджет"
        /// </summary>
        public const string Budget = "Бюджет";

        /// <summary>
        ///     Заголовок "Бюджет с НДС"
        /// </summary>
        public const string BudgetWithVat = "Бюджет с НДС";

        /// <summary>
        ///     Заголовок "TRP"
        /// </summary>
        public const string TRP = "TRP";

        /// <summary>
        ///     Заголовок "GRP"
        /// </summary>
        public const string GRP = "GRP";

        /// <summary>
        ///     Заголовок "Минуты"
        /// </summary>
        public const string Minutes = "Минуты";

        /// <summary>
        ///     Заголовок "Prime"
        /// </summary>
        public const string Prime = "Prime";

        /// <summary>
        ///     Заголовок "Off Prime"
        /// </summary>
        public const string OffPrime = "Off Prime";

        /// <summary>
        ///     Заголовок "В день"
        /// </summary>
        public const string PerDay = "В день";

        /// <summary>
        ///     Заголовок "Выходы"
        /// </summary>
        public const string Outputs = "Выходы";

        /// <summary>
        ///     Заголовок "Клиент"
        /// </summary>
        public const string Client = "Клиент";

        /// <summary>
        ///     Заголовок "Период"
        /// </summary>
        public const string Period = "Период";

        /// <summary>
        ///     Заголовок "Хронометражи"
        /// </summary>
        public const string Timings = "Хронометражи";

        /// <summary>
        ///     Заголовок "Целевая аудитория"
        /// </summary>
        public const string TargetAudience = "Целевая аудитория";

        /// <summary>
        ///     Заголовок "Не определено"
        /// </summary>
        public const string Undefined = "Не определено";

        /// <summary>
        ///     Заголовок "Средний хр-ж"
        /// </summary>
        public const string AverageTiming = "Средний хр-ж";

        /// <summary>
        ///     Заголовок "Наценка за хр-ж"
        /// </summary>
        public const string TimingMargin = "Наценка за хр-ж";

        /// <summary>
        ///     Заголовок "Индентификатор медиаплана"
        /// </summary>
        public const string IDProject = "Индентификатор медиаплана";
    }
}
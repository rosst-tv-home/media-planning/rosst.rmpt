﻿namespace RMPT.COMMON.Dictionaries {
    public static class CommonDictionary {
        /// <summary>
        ///     Название компании
        /// </summary>
        public const string CompanyName = "ROSST";

        /// <summary>
        ///     Название приложения
        /// </summary>
        public const string ApplicationName = "Rosst Media Planning Tool";

        /// <summary>
        ///     Короткое название приложения
        /// </summary>
        public const string ApplicationNameShort = "RMPT";

        /// <summary>
        ///     Наименование "Загрузить сырьё"
        /// </summary>
        public const string DownloadRawData = "Загрузить сырьё";

        /// <summary>
        ///     Наименование "Создать проект"
        /// </summary>
        public const string CreateProject = "Создать проект";

        /// <summary>
        ///     Наименование "Применить"
        /// </summary>
        public const string Apply = "Применить";
    }
}
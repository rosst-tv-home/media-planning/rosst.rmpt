﻿using RMPT.CORE;
using System.Runtime.CompilerServices;

namespace RMPT.COMMON.Models {
    /// <summary>
    ///     Абстрактная модель проекта
    /// </summary>
    public class AbstractObservable : AbstractM, IObservable {
        /// <summary>
        ///     Метод обработки события
        /// </summary>
        public event EventHandler? OnPropertyChangedCore;

        /// <summary>
        ///     Метод вызова обработчика события
        /// </summary>
        /// <param name="value">Новое значение свойства</param>
        /// <param name="eventType">Тип доставки события</param>
        /// <param name="propertyName">Наименование свойства, значение которого изменилось</param>
        public void InvokePropertyChangedCore(
            object? value = null,
            EventType eventType = EventType.Ui,
            [CallerMemberName] string propertyName = ""
        ) => OnPropertyChangedCore?.Invoke(this, new EventArgument(value, eventType, propertyName));

        /// <summary>
        ///     Метод оповещения пользовательского интерфейса об изменении значения свойства
        /// </summary>
        /// <param name="propertyName">Наименование свойства, значение которого изменилось</param>
        public void NotifyUI([CallerMemberName] string propertyName = "") => RaisePropertyChanged(propertyName);
    }
}
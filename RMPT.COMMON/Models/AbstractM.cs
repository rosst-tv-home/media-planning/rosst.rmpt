﻿using DevExpress.Mvvm;
using System;
using System.ComponentModel;

namespace RMPT.COMMON.Models {
    /// <summary>
    ///     Базовая модель приложения
    /// </summary>
    public class AbstractM : ViewModelBase {
        /// <summary>
        ///     Конструктор
        /// </summary>
        protected internal AbstractM() {
            // Применяем начальные значения свойств из дескриптора [DefaultValue]
            ApplyDefaultValuePropertyDescriptor();
        }

        /// <summary>
        ///     Метод отправки сообщения-события
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <typeparam name="T">Тип сообщения</typeparam>
        protected void SendMessage<T>(T message) => Messenger.Default.Send(message);

        /// <summary>
        ///     Метод обработки сообщения-события
        /// </summary>
        /// <param name="action">Метод обработки сообщения</param>
        /// <typeparam name="T">Тип сообщения</typeparam>
        protected void HandleMessage<T>(Action<T> action) => Messenger.Default.Register(this, action);

        /// <summary>
        ///     Метод применения начального значения свойства, по указанному значению в дескрипторе [DefaultValue]
        /// </summary>
        private void ApplyDefaultValuePropertyDescriptor() {
            // Итерируемся по свойствам модели
            foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(this)) {
                // Пытаемся найти дескриптор [DefaultValue]
                var attr = prop.Attributes[typeof(DefaultValueAttribute)] as DefaultValueAttribute;
                // Если атрибут не найден - переходим к следующему свойству
                if (attr == null) {
                    continue;
                }

                // Устанавливаем значение свойства из дескриптора
                prop.SetValue(this, attr.Value);
            }
        }
    }
}
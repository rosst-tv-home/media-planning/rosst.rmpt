﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.UI;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;

namespace RMPT.COMMON.Models {
    /// <summary>
    ///     Абстрактная модель представления, применяется на модели, которые выступают в качестве
    ///     <see cref="FrameworkElement.DataContext" /> контекста данных элементов интерфейса.
    /// </summary>
    public class AbstractVM : AbstractM {
        #region Сервисы

        /// <summary>
        ///     Сервис доступа к текущему окну
        /// </summary>
        protected ICurrentWindowService? CurrentWindowSRV => GetService<ICurrentWindowService>();

        #endregion

        #region Команды

        /// <summary>
        ///     Команда вызова метода обработки события загрузки <see cref="FrameworkElement" /> элемента интерфейса
        /// </summary>
        public AsyncCommand<RoutedEventArgs?> OnLoadedAsyncCMD => new(OnLoadedAsync);

        /// <summary>
        ///     Команда вызова метода обработки события закрытия <see cref="Window" />.<see cref="Window.Closing" />
        ///     окна интерфейса
        /// </summary>
        public AsyncCommand<CancelEventArgs> OnClosingAsyncCMD => new(OnClosingAsync);

        /// <summary>
        ///     Команда вызова метода обработки события выгрузки
        ///     <see cref="FrameworkElement" />.<see cref="FrameworkElement.Unloaded" /> элемента интерфейса
        /// </summary>
        public AsyncCommand<RoutedEventArgs?> OnUnloadedAsyncCMD => new(OnUnloadedAsync);

        #endregion

        #region Методы

        #region Методы обработки событий

        /// <summary>
        ///     Метод обработки события <see cref="FrameworkElement" />.<see cref="FrameworkElement.Loaded" />
        /// </summary>
        /// <param name="arguments">Аргументы события</param>
        /// <exception cref="NotImplementedException">
        ///     Если в разметке указан <see cref="EventToCommand" />.<see cref="EventToCommand.EventName" /> "Loaded",
        ///     но метод обработки события не переопределён в модели представления
        /// </exception>
        protected virtual Task OnLoadedAsync(RoutedEventArgs? arguments) => throw new NotImplementedException(
            $"Переопределите метод {nameof(OnLoadedAsync)} в {GetType().Name}"
        );

        /// <summary>
        ///     Метод обработки события <see cref="Window" />.<see cref="Window.Closing" />
        /// </summary>
        /// <param name="arguments">Аргументы события</param>
        /// <exception cref="NotImplementedException">
        ///     Если в разметке указан <see cref="EventToCommand" />.<see cref="EventToCommand.EventName" /> "Closing",
        ///     но метод обработки события не переопределён в модели представления
        /// </exception>
        protected virtual Task OnClosingAsync(CancelEventArgs arguments) => throw new NotImplementedException(
            $"Переопределите метод {nameof(OnClosingAsync)} в {GetType().Name}"
        );

        /// <summary>
        ///     Метод обработки события <see cref="FrameworkElement" />.<see cref="FrameworkElement.Unloaded" />
        /// </summary>
        /// <param name="arguments">Аргументы события</param>
        /// <exception cref="NotImplementedException">
        ///     Если в разметке указан <see cref="EventToCommand" />.<see cref="EventToCommand.EventName" /> "Unloaded",
        ///     но метод обработки события не переопределён в модели представления
        /// </exception>
        protected virtual Task OnUnloadedAsync(RoutedEventArgs? arguments) => throw new NotImplementedException(
            $"Переопределите метод {nameof(OnUnloadedAsync)} в {GetType().Name}"
        );

        #endregion

        /// <summary>
        ///     Метод открытия диалогового окна
        /// </summary>
        /// <param name="parameters">Параметры модели представления диалогового окна</param>
        /// <typeparam name="T">Тип диалогового окна</typeparam>
        protected async Task ShowDialogWindowAsync<T>(object? parameters = null) {
            // Создаём новый экземпляр указанного типа
            var instance = Activator.CreateInstance<T>();
            // Если модель не является наследником System.Windows.Window - завершаем выполнение метода 
            if (instance is not Window window) {
                throw new ArgumentException(
                    $"Невозможно создать диалоговое окно из модели, которая не является наследником {typeof(Window)}",
                    nameof(T)
                );
            }

            // Если есть доступ к главному окну через сервис
            if (CurrentWindowSRV is CurrentWindowService service) {
                // Устанавливаем текущее окно как родителя диалогового окна, которое открываем
                window.Owner = service.ActualWindow;
            }

            // Если параметры не передаются - открываем диалоговое окно
            if (parameters == null) {
                await window.Dispatcher.InvokeAsync(() => window.ShowDialog());
                return;
            }

            // Если контекст диалогового окна не является наследником ISupportParameter
            if (window.DataContext is not ISupportParameter viewModel) {
                throw new ArgumentException(
                    $"Невозможно передать параметры в контекст диалогового окна, так как контекст не является наследником {typeof(ISupportParameter)}",
                    nameof(window.DataContext)
                );
            }

            // Передаём параметры в модель представления диалогового окна
            viewModel.Parameter = parameters;

            // Открываем диалоговое окно
            await window.Dispatcher.InvokeAsync(() => window.ShowDialog());
        }

        #endregion
    }
}
﻿using DevExpress.Mvvm;
using Newtonsoft.Json;
using RMPT.EVENT;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RMPT.RATING {
    public static class RatingDownloader {
        public static async Task<List<RatingResponse>> Download(List<int> ids, Collection<DateTime> months) {
            var url = "http://192.168.0.14:81";
            var client = new HttpClient {
                BaseAddress = new Uri(url)
            };
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Bearer",
                "eyJhbGciOiJIUzUxMiJ9."          +
                "eyJzdWIiOiJpbXVsbGlrYTEifQ."    +
                "I8fZOS0SW5AO94gT2yoIlJt5nR7"    +
                "WqfSO5SrtInKpay73FLPo20mjT3LIN" +
                "gls_TJgAJvazv1VzPq-J62wxAj0Eg"
            );
            var result = new List<RatingResponse>();
            HttpResponseMessage? response = null;
            try {
                response = await client.PostAsync("rmpt/ratings", new StringContent(
                    JsonConvert.SerializeObject(new RatingRequest {
                        IDs = ids,
                        Months = months
                    })
                  , Encoding.UTF8, "application/json"));
            } catch (WebException exception) {
                if (exception.Status == WebExceptionStatus.ConnectFailure) {
                    Messenger.Default.Send(new EventInitializerMessage("Ошибка подключения", MessageType.Warn));
                    await Task.Delay(250);
                    return result;
                }

                if (exception.Status == WebExceptionStatus.Timeout) {
                    Messenger.Default.Send(new EventInitializerMessage("Сервер не отвечает", MessageType.Warn));
                    await Task.Delay(250);
                    return result;
                }
            }
            if (response == null) return result;

            JsonConvert
               .DeserializeObject<List<RatingResponse>>(await response.Content.ReadAsStringAsync())
               .ForEach(result.Add);

            return result;
        }
    }
}
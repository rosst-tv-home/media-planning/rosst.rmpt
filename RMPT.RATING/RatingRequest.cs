﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RMPT.RATING {
    public class RatingRequest {
        [JsonProperty("ids")]
        public List<int> IDs { get; set; } = new();

        [JsonProperty("months")]
        public Collection<DateTime> Months { get; set; } = new();
    }
}
﻿using Newtonsoft.Json;
using System;

namespace RMPT.RATING {
    public class RatingResponse {
        [JsonProperty("rating")]
        public double Rating;

        [JsonProperty("primaryKey")]
        public PrimaryKey PrimaryKey { get; set; } = null!;
    }
    
    public class PrimaryKey {
        [JsonProperty("rmptID")]
            public int ID { get; set; }
            [JsonProperty("month")]
            public DateTime Month { get; set; }
            [JsonProperty("isPrime")]
            public bool IsPrime { get; set; }
    }
}
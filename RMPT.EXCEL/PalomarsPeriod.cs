﻿using System;
using System.Collections.ObjectModel;

namespace RMPT.EXCEL {
    public class PalomarsPeriod {
        public DateTime Date;
        public PalomarsChannel Channel;
        public Collection<PalomarsAudience> Audiences = new();
        public PalomarsPeriod(PalomarsChannel channel, DateTime date) {
            this.Date = date;
            this.Channel = channel;
        }
    }
}
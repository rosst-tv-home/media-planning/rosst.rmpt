﻿namespace RMPT.EXCEL {
    public class PalomarsAudience {
        public PalomarsPeriod Period;

        public bool IsTarget;
        public string Name;
        public string FrequencyName;
        public double Tvr;
        
        public double CumReach { get; set; }
        public double WeightTotal { get; set; }
        public double FrequencyValue { get; set; }
        public double Sample { get; set; }

        public PalomarsAudience(PalomarsPeriod period, bool isTarget, string name, string frequencyName, double tvr) {
            this.Period = period;
            this.IsTarget = isTarget;
            this.Tvr = tvr;
            this.Name = name;
            this.FrequencyName = frequencyName;
        }
    }
}
﻿using System.Collections.ObjectModel;

namespace RMPT.EXCEL {
    public class PalomarsRegion {
        public string Name;
        public Collection<PalomarsChannel> Channels = new();

        public PalomarsRegion(string name) {
            this.Name = name;
        }
    }
}
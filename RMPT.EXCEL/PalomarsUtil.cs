﻿using DevExpress.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RMPT.EXCEL {
    public static class PalomarsUtil {
        /// <summary>
        ///     Excel книга
        /// </summary>
        private static readonly Workbook Workbook = new();

        /// <summary>
        ///     Лист Excel книги
        /// </summary>
        private static Worksheet? _worksheet;

        /// <summary>
        ///     Словарь (Название аудитории -> признак "целевая аудитория")
        /// </summary>
        private static readonly Dictionary<string, bool> Audiences = new();

        /// <summary>
        ///     Словарь (Название аудитории не нормализованное -> название аудитории нормализованное)
        /// </summary>
        private static readonly Dictionary<string, string> AudiencesCache = new();

        /// <summary>
        ///     Частота
        /// </summary>
        private static string Frequency { get; set; } = null!;

        /// <summary>
        ///     Регионы
        /// </summary>
        private static readonly Collection<PalomarsRegion> Regions = new();

        /// <summary>
        ///     Метод чтения Excel файла выгрузки из Palomars
        /// </summary>
        /// <param name="stream">Поток чтения файла</param>
        /// <exception cref="ArgumentException">Ошибка чтения Excel файла</exception>
        public static async Task<List<PalomarsRegion>> ReadFileAsync(Stream stream) {
            // Если не удалось загрузить Excel файл - выбрасываем исключение
            if (!await Workbook.LoadDocumentAsync(stream, DocumentFormat.Xlsx)) {
                throw new ArgumentException("Не удалось загрузить Excel файл", nameof(stream));
            }

            // Если количество листов в книге больше или меньше одного - выбрасываем исключение
            if (Workbook.Worksheets.Count != 1) {
                throw new ArgumentException("В Excel книге должен быть один лист", nameof(Workbook.Worksheets));
            }

            // Сохраняем лист в переменную
            _worksheet = Workbook.Worksheets.ActiveWorksheet;

            // Выполняем обработку файла
            await ProcessFileAsync();

            // Копируем значения
            var data = Regions.ToList();

            // Очищаем списки
            Audiences.Clear();
            AudiencesCache.Clear();
            Frequency = "";
            Regions.Clear();
            Workbook.CreateNewDocument();

            return data;
        }

        /// <summary>
        ///     Метод обработки файла
        /// </summary>
        private static async Task ProcessFileAsync() {
            // Если лист Excel книги отсутсвует - выбрасываем исключение
            if (_worksheet == null) {
                throw new NullReferenceException($"Отсутсвует лист Excel книги в переменной {nameof(_worksheet)}");
            }

            // Выбираем все ячейки с данными
            CellRange range = _worksheet.GetDataRange();

            // Обрабатываем аудитории
            Task processAudiencesRowAsyncTask = ProcessAudiencesRowAsync(range);
            // Обрабатываем частоту
            Task processFrequencyAsyncTask = ProcessFrequencyAsync(range);
            // Обрабатываем данные
            Task processDataAsyncTask = ProcessData(range);

            await processAudiencesRowAsyncTask;
            await processFrequencyAsyncTask;
            await processDataAsyncTask;
        }

        /// <summary>
        ///     Метод обработки аудиторий
        /// </summary>
        /// <param name="range">Диапазон ячеек с данными</param>
        private static Task ProcessAudiencesRowAsync(CellRange range) {
            // Итерируемся по столбцам первой строки листа
            for (int col = 0; col < range.ColumnCount; col++) {
                // Определяем ячейку
                Cell cell = range[0, col];

                // Если ячейка не имеет данных или не содержит текст - переходим к следующему столбцу
                if (!cell.HasData || !cell.Value.IsText) continue;

                // Значение ячейки
                string cellValue = cell.Value.TextValue.Trim();

                // Если данное наименование аудитории уже есть в кэше - переходим к следующему столбцу
                if (AudiencesCache.ContainsKey(cellValue)) continue;

                // Группы совпадений по регулярному выражению
                var groups = Regex
                   .Match(cellValue, @"^(([a-zA-Z]*)=?\s?(\d{1,2}[-\+]\d{0,2})\s?([a-z]*)?([A-C]*)?)$")
                   .Groups
                   .Cast<Group>()
                   .Skip(2)
                   .Where(group => !string.IsNullOrWhiteSpace(group.Value))
                   .ToList();

                // Нормализованное название аудитории
                var name = groups.Aggregate("", (str, group) => {
                    string normalizedPart;
                    if (groups.IndexOf(group) == 2 && groups.Count > 3) {
                        normalizedPart = group.Value.Trim() + '.';
                    } else {
                        normalizedPart = group.Value.Trim().ToUpper();
                    }

                    return str + $"{normalizedPart} ";
                }).Trim();

                // Добавляем аудиторию в кэш
                AudiencesCache.Add(cellValue, name);

                // Если аудитория содержится в списке - переходим к следующему столбцу
                if (Audiences.ContainsKey(name)) continue;

                // Добавляем аудиторию в список (если первая аудитория - то целевая)
                Audiences.Add(name, Audiences.Count == 0);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки частоты
        /// </summary>
        /// <param name="range">Диапазон ячеек с данными</param>
        private static Task ProcessFrequencyAsync(CellRange range) {
            // Итерируемся по столбцам первой строки листа
            for (int col = 0; col < range.ColumnCount; col++) {
                // Определяем ячейку
                Cell cell = range[2, col];

                // Если ячейка не имеет данных или не содержит текст - переходим к следующему столбцу
                if (!cell.HasData || !cell.Value.IsText || string.IsNullOrWhiteSpace(cell.Value.TextValue)) continue;

                // Значение ячейки
                Frequency = cell.Value.TextValue.Trim();

                // Завершаем выполнение метода
                return Task.CompletedTask;
            }

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Метод обработки данных
        /// </summary>
        /// <param name="range">Диапазон ячеек с данными</param>
        private static Task ProcessData(CellRange range) {
            for (int row = 3; row < range.RowCount; row++) {
                int audienceIndex = 0;
                PalomarsRegion? region = null;
                PalomarsChannel? channel = null;
                PalomarsPeriod? period = null;
                for (int col = 0; col < range.ColumnCount; col++) {
                    PalomarsAudience? audience = null;

                    // Определяем ячейку
                    Cell cell = range[row, col];

                    // Если ячейка не имеет данных - переходим к следующему столбцу
                    if (!cell.HasData) continue;

                    #region Регион

                    if (col == 0) {
                        // Если ячейка не содержит текст - переходим к следующему столбцу
                        if (!cell.Value.IsText) continue;

                        // Наименование региона
                        string value = cell.Value.TextValue.Trim();
                        
                        foreach (var item in Regions) {
                            if (item.Name.Equals(value)) {
                                region = item;
                                break;
                            }
                        }

                        // Если регион уже есть в списке - переходим к следующей ячейке
                        if (region != null) continue;

                        // Добавляем регион в список
                        region = new PalomarsRegion(value);
                        Regions.Add(region);

                        // Переходим к следующей ячейке
                        continue;
                    }

                    #endregion

                    #region Канал

                    if (col == 1) {
                        // Определяем регион
                        if (region == null) {
                            throw new NullReferenceException("Не найден хотябы один регион");
                        }
                        
                        // Если ячейка не содержит текст - переходим к следующему столбцу
                        if (!cell.Value.IsText) continue;

                        // Наименование канала
                        string value = Regex.Match(
                            cell.Value.TextValue,
                            @".+(?=\s\(.+\))"
                        ).Value.Trim();
                        if (string.IsNullOrWhiteSpace(value)) {
                            value = cell.Value.TextValue.Trim();
                        }
                        
                        foreach (var item in region.Channels) {
                            if (item.Name.Equals(value)) {
                                channel = item;
                                break;
                            }
                        }

                        // Если канал уже есть в списке - переходим к следующей ячейке
                        if (channel != null) continue;

                        // Добавляем канала в список
                        channel = new PalomarsChannel(region, value);
                        region.Channels.Add(channel);

                        continue;
                    }

                    #endregion

                    #region Период

                    if (col == 2) {
                        // Определяем регион
                        if (region == null) {
                            throw new NullReferenceException("Не найден хотябы один регион");
                        }
                        
                        // Определяем канал
                        if (channel == null) {
                            throw new NullReferenceException("Не найден хотябы один канал");
                        }

                        // Если ячейка не содержит дату - переходим к следующему столбцу
                        if (!cell.Value.IsDateTime) continue;

                        // Дата
                        DateTime value = cell.Value.DateTimeValue;
                        
                        foreach (var item in channel.Periods) {
                            if (item.Date.Equals(value)) {
                                period = item;
                                break;
                            }
                        }

                        // Если дата уже есть в канале - переходим к следующей ячейке
                        if (period != null) continue;

                        // Добавляем период в канал
                        period = new PalomarsPeriod(channel, value);
                        channel.Periods.Add(period);

                        continue;
                    }

                    // Определяем регион
                    if (region == null) {
                        throw new NullReferenceException("Не найден хотябы один регион");
                    }
                        
                    // Определяем канал
                    if (channel == null) {
                        throw new NullReferenceException("Не найден хотябы один канал");
                    }
                    
                    // Определяем период
                    if (period == null) {
                        throw new NullReferenceException("Не найден хотябы один период");
                    }

                    // Если в ячейке не число - переходим к следующей ячейке
                    if (!cell.Value.IsNumeric) continue;

                    double numericValue = cell.Value.NumericValue;

                    switch (col % 5) {
                        #region TVR

                        case 4: {
                            audience = new PalomarsAudience(
                                period,
                                audienceIndex == 0,
                                Audiences.Select(x => x.Key).ToList()[audienceIndex],
                                Frequency,
                                numericValue
                            );
                            period.Audiences.Add(audience);

                            audienceIndex++;
                            continue;
                        }

                        #endregion

                        #region Cum. Reach

                        case 0: {
                            audience ??=
                                period.Audiences.LastOrDefault() ??
                                throw new NullReferenceException("Аудитория не определена");
                            audience.CumReach = numericValue;
                            continue;
                        }

                        #endregion

                        #region Weight Total

                        case 1: {
                            audience ??=
                                period.Audiences.LastOrDefault() ??
                                throw new NullReferenceException("Аудитория не определена");
                            audience.WeightTotal = numericValue;
                            continue;
                        }

                        #endregion

                        #region Frequency

                        case 2: {
                            audience ??=
                                period.Audiences.LastOrDefault() ??
                                throw new NullReferenceException("Аудитория не определена");
                            audience.FrequencyValue = numericValue;
                            continue;
                        }

                        #endregion

                        #region Sample

                        case 3: {
                            audience ??=
                                period.Audiences.LastOrDefault() ??
                                throw new NullReferenceException("Аудитория не определена");
                            audience.Sample = numericValue;
                            continue;
                        }

                        #endregion
                    }

                    #endregion
                }
            }

            return Task.CompletedTask;
        }
    }
}
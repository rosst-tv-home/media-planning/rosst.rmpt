﻿using System.Collections.ObjectModel;

namespace RMPT.EXCEL {
    public class PalomarsChannel {
        public string Name;
        public PalomarsRegion Region;
        public Collection<PalomarsPeriod> Periods = new();

        public PalomarsChannel(PalomarsRegion region, string name) {
            this.Name = name;
            this.Region = region;
        }
    }
}
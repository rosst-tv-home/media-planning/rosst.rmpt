﻿using System;

namespace RMPT.VALIDATE {
    public static class ValuesValidator {
        /// <summary>
        ///     Метод проверки валидности даты начала и окончания недельного периода
        /// </summary>
        /// <param name="dateFrom">Дата начала</param>
        /// <param name="dateBefore">Дата окончания</param>
        public static void CheckPeriod(DateTime dateFrom, DateTime dateBefore) {
            // Если дата начала периода больше даты окончания периода - выбрасываем исключение
            if (dateFrom > dateBefore) {
                throw new ArgumentException(
                    "Дата начала недельного периода является более поздней, " +
                    "чем дата окончания периода, что является недопустимым"
                );
            }

            // Если количество дней между началом периода и окончанием периода больше 7 - выбрасываем исключение
            if ((dateBefore.Date - dateFrom.Date).Days > 7) {
                throw new ArgumentException(
                    "Указанные даты недельного периода имеет диапозон больше 7 дней, что недопустимо"
                );
            }
        }

        /// <summary>
        ///     Метод проверки валидности указанной доли хронометража
        /// </summary>
        /// <param name="value">Указанное значение доли хронометража</param>
        /// <exception cref="System.ArgumentException">
        ///     Выбрасывается в случае, если указанное значение доли хронометража меньше 0% или больше 100%
        /// </exception>
        public static void IsValidPortion(double value) {
            if (value is < 0 or > 1) {
                throw new ArgumentException(
                    "Доля хронометража не может быть меньше 0% и не может превышать 100%"
                );
            }
        }

        /// <summary>
        ///     Метод проверки валидности значения хронометража
        /// </summary>
        /// <param name="value">Значение хронометража</param>
        /// <exception cref="ArgumentException">Выбрасывается в случае, если указанно значение хронометража не кратно 5</exception>
        public static void IsValidTimingValue(int value) {
            // Если значение хронометража не кратно 5
            if (value % 5 != 0) {
                // Выбрасываем исключение
                throw new ArgumentException("Значение хронометража не кратно 5", nameof(value));
            }
        }

        /// <summary>
        ///     Метод возврата нулевого значения при делении на ноль
        /// </summary>
        /// <param name="value">Значение которое получилось при расчёте</param>
        /// <returns>Значение расчёта или ноль в случае ошибки</returns>
        public static double NanOrZero(double value) =>
            double.IsNaN(value) ? 0.0 : double.IsInfinity(value) ? 0.0 : value;
    }
}
﻿using DevExpress.Xpf.Grid;

namespace RMPT.VALIDATE {
    /// <summary>
    ///     Класс с методами проверок введённых значений в таблице <see cref="GridControl" />
    /// </summary>
    public static class GridValidator {
        /// <summary>
        ///     Метод проверки значения ячейки на валидный тип модели, которая выбирается
        /// </summary>
        /// <param name="argument">Аргумент события</param>
        /// <typeparam name="T">Тип модели <see cref="T" /></typeparam>
        /// <returns>Признак валидности типа значения ячейки</returns>
        public static bool IsValidCellType<T>(GridCellValidationEventArgs argument) {
            // Если тип ячейки соответсвует типу выбираемой модели
            if (argument.Value is T) {
                return true;
            }

            // Оповещаем пользователя об ошибке
            argument.ErrorContent =
                "Значение ячейки не соответсвует выбираемой " +
                $"модели {typeof(T)}, обратитесь к разработчику";

            return false;
        }

        /// <summary>
        ///     Метод проверки строки таблицы на валидный тип модели, которая заполняется
        /// </summary>
        /// <param name="argument">Аргумент события <see cref="GridRowValidationEventArgs" /></param>
        /// <typeparam name="T">Тип модели <see cref="T" /></typeparam>
        /// <returns>Признак валидности строки таблицы</returns>
        public static bool IsValidRowType<T>(GridRowValidationEventArgs argument) {
            // Если тип строки соответсвует типу заполняемой модели
            if (argument.Row is T) {
                return true;
            }

            // Оповещаем пользователя об ошибке
            argument.ErrorContent = $"Строка таблицы не является моделью {typeof(T)}, обратитесь к разработчику";

            return false;
        }

        /// <summary>
        ///     Метод проверки значения ячейки таблицы на валидное строковое значение
        /// </summary>
        /// <param name="argument">Аргумент события <see cref="GridCellValidationEventArgs" /></param>
        /// <returns>Признак валидности значения ячейки таблицы</returns>
        public static bool IsValidStringifyCellValue(GridCellValidationEventArgs argument) {
            // Если значение не является строкой
            if (argument.Value is not string stringifyValue) {
                argument.ErrorContent = "Значение должно являться строкой";
                return false;
            }

            // Если строка не пустая
            if (!string.IsNullOrWhiteSpace(stringifyValue.Trim())) {
                return true;
            }

            // Оповещаем пользователя об ошибке
            argument.ErrorContent = "Ячейка не может быть пустой или состоять только из пробелов";

            return false;
        }

        /// <summary>
        ///     Метод проверки значения ячейки таблицы на валидное натуральное число
        /// </summary>
        /// <param name="argument">Аргумент события <see cref="GridCellValidationEventArgs" /></param>
        /// <returns>Признак валидности значения ячейки таблицы</returns>
        public static bool IsValidIntCellValue(GridCellValidationEventArgs argument) {
            // Если значение является натуральным числом
            if (argument.Value is int) {
                return true;
            }

            // Оповещаем пользователя об ошибке
            argument.ErrorContent = "Значение должно являться натуральным числом";

            return false;
        }

        /// <summary>
        ///     Метод проверки значения ячейки таблицы на валидное число с плавающей точкой
        /// </summary>
        /// <param name="argument">Аргумент события <see cref="GridCellValidationEventArgs" /></param>
        /// <returns>Признак валидности значения ячейки таблицы</returns>
        public static bool IsValidDoubleCellValue(GridCellValidationEventArgs argument) {
            // Если значение не является числом с плавающей точкой
            if (argument.Value is double) {
                return true;
            }

            argument.ErrorContent = "Значение должно являться числом с плавающей точкой";
            return false;
        }
    }
}